/*	nmrdata.h
	Brian E. Coggins, 24 Jun 2004, rev. 21 Jul 2010, v.1.1.2
	Copyright (c) 2004-2010, Brian E. Coggins.  All rights reserved.

	NMRData Library:  A library for access to and manipulation of NMR data.

	Please see the library documentation for details on the use of this
	library.

	This library may only be used or distributed under the terms of a
	license received from the author.  Please consult the accompanying
	license document to determine which rights and permissions have
	been granted to you.  If you did not receive a license document
	together with this file, you MUST contact the author to obtain a
	license before using or redistributing this file.
*/

/**	\file
	\brief NMRData Library Header File.

	This file contains the declarations, and some definitions, for the
	NMRData library for accessing and manipulating NMR spectral data.
*/


#ifndef BEC_NMRDATA_INCLUDED
#define BEC_NMRDATA_INCLUDED

#include <stdio.h>
#include <math.h>
#include <stddef.h>
#include <vector>
#include <string>
#include <iterator>
#include <functional>
#include <map>
#include <set>
#include <queue>
#include <valarray>
#include <bec_misc/becmisc.h>

//	N.B.  Eliminates a name conflict with windows.h.
#ifdef CreateFile
#undef CreateFile
#endif

///	NMRData Library:  A library for access to and manipulation of NMR data.
/**	This namespace contains the NMRData Library for access to NMR spectral data.
	Please see the \ref NMRData_intro (introduction) for general information.
*/
namespace BEC_NMRData
{	
	static const char *VersionString = "1.1.2";
	static const char *InitialDate = "24 Jun 2004";
	static const char *VersionReleaseDate = "21 Jul 2010";
	static const char *RevisionDate = "21 Jul 2010";
	static const char *Copyright = "Copyright (c) 2004-2010, Brian E. Coggins.  All rights reserved.";

	struct OpenDataPtr;
	struct DataPage;
	struct DataPageSubsetTableEntry;
	struct SubsetPtrTableEntry;
	struct SubsetPageTableEntry;
	class Value;
	template< typename ParentClass > class DimPosition;
	class Reference;
	class Subset;
	struct SubsetData;
	class NMRData;
	class NMRData_nmrPipe;
	class NMRData_Text;
	class NMRDataSubmatrix;
	class NMRData_XEASY;
	class NMRData_UCSF;
	class NMRData_NMRView;
	template< typename T_LHS, typename T_RHS, typename T_Op > class NMRDataMathOp;
	class NMRDataMathScalar;
	class NMRDataMathVal;
	class NMRDataMathRef;
	class NMRDataMathSubset;
	class NMRDataMathObj;
	template< typename T > class NMRDataMathSelector;
	class RawVarianData;

/**	\page	NMRData_intro	NMRData Library Introduction and Tutorial

	The %NMRData Library is designed to provide straightforward and efficient
	access to NMR spectral data in a variety of common file formats, or in
	memory, from C++ code.  It was constructed using modern C++ design
	paradigms and approaches, and follows idioms that will be familiar to
	any user of the C++ standard library.  The usual technical issues that
	make it hard to write programs manipulating NMR data - keeping track of
	where complex and hypercomplex components are found, finding the right
	data point in the face of different dimension orders, disentangling
	submatrix vs. interleaved formats, moving data in and out of memory,
	tracking dimension calibrations, etc. - are handled automatically by
	the library.  Most common operations can be performed with only a single
	call to a library function.  At the same time, the library's internal
	implementation was carefully designed to maximize speed, such that the
	high-level features of the library impose little or no performance
	penalty vs. using low level code.

	This document is intended as an introduction to the %NMRData Library's
	features, and some of the ways it can be used.  Please consult the
	reference documentation for more detailed information about individual
	classes and functions.

	\section	NMRData_intro_toc				Table of Contents

	- \ref NMRData_intro_setup
	    - \ref NMRData_intro_requirements
		- \ref NMRData_intro_install
		- \ref NMRData_intro_debug
	- \ref NMRData_intro_baseclass
	- \ref NMRData_intro_datasets
	    - \ref NMRData_intro_state
	    - \ref NMRData_intro_opening
		- \ref NMRData_intro_creating
		- \ref NMRData_intro_tempdata
		- \ref NMRData_intro_textbin
		- \ref NMRData_intro_extensions
		- \ref NMRData_intro_readonly
		- \ref NMRData_intro_copying
		- \ref NMRData_intro_cache
		- \ref NMRData_intro_closing
	- \ref NMRData_intro_dimensions
	    - \ref NMRData_intro_org
	    - \ref NMRData_intro_dimsizes
		- \ref NMRData_intro_dimlabels
		- \ref NMRData_intro_dimlimits
		- \ref NMRData_intro_calibration
		- \ref NMRData_intro_complex
	- \ref NMRData_intro_data
		- \ref NMRData_intro_subscripts
		- \ref NMRData_intro_dimpositions
		- \ref NMRData_intro_references
		- \ref NMRData_intro_complexref
		- \ref NMRData_intro_refmath
		- \ref NMRData_intro_subsets
		- \ref NMRData_intro_subscriptsummary
		- \ref NMRData_intro_constrefs
	- \ref NMRData_intro_operations
		- \ref NMRData_intro_bulkmath
		- \ref NMRData_intro_complexbulkmath
		- \ref NMRData_intro_applytoall
		- \ref NMRData_intro_stats
	- \ref NMRData_intro_lowlevel
	- \ref NMRData_intro_misc
		- \ref NMRData_intro_stl
		- \ref NMRData_intro_threads
		- \ref NMRData_intro_memory

	\section	NMRData_intro_setup				Setup and Installation Issues

	\subsection	NMRData_intro_requirements		Library Requirements

	It should be possible to compile the library with any C++ compiler that
	conforms to the ISO C++ standard.  Because the library uses some of the
	more advanced template features of standard C++, you will probably need
	a relatively new compiler.  The following are known to compile the
	library correctly:  
	
	\li	GCC 3.2 and later
	\li	Microsoft Visual C++ 7.1 and later.

	Some compilers provide options to compile without exception handling or
	without runtime type information.  These options must NOT be used when
	compiling this software.

	The C++ standard library must also be available.

	\subsection	NMRData_intro_install			Installing the Library and Including It in Projects

	The library consists of two main files, nmrdata.h and nmrdata.cpp, plus
	one support file, becmisc.h.  The header files should be installed to a
	location on your compiler's include path.

	To use the library in a project, first add the following line to the top 
	of each source file that will use the library:

	\code
	#include "nmrdata.h"
	\endcode

	Then be sure to include nmrdata.cpp in the compilation list.  For best
	results, set your compiler for full optimizations.

	Note that the library is contained within the namespace BEC_NMRData.  The
	examples below assume that you have used the following to make the library
	available directly:

	\code
	using namespace BEC_NMRData;
	\endcode

	but you can always choose to use fully-qualified names instead.

	\subsection	NMRData_intro_debug				A Note on Error Reporting

	You have two options for error checking and reporting:

	\li		If you do nothing, the library will not check input parameters,
			and will not generally report errors in a sane way.  This is
			useful for maximizing the speed of the code, and in some cases
			may be essential to obtaining satisfactory performance.  However,
			it can make it very difficult to debug problems, because many
			errors will result in segmentation faults deep inside library
			code.

	\li		If you define BEC_NMRDATA_DEBUG, error checking and reporting will
			be enabled.  The library will then check almost all input
			arguments for their validity, and will also frequently test certain
			conditions for logical validity.  Any errors that are detected are
			reported via C++ exceptions in the Exceptions namespace.  This
			error checking can result in a significant performance penalty in
			some cases.

	I personally prefer to enable BEC_NMRDATA_DEBUG while initially testing and
	debugging code, and then to disable it for final release versions.  What
	strategy you choose should depend somewhat on your ability to respond to
	any detected errors in a way other than program termination.  Most of the
	errors detected by the library are indications of failures in program logic,
	rather than routine runtime conditions.  If the library reports that your
	program passed it a null pointer, I'm not sure what you can do about that
	at run time on a user's machine, and I'm not sure how it would help your 
	user to know such details.  Hence my approach of disabling error checking 
	on code that goes to users.  But in other circumstances, you might find 
	advantages in detecting library	problems and avoiding program termination.

	Note that exceptions can be thrown by library code even when 
	BEC_NMRDATA_DEBUG is not defined!  These would primarily be exceptions
	derived from Exceptions::FileError, which all have to do with runtime file
	access problems, like files that are not found, or which cannot be opened
	due to access restrictions.  It is wise to include a catch block for  
	references to Exceptions::FileError at the top of your program, even in
	release code, and you probably will want to check for these errors
	immediately after any call to open or create a disk file, as well.

	Also, there is always a chance of a std::bad_alloc being thrown if memory
	is depleted.

	\section	NMRData_intro_baseclass			The NMRData Class Hierarchy

	The starting point for most of the library's features is the NMRData class.
	This class provides the interface for interacting with a single dataset;
	logically, an instance of the NMRData class constitutes a single spectrum
	or time-domain dataset.	The NMRData class is actually the base class for a 
	small class hierarchy, which is used to support different file formats.  
	The derived classes	NMRData_nmrPipe, NMRData_NMRView, NMRData_UCSF, 
	NMRData_XEASY and NMRData_Text provide the facilities for working with 
	datasets in the	NMRPipe, NMRView, UCSF/Sparky, XEASY and ASCII text formats,
	respectively.

	From the standpoint of client code, it is rarely important to distinguish
	between the different derived classes and the base class.  Instead, one
	can refer to any dataset of any format using a pointer or a reference to
	the typename NMRData.  The standard interface can then be used, and the
	appropriate mechanisms are invoked depending on the format in use.

	The only time when it is necessary to distinguish between the derived
	classes is when creating or opening datasets.  When the format is known,
	one can of course simply create an object of the preferred type:

	\code
	NMRData_NMRView data;
	\endcode

	However, a facility is provided for obtaining NMRData objects of the
	appropriate type to match a filename extension.  The static member
	function NMRData::GetObjForFileType() accepts a filename and returns a
	pointer to an object of the appropriate type.  For example, to get the
	right kind of object for a file of extension .nv, one could simply write:

	\code
	std::string filename = "nvfile.nv";
	NMRData *dataptr = NMRData::GetObjForFileType( filename );
	\endcode

	The returned object is allocated on the heap using the new operator, and
	you must delete it with the delete operator when you have finished using it.
	While a pointer is a perfectly reasonable way to work with polymorphic
	objects, I find references easier to handle when working with classes like 
	NMRData, which make heavy use of overloaded operators.  Thus I like to 
	create a reference to any object returned from NMRData::GetObjForFileType().
	The following example illustrates the appropriate usage:

	\code
	std::string filename = "nvfile.nv";
	NMRData *dataptr = NMRData::GetObjForFileType( filename );
	NMRData &data = *dataptr;
	
	// use object...

	delete dataptr;
	\endcode

	Smart pointer objects can be a very convenient way to work with 
	heap-allocated objects, ensuring that they are properly deleted.  I have
	developed my own smart pointers in becmisc.h, but the choice of a smart 
	pointer seems to be a matter of deep personal preference.  Whichever one 
	you prefer, strongly consider using it.

	Given a pointer or reference to an NMRData object, the file type can be
	determined using one of three different mechanisms:

	\li	The C++ dynamic_cast<> mechanism:  Attempt to cast the object to a
		more derived type.  This is a notoriously slow mechanism, but it is
		useful or even necessary in some cases.

	\li	The NMRData::GetFileTypeCode() function, which returns an integer
		corresponding to a file type code defined in the NMRData class.  Since
		this mechanism uses virtual functions instead of RTTI, it is often
		much faster than dynamic_cast<>.

	\li	The NMRData::GetFileTypeStr() function, which returns a string
		identifying the file type, i.e. suitable for presentation to a user.

	NMRData objects are not copyable; attempts to pass an NMRData object by
	value will result in a compiler error about an inaccessible constructor.
	Copy semantics do not make sense for objects that represent files on disk
	or large temporary datasets in memory, and copy semantics are dangerous in
	the context of polymorphic classes.  Always pass NMRData objects using
	references or pointers.  Of course, it is possible to make a copy of a
	dataset using this library, but it must be done deliberately, in one of the
	ways described below, rather than via the simple and automatic copying
	mechanisms of C++ pass-by-value.

	\section	NMRData_intro_datasets			Working with Datasets

	\subsection	NMRData_intro_state				Open and Closed States

	NMRData objects exist in two states: open and closed.  In an open state the
	object is associated with a file or a temporary dataset in memory; in the
	closed state, it is not tied to any data.  The state of an NMRData object 
	can be examined by testing the NMRData::GotData member variable:

	\code
	bool TestIfOpen( NMRData *data )
	{
		return data->GotData;
	}
	\endcode

	The sections below describe how one uses NMRData objects to create or open
	data files, or to create temporary datasets.  Any of these three operations
	results in an "open" NMRData object.  When one is finished with the data,
	the resource is closed, giving a "closed" NMRData object.  Generally, an
	NMRData object is open except when setting parameters for file creation.

	(Note to Advanced C++ Users: This behavior of course violates the C++ 
	resource-acquisition-is-initialization (RAII) paradigm, but it is 
	straightforward and logical, and I have yet to find an instance where it
	caused confusion or problems.  The rest of the library relies exclusively
	on RAII designs, which I find to be a very useful programming paradigm.)
	
	\subsection	NMRData_intro_opening			Opening Existing Data Files

	The process of opening an existing file is quite simple, and is therefore
	introduced first.  If you know the file type already, you can create an
	object of the correct type and call NMRData::OpenFile():

	\code
	NMRData_NMRView data;
	data.OpenFile( "nvfile.nv" );
	\endcode

	This code is the full extent required to open the file "nvfile.nv."  Of
	course, it is good idea to watch for file errors at the same time:

	\code
	NMRData_NMRView data;
	try
	{
		data.OpenFile( "nvfile.nv" );
	}
	catch( Exceptions::CantOpenFile &e )
	{
		printf( e.what() );
		//	do something to handle the exception...
	}
	\endcode

	This code looks for the Exceptions::CantOpenFile exception, which marks 
	most common problems like files not existing, or being unavailable due to
	access restrictions.  The catch handler calls the exception's what()
	function, which prints an error message suitable for an end user.

	It is also possible to create and open a dataset in one step:

	\code
	NMRData_NMRView data( "nvfile.nv" );
	\endcode

	This statement invokes a constructor that automatically opens a file 
	during object creation.  However, don't forget that the following doesn't
	work:

	\code
	try
	{
		NMRData_NMRView data( "nvfile.nv" );
	}
	catch( Exceptions::CantOpenFile &e )
	{
		printf( e.what() );
		//	do something to handle the exception...
	}
	//	oops, data went out of scope and was destroyed at the end of the try block...
	\endcode

	Thus the direct constructor approach can only be used when there is some
	flexibility about the placement of the error-handling code.

	It is more often the case that one simply has a string with a filename, and
	does not know the file type.  In those cases, one can use the static 
	"factory function" NMRData::GetObjForFile(), which allocates an object of
	the correct type on the heap, based on the file extension, and then opens
	that file, before returning a pointer to the open NMRData object.  It can
	be used as follows:

	\code
	NMRData *dataptr = 0;
	try
	{
		dataptr = NMRData::GetObjForFile( "nvfile.nv" );
	}
	catch( Exceptions::FileTypeNotRecognized &e )
	{
		printf( e.what() );
		//	do something to handle the exception...
	}
	catch( Exceptions::CantOpenFile &e )
	{
		printf( e.what() );
		//	do something to handle the exception...
	}
	//	dataptr is now ready to go...
	\endcode

	This example code adds a check for the Exceptions::FileTypeNotRecognized
	error, which is generated when the file type cannot be recognized from the
	extension.

	3-D and 4-D NMRPipe datasets can be somewhat complicated to work with.
	The NMRData file-opening functions all accept NMRPipe file specifications
	according to the same rules as NMRPipe itself (in fact, the code is modeled
	directly after the NMRPipe code for processing file specifications).  This
	means that it should generally be able to handle any file specification
	that NMRPipe itself can understand.  The Exceptions::PipeFileListError
	exception is generated if the specification cannot be understood.

	A number of different errors can occur when trying to open a file, and it
	is wise to examine the list of exceptions in the Exceptions namespace to
	catch any that might be applicable.  It is also wise to close any block
	that tries to open data files with a catch block capable of receiving any 
	Exceptions::FileError exception.

	\subsection	NMRData_intro_creating			Creating New Data Files

	Making a new file requires you to (1) get an object of the appropriate type,
	(2) set up parameters describing the dataset to be created, and (3) call
	NMRData::CreateFile() to build the file.

	One obtains an object of the appropriate derived type for the file format
	exactly as described above for opening existing data: either create an
	object of the right type directly on the stack, or use a "factory function"
	to get an object of the right type based on the file extension.  Of course, 
	in this case you would need NMRData::GetObjForFileType() and <em> NOT </em>
	NMRData::GetObjForFile().

	Once an object is available, one must configure it with the right 
	parameters.  Prior to file creation, one <em> must </em> set the number of
	dimensions (using the NMRData::SetNumOfDims() member function) and the size
	of each dimension (using the NMRData::SetDimensionSize() member function).
	If the data are to be complex and/or time domain, one must additionally use
	the NMRData::SetDimensionComplexState() and NMRData::SetDimensionTD()
	member functions to establish which dimensions are to have these
	characteristics.  By default, it is assumed that all dimensions are real
	and in the frequency domain, unless you provide notice otherwise.  Setting 
	the dimension calibration parameters or dimension labels is optional and
	can also always be done later.

	Finally, one calls NMRData::CreateFile() to build the file.

	Here is an example for a 2-D 128 x 128 spectrum where the type is known a
	priori:

	\code
	NMRData_NMRView data;

	data.SetNumOfDims( 2 );
	data.SetDimensionSize( 0, 128 );
	data.SetDimensionSize( 1, 128 );

	data.CreateFile( "newfile.nv" );
	\endcode

	A more complicated example creates a dataset from a generic filename, 
	determining the file type at run time.  The dataset is to have two dimensions,
	and is to be complex in one of them, with a size of 64 x 64 points:

	\code
	//	assume a char* or std::string variable "filename" with the filename and extension...
	NMRData *dataptr = NMRData::GetObjForFileType( filename );

	dataptr->SetNumOfDims( 2 );
	dataptr->SetDimensionSize( 0, 64 );
	dataptr->SetDimensionComplexState( 0, true );
	dataptr->SetDimensionSize( 1, 64 );
	
	dataptr->CreateFile( filename );
	\endcode

	When a new dataset is created, its contents are initialized to the value
	zero.

	\subsection	NMRData_intro_tempdata			Temporary Datasets

	Sometimes it is convenient to have a temporary dataset, which exists only
	for the duration of the program or of a routine, and which is not tied to 
	any file on disk.  This can be achieved by creating an NMRData object, 
	setting the dimension parameters as one would do for creating a new file, 
	and then calling the NMRData::BuildInMem() member function.  Here is an 
	example:

	\code
	NMRData data;

	data.SetNumOfDims( 2 );
	data.SetDimensionSize( 0, 128 );
	data.SetDimensionSize( 1, 128 );

	data.BuildInMem();
	\endcode

	The new dataset will exist only in memory, unless it is too big to fit in
	memory, in which case a temporary disk file will be used, allocated
	according to the operating system's temporary file mechanism, and deleted
	automatically when the object is deleted or closed.

	Temporary datasets can <em> only </em> be constructed with the NMRData
	base class, not with any of the derived classes.  Calling 
	NMRData::BuildInMem() on a derived class will generate the
	Exceptions::MemOnlyRequiresBaseClass exception.

	\subsection	NMRData_intro_textbin			Text and Raw Binary Files

	ASCII text and raw binary datasets are supported, but present several
	difficulties to the programmer that must be addressed.  Because these files
	have no header information describing the data, when opening these files 
	that information must be provided by the client code instead.  One can do
	this by creating the object first, setting the parameters, and then calling
	NMRData::OpenFile():

	\code
	NMRData_Text data;

	data.SetNumOfDims( 2 );
	data.SetDimensionSize( 0, 128 );
	data.SetDimensionSize( 1, 128 );

	data.OpenFile( "file.txt" );
	\endcode

	The number of data points in the text file must match to the expected size,
	or an error will be generated.  Of course, the library has no way to
	validate that the dimensionality and dimension sizes are actually correct.

	There is a second mechanism for supplying information about text and binary
	data files, the "parameter callback."  To use this, one creates a callback
	function with the signature:

	\code
	bool callback( NMRData *obj )
	\endcode

	This function must determine the correct parameters for the supplied NMRData
	instance, set those parameters, and return true, OR return false if it is not
	possible to set the parameters.  Your code should then set the static member
	variable NMRData::ParameterCallback to the address of the callback function.
	
	The intention of the callback mechanism is that the code to open data files 
	would use the NMRData::GetObjForFile() mechanism to handle user-supplied 
	files of any supported type.  Then, if the user supplies a filename for a
	text or raw binary file, the callback would be called to ask the user for
	the corresponding parameters.  In a text-mode program, that might involve
	text prompts; in a GUI program, it might involve a special dialog box.  As
	an example, consider the following:

	\code
	bool callback( NMRData *obj )
	{
		//	determine parameters and configure obj...
		return true;
	}

	NMRData *openfile( const char *filename )
	{
		NMRData::ParameterCallback = &callback;

		return NMRData::GetObjForFile( filename );
	}
	\endcode

	This code can handle any of the supported file formats.  The parameter
	callback is only used when needed, for text and binary files.  Of course,
	in real code you would want to add error checking.

	Raw binary files are supported through the NMRData base class itself. They
	are expected to contain 4-byte (32 bit) floating point values with the same
	endian organization as the machine itself.  The ordering of data values is
	expected to match the general model described below for the subscripting
	operator and for low-level data access.  The file may not contain a binary
	header.  A binary file may have any extension if one opens it using
	NMRData::OpenFile(), but the NMRData::GetObjForFile() mechanism identifies
	binary files by the extension ".bin".

	ASCII text files are supported via the NMRData_Text derived class.  For real
	data, they are expected to contain one data value per line, in any format
	that can be parsed by the C library function atof().  For complex data, the
	various complex components corresponding to a single complex point should be
	on the same line, separated by spaces or tabs, with one line per complex 
	point.  The file may not contain blank lines or comment lines.  The 
	organization of data points should be as described for binary files.  A 
	text file may have any extension if one opens it using 
	NMRData_Text::OpenFile(), but the NMRData::GetObjForFile() mechanism 
	identifies text files by the extension ".txt".

	\subsection	NMRData_intro_extensions		A Note on File Extensions

	File extensions are only checked in the routines NMRData::GetObjForFile()
	and NMRData::GetObjForFileType().  They are <em> not </em> checked in the
	NMRData::OpenFile() or NMRData::CreateFile() routines.  Thus the following
	will make a raw binary file that has a .nv extension.  It will <em> not
	</em> make a valid NMRView file!

	\code
	NMRData data;
	data.SetNumOfDims( 1 );
	data.SetDimensionSize( 0, 64 );
	data.CreateFile( "file.nv" );		//	raw binary file, NOT an NMRView file
	\endcode

	Likewise, the following makes an NMRPipe file with an NMRView file extension:

	\code
	NMRData_NMRPipe data
	data.SetNumOfDims( 1 );
	data.SetDimensionSize( 0, 64 );
	data.CreateFile( "file.nv" );		//	raw binary file, NOT an NMRView file
	\endcode

	One should exercise care when creating NMRData objects of derived types
	directly, to ensure that only objects of the correct types are used.

	The functions NMRData::GetObjForFile() and NMRData::GetObjForFileType()
	match .dat, .ft, .ft2 and .ft3 to NMRPipe files, .nv to NMRView files,
	.ucsf to UCSF/Sparky files, and .3D.param to XEASY files.  Note that for
	XEASY files one should specify the .3D.param file, and the corresponding
	.3D.16 file is opened automatically.

	\subsection	NMRData_intro_readonly			Read-Only (const) vs. Writeable (non-const) Data

	By default, any NMRData object is capable of modifying the data to which it
	is connected.  In many cases, however, there is no need to modify the data,
	and it is safest if such data are protected from accidental modification.
	There are two levels of protection available, and it is generally
	recommended that you use both.  The best protection is to use the C++
	"const" facility to mark an NMRData object as unmodifiable.  A const object
	cannot be modified, and any action that would change its data or its
	parameters will be rejected by the compiler as invalid.  The second level
	of protection is to indicate to the library that an object is "read-only".
	The read-only status is a runtime mechanism implemented by the library,
	without enforcement from the compiler.

	The two protection mechanisms often overlap, but there are cases where one
	or the other is more appropriate.  Using the library "read-only" marker 
	alone is most useful when the read-only status cannot be determined at
	compile time.  The const marker, however, is useful even with writeable
	data, as a temporary marker that can be applied when passing objects into
	procedures that should not modify them.  They are used together when working
	with data that one knows at compile time should not be modified under any 
	circumstance.

	(As a point of clarification, designating data as const or read-only does
	not affect the operating system permissions of any associated data file on
	disk; it merely determines which operations the library permits on the data.)

	To mark a dataset as const on a temporary basis, simply cast it to a const,
	or assign it to a const pointer or reference:

	\code
	NMRData *a = NMRData::GetObjForFile( "file.nv" );
	const NMRData *b = a;

	a->SetDimensionLabel( 0, "y" );		//	allowed
	b->SetDimensionLabel( 0, "y" );		//	rejected by the compiler
	\endcode

	The read-only designation is conferred when opening a file.  It involves
	passing an extra boolean parameter to the opening routine:

	\code
	NMRData *a = NMRData::GetObjForFile( "file.nv", true );		//	a is readonly

	//	OR

	NMRData_NMRView b;
	b.OpenFile( "file.nv", true );								//	b is readonly
	\endcode

	It is important to note that while the read-only status is enforced strictly,
	attempting to make changes to a read-only dataset does <em> not </em>
	generate run-time errors.  Instead, such operations are simply ignored.  This
	is viewed as a "feature" and not a "bug", allowing one to write
	general-purpose code that changes writeable data, while having no effect on
	read-only data.  If you would like to see error messages on any accidental
	attempt to write to a read-only dataset, you should also mark it as const.

	To open a read-only dataset as a const object, you should invoke both
	mechanisms at the time the object is created:

	\code
	const NMRData *data = NMRData::GetObjForFile( "file.nv", true );
	\endcode

	Creating a genuine const object on the stack is possible, but must be
	carried out using the constructor approach to file opening:

	\code
	const NMRData_NMRView data( "file.nv", true );
	\endcode

	Note that this constructor also accepts the optional read-only parameter,
	which should be used in any case like this where the data are declared
	const.

	In C++, marking an object as const implies not only that its data cannot
	change, but also that its "logical state" cannot change.  Thus it
	should not be possible to close a const object and reopen it to refer to
	a different dataset, for example.  This is especially important when
	passing objects to functions in other libraries or programs:  The const
	designation provides you with a guarantee that the foreign code cannot
	change what the object references, any more than it can change the data.
	This library treats const in the same way.  Thus it is not possible to
	do the following:

	\code
	const NMRData_NMRView data;
	data.OpenFile( "file.nv", true );		//	compiler error: cannot modify const object
	\endcode

	It is always possible to make a non-const object and then assign to a
	const reference after opening the file:

	\code
	NMRData_NMRView data_nc;				//	data_nc is not protected
	data_nc.OpenFile( "file.nv", true );

	const NMRData &data = data_nc;			//	data is protected
	data.SetDimensionLabel( 0, "y" );		//	not allowed
	data_nc.SetDimensionLabel( 0, "y" );	//	allowed
	\endcode

	\subsection	NMRData_intro_copying			Copying a Dataset

	To create a complete copy of a dataset, use the NMRData::CopyDataset()
	member function.  Copying to a temporary destination is the default:

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );
	NMRData dest;
	dest.CopyDataset( source );
	\endcode

	This code constructs a temporary (memory-only) copy of source in dest.  
	This can also be accomplished with the static version of 
	NMRData::CopyDataset():

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );
	NMRData dest;
	CopyDataset( source, &dest );
	\endcode

	Note that NMRData::CopyDataset accepts pointers rather than references.  The
	use of const for the source is of course optional, but recommended if you
	will not be changing the source in any way.

	Making a copy on disk is slightly trickier:  The destination object must
	first be opened with either a new or existing file, which is then overwritten
	when you call NMRData::CopyDataset():

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );

	NMRData *dest = NMRData::GetObjForFileType( "copy.nv" );
	dest->SetNumOfDims( 1 );
	dest->SetDimensionSize( 1 );
	dest->CreateFile( "copy.nv" );

	CopyDataset( source, dest );
	\endcode

	If the destination already exists, it can be overwritten as follows:

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );
	NMRData *dest = NMRData::GetObjForFile( "copy.nv" );
	CopyDataset( source, dest );
	\endcode

	Sometimes you would like to create an empty dataset (all zeroes) that has
	the same size, dimensionality and parameters as an existing dataset.
	This is accomplished using NMRData::CopyParameters():

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );
	NMRData	*dest = NMRData::GetObjForFileType( "copy.nv" );
	CopyParameters( source, dest );
	dest->CreateFile( "copy.nv" );
	\endcode

	In this case, dest is not opened initially, and instead the parameters in
	source are copied over.  A new file is then created which has the same
	size, dimensionality, dimension labels, etc. as source, but with all values
	set to zero.

	If you wanted, you could copy the parameters and then change them before
	creating the file.  Thus the following makes dimension zero of dest 10
	points larger than that of source:

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );
	NMRData	*dest = NMRData::GetObjForFileType( "copy.nv" );
	CopyParameters( source, dest );
	dest->SetDimensionSize( 0, dest->GetDimensionSize( 0 ) + 10 );
	dest->CreateFile( "copy.nv" );
	\endcode

	There are also cases where one needs to copy the values from one dataset to
	another, without changing the parameters like dimension labels.  Of course,
	the dimensionality and size must be the same for this operation to have any
	meaning, but one may not wish to overwrite the other parameters.  This is
	accomplished using the assignment operator:

	\code
	const NMRData *source = NMRData::GetObjForFile( "file.nv", true );
	NMRData *dest = NMRData::GetObjForFile( "file2.nv" );

	*dest = *source;

	//	a prettier version, if references are first declared:
	const NMRData &s = *source;
	NMRData &d = *dest;

	d = s;
	\endcode

	\subsection	NMRData_intro_cache				Cache Configuration

	Internally, NMRData objects maintain a cache in memory of recently-accessed 
	data.  The default cache settings should be appropriate in most cases,
	but there are occasions where it may be useful to reconfigure the cache.
	Once a dataset is open, the cache parameters are changed by calling the
	NMRData::InitCache() member function, which reinitializes the cache using
	different parameters.

	The most important cache parameters are the cache size and the cache mode.
	The size is the maximum number of data values that the cache will hold in
	memory at any given time.  It actually refers to the number of complex
	points, in cases of complex data (i.e. for complex data with one real and
	one imaginary component, a cache size of 10 means that the cache is limited
	to 10 complex points, or 20 float values, or 80 bytes total).

	If you have lots of physical memory on your system, you may want to change
	the default cache size.  See the section \ref NMRData_intro_misc 
	below.

	The cache mode determines how cache pages are structured.  For linear files,
	the default mode is "linear," in which case each cache page is a chunk of
	data values that are contiguous on disk.  For submatrix files, the mode is
	"submatrix," and each cache page is a submatrix.  There are also "vector"
	and "plane" modes.  Programs that work primarily on individual vectors or
	planes may see reduced cache miss rates by switching to these cache modes, while
	programs that tend to analyze values located adjacent to one another in
	the n-D data space may see reduced cache misses with submatrix organization.
	However, choosing a different cache mode could drastically increase the cost
	of a loading a single cache page.  A decision to change cache modes should
	be undertaken only with guidance from a performance profiler showing an
	average improvement in performance from the change.

	\subsection	NMRData_intro_closing			Flushing and Closing a Dataset

	Changes that you make to a dataset are not necessarily written to disk
	immediately, but may instead sit in the cache for extended periods of time.
	To flush all changes to disk, call the NMRData::Flush() member function.
	
	An open NMRData object is automatically flushed and closed when the object 
	is deleted.  The NMRData::Close() member function will also close a
	dataset explicitly.

	The NMRData::CreateFile() and NMRData::OpenFile() member functions cannot
	be called on currently open objects: You must call NMRData::Close() first
	to close the open data explicitly.

	\section	NMRData_intro_dimensions		Dimensions and Data Organization

	\subsection	NMRData_intro_org				Logical Data Organization

	Keeping track of dimensions and data point locations in multidimensional
	datasets can be challenging.  While the actual storage of data on disk or 
	in memory may assume many different forms depending on the file format and
	cache mode, the library's interface to client code always follows a simple 
	and uniform model for the "logical organization" of the data. This model 
	closely adheres to the standard system used in C and C++ for	
	multidimensional arrays.  As an example, consider a 2-D C array of
	5 x 5 data values.  A multidimensional C array is stored as a contiguous
	string of data values, in "row-major" order:

	<TABLE>
	<TR>	<TH rowspan="2" colspan="2"></TH>				<TH colspan="5">Dimension 1</TH>							</TR>
	<TR>													<TH>0</TH>	<TH>1</TH>	<TH>2</TH>	<TH>3</TH>	<TH>4</TH>	</TR>
	<TR>	<TH rowspan="5">Dimension 0</TH>	<TH>0</TH>	<TD>0</TD>	<TD>1</TD>	<TD>2</TD>	<TD>3</TD>	<TD>4</TD>	</TR>
	<TR>										<TH>1</TH>	<TD>5</TD>	<TD>6</TD>	<TD>7</TD>	<TD>8</TD>	<TD>9</TD>	</TR>
	<TR>										<TH>2</TH>	<TD>10</TD>	<TD>11</TD>	<TD>12</TD>	<TD>13</TD>	<TD>14</TD>	</TR>
	<TR>										<TH>3</TH>	<TD>15</TD>	<TD>16</TD>	<TD>17</TD>	<TD>18</TD>	<TD>19</TD>	</TR>
	<TR>										<TH>4</TH>	<TD>20</TD>	<TD>21</TD>	<TD>22</TD>	<TD>23</TD>	<TD>24</TD>	</TR>
	</TABLE>

	The table shows how the two coordinates of the array map to the locations
	of data values in the continguous memory block.  For example, the value at
	position (3,1) is stored at a memory offset of 16 from the start of the
	memory block, and as the C/C++ code below shows, either the coordinates or
	the linear offset can be used to access the data:

	\code
	int array[ 5 ][ 5 ];
	array[ 3 ][ 1 ] = 2;
	if( *( array + 16 ) == 2 ) printf( "Value is correct." );	//	should always print!
	\endcode

	The %NMRData library follows this nomenclature and logical organization.
	The dimension whose coordinate values change most slowly in a linear
	traversal of the data on disk or in memory is designated as dimension 0,
	the dimension whose coordinate values change second-most slowly as
	dimension 1, etc.  The coordinates for each dimension are numbered beginning
	at zero, and extending to the size of the dimension minus one.  Each data
	point has a corresponding "offset" which is an integer between 0 and the
	full size of the dataset minus one.  Offsets are calculated using the
	same method as illustrated above for the 2-D C/C++ array.

	It may be helpful to keep in mind that this convention for dimension
	numbering is essentially the reverse of the NMRPipe convention.  For 2-D
	data, the NMRPipe X and Y dimensions map to %NMRData dimensions 1 and 0, 
	while for 3-D data, the NMRPipe X, Y and Z dimensions map to this library's
	dimensions 2, 1 and 0, respectively.

	\subsection	NMRData_intro_dimsizes			Setting and Accessing Dimensionality and Dimension Sizes

	The number of dimensions in a dataset is set using the 
	NMRData::SetNumOfDims() member function.  The size of a dimension is set
	using the NMRData::SetDimensionSize() member function.  These functions can
	only be called for NMRData objects which are not open, and will specify the
	sizes used when creating new datasets, or when opening text or binary data
	with no header information.  They are ignored when opening files that have
	header information.

	The dimensionality of a dataset can be obtained using 
	NMRData::GetNumOfDims(), and the size of a dimension is accessible using
	NMRData::GetDimensionSize().

	It is also possible to get or set the sizes of all dimensions simultaneously,
	using the NMRData::GetDimensionSizes(), NMRData::GetDimensionSizeVector()
	and NMRData::SetDimensionSizes() functions.  The first two function differ
	in their return types, the one returning a pointer to a heap-allocated 
	array of int values giving the sizes of all dimensions, and the other
	returning an STL vector of int values giving the sizes of all dimensions.
	The NMRData::SetDimensionSizes() function accepts either an STL vector of
	int values, or a pointer to an array of int sizes.

	(This pattern of dual interfaces with STL containers and C-style pointers
	to arrays is used through much of the library.  I am making a transition to
	STL containers in all of my C++ code, and I would recommend them as much
	easier to use and much less error-prone than low-level array coding. 
	Nonetheless, the library supports both methods in most of its interfaces.)

	\subsection NMRData_intro_dimlabels			Dimension Labels

	Each dimension can have a text label, which is set using the 
	NMRData::SetDimensionLabel() member function, and which is accessed using
	either the NMRData::GetDimensionLabel() or NMRData::GetDimensionLabelStr()
	function.  Once again, support is available for both C-style character
	strings and STL std::string objects, through the dual interfaces.

	The library imposes no restriction on the content of the string, but many
	file formats severely restrict the number of characters, and the library will
	truncate label strings as needed when saving the data.  Labels may be
	set at any time, for open or closed datasets, provided that the data
	are writeable.

	\subsection	NMRData_intro_dimlimits			Limitations on Dimensionality and Dimension Sizes

	The library itself imposes no limit on the dimensionality of the data, but
	most of the file formats have restrictions.  For example, NMRPipe files can
	have no more than 4 dimensions, and NMRView files are restricted to 8.
	Other formats permit arbitrary dimensionality.  However, most NMR software
	programs impose even more stringent limitations than the file formats on
	the number of dimensions that the programs will work with.

	The library also does not impose a restriction on dimension or dataset sizes,
	except that the total size of the file must be indexable by a "long int" on
	your platform.  This restriction is inherent in the C library's file access
	routines, and in many cases will restrict total file size to 2 or 4 GB.
	Some 64-bit platforms have alleviated this file size restriction.

	The library always represents dimension numbers and sizes as "int" values,
	and dataset sizes and data point offsets as "size_t" or "ptrdiff_t" values.

	\subsection	NMRData_intro_calibration		Calibrating Chemical Shifts for a Dimension

	Each dimension of a dataset has attributes associated with it defining its
	calibration or referencing.  Following the model used in NMRPipe, these
	parameters are the spectrometer frequency (MHz), the spectral width (Hz)
	and the PPM value at the center of the dimension.  There is often confusion
	about whether the spectral width and center PPM values used for calibration
	are based on the size of the dimension, or the size plus one.  This library
	follows the convention that the spectral width is the frequency difference
	between the first and last point in the dimension, and the center PPM value
	is the chemical shift at the location halfway between the first and last
	points (in other words, I use "size" rather than "size plus one").

	The easiest way to get the calibration is using the functions
	NMRData::GetDimensionSF(), NMRData::GetDimensionSW() and 
	NMRData::GetDimensionCenterPPM(), which return the spectrometer frequency in
	MHz, the spectral width in Hz and the center chemical shift in PPM,
	respectively, for a given dimension.  Corresponding functions are provided
	for setting the calibration.  The calibration may be set at any time,
	including for currently open data, in which case the changed calibration
	will be written to disk when the file is saved.

	\subsection NMRData_intro_complex			Complex, Hypercomplex and Time Domain Data

	Provided that the file format supports it, complex and hypercomplex data
	can be stored in NMRData datasets.  When attempting to create complex or
	hypercomplex data, it is wise to check for the 
	Exceptions::ComplexNotSupported exception, which is generated when the
	format does not support complex data.  To determine whether a dimension
	is complex, call NMRData::GetDimensionComplexState().  Prior to creating
	a new dataset, one can call NMRData::SetDimensionComplexState() to mark
	a dimension as complex.

	Complex and hypercomplex components extend the model for logical data
	organization, following a convention very similar to that used for data
	point offsets and dimension coordinates.  Each complex or hypercomplex
	component is numbered, and they are ordered in row-major order, real
	before imaginary.  Thus for a 2-D hypercomplex dataset,	the	components for 
	a single data point are numbered as follows:

	<TABLE>
	<TR>	<TH rowspan="2" colspan="2"></TH>						<TH colspan="2">Dimension 1</TH>	</TR>
	<TR>															<TH>Real</TH>	<TH>Imaginary</TH>	</TR>
	<TR>	<TH rowspan="2">Dimension 0</TH>	<TH>Real</TH>		<TD>0</TD>	<TD>1</TD>				</TR>
	<TR>										<TH>Imaginary</TH>	<TD>2</TD>	<TD>3</TD>				</TR>
	</TABLE>

	The total number of components for an NMRData object can be obtained using 
	NMRData::GetNumOfComponents().

	The library also tracks which dimensions are in the time domain, which is
	an independent property from the real/complex state.  By default, a
	dimension is assumed to be in the frequency domain.  One determines if a
	dimension is in the time domain using NMRData::GetDimensionTD(), and for
	new datasets this attribute is set using NMRData::SetDimensionTD().

	\section	NMRData_intro_data				Working with Data

	\subsection	NMRData_intro_subscripts		Accessing a Single Data Point

	Following the standard C++ approach, individual data points are accessed
	using subscripting operators.  I say "operators" because two different
	subscripting operators have been implemented, the one using the standard
	square brackets, and the other using parentheses, which have slightly
	different meanings.  The subscripting operators return objects of type
	Reference, which are proxy objects allowing access to the actual data.
	For real data, Reference objects convert directly into float values, and
	can be used in the same way as a raw float.  With complex data, the
	situation is a little more complicated.

	We will begin with the simplest example of using the subscripting
	mechanism.  Given an NMRData object reference called data, created with
	code like the following and referring to real (not complex) spectral data:

	\code
	NMRData *dataptr = NMRData::GetObjForFile( filename );
	NMRData &data = *dataptr;									//	make a reference, so that operators are easier to access
	\endcode

	we can obtain the value of the 17th data point in the dataset (that is, 
	the data point with offset 16 as defined above under \ref NMRData_intro_org) 
	as follows:

	\code
	float value = data[ 16 ];
	\endcode

	The returned reference is an "L-value" in C++ nomenclature, so we can also 
	assign to it:

	\code
	data[ 16 ] = 2.0F;
	\endcode

	In the preceding two examples, we have interacted with the proxy object only
	indirectly.  However, sometimes one may wish to hang onto a reference to a
	particular data point, in which case we would need to save the Reference
	object that is returned from the subscripting operator:

	\code
	Reference ref = data[ 16 ];

	float value = ref;
	ref = 2.0F;
	\endcode

	Another form of the subscripting operator accepts a vector of coordinate
	values.  Assume that we would like a reference to the data point at
	position (3,1).  This could be obtained using the following:

	\code
	std::vector< int > coordinates( 2 );		//	make a vector with two values, to hold the coordinates

	coordinates[ 0 ] = 3;						//	set the coordinate for dimension 0
	coordinates[ 1 ] = 1;						//	set the coordinate for dimension 1

	Reference ref = data[ coordinates ];
	\endcode

	This kind of code is particularly nice when we need to be able to handle
	arbitrary dimensionality at runtime, since it easily generalizes to
	an arbitrary number of dimensions.  When we know the number of dimensions
	at compile time, we can use the other form of subscripting operator, the
	parentheses operator, to write a more elegant version of the preceding:

	\code
	Reference ref = data( 3 )( 1 );
	\endcode

	This code works just as well with variables:

	\code
	int y = 3, x = 1;
	Reference ref = data( y )( x );		//	yes, it is y then x - see the section on dimensions...
	\endcode

	At this point, we can imagine an obvious way to set all the data points to
	a given value:

	\code
	//	assume float variable named "value"
	int ysize = data.GetDimensionSize( 0 ), xsize = data.GetDimensionSize( 1 );
	for( int y = 0; y < ysize; y++ )
		for( int x = 0; x < xsize; x++ )
			data( y )( x ) = value;
	\endcode

	However, while this would certainly work well enough, we caution you that
	there are even easier and more efficient ways to carry out actions on all
	of the data points in a dataset or even a subset of a dataset, as
	described under \ref NMRData_intro_bulkmath.

	\subsection	NMRData_intro_dimpositions		Using DimPosition Objects

	In many circumstances, calculating data point offsets or supplying data 
	point coordinates will be a reasonable approach for locating a data point.  
	However, converting between other coordinate systems, such as PPM chemical
	shifts, can be tedious and error-prone, and for that reason the library
	supplies another method for referencing positions on dimensions, the
	DimPosition class.  A DimPosition object is associated with a particular
	dimension of a particular parent dataset, and knows about the calibration
	of that dimension.  It is able to register positions in terms of PPM or
	Hz chemical shifts, as well as data points, and to convert between these
	systems.  With time domain data, it understands dwell times and evolution
	times.

	To use DimPosition objects, we must first obtain them from the parent
	dataset.  Again assuming a dataset named "data" with two dimensions, we
	can do this using the following:

	\code
	DimPosition< NMRData > Y = data.GetDimPosObj( 0 );
	DimPosition< NMRData > X = data.GetDimPosObj( 1 );
	\endcode

	Note that the DimPosition class is actually a template, and the template
	parameter is the typename of the parent class.  In this case, since our
	DimPosition objects refer to dimensions from NMRData parent objects, we
	set the type to NMRData.

	Now that we have the two DimPosition objects, we could use them to get
	the value at position (3,1) analogously to using int variables above:

	\code
	Y = 3;
	X = 1;
	Reference ref = data( Y )( X );
	\endcode

	That is not really a step forward.  However, presume instead that we want
	the value at the closest data point to the chemical shifts (118.6 ppm,
	7.2 ppm).  This could be achieved as:

	\code
	X.SetUnits( ppm );
	Y.SetUnits( ppm );

	Y = 118.6F;
	X = 7.2F;

	Reference ref = data( Y )( X );
	\endcode

	A DimPosition object can be set to operate in points, Hz, ppm or 
	(for time domain data) seconds using the DimPosition::SetUnits() member
	function.  Numeric values are then interpreted by the DimPosition
	object in terms of the chosen coordinate system.  A set of functions
	is also provided that sets or obtains the current position in terms of
	any desired units, without changing the current units:

	\code
	X.SetPosPPM( 7.2F );	//	doesn't change the units for X
	\endcode

	Thus we could set the position in PPM and then get the position in terms of
	data points:

	\code
	X.SetPosPPM( 7.2F );
	int xpts = X.GetPosPts();
	\endcode

	Imagine that we have two datasets, data and data2, which overlap, but
	which do not cover the exact same region.  If we were working with raw
	data points, the conversion of equivalent positions between the two would
	be quite tedious.  However, the DimPosition objects allow us to translate
	positions directly.  First, we obtain the DimPosition objects and set them
	all to PPM coordinates mode:

	\code
	DimPosition< NMRData > Y1 = data.GetDimPosObj( 0 );
	DimPosition< NMRData > X1 = data.GetDimPosObj( 1 );
	DimPosition< NMRData > Y2 = data2.GetDimPosObj( 0 );
	DimPosition< NMRData > X2 = data2.GetDimPosObj( 1 );

	Y1.SetUnits( ppm );
	X1.SetUnits( ppm );
	Y2.SetUnits( ppm );
	X2.SetUnits( ppm );
	\endcode

	Presume that we want to find the data point in data2 that has the same
	chemical shifts as data point (3,1) in data, and set the point in data2 to
	the same value as the point in data.  This is done with:

	\code
	Y1.SetPosPts( 3 );		//	set this position in terms of points
	X1.SetPosPts( 1 );		//	...translated automatically to PPM

	Y2 = Y1;				//	get equivalent position in data2 based on PPM
	X2 = X1;

	data2( Y2 )( X2 ) = data( Y1 )( X1 );
	\endcode

	What would have happened if there were no equivalent position?  DimPosition
	objects are allowed to represent locations that are outside the range of
	their parent dimension.  Thus, the assignment of Y2 = Y1 and X2 = X1 would
	have been permitted.  However, the subscripting operators would have thrown
	the exception Exceptions::BadCoordinates, reflecting the fact that one or
	more coordinate values was out of range.  One can check whether a DimPosition
	object is in range using the DimPosition::InRange function:

	\code
	Y2 = Y1;
	if( !Y2.InRange() ) printf( "No equivalent position for Y dim." );
	\endcode

	The check can also be made before the assignment, using 
	DimPosition::CheckRange:

	\code
	if( !Y2.CheckRange( Y1 ) ) printf( "No equivalent position for Y dim." );
	\endcode

	Note that DimPosition objects are rigidly bound to a particular parent and
	a particular dimension of that parent:

	\code
	data( X1 )( Y1 ) = 2.0F;	//	ERROR!  Wrong dimension order...
	data( Y2 )( X2 ) = 2.0F;	//	ERROR!  Wrong parent object...
	\endcode

	The DimPosition class provide a rich interface for working with coordinates.
	Standard arithmetic operators are supported:

	\code
	X1 += 2.0F;		//	increases the coordinate by 2.0 ppm
	\endcode

	as is incrementing and decrementing:

	\code
	X1++;			//	moves to the next data point
	\endcode

	as well as named actions that change the position:

	\code
	X1.MoveToDownfieldEnd()
	\endcode

	and that obtain or set dimension calibration information:

	\code
	float sw = X1.GetSW();
	\endcode

	One can easily write a loop over a dimension:

	\code
	for( X1.MoveToDownfieldEnd(); X1.InRange(); X1++ )
		//	do something...
	\endcode

	Please see the DimPosition reference for complete details.  Note also that
	DimPosition objects are freely copyable:

	\code
	DimPosition< NMRData > copy = X1;		//	copy is bound to same parent as X1
	\endcode

	The above code works well when the number of dimensions are known at compile
	time, but what about cases where the dimensionality is not known until
	runtime?  For such situations, one can call NMRData::GetDimPosSet to obtain
	a vector of DimPosition objects preassigned to each of the dataset's
	dimensions:

	\code
	std::vector< DimPosition< NMRData > > coordinates = data.GetDimPosSet();
	\endcode

	Such a set can then be supplied to the square bracket subscripting operator
	to get a reference to a data point:

	\code
	coordinates[ 0 ] = 3;
	coordinates[ 1 ] = 1;
	Reference ref = data[ coordinates ];
	\endcode

	\subsection	NMRData_intro_references		Working with Reference Objects

	The advantage of having proxy objects such as Reference objects is that they
	remain connected to the particular data point that they reference.  This is
	a customary role for pointers, but pointers cannot be used with NMRData
	objects to reference internal data, since the data point may be moved in
	memory or even unloaded from memory, as determined by the caching code.
	Reference objects are also slightly easier to use than pointers, since they
	automatically dereference themselves.

	A Reference object is guaranteed to be valid for as long as the dataset
	remains open.  They can be freely copied and passed by value.  They accept
	float values in assignment statements, and convert freely to floats.

	Note, however, that while the interconversion with float values is automatic,
	compiler warnings may be generated with double values:

	\code
	Reference ref = data[ 16 ];

	ref = 2.0;		//	may give a compiler warning about truncating a double value
	ref = 2.0F;		//	the suffix makes this a float, so there will be no warning
	\endcode

	Note also that an explicit conversion to float is occasionally necessary.
	The most obvious example is the printf family of functions, since the
	compiler cannot apply type conversions on their arguments:

	\code
	printf( "%f", ref );				//	This will almost certainly give the wrong answer -
										//	may also crash the machine!

	printf( "%f", ( float ) ref );		//	Casting to float solves the problem...
	\endcode

	Given a Reference object, its offset can be determined using Reference::GetOffset,
	while the full set of DimPosition coordinates referencing its position can be
	obtained with Reference::GetCoordinates:

	\code
	std::vector< DimPosition< NMRData > > coordinates = ref.GetCoordinates();
	\endcode

	Assigning one reference to another does not cause them to refer to the same
	data point.  Rather, it copies the value of the one into the location
	referenced by the other:

	\code
	Reference ref1 = data[ 16 ];
	Reference ref2 = data[ 17 ];
	ref1 = ref2;					//	ref1 still references point 16; copies data value instead
	\endcode

	\subsection	NMRData_intro_complexref		Working with References to Complex Data

	The utility of Reference objects increases for complex data.  A Reference to
	a complex data point is obtained in the same manner as for real data:

	\code
	Reference ref = complex_data[ 16 ];
	\endcode

	However, ref now refers to more than one complex component.  Assume here
	that complex_data is complex in both dimensions (i.e. that it has four
	components, Re/Re, Re/Im, Im/Re and Im/Im).  The individual components can
	be accessed using subscripting operators, which work analogously to those
	found in the NMRData class.  The square bracket operator accepts component
	numbers:

	\code
	float re_re = ref[ 0 ];		//	remember the dimension order!
	float re_im = ref[ 1 ];
	float im_re = ref[ 2 ];
	float im_im = ref[ 3 ];
	\endcode

	while the parentheses operator accepts boolean values, with false = real
	and true = imaginary:

	\code
	float re_re = ref( false )( false );
	float re_im = ref( false )( true );
	float im_re = ref( true )( false );
	float im_im = ref( true )( true );
	\endcode

	One can chain together subscripting operators from the NMRData class and
	from the Reference class.  Thus to get a reference to the object at (3,1)
	or offset 16 in a dataset and change the value of its im/re component, or 
	component 2, to 2.0, we can use any of the following approaches:

	\code
	complex_data[ 16 ][ 2 ] = 2.0F;
	complex_data[ 16 ]( true )( false ) = 2.0F;
	complex_data( 3 )( 1 )[ 2 ] = 2.0F;
	complex_data( 3 )( 1 )( true )( false ) = 2.0F;
	\endcode

	Whether writing this kind of code is wise is another matter.  It is probably
	less confusing in almost all cases to separate the step of obtaining the
	reference from that of accessing a particular complex component.

	References to complex data do <em> not </em> convert directly to 
	float values without selecting a component.  Thus the following is an
	error:

	\code
	Reference ref = complex_data[ 16 ];

	float value = ref;				//	ERROR: No component selected...
	float value = ref[ 2 ];			//	This works.
	\endcode

	\subsection	NMRData_intro_refmath			Mathematical Operations with References

	References support all of the standard mathematical operators.  For real
	data, these behave as expected:

	\code
	Reference ref = data[ 16 ], ref2 = data[ 17 ];

	ref += 68.0F;
	ref /= 2.0F;
	float result = ref2 + ref * 10.0F;
	\endcode

	Of course, one needn't bother with the explicit Reference objects:

	\code
	float result = data[ 17 ] + ( data[ 16 ] + 68 ) * 5.0F;
	\endcode

	The situation with complex data is slightly more complicated.  I have not
	followed the standard rules for complex arithmetic, in part because the
	extension of those rules to arbitrary dimensionality can be constructed in
	several different ways, none of which are easy to implement, and none of
	which are universally accepted (although quaternions are probably the most
	popular).

	In the scheme implemented here, arithmetic operations involving complex
	values are performed on each channel independently.  Thus for four
	components:

	\code
	complex_data[ 16 ] *= complex_data[ 17 ];

	//	is equivalent to

	complex_data[ 16 ][ 0 ] *= complex_data[ 17 ][ 0 ];
	complex_data[ 16 ][ 1 ] *= complex_data[ 17 ][ 1 ];
	complex_data[ 16 ][ 2 ] *= complex_data[ 17 ][ 2 ];
	complex_data[ 16 ][ 3 ] *= complex_data[ 17 ][ 3 ];
	\endcode

	Scalars affect all channels, except for assignment:

	\code
	ref += 2.0F;		//	all components are affected
	ref *= 2.0F;		//	all components are affected
	ref = 2.0F;			//	only component 0 is affected
	\endcode

	This behavior might seem surprising.  I caution the user that this system 
	of complex arithmetic does <em> not </em> agree with standard complex
	algebra.

	One can write expressions involving individual components:

	\code
	ref[ 3 ] = ref[ 1 ] + 2.0F;
	\endcode

	The library expects the Reference objects in an expression to have the same
	number of components, or to each refer unambiguously to a single
	component.  For example, given a 2-D hypercomplex Reference hc_ref with four
	components, a 2-D complex Reference c_ref with two components, and a real
	Reference r_ref with one component, the following are valid, because each
	use of a complex-valued Reference is qualified with a selector specifying
	which component to use:

	\code
	r_ref = hc_ref[ 0 ] + c_ref[ 1 ];
	hc_ref[ 1 ] = r_ref * c_ref[ 0 ];
	\endcode

	whereas the following are not legal:

	\code
	hc_ref += c_ref;
	hc_ref *= r_ref;
	\endcode

	(note that a Reference with a single component, as in the last line of the
	example, is not considered a scalar - only values of type "float" are 
	scalars under these rules)

	An expression involving Reference objects produces a new object of type
	Value, which stores the intermediate result of evaluating the expression.
	In the examples above, temporary Value objects were in fact generated and 
	then assigned back to a Reference.  We can also store Value objects 
	explicitly:

	\code
	Value result = ref + ref2;
	\endcode

	Value objects are generated and destroyed automatically as expressions are
	evaluated, but it is also possible to obtain a blank Value object with the
	appropriate number of components for a given dataset using 
	NMRData::GetBlankValue().  Value objects have an identical interface to
	Reference objects, including identical subscripting behavior.  The only 
	exception is the behavior of the assignment operator, which clears the 
	object and gives it the appropriate number of components to hold the value
	that is assigned to it:

	\code
	Value val = complex_data.GetBlankValue();		//	multiple components
	val = 2.0F;										//	now becomes a real, with one component
	val = complex_data[ 16 ];						//	now back to being complex
	\endcode

	References convert automatically to Value objects:
	
	\code
	Value val = ref;
	\endcode

	It is also always possible to assign a Value object to a Reference, 
	provided that they have the same number of complex components:

	\code
	Reference ref = data[ 16 ];
	ref = val;
	\endcode

	But one cannot write the following:

	\code
	Reference ref = val;
	\endcode

	...since the Reference would not refer to any data.  Reference objects
	must always be obtained from the parent dataset, while Value objects have
	no association with a dataset and may be freely created as desired.

	Besides their use in evaluating mathematical expressions, Value objects can
	be a convenient way to store or pass a complex data point from a complex 
	dataset without maintaining an association with the original data point.  

	In terms of performance, Reference objects can be created and destroyed
	much more quickly than Value objects, and operations that can be
	performed directly with Reference objects will be faster than converting to
	a Value.

	Complex mathematical expressions involving Reference and Value objects are
	convenient, but can be relatively slow due to the time required to create
	and destroy numerous temporary objects.  For operations that must be
	carried out repeatedly on all the points of a dataset or a subset of a
	dataset, it is more efficient to use the dedicated operators and
	functions described under \ref NMRData_intro_bulkmath.

	It is also possible to apply a user-defined operation to each component of
	a complex Reference or Value.  To do so, one first writes a functor
	implementing the operation (for those new to C++, a "functor" is a "function
	object," an object implementing an operation and behaving like a function;
	see a standard reference on C++ for more information).  The functor's 
	function call operator should accept a float - the current value of the 
	complex component - and return a float - the new value to store there.  
	As an example, let us presume that one wishes to replace each component of
	a complex value with its sin.  The functor would be written as:

	\code
	struct compute_sin
	{
		float operator()( float input )
		{
			return sinf( input );
		}
	};
	\endcode

	and the operation would be applied to point 16 of a complex	dataset using 
	the Reference::ApplyToAll function:

	\code
	complex_data[ 16 ].ApplyToAll( compute_sin() );
	\endcode

	Note that this library follows the somewhat unusual practice of accepting
	references to functor objects, rather than producing copies.  This allows
	you to use functors that have state information and which return information
	to you.  However, depending on your compiler you may have to introduce a
	temporary variable even in cases when you are not collecting information:

	\code
	compute_sin functor;						//	this is not needed, except for certain compilers
	complex_data[ 16 ].ApplyToAll( functor );
	\endcode

	(The problem seems to be a disagreement over whether a temporary object of
	type T is an L-value (and thus assignable to a reference).  Yes, standard
	C++ says that temporaries are R-values, but there are certain constructs
	that legitimately work around that, and then there are compilers that
	don't enforce the restriction correctly.  In the end, some expressions like
	this one will fly, and some will not.  We could "fix" this problem by
	changing the library to accept references to const functors, but then you
	would have to play games with constness to get the functors to update
	their states.  There's not really a pretty answer.)

	\subsection	NMRData_intro_subsets			Accessing a Subset of a Dataset

	The library provides a Subset class for representing subsets out of full
	datasets.  Like Reference objects, Subset objects are proxies that provide
	indirect access to the actual data stored in the cache or on disk.  Also
	like a Reference object, a Subset is obtained from the parent NMRData object
	using a subscripting operator.  A Subset can represent any portion of a
	full dataset, with any dimensionality up to that of the dataset itself.

	To obtain a Subset, one specifies the coordinates for the downfield and
	upfield corners.  For example, given a 2-D datset of 5 x 5 points, a
	Subset could be requested for a region beginning at (1,1) and extending to
	(3,3), inclusive.  Such a region would be 3 x 3 and have two dimensions.
	The coordinates of the corners must be encoded either as a vector of int
	values, or as a vector of DimPosition objects, and are then supplied to
	a version of the parentheses subscripting operator.  Using int values:

	\code
	//	assume an NMRData named "data"
	std::vector< int > corner1( 2 ), corner2( 2 );
	corner1[ 0 ] = 1;	corner1[ 1 ] = 1;			//	point (1,1)
	corner2[ 0 ] = 3;	corner2[ 1 ] = 3;			//	point (3,3)

	Subset region = data( corner1, corner2 );
	\endcode

	For access to data points, and for mathematical operations, Subset objects 
	support the same interface as NMRData objects.  Thus the following are all
	valid:

	\code
	region[ 0 ] += 2.0F;				//	returns a Reference, then invokes addition/assignment
	float val = region( 1 )( 1 );		//	returns a Reference, then converts to float
	region( 0 )( 2 ) = data[ 16 ];		//	both sides of "=" return Reference objects
	\endcode

	One can obtain a Subset of a Subset:

	\code
	std::vector< int > corner1( 2 ), corner2( 2 );
	corner1[ 0 ] = 0;	corner1[ 1 ] = 0;			//	point (0,0) of region -> (1,1) overall
	corner2[ 0 ] = 1;	corner2[ 1 ] = 2;			//	point (1,2) of region -> (2,3) overall

	Subset region2 = region( corner1, corner2 );
	\endcode

	Subset objects can also return DimPosition objects:

	\code
	DimPosition< Subset > pos = region.GetDimPosObj( 0 );
	\endcode

	Note that these DimPosition objects take a template parameter of Subset, the
	type of the parent object.  All of the normal DimPosition interface features
	work, but they adapt their values to the size of the region.  Thus a call of
	DimPosition::GetSW() for a DimPosition bound to a Subset will return the
	spectral width of the Subset's dimension, not of the original dimension in
	the original NMRData.

	Subset objects can be freely copied.  A Subset is valid for as long as the
	parent NMRData object is open.  Note that when one obtains a Subset from a
	Subset, the new Subset does not remain connected to the old, but rather to
	the ultimate parent, the NMRData.  Thus in the example above, although the
	region2 object was obtained from region, it directly connects to data, and
	its lifetime and behavior are in no way dependent on that of region.
	
	The Subset implementation has been carefully optimized to ensure maximal 
	speed, but note that it can require a fair amount of memory.  Unlike their
	parent NMRData objects, Subset objects cannot page their contents in and
	out of memory; the full Subset representation must fit in memory at all times.
	The consequence of this is that 2-D and 3-D Subset objects should behave
	without a problem no matter their size, but a 4-D Subset will likely have to
	be restricted in size substantially from the size of the full dataset.

	\subsection	NMRData_intro_subscriptsummary	Summary of Subscripting Operator Notation

	Here is a summary of the different subscripting operators for NMRData and
	Subset objects:

	\code
	Reference ref = data[ offset ];				
	//	where offset is a size_t data point offset

	Reference ref2 = data( dim0 )( dim1 );		
	//	where dim0 and dim1 are int or DimPosition coordinates

	Reference ref3 = data[ coordinates ];		
	//	where coordinates is a std::vector of int or DimPosition coordinates
	
	Subset sub = data( corner1, corner2 );		
	//	where corner1 and corner2 are std::vector of int or DimPosition coordinates
	\endcode

	Here is a summary of the different subscripting operators for complex 
	Reference and Value objects:

	\code
	float val = complex_ref[ component ];				
	//	where component is an int component number

	Reference ref2 = data( y_is_imag )( x_is_imag );		
	//	where y_is_imag and x_is_imag are bool values, false = real and true = imaginary
	\endcode

	\subsection NMRData_intro_constrefs			Subscripting Operators and Const Data

	Invoking the subscripting operators on datasets marked const will return
	"const Reference" or "const Subset" objects, which behave exactly like the
	Reference and Subset objects described above, accept that it is not possible
	to assign values to them.

	\section	NMRData_intro_operations		Operations on Full Datasets

	\subsection	NMRData_intro_bulkmath			Mathematical Operations on Datasets

	NMRData and Subset objects support the full complement of mathematical
	operators.  We will consider real data first, saving complex data for the
	next section.

	Arithmetic with multiple datasets behaves as expected:

	\code
	data2 = data;		//	sets all values in data2 to the corresponding values in data
	data -= data3;		//	subtracts the corresponding value in data3 from each value in data
	\endcode

	Note that when an operation involves two or more datasets, they must each
	have the same dimensionality and size.

	Operations with scalars uniformly affect all points in the dataset:

	\code
	data *= 2.0F;						//	scale all points by two-fold
	data2 = ( data + 5.0F ) / 3.0F;		//	5 is added to all points, then all points are divided by 3 before assignment
	\endcode

	In like manner, an operation with a Reference or Value applies the value to
	all of the data points:

	\code
	data2 = data[ 16 ];					//	all points in data2 are set to the value of data[ 16 ]
	data = ref * data2;					//	all points in data2 are multiplied by the value of ref before assignment
	data3 = data2 / ( ref + ref2 );		//	the two Reference objects are added to make a Value, which is divided into data2
	\endcode

	Subset objects can also be used in mathematical expressions, provided that
	their sizes and dimensionalities match with those of the other objects
	in the expression:
	
	\code
	region = ( region2 + data ) * 2.0F;
	\endcode

	Expressions are supported using one of the most interesting approaches in
	modern C++, the <em> expression template </em> method.  In evaluating an
	expression like the example immediately above, the naive implementation
	would be to form a new temporary dataset containing the sum of region2 and
	data, and then to multiply all of the values in that temporary dataset by
	two, and then finally to copy the result into region.  Indeed, the most
	trivial implementation would form new temporary copies after each step,
	i.e. after the addition and then again after the multiplication.  I used 
	this kind of simple approach to implement operations with Reference and
	Value objects, but it is certainly not acceptable to form temporary copies
	of full multidimensional NMR datasets as part of such an arithmetic
	operation.

	The expression template mechanism avoids making temporary copies and
	completes the full operation with a single pass through the data, and
	without (slow) runtime function calls.  Instead of computing intermediate 
	results, the operators themselves return objects that describe the full 
	computation, which are collected as a tree data structure of objects owning
	objects.  Formally, function calls down the tree structure are then used to 
	execute the calculation in a single pass and without making temporary copies.

	While this approach as described would be significantly faster than any 
	version producing copies of the data or requiring multiple passes, we gain 
	an additional performance boost from the use of templates.  Provided that
	an optimizing compiler with inlining capabilities is used, the entire tree
	structure for evaluation of the expression can be collapsed to a single
	function.  Thus we additionally avoid function calls during the evaluation
	of expressions.

	In designing this implementation, I have also realized significant
	performance advantages by designing a mechanism that allows the data points
	to be evaluated in whatever order is fastest, given the file and cache 
	organization.

	What is the result of all this?  Performing arithmetic on datasets using
	the arithmetic operators is not only easier than writing code by hand, it
	is also at least an order of magnitude faster.  To return to the example
	given several sections above, <em> DON'T DO THIS! </em>

	\code
	//	assume float variable named "value"
	int ysize = data.GetDimensionSize( 0 ), xsize = data.GetDimensionSize( 1 );
	for( int y = 0; y < ysize; y++ )
		for( int x = 0; x < xsize; x++ )
			data( y )( x ) = value;
	\endcode

	But rather use:

	\code
	data = value;
	\endcode

	which is not only easier to code, but also much faster!

	The only downside to the expression template approach is that it requires
	the most recent generation of C++ compilers.  <em> It also requires that you
	turn on full optimization when compiling. </em>

	\subsection	NMRData_intro_complexbulkmath	Mathematical Operations on Complex or Hypercomplex Datasets

	Expressions involving complex datasets follow the same general patterns as 
	for expressions involving complex Reference and Value objects.  All NMRData,
	Subset, Reference and Value objects in the expression should have the same 
	number of complex components, and operations involving these objects will
	be evaluated independently for each component.  For example:

	\code
	complex_data2 = complex_data + complex_ref;
	\endcode

	assumes that complex_data and complex_data2 have the same size and
	dimensionality, and that all three objects in the expression have the same
	number of complex components.  The expression is then evaluated independently
	for each complex component of each data point in each object.

	As with Reference and Value objects, scalars are applied to all components:

	\code
	complex_data *= 2.0F;		//	affects all components of all points
	\endcode

	It is also possible to do math on a single component.  Unlike with Reference
	and Value objects, this is <em> not </em> done with subscripting operators,
	but rather with the special functions NMRData::SetComponentTo() and
	GetComponent().  As an example, consider:

	\code
	complex_data2.SetComponentTo( 0, GetComponent( 1, complex_data + complex_ref ) );
	\endcode

	This expression sets the real component of complex_data2 to the second
	complex component of the sum of complex_data and complex_ref.  As a second
	example, the following extracts the real component of a complex dataset and
	assigns it to a real dataset:

	\code
	data = GetComponent( 0, complex_data );
	\endcode

	In an expression involving complex data, inside of a GetComponent() call 
	one can freely carry out operations	that affect all components , but all 
	parts of the expression beneath the GetComponent() call should involve only 
	a single component.  Note that real datasets always involve only a single 
	component.

	\warning	At least in the present version of the library, it is not possible
				to mix together expressions involving the subscripting operators
				of Reference and Value objects with operators on full datasets.
				If you are writing an expression that includes full datasets and
				needs to extract a single component from a Reference or Value, 
				use GetComponent() instead of the subscripting operator.

	\subsection	NMRData_intro_applytoall		Applying an Operation to All Data Points

	One can apply a user-specified operation to all of the data points in an
	NMRData or Subset object using the ApplyToAll member functions.  These
	functions accept functors defining the operation to be performed.  They
	are optimized for speed and should be able to carry out the operation more
	quickly than with a hand-coded loop.

	As an example, consider the functor defined above for taking the sine of a
	data value:

	\code
	struct compute_sin
	{
		float operator()( float input )
		{
			return sinf( input );
		}
	};
	\endcode

	This could be applied to all of the points in a dataset using the
	NMRData::ApplyChangeToAll() member function:

	\code
	data.ApplyChangeToAll( compute_sin() );
	\endcode

	With this call, the functor will be applied to all points in the dataset.
	If the data are complex, it will be applied to each component of each
	point.  (Thanks to templates and inlining, in most cases the functor will
	not be called for each data value, but rather the compiler will generate
	a single function that combines the library's traversal code with the
	functor.)

	When one doesn't need to change the data, but merely to access each point,
	for example to collect information about the dataset, the NMRData::ApplyToAll()
	function can be used.  This is usually combined with a functor that preserves
	state between calls.  For example, to determine the two largest data values,
	one could write a functor to record the information:

	\code
	struct get_largest_values
	{
		float val1, val2;											//	largest and second largest

		get_largest_values() : val1( -1E38F ), val2( -1E38F ) {  }	//	constructor: initialize to very small values

		void operator()( float input )
		{
			if( input > val1 )
			{
				val2 = val1;
				val1 = input;
			}
			else if( input > val2 )
				val2 = input;
		}
	};
	\endcode

	This would then be run as:

	\code
	get_largest_values a;
	data.ApplyToAll( a );
	printf( "The largest values are: %f, %f.", a.val1, a.val2 );
	\endcode

	The NMRData::ApplyToComponent() and NMRData::ApplyChangeToComponent()
	functions affect only a single component out of each complex data point:

	\code
	data.ApplyChangeToComponent( compute_sin(), 0 );	//	only affects the real component
	\endcode

	In all of the above cases, it is assumed that the functor can be applied to
	the data points in any order.  When it is instead necessary that it be
	applied in a known order, one can use the functions marked "Linear" to apply
	the functor in the order of data point offsets.  For example, to initialize
	each data point to its data point offset number, one could write a functor:

	\code
	struct init_point
	{
		int next_offset;

		init_point() : next_offset( 0 ) {  }		//	constructor:  initialize to zero

		float operator()( float input )				//	we can actually ignore the input
		{
			return ( float ) next_offset++;
		}
	}
	\endcode

	This is then applied using:

	\code
	data.ApplyChangeToAllLinear( init_point() );
	\endcode

	Whenever possible, one should use the versions that apply the functor to the
	data in an arbitrary order, as these functions will often be much faster.

	An exactly equivalent set of functions is available for Subset objects.

	\subsection	NMRData_intro_stats				Getting Statistics and Noise Level Estimates on a Dataset

	The NMRData and Subset classes provide functions for calculating certain
	basic statistics on datasets.  The NMRData::CalculateStats() function
	determines the minimum, maximum, mean and standard deviation of the data,
	storing these values in the Min, Max, Mean and StdDev member variables,
	respectively.  This function also calculates an estimate of the noise
	level, which is placed in the NoiseEstimate member variable.  Two
	additional member variables, GotStats and GotNoiseEstimate, are set to
	true after the statistics have been calculated and are available.

	The NMRData::CalculateStats() member function must make two full passes
	through the full data, which can be a slow process for a large dataset.
	If one only needs an estimate of the noise level, the 
	NMRData::CalculateNoiseEstimate() function is a better choice.  This
	function estimates the standard deviation of the noise using a variant of
	the NMRPipe/NMRDraw noise estimation algorithm.  After it has been called,
	the GotNoiseEstimate member variable will be set to true.

	Note that for writeable data, the statistics are only guaranteed to be
	correct between the time when they are calculated, and the next change to
	the data.  The library does not attempt to keep the statistics up to date
	as changes are made, but the previously calculated statistics will continue
	to be available through the duration of the object's lifetime.  To refresh
	the statistics, simply call either NMRData::CalculateNoiseEstimate() or
	NMRData::CalculateStats() to recalculate the statistics according to
	the current data values.

	An exactly equivalent set of function is available for Subset objects.

	\section	NMRData_intro_lowlevel			Low-Level Access to Data

	The high level interfaces described in this document are the preferred way
	to work with data using the library, and they have been carefully optimized
	to achieve excellent performance.  Unfortunately, however, there are cases 
	where even greater speed is needed, and for those cases the only answer is 
	to write dedicated low-level code operating directly on data values.  To
	support those cases, I have provided some low-level facilities allowing
	access to data in more primitive formats.  These mechanisms provide copies
	of the data, which are not managed by the library, and can be freely 
	manipulated.  When one is finished, it is possible to copy the changed 
	values back into library-managed storage.

	The function NMRData::GetDataCopyValarray() returns a std::valarray object
	containing a complete copy of the dataset.  This copy is organized in a
	linear fashion according to data point offsets and, for complex data, 
	component numbers.  With complex data, all of the complex components for a
	given data point are stored contiguously in the order of the component
	numbers.  The std::valarray is the preferred C++ mechanism for
	high-speed computation involving arrays of values, and it is intended
	that C++ std::valarray implementations will use all possible optimizations,
	including any dedicated vector computation hardware that may be available
	on your platform.  A data copy returned by this function does not maintain 
	a connection to the library, and any changes made to it will not be recorded 
	in the original dataset unless one calls NMRData::SetDataFromCopyValarray(), 
	which will copy the data values back.

	One can also obtain a copy in a standard C-style array of floats, using
	NMRData::GetDataCopyPtr().  This function returns a pointer to a
	heap-allocated array containing a full copy of the data.  The calling
	code is assumed to take responsibility for deallocation of the data block.
	The copy is organized as described for std::valarray copies.  One can copy
	changed values back into the dataset using NMRData::SetDataFromCopyPtr().

	These functions return complete copies of the full dataset.  Needless to say,
	there must be enough memory available for this to be successful.
	The library will attempt to create a large enough block to store the copy,
	and any resulting out-of-memory exception from the system will be passed 
	along to the client code.

	If one would like a copy of only a portion of the data, the Subset object
	has equivalent functions that return std::valarray or float pointer copies
	of the Subset contents.  Thus one should create a Subset representing the
	desired portion of the data, and then obtain a low-level copy.

	\section	NMRData_intro_misc				Miscellaneous Issues

	\subsection	NMRData_intro_stl				STL Support

	The NMRData and Subset objects provide random-access STL iterators, as well
	as the standard STL begin(), end(), rbegin() and rend() functions, allowing
	one to use NMRData and Subset objects with some of the standard C++
	algorithms.  Of course, any algorithm requiring insert() or erase()
	capabilities will not function with these objects, but those designed for
	use with std::vector should work fine with NMRData and Subset objects.  Note
	that when there is a dedicated library mechanism available, it is usually
	considerably faster to use that mechanism than the STL algorithm.  For example,
	using the assignment operator will almost always be faster than std::copy().
	But STL algorithms can be useful in other situations, e.g. if one needs to
	sort the data.

	Almost all of the library interface routines use standard C++ containers,
	especially the std::vector and the std::string.  For many functions,
	C-style interfaces accepting or providing pointers to data are also
	available.  In particular, all routines involving strings have versions
	that accept or return C-style strings, as well versions using std::string.

	Arrays returned from this library are always heap-allocated, and their
	deallocation is always the responsibility of the calling code.  The only
	exception to this is NMRData::GetFileTypeString(), which returns a static
	string that may not be deallocated.

	\subsection NMRData_intro_threads			Using the Library in Multithreaded Environments

	This library may safely be used in a multithreaded environment, provided a
	few rules are followed.  There are no global library objects, so there is
	no need to synchronize all library accesses between threads.  However, any
	particular NMRData object should be localized to a single thread, or if
	shared between threads, all access to that object should be synchronized.  
	Likewise, all auxiliary objects obtained from an NMRData, including 
	Reference and Subset objects and all DimPosition objects and iterators, 
	should be placed under the same synchronization control as the parent 
	NMRData.

	\subsection NMRData_intro_memory			Memory Allocation Issues and Size Limits

	When working with very large datasets, such as 4-D spectra, one may
	encounter problems pertaining to the limited availability of physical
	memory.  It is always advisable to avoid operating this library using virtual
	memory; the library has its own optimized paging mechanisms which are
	more efficient for the file types in use.  The default cache size is
	probably appropriate for most cases, but in the situation that you have
	a very large amount of physical memory available, it may be worthwhile
	to increase the cache size, thereby reducing the need to page data in and
	out of memory.

	On Linux systems, some care should be taken with setting a large cache limit.
	By default, the Linux kernel uses a memory allocation technique that grants
	all memory requests, and then kills off processes arbitrarily when memory has
	been exhausted.  (It is beyond me how they can consider such a strategy
	acceptable - it makes it impossible to write programs that fail gracefully.)
	If your software sets the cache size to a very high limit and then opens a
	very large dataset, it is easily possible to invoke the out-of-memory killer,
	which will probably kill your process.  The answer is either to set a lower
	cache size or to change the kernel settings to disable the overcommitment of
	memory.  There is no way in Linux to find out the actual memory available, or
	to scale requests automatically to suit the memory available, without manual
	input from the user.

	Note that there are a few library API calls that can attempt to allocate a
	fairly large amount of memory.  The most significant of these is the creation
	of a Subset object.  If these calls fail, they will restore the original 
	state of the parent object, and will pass along the out-of-memory exception.
	While such exceptions will not be generated on most Linux systems, which
	usually grants all memory requests, they can be encountered under Windows.

*/

	//	Structures for various types of file headers

	//	N.B.:  These structures assume that "char" is 1 byte, "int" is 4 bytes, "short" is
	//	2 bytes and "float" is 4 bytes, and that there is no padding between elements.
	//	If these conditions aren't true on your platform, you may have to adjust the code!

	struct UCSF_FILE_HEADER
	{
		char id[ 10 ];
		char NumOfDimensions;
		char IsComplex;
		char unused1;
		char FormatVersion;
		char unused[ 166 ];
	};

	struct UCSF_DIM_HEADER
	{
		char Label[ 7 ];
		char unused1;
		int Size;
		int unused2;
		int SubmatrixSize;
		float sf;
		float sw;
		float CenterPPM;
		char unused[ 96 ];
	};

	struct NVDIMHEADER
	{
		int size;
		int blksize;
		int nblks;
		int offblk;
		int blkmask;
		int offpt;
		float sf;
		float sw;
		float refpt;
		float ref;
		int refunits;
		float foldup;
		float folddown;
		char label[ 16 ];
		int complex;
		int spare2;
		int spare3;
		int spare4;
		int spare5;
		int spare6;
		int spare7;
		int spare8;
		int spare9;
		int spare10;
		int spare11;
		int spare12;
		int spare13;
		int spare14;
		int spare15;
	};

	struct NVHEADER
	{
		int magic;
		int spare1;
		int spare2;
		int fheadersz;
		int bheadersz;
		int blkelems;
		int ndim;
		float temperature;
		char sequence[ 32 ];
		char comment[ 160 ];
		int month;
		int day;
		int year;
		int posneg;
		float lvl;
		float scale;
		int rdims;
		int spare[ 193 ];
		NVDIMHEADER dim[ 8 ];
	};

	struct VNMRFileHeader
	{
		long nblocks;
		long ntraces;
		long np;
		long ebytes;
		long tbytes;
		long bbytes;
		short vers_id;
		short status;
		long nbheaders;
	};

	struct VNMRBlockHeader
	{
		short scale;
		short status;
		short index;
		short mode;
		long ctcount;
		float lpval;
		float rpval;
		float lvl;
		float tlt;
	};


	//	Struct and class definitions for the NMRData class library
	//	See the library documentation for explanations of all classes and members


	struct OpenDataPtr
	{
		bool Writeable;
		bool Contiguous;
		int *Sizes;
		size_t StartOffset;
		int *OriginCoordinates;
		size_t Size;
		float *DataPtr;
	};


	struct DataPage
	{
		size_t Index;
		float *DataPtr;
		DataPage *Prev;
		DataPage *Next;
		bool Dirty;
		std::set< SubsetData * > RegisteredSubsets;
	};


	struct SubsetPtrTableEntry
	{
		size_t ParentOffset;
		float *DataPtr;
		SubsetPageTableEntry *PageTableEntry;
		SubsetPtrTableEntry *NextEntryForPage;
	};


	struct SubsetPageTableEntry
	{
		size_t PageIndex;
		DataPage *Page;
		bool Valid;
		SubsetPtrTableEntry *FirstEntryForPage;
		SubsetPageTableEntry *NextListEntry;
	};


	namespace Exceptions
	{
		class BaseException
		{
			public:
				BaseException( void );
				virtual ~BaseException( void );

				virtual const char *what( void ) const;
		};

		class BadArgument : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class FileError : public BaseException
		{
			public:
				virtual const char *what( void ) const = 0;
		};

		class DataAreOpen : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DataNotOpen : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class NullPointerArg : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadDimNumber : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadCacheSize : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadDataPtrRequestCoord : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadDataPtrRequestOffset : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadDataPtrRequestSize : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class DataPtrRequestTooLarge : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadDataPtr : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadOffset : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadSize : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadCoordinates : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadSubmatrixNumber : public BadArgument
		{
			public:
				virtual const char *what( void ) const;
		};

		class FileTypeNotRecognized : public FileError
		{
			public:
				FileTypeNotRecognized( void );
				FileTypeNotRecognized( const char *Filename );
				FileTypeNotRecognized( const FileTypeNotRecognized &Source );
				virtual ~FileTypeNotRecognized( void );

				virtual const char *what( void ) const;
				virtual FileTypeNotRecognized &operator=( const FileTypeNotRecognized &Source );

			private:
				char *msg;
		};

		class ComplexNotSupported : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};
        
		class BadXEASYFilename : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class DimensionalityNotSet : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class CantOpenFile : public FileError
		{
			public:
				CantOpenFile( void );
				CantOpenFile( const char *Filename );
				CantOpenFile( const CantOpenFile &Source );
				virtual ~CantOpenFile( void );

				virtual const char *what( void ) const;
				virtual CantOpenFile &operator=( const CantOpenFile &Source );

			private:
				char *msg;
		};

		class BadDimSize : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class NoParamsAvailable : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DatasetTooLarge : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class CantReadFile : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class CantWriteFile : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class MemOnlyRequiresBaseClass : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class CantGetTmpFile : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class SubmatrixParamsNotSet : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class CouldntCalculateSubmatrixSizes : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class NoFileOpen : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class PipeFileListError : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class TooManyDims : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadFileHeader : public FileError
		{
			public:
				virtual const char *what( void ) const;
		};

		class PipeStreamFormatNotSupported : public FileError
		{
        public:
            virtual const char *what( void ) const;
		};
        
		class BadComplexConfig : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class ComponentsDontMatch : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DimsDontMatch : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class ComplexConfigDoesntMatch : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadComponentIndex : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadComponentSelector : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DimNotComplex : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadChainedSelector : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class ValueIsEmpty : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class NotCompatible : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class ChainedSelectionError : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class ConversionRequiresSelection : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class RefIsAmbig : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class PosOutOfRange : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DimNotFD : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DimNotTD : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class RefHasWrongParent : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};
	
		class RefIsOutsideSubset : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};
	
		class SubsetRequestTooLarge : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};
	
		class BadSubsetCorner : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};
	
		class BadSubsetSize : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};
	
		class PtOutsideSubset : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class WriteCallOnConstData : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class DataCopyRequestTooLarge : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadSubsetString : public BaseException
		{
			public:
				virtual const char *what( void ) const;
		};

		class BadSubsetString_DimID : public BaseException
		{
			public:
				BadSubsetString_DimID( std::string Param ) : param( Param ) {  }
				virtual const char *what( void ) const;

			private:
				std::string param;
		};
	}


	class Value
	{
		friend class Reference;

		public:
			Value( void );
			explicit Value( float RealVal );
			Value( std::vector< bool > ComplexByDim, const float *ComplexVal, int SelectorDim = 0, int SelectorIndex = 0 );
			Value( const Value &Source );
			~Value( void );

			Value &operator=( const Value &Source );
			Value &operator=( float RealVal );
			Value &operator=( const Reference &Source );
			bool operator==( const Value &Operand ) const;
			bool operator==( float RealVal ) const;
			bool operator!=( const Value &Operand ) const;
			bool operator!=( float RealVal ) const;
			bool operator<( const Value &Operand ) const;
			bool operator<( float RealVal ) const;
			bool operator<=( const Value &Operand ) const;
			bool operator<=( float RealVal ) const;
			bool operator>( const Value &Operand ) const;
			bool operator>( float RealVal ) const;
			bool operator>=( const Value &Operand ) const;
			bool operator>=( float RealVal ) const;
			Value &operator+=( const Reference &Operand );
			Value &operator+=( const Value &Operand );
			Value &operator+=( float Scalar );
			Value &operator-=( const Reference &Operand );
			Value &operator-=( const Value &Operand );
			Value &operator-=( float Scalar );
			Value &operator*=( const Reference &Operand );
			Value &operator*=( const Value &Operand );
			Value &operator*=( float Scalar );
			Value &operator/=( const Reference &Operand );
			Value &operator/=( const Value &Operand );
			Value &operator/=( float Scalar );
			float &operator[]( int Component );
			const float &operator[]( int Component ) const;
			float &operator[]( std::vector< bool > Selector );
			const float &operator[]( std::vector< bool > Selector ) const;
			Value operator[]( bool Selector );
			const Value operator[]( bool Selector ) const;
			operator float &( void );
			operator const float &( void ) const;

			template< typename Function > Function ApplyToComponents( Function F );
			bool IsCompatible( Value &Operand ) const;
			bool IsComplex( void ) const;
			bool IsEmpty( void ) const;
			std::vector< bool > GetComplexByDim( void ) const;
			int GetNumOfComponents( void ) const;
			int GetNumOfDims( void ) const;
			float GetRealValue( void ) const;
			float GetSelectedVal( void ) const;
			bool Selected( void ) const;

		private:
			float realval;
			float *complexval;
			int dims;
			int components;
			bool *complexbydim;
			mutable int selectordim;
			mutable int selectorindex;
	};

	
	inline bool operator<( float Scalar, const Value &Operand );
	inline bool operator<=( float Scalar, const Value &Operand );
	inline bool operator>( float Scalar, const Value &Operand );
	inline bool operator>=( float Scalar, const Value &Operand );
	inline Value operator+( const Value &Operand1, const Value &Operand2 );
	inline Value operator+( float Scalar, const Value &Operand );
	inline Value operator+( const Value &Operand, float Scalar );
	inline Value operator-( const Value &Operand1, const Value &Operand2 );
	inline Value operator-( float Scalar, const Value &Operand );
	inline Value operator-( const Value &Operand, float Scalar );
	inline Value operator*( const Value &Operand1, const Value &Operand2 );
	inline Value operator*( float Scalar, const Value &Operand );
	inline Value operator*( const Value &Operand, float Scalar );
	inline Value operator/( const Value &Operand1, const Value &Operand2 );
	inline Value operator/( float Scalar, const Value &Operand );
	inline Value operator/( const Value &Operand, float Scalar );

	enum UnitsType
	{
		pts,
		Hz,
		ppm,
		sec
	};

	template< typename ParentClass >
	class DimPosition
	{
		//	N.B.:	The right answer here should be:
		//		friend ParentClass;
		//	However, I was shocked to find that this is prohibited by the C++
		//	standard due to obscure and seemingly unrelated technical issues.
		//	This is a serious omission that hopefully will be corrected in a
		//	future C++ revision.
		friend class NMRData;
		friend class Subset;

		friend class Reference;

		public:
			DimPosition( const DimPosition< ParentClass > &Source );

			template< typename ParentClass2 > DimPosition &operator=( const DimPosition< ParentClass2 > &Source );
			template< typename T > DimPosition< ParentClass > &operator=( T Value );
			template< typename ParentClass2 > bool operator==( const DimPosition< ParentClass2 > &Operand ) const;
			template< typename T > bool operator==( T Value ) const;
			template< typename ParentClass2 > bool operator!=( const DimPosition< ParentClass2 > &Operand ) const;
			template< typename T > bool operator!=( T Value ) const;
			template< typename ParentClass2 > bool operator<( const DimPosition< ParentClass2 > &Operand ) const;
			template< typename T > bool operator<( T Value ) const;
			template< typename ParentClass2 > bool operator<=( const DimPosition< ParentClass2 > &Operand ) const;
			template< typename T > bool operator<=( T Value ) const;
			template< typename ParentClass2 > bool operator>( const DimPosition< ParentClass2 > &Operand ) const;
			template< typename T > bool operator>( T Value ) const;
			template< typename ParentClass2 > bool operator>=( const DimPosition< ParentClass2 > &Operand ) const;
			template< typename T > bool operator>=( T Value ) const;
			template< typename ParentClass2 > DimPosition< ParentClass > &operator+=( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > &operator+=( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > &operator-=( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > &operator-=( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > &operator*=( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > &operator*=( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > &operator/=( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > &operator/=( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > operator+( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > operator+( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > operator-( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > operator-( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > operator*( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > operator*( T Value );
			template< typename ParentClass2 > DimPosition< ParentClass > operator/( const DimPosition< ParentClass2 > &Operand );
			template< typename T > DimPosition< ParentClass > operator/( T Value );
			DimPosition< ParentClass > &operator++( void );
			DimPosition< ParentClass > operator++( int );
			DimPosition< ParentClass > &operator--( void );
			DimPosition< ParentClass > operator--( int );

			void Attach( const ParentClass *NewParent, int NewDim );
			void AttachToSame( const DimPosition< ParentClass > &Source );
			template< typename T > bool CheckRange( T Value ) const;
			template< typename ParentClass2 > bool CheckRange( const DimPosition< ParentClass2 > &Operand ) const;
			const ParentClass *GetParent( void ) const;
			float GetCenterPPM( void ) const;
			float GetDownfieldHz( void ) const;
			float GetDownfieldPPM( void ) const;
			int GetDimNumber( void ) const;
			float GetDwellTime( void ) const;
			char *GetLabel( void ) const;
			std::string GetLabelStr( void ) const;
			float GetMinEvolutionTime( void ) const;
			float GetMaxEvolutionTime( void ) const;
			float GetPosHz( void ) const;
			float GetPosPPM( void ) const;
			int GetPosPts( void ) const;
			float GetPosSec( void ) const;
			float GetResolutionHz( void ) const;
			float GetResolutionPPM( void ) const;
			float GetSF( void ) const;
			int GetSize( void ) const;
			float GetSW( void ) const;
			UnitsType GetUnits( void ) const;
			float GetUpfieldHz( void ) const;
			float GetUpfieldPPM( void ) const;
			bool InRange( void ) const;
			bool IsTimeDomain( void ) const;
			void MoveToCenter( void );
			void MoveToDownfieldEnd( void );
			void MoveToFIDStart( void );
			void MoveToFIDEnd( void );
			void MoveToUpfieldEnd( void );
			template< typename T > void SetPos( T Value );
			void SetPosHz( float HzPos );
			void SetPosPPM( float PPMPos );
			void SetPosPts( int PtsPos );
			void SetPosSec( float SecPos );
			void SetUnits( UnitsType Units );

		private:
			DimPosition( void );
			DimPosition( const ParentClass *Parent, int Dim, int pos, float fpos, UnitsType Units );

			const ParentClass *parent;
			int dim;
			int pos;
			float fpos;
			UnitsType units;
	};

	template<> template< typename ParentClass2 > inline bool DimPosition< NMRData >::CheckRange( const DimPosition< ParentClass2 > &Operand ) const;
	template<> template< typename T > inline bool DimPosition< NMRData >::CheckRange( T Value ) const;
	template<> inline float DimPosition< NMRData >::GetCenterPPM( void ) const;
	template<> inline float DimPosition< NMRData >::GetDownfieldHz( void ) const;
	template<> inline float DimPosition< NMRData >::GetDownfieldPPM( void ) const;
	template<> inline float DimPosition< NMRData >::GetDwellTime( void ) const;
	template<> inline float DimPosition< NMRData >::GetMaxEvolutionTime( void ) const;
	template<> inline float DimPosition< NMRData >::GetPosHz( void ) const;
	template<> inline float DimPosition< NMRData >::GetPosPPM( void ) const;
	template<> inline float DimPosition< NMRData >::GetPosSec( void ) const;
	template<> inline float DimPosition< NMRData >::GetResolutionHz( void ) const;
	template<> inline float DimPosition< NMRData >::GetResolutionPPM( void ) const;
	template<> inline float DimPosition< NMRData >::GetSF( void ) const;
	template<> inline int DimPosition< NMRData >::GetSize( void ) const;
	template<> inline float DimPosition< NMRData >::GetSW( void ) const;
	template<> inline float DimPosition< NMRData >::GetUpfieldHz( void ) const;
	template<> inline float DimPosition< NMRData >::GetUpfieldPPM( void ) const;
	template<> inline bool DimPosition< NMRData >::InRange( void ) const;
	template<> inline bool DimPosition< NMRData >::IsTimeDomain( void ) const;
	template<> inline void DimPosition< NMRData >::MoveToCenter( void );
	template<> inline void DimPosition< NMRData >::MoveToFIDEnd( void );
	template<> inline void DimPosition< NMRData >::MoveToUpfieldEnd( void );
	template<> template< typename T > inline void DimPosition< NMRData >::SetPos( T Value );
	template<> inline void DimPosition< NMRData >::SetPosHz( float HzPos );
	template<> inline void DimPosition< NMRData >::SetPosPPM( float PPMPos );
	template<> inline void DimPosition< NMRData >::SetPosPts( int PtsPos );
	template<> inline void DimPosition< NMRData >::SetPosSec( float SecPos );


	class Reference
	{
		friend class NMRData;
		friend class Subset;

		public:
			Reference( const Reference &Source );

			Reference &operator=( const Reference &Source );
			Reference &operator=( const Value &Source );
			Reference &operator=( float RealVal );
			bool operator==( const Reference &Operand ) const;
			bool operator==( const Value &Operand ) const;
			bool operator==( float RealValue ) const;
			bool operator!=( const Reference &Operand ) const;
			bool operator!=( const Value &Operand ) const;
			bool operator!=( float RealValue ) const;
			bool operator<( const Reference &Operand ) const;
			bool operator<( const Value &Operand ) const;
			bool operator<( float RealValue ) const;
			bool operator<=( const Reference &Operand ) const;
			bool operator<=( const Value &Operand ) const;
			bool operator<=( float RealValue ) const;
			bool operator>( const Reference &Operand ) const;
			bool operator>( const Value &Operand ) const;
			bool operator>( float RealValue ) const;
			bool operator>=( const Reference &Operand ) const;
			bool operator>=( const Value &Operand ) const;
			bool operator>=( float RealValue ) const;
			Reference &operator+=( const Reference &Operand );
			Reference &operator+=( const Value &Operand );
			Reference &operator+=( float Scalar );
			Reference &operator-=( const Reference &Operand );
			Reference &operator-=( const Value &Operand );
			Reference &operator-=( float Scalar );
			Reference &operator*=( const Reference &Operand );
			Reference &operator*=( const Value &Operand );
			Reference &operator*=( float Scalar );
			Reference &operator/=( const Reference &Operand );
			Reference &operator/=( const Value &Operand );
			Reference &operator/=( float Scalar );
			float &operator[]( int Component );
			const float &operator[]( int Component ) const;
			float &operator[]( std::vector< bool > Selector );
			const float &operator[]( std::vector< bool > Selector ) const;
			Reference operator[]( bool Selector );
			Reference const operator[]( bool Selector ) const;
			Reference operator()( int Position );
			Reference const operator()( int Position ) const;
			Reference operator()( const DimPosition< NMRData > &Position );
			Reference operator()( const DimPosition< Subset > &Position );
			Reference const operator()( const DimPosition< NMRData > &Position ) const;
			Reference const operator()( const DimPosition< Subset > &Position ) const;
			operator Value( void ) const;
			//operator float( void ) const;
			operator float &( void );
			operator const float &( void ) const;

			template< typename Function > Function ApplyToComponents( Function F );
			void Assign( const Reference &Source );
			Value ConvertToValue( void ) const;
			bool IsCompatible( const Reference &Operand ) const;
			bool IsCompatible( const Value &Operand ) const;
			bool IsComplex( void ) const;
			std::vector< bool > GetComplexByDim( void ) const;
			std::vector< DimPosition< NMRData > > GetCoordinates( void ) const;
			size_t GetOffset( void ) const;
			const NMRData *GetParent( void ) const;
			float GetRealValue( void ) const;
			Value GetValue( void ) const;
			bool Selected( void ) const;
			float GetSelectedVal( void ) const;

		private:
			Reference( void );
			Reference( size_t Offset, DataPage *Page, float *Ptr, bool ConstState, const NMRData *Parent );
			Reference( DimPosition< NMRData > FirstDimPos, bool ConstState, const NMRData *Parent );
			Reference( std::vector< DimPosition< NMRData > > DimPosSet, std::queue< int > DimsToSet, bool ConstState, const NMRData *Parent );

			void SetSelectedPos( void ) const;
			void update( void ) const;

			mutable float *value;
			mutable DataPage *page;
			const NMRData *parent;
			mutable size_t offset;
			mutable size_t pageindex;
			bool isconst;
			mutable bool ambig;
			mutable int cselectordim;
			mutable int cselectorindex;
			mutable int pselectordim;
			mutable int pselectordimmax;
			mutable std::vector< DimPosition< NMRData > > pselectorcoord;
			mutable std::vector< DimPosition< NMRData > > cachedcoord;
			mutable bool subset;
			mutable std::queue< int > dims_to_set;
	};


	inline bool operator<( const Value &Operand1, const Reference &Operand2 );
	inline bool operator<( float RealVal, const Reference &Operand );
	inline bool operator<=( const Value &Operand1, const Reference &Operand2 );
	inline bool operator<=( float RealVal, const Reference &Operand );
	inline bool operator>( const Value &Operand1, const Reference &Operand2 );
	inline bool operator>( float RealVal, const Reference &Operand );
	inline bool operator>=( const Value &Operand1, const Reference &Operand2 );
	inline bool operator>=( float RealVal, const Reference &Operand );
	inline Value operator+( const Reference &Operand1, const Reference &Operand2 );
	inline Value operator+( const Value &Operand1, const Reference &Operand2 );
	inline Value operator+( float RealVal, const Reference &Operand );
	inline Value operator+( const Reference &Operand1, const Value &Operand2 );
	inline Value operator+( const Reference &Operand1, float RealVal );
	inline Value operator-( const Reference &Operand1, const Reference &Operand2 );
	inline Value operator-( const Value &Operand1, const Reference &Operand2 );
	inline Value operator-( float RealVal, const Reference &Operand );
	inline Value operator-( const Reference &Operand1, const Value &Operand2 );
	inline Value operator-( const Reference &Operand1, float Scalar );
	inline Value operator*( const Reference &Operand1, const Reference &Operand2 );
	inline Value operator*( const Value &Operand1, const Reference &Operand2 );
	inline Value operator*( float RealVal, const Reference &Operand );
	inline Value operator*( const Reference &Operand1, const Value &Operand2 );
	inline Value operator*( const Reference &Operand1, float Scalar );
	inline Value operator/( const Reference &Operand1, const Reference &Operand2 );
	inline Value operator/( const Value &Operand1, const Reference &Operand2 );
	inline Value operator/( float RealVal, const Reference &Operand );
	inline Value operator/( const Reference &Operand1, const Value &Operand2 );
	inline Value operator/( const Reference &Operand1, float Scalar );


	class Subset
	{
		public:
			friend class NMRData;
			friend class Reference;
			friend class NMRDataMathSubset;

			class iterator : public std::iterator< std::random_access_iterator_tag, Value, ptrdiff_t, Reference *, Reference >
			{
				friend class Subset;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					iterator( const iterator &Source );
					iterator &operator=( const iterator &Source );
					bool operator==( const iterator &x ) const;
					bool operator!=( const iterator &x ) const;
					Reference &operator*( void ) const;
					Reference *operator->( void ) const;
					iterator &operator++( void );	//prefix ++
					iterator operator++( int );		//postfix ++
					iterator &operator--( void );	//prefix --
					iterator operator--( int );		//postfix --
					iterator &operator+=( size_type n );
					iterator operator+( size_type n ) const;
					iterator &operator-=( size_type n );
					iterator operator-( size_type n ) const;
					difference_type operator-( const iterator &a ) const;
					Reference &operator[]( size_type n ) const;
					bool operator<( const iterator &x ) const;
					bool operator<=( const iterator &x ) const;
					bool operator>( const iterator &x ) const;
					bool operator>=( const iterator &x ) const;

				private:
					iterator( void );
					iterator( Subset *Parent, size_t Offset, bool PastEnd );
						
					size_t offset;
					mutable Reference current;
					bool past_end;
					mutable bool is_current;
					Subset *parent;
			};

			class const_iterator : public std::iterator< std::random_access_iterator_tag, Value, ptrdiff_t, Reference *, Reference >
			{
				friend class Subset;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					const_iterator( const const_iterator &Source );
					const_iterator &operator=( const const_iterator &Source );
					bool operator==( const const_iterator &x ) const;
					bool operator!=( const const_iterator &x ) const;
					const Reference &operator*( void ) const;
					const Reference *operator->( void ) const;
					const_iterator &operator++( void );	//prefix ++
					const_iterator operator++( int );		//postfix ++
					const_iterator &operator--( void );	//prefix --
					const_iterator operator--( int );		//postfix --
					const_iterator &operator+=( size_type n );
					const_iterator operator+( size_type n ) const;
					const_iterator &operator-=( size_type n );
					const_iterator operator-( size_type n ) const;
					difference_type operator-( const const_iterator &a ) const;
					const Reference &operator[]( size_type n ) const;
					bool operator<( const const_iterator &x ) const;
					bool operator<=( const const_iterator &x ) const;
					bool operator>( const const_iterator &x ) const;
					bool operator>=( const const_iterator &x ) const;

				private:
					const_iterator( void );
					const_iterator( const Subset *Parent, size_t Offset, bool PastEnd );
						
					size_t offset;
					mutable Reference current;
					bool past_end;
					mutable bool is_current;
					const Subset *parent;
			};


			Subset( const Subset &Source );
			~Subset( void );

			int GetNumOfDims( void ) const;
			int GetDimensionSize( int DimNum ) const;
			int *GetDimensionSizes( void ) const;
			std::vector< int > GetDimensionSizeVector( void ) const;
			size_t GetFullSize( void ) const;
			bool IsComplexAvailable( void ) const;
			int GetNumOfComponents( void ) const;
			bool IsTimeDomainAvailable( void ) const;
			bool *IsComplexAvailableByDim( void ) const;
			std::vector< bool > IsComplexAvailableByDimVector( void ) const;
			bool *IsTimeDomainAvailableByDim( void ) const;
			std::vector< bool > IsTDAvailableByDimVector( void ) const;
			char *GetDimensionLabel( int DimNum ) const;
			std::string GetDimensionLabelStr( int DimNum ) const;
			void GetDimensionCalibration( int DimNum, float &SW, float &SF, float &CenterPPM ) const;
			void GetFullDimCalibration( int DimNum, float &SW, float &SF, float &CenterPPM, float &OriginPPM, float &WidthPPM ) const;
			float GetDimensionSW( int DimNum ) const;
			float GetDimensionSF( int DimNum ) const;
			float GetDimensionCenterPPM( int DimNum ) const;
			float GetDimensionCenterHz( int DimNum ) const;
			float GetDimensionStartTime( int DimNum ) const;
			float GetDimensionEndTime( int DimNum ) const;
			bool GetDimensionComplexState( int DimNum ) const;
			bool GetDimensionTD( int DimNum ) const;
			DimPosition< Subset > GetDimPosObj( int DimNum ) const;
			std::vector< DimPosition< Subset > > GetDimPosSet( void ) const;
			Value GetBlankValue( void ) const;
			char *GetFilename( void ) const;
			std::string GetFilenameStr( void ) const;
			char *GetDatasetName( void ) const;
			std::string GetDatasetNameStr( void ) const;

			std::valarray< float > GetDataCopyValarray( void ) const;
			float *GetDataCopyPtr( float *Destination = 0 ) const;
			void SetDataFromCopyValarray( std::valarray< float > &Input );
			void SetDataFromCopyPtr( const float *Input );
			float GetInterpolatedValue( const float *Coordinates ) const;
			float GetInterpolatedValue( std::vector< float > Coordinates ) const;
			iterator GetIteratorStart( void );
			const_iterator GetIteratorStart( void ) const;
			iterator GetIteratorAt( size_t Offset );
			const_iterator GetIteratorAt( size_t Offset ) const;
			iterator GetIteratorForReference( const Reference &Ref );
			const_iterator GetIteratorForReference( const Reference &Ref ) const;
			iterator GetIteratorEnd( void );
			const_iterator GetIteratorEnd( void ) const;
			void Flush( void );
			void CalculateStats( void ) const;
			void CalculateNoiseEstimate( void ) const;

			Reference operator[]( size_t Offset );
			const Reference operator[]( size_t Offset ) const;
			Reference operator[]( std::vector< int > Coordinates );
			const Reference operator[]( const std::vector< int > Coordinates ) const;
			Reference operator[]( std::vector< DimPosition< Subset > > Coordinates );
			const Reference operator[]( const std::vector< DimPosition< Subset > > Coordinates ) const;
			Subset operator()( std::vector< int > Corner1, std::vector< int > Corner2 );
			const Subset operator()( std::vector< int > Corner1, std::vector< int > Corner2 ) const;
			Subset operator()( std::vector< DimPosition< Subset > > Corner1, std::vector< DimPosition< Subset > > Corner2 );
			const Subset operator()( std::vector< DimPosition< Subset > > Corner1, std::vector< DimPosition< Subset > > Corner2 ) const;
			Reference operator()( int Coordinate );
			const Reference operator()( int Coordinate ) const;
			Reference operator()( DimPosition< Subset > Coordinate );
			const Reference operator()( const DimPosition< Subset > Coordinate ) const;

			Subset &operator=( float Scalar );
			Subset &operator=( const Value &Scalar );
			Subset &operator=( const Reference &Scalar );
			Subset &operator=( const Subset &Operand );
			Subset &operator=( const NMRData &Operand );
			template< typename T > Subset &operator=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > Subset &operator=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );

			void operator+=( float Scalar );
			void operator+=( const Value &Scalar );
			void operator+=( const Reference &Scalar );
			void operator+=( const Subset &Operand );
			void operator+=( const NMRData &Operand );
			template< typename T > void operator+=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator+=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );
			void operator-=( float Scalar );
			void operator-=( const Value &Scalar );
			void operator-=( const Reference &Scalar );
			void operator-=( const Subset &Operand );
			void operator-=( const NMRData &Operand );
			template< typename T > void operator-=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator-=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );
			void operator*=( float Scalar );
			void operator*=( const Value &Scalar );
			void operator*=( const Reference &Scalar );
			void operator*=( const Subset &Operand );
			void operator*=( const NMRData &Operand );
			template< typename T > void operator*=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator*=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );
			void operator/=( float Scalar );
			void operator/=( const Value &Scalar );
			void operator/=( const Reference &Scalar );
			void operator/=( const Subset &Operand );
			void operator/=( const NMRData &Operand );
			template< typename T > void operator/=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator/=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );

			void SetComponentTo( int Component, float Scalar );
			void SetComponentTo( int Component, const Value &Scalar );
			void SetComponentTo( int Component, const Reference &Scalar );
			void SetComponentTo( int Component, const Subset &Operand );
			void SetComponentTo( int Component, const NMRData &Operand );
			template< typename T > void SetComponentTo( int Component, const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void SetComponentTo( int Component, const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );

			template< typename T > void ApplyChangeToAll( T &Operation );
			template< typename T > void ApplyToAll( T &Operation ) const;
			template< typename T > void ApplyChangeToComponent( T &Operation, int Component );
			template< typename T > void ApplyToComponent( T &Operation, int Component ) const;
			template< typename T > void ApplyChangeToAllLinear( T &Operation );
			template< typename T > void ApplyToAllLinear( T &Operation ) const;
			template< typename T > void ApplyChangeToComponentLinear( T &Operation, int Component );
			template< typename T > void ApplyToComponentLinear( T &Operation, int Component ) const;

			mutable float Mean;
			mutable float StdDev;
			mutable float NoiseEstimate;
			mutable float Min;
			mutable float Max;
			mutable bool GotStats;
			mutable bool GotNoiseEstimate;

			typedef Reference reference;
			typedef const float const_reference;
			typedef float* pointer;
			typedef const float* const_pointer;
			typedef size_t size_type;
			typedef ptrdiff_t difference_type;
			typedef Value value_type;
			typedef std::reverse_iterator< iterator > reverse_iterator;
			typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

			iterator begin( void );
			const_iterator begin( void ) const;
			iterator end( void );
			const_iterator end( void ) const;
			reverse_iterator rbegin( void );
			const_reverse_iterator rbegin( void ) const;
			reverse_iterator rend( void );
			const_reverse_iterator rend( void ) const;

		private:
			Subset( void );
			Subset( const NMRData *Parent, std::vector< int > Corner, std::vector< int > Size, bool Const );

			template< typename T > void PerformMathAssignment( const T &Operand );
			template< typename T > void PerformMathAssignment_Component( int Component, const T &Operand );
			template< typename T > void ApplyChangeToAll_Ptr( T &Operation );

			mutable SubsetData *Data;
	};


	struct SubsetData
	{
		SubsetData( void );
		SubsetData( const NMRData *Parent, std::vector< int > Corner, std::vector< int > Size, bool Const );
		~SubsetData( void );

		void BuildPtrTable( void );
		SubsetPtrTableEntry *GetElement( size_t Offset );
		SubsetPtrTableEntry *GetElementForSubsetCoord( const int *Coordinates );
		SubsetPtrTableEntry *GetElementForSubsetCoord( std::vector< int > Coordinates );
		SubsetPtrTableEntry *GetElementForSubsetCoord( std::vector< DimPosition< Subset > > Coordinates );
		SubsetPtrTableEntry *GetElementForParentCoord( const int *Coordinates );
		SubsetPtrTableEntry *GetElementForParentCoord( std::vector< DimPosition< NMRData > > Coordinates );
		void MarkPageInvalid( size_t PageIndex );
		void UpdatePtrTableStatus( void );
		void ReloadPtrsForPage( SubsetPageTableEntry *PageTableEntry );
		ptrdiff_t TranslateParentOffsetToSubsetOffset( size_t ParentOffset );

		int RefCount;
		SubsetPtrTableEntry *PtrTable;
		std::map< size_t, SubsetPageTableEntry * > PageTable;
		SubsetPageTableEntry *NewlyInvalidPageList;
		std::set< DataPage * > RegisteredWith;
		const NMRData *Parent;
		int NumOfDimensions;
		int *Corner;
		int *SizeExt;
		int *SizeInt;
		size_t FullSize;
		bool Const;
		size_t *DimIncrSize;
		std::vector< int > DimMap;
	};
    
	class NMRData
	{
		public:
			friend class DimPosition< NMRData >;
			friend class Reference;
			friend class Subset;
			friend struct SubsetData;
			friend class NMRDataMathObj;

			enum CacheModes
			{
				Linear = 0,
				Vector = 1,
				Plane = 2,
				Submatrix = 3,
				MemOnly = 4
			};

			class iterator : public std::iterator< std::random_access_iterator_tag, Value, ptrdiff_t, Reference *, Reference >
			{
				friend class NMRData;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					iterator( const iterator &Source );
					iterator &operator=( const iterator &Source );
					bool operator==( const iterator &x ) const;
					bool operator!=( const iterator &x ) const;
					Reference &operator*( void ) const;
					Reference *operator->( void ) const;
					iterator &operator++( void );	//prefix ++
					iterator operator++( int );		//postfix ++
					iterator &operator--( void );	//prefix --
					iterator operator--( int );		//postfix --
					iterator &operator+=( size_type n );
					iterator operator+( size_type n ) const;
					iterator &operator-=( size_type n );
					iterator operator-( size_type n ) const;
					difference_type operator-( const iterator &a ) const;
					Reference &operator[]( size_type n ) const;
					bool operator<( const iterator &x ) const;
					bool operator<=( const iterator &x ) const;
					bool operator>( const iterator &x ) const;
					bool operator>=( const iterator &x ) const;

				private:
					iterator( void );
					iterator( NMRData *Parent, size_t Offset, bool PastEnd );
						
					size_t offset;
					mutable Reference current;
					bool past_end;
					mutable bool is_current;
					NMRData *parent;
			};

			class const_iterator : public std::iterator< std::random_access_iterator_tag, Value, ptrdiff_t, Reference *, Reference >
			{
				friend class NMRData;

				public:
					typedef size_t size_type;
					typedef ptrdiff_t difference_type;

					const_iterator( const const_iterator &Source );
					const_iterator &operator=( const const_iterator &Source );
					bool operator==( const const_iterator &x ) const;
					bool operator!=( const const_iterator &x ) const;
					const Reference &operator*( void ) const;
					const Reference *operator->( void ) const;
					const_iterator &operator++( void );	//prefix ++
					const_iterator operator++( int );		//postfix ++
					const_iterator &operator--( void );	//prefix --
					const_iterator operator--( int );		//postfix --
					const_iterator &operator+=( size_type n );
					const_iterator operator+( size_type n ) const;
					const_iterator &operator-=( size_type n );
					const_iterator operator-( size_type n ) const;
					difference_type operator-( const const_iterator &a ) const;
					const Reference &operator[]( size_type n ) const;
					bool operator<( const const_iterator &x ) const;
					bool operator<=( const const_iterator &x ) const;
					bool operator>( const const_iterator &x ) const;
					bool operator>=( const const_iterator &x ) const;

				private:
					const_iterator( void );
					const_iterator( const NMRData *Parent, size_t Offset, bool PastEnd );
						
					size_t offset;
					mutable Reference current;
					bool past_end;
					mutable bool is_current;
					const NMRData *parent;
			};

			class TraverserOffsetOrder
			{
				friend class NMRData;

				public:
					const float *operator*( void ) const;
					void operator++( void );	//prefix++
					void operator++( int );		//postfix++

				private:
					TraverserOffsetOrder( const NMRData *Parent ) : 
					   parent( Parent ), offset( 0 ), current( Parent->TranslateOffsetToPtr( 0, true ) ) {  }
					TraverserOffsetOrder( void );

					size_t offset;
					const float *current;
					const NMRData *parent;
			};

			class TraverserCacheOrder
			{
				friend class NMRData;

				public:
					~TraverserCacheOrder( void );

					const float *operator*( void ) const;
					void operator++( void );		//prefix++

				private:
					TraverserCacheOrder( const NMRData *Parent );
					TraverserCacheOrder( void );
					TraverserCacheOrder( const TraverserCacheOrder &Source );

					std::map< size_t, DataPage * >::const_iterator end;
					std::map< size_t, DataPage * >::const_iterator currentpage;
					size_t pageoffset;
					size_t NextIndex;
					size_t PageSize;
					float *TempData;
					bool usetemp;
					bool done;
					int *TempCoord;
					size_t *TempPlaneIncrSizes;
					const NMRData *parent;
					size_t CurrentPageSize;
			};


			NMRData( void );
			NMRData( int NumOfDims );
			NMRData( const char *Filename, bool ReadOnly = false );
			NMRData( std::string Filename, bool ReadOnly = false );
			virtual ~NMRData( void );

			virtual int GetNumOfDims( void ) const;
			virtual void SetNumOfDims( int NumOfDims );
			virtual int GetDimensionSize( int DimNum ) const;
			virtual void SetDimensionSize( int DimNum, int Size ) const;
			virtual int *GetDimensionSizes( void ) const;
			virtual std::vector< int > GetDimensionSizeVector( void ) const;
			virtual void SetDimensionSizes( int *NewSizes );
			virtual void SetDimensionSizes( std::vector< int > NewSizes );
			virtual size_t GetFullSize( void ) const;
			virtual bool IsComplexAvailable( void ) const;
			virtual int GetNumOfComponents( void ) const;
			virtual bool IsTimeDomainAvailable( void ) const;
			virtual bool *IsComplexAvailableByDim( void ) const;
			virtual std::vector< bool > IsComplexAvailableByDimVector( void ) const;
			virtual bool *IsTimeDomainAvailableByDim( void ) const;
			virtual std::vector< bool > IsTDAvailableByDimVector( void ) const;
			virtual void SetComplexByDim( bool *ComplexByDim );
			virtual void SetComplexByDim( std::vector< bool > ComplexByDim );
			virtual void SetTDByDim( bool *TDByDim );
			virtual void SetTDByDim( std::vector< bool > TDByDim );
			virtual char *GetDimensionLabel( int DimNum ) const;
			virtual std::string GetDimensionLabelStr( int DimNum ) const;
			virtual void SetDimensionLabel( int DimNum, const char *Label );
			virtual void SetDimensionLabel( int DimNum, std::string Label );
			virtual void GetDimensionCalibration( int DimNum, float &SW, float &SF, float &CenterPPM ) const;
			virtual void GetFullDimCalibration( int DimNum, float &SW, float &SF, float &CenterPPM, float &OriginPPM, float &WidthPPM ) const;
			virtual float GetDimensionSW( int DimNum ) const;
			virtual float GetDimensionSF( int DimNum ) const;
			virtual float GetDimensionCenterPPM( int DimNum ) const;
			virtual float GetDimensionStartTime( int DimNum ) const;
			virtual float GetDimensionEndTime( int DimNum ) const;
			virtual bool GetDimensionComplexState( int DimNum ) const;
			virtual bool GetDimensionTD( int DimNum ) const;
			virtual void SetDimensionCalibration( int DimNum, float SW, float SF, float CenterPPM );
			virtual void SetDimensionSW( int DimNum, float SW );
			virtual void SetDimensionSF( int DimNum, float SF );
			virtual void SetDimensionCenterPPM( int DimNum, float CenterPPM );
			virtual void SetDimensionComplexState( int DimNum, bool Complex );
			virtual void SetDimensionTD( int DimNum, bool TD );
			virtual DimPosition< NMRData > GetDimPosObj( int DimNum ) const;
			//virtual DimPosition< const NMRData > GetDimPosObj( int DimNum ) const;
			//virtual DimPosition< NMRData > GetDimPosObj( int DimNum );
			virtual std::vector< DimPosition< NMRData > > GetDimPosSet( void ) const;
			//virtual std::vector< DimPosition< const NMRData > > GetDimPosSet( void ) const;
			//virtual std::vector< DimPosition< NMRData > > GetDimPosSet( void );
			virtual Value GetBlankValue( void ) const;
			virtual char *GetFilename( void ) const;
			virtual std::string GetFilenameStr( void ) const;
			virtual char *GetDatasetName( void ) const;
			virtual std::string GetDatasetNameStr( void ) const;
			virtual void ClearParameters( void );
			virtual void CopyParameters( const NMRData *Source );
			virtual void CopyDataset( const NMRData *Source );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false );
			virtual void OpenFile( std::string Filename, bool ReadOnly = false );
			virtual void CreateFile( const char *Filename );
			virtual void CreateFile( std::string Filename );
			virtual void BuildInMem( void );
			virtual void LoadFileHeader( void );
			virtual void WriteFileHeader( void );
			virtual void WriteFileHeader( void ) const;
			virtual void InitCache( size_t CacheSize, size_t PageSize, CacheModes Mode, int Dim1 = -1, int Dim2 = -1 ) const;
			virtual float *GetDataPtr( int *OriginCoordinates, int *Sizes );
			virtual float *GetDataPtr( std::vector< int > OriginCoordinates, std::vector< int > Sizes );
			virtual float *GetDataPtr( size_t StartOffset, size_t Size );
			virtual const float *GetReadonlyDataPtr( int *OriginCoordinates, int *Sizes ) const;
			virtual const float *GetReadonlyDataPtr( std::vector< int > OriginCoordinates, std::vector< int > Sizes ) const;
			virtual const float *GetReadonlyDataPtr( size_t StartOffset, size_t Size ) const;
			virtual void CloseDataPtr( float *DataPtr );
			virtual void CloseDataPtr( const float *DataPtr ) const;
			virtual std::valarray< float > GetDataCopyValarray( void ) const;
			virtual float *GetDataCopyPtr( float *Destination = 0 ) const;
			virtual void SetDataFromCopyValarray( std::valarray< float > &Input );
			virtual void SetDataFromCopyPtr( const float *Input );
			virtual float GetInterpolatedValue( const float *Coordinates ) const;
			virtual float GetInterpolatedValue( std::vector< float > Coordinates ) const;
			virtual iterator GetIteratorStart( void );
			virtual const_iterator GetIteratorStart( void ) const;
			virtual iterator GetIteratorAt( size_t Offset );
			virtual const_iterator GetIteratorAt( size_t Offset ) const;
			virtual iterator GetIteratorForReference( const Reference &Ref );
			virtual const_iterator GetIteratorForReference( const Reference &Ref ) const;
			virtual iterator GetIteratorEnd( void );
			virtual const_iterator GetIteratorEnd( void ) const;
			virtual void Flush( void );
			virtual void Close( void );
			virtual void TransposeDimensions( int Dim1, int Dim2 );
			virtual int GetFileTypeCode( void ) const;
			virtual const char *GetFileTypeString( void ) const;
			virtual std::string GetFileTypeStr( void ) const;
			virtual void CalculateStats( void ) const;
			virtual void CalculateNoiseEstimate( void ) const;

			Reference operator[]( size_t Offset );
			const Reference operator[]( size_t Offset ) const;
			Reference operator[]( std::vector< int > Coordinates );
			const Reference operator[]( std::vector< int > Coordinates ) const;
			Reference operator[]( std::vector< DimPosition< NMRData > > Coordinates );
			const Reference operator[]( std::vector< DimPosition< NMRData > > Coordinates ) const;
			Subset operator()( std::vector< int > Corner1, std::vector< int > Corner2 );
			const Subset operator()( std::vector< int > Corner1, std::vector< int > Corner2 ) const;
			Subset operator()( std::vector< DimPosition< NMRData > > Corner1, std::vector< DimPosition< NMRData > > Corner2 );
			const Subset operator()( std::vector< DimPosition< NMRData > > Corner1, std::vector< DimPosition< NMRData > > Corner2 ) const;
			Reference operator()( int Coordinate );
			const Reference operator()( int Coordinate ) const;
			Reference operator()( DimPosition< NMRData > Coordinate );
			const Reference operator()( DimPosition< NMRData > Coordinate ) const;

			NMRData &operator=( float Scalar );
			NMRData &operator=( const Value &Scalar );
			NMRData &operator=( const Reference &Scalar );
			NMRData &operator=( const Subset &Operand );
			NMRData &operator=( const NMRData &Operand );
			template< typename T > NMRData &operator=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > NMRData &operator=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );

			void operator+=( float Scalar );
			void operator+=( const Value &Scalar );
			void operator+=( const Reference &Scalar );
			void operator+=( const Subset &Operand );
			void operator+=( const NMRData &Operand );
			template< typename T > void operator+=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator+=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );
			void operator-=( float Scalar );
			void operator-=( const Value &Scalar );
			void operator-=( const Reference &Scalar );
			void operator-=( const Subset &Operand );
			void operator-=( const NMRData &Operand );
			template< typename T > void operator-=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator-=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );
			void operator*=( float Scalar );
			void operator*=( const Value &Scalar );
			void operator*=( const Reference &Scalar );
			void operator*=( const Subset &Operand );
			void operator*=( const NMRData &Operand );
			template< typename T > void operator*=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator*=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );
			void operator/=( float Scalar );
			void operator/=( const Value &Scalar );
			void operator/=( const Reference &Scalar );
			void operator/=( const Subset &Operand );
			void operator/=( const NMRData &Operand );
			template< typename T > void operator/=( const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void operator/=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );

			void SetComponentTo( int Component, float Scalar );
			void SetComponentTo( int Component, const Value &Scalar );
			void SetComponentTo( int Component, const Reference &Scalar );
			void SetComponentTo( int Component, const Subset &Operand );
			void SetComponentTo( int Component, const NMRData &Operand );
			template< typename T > void SetComponentTo( int Component, const NMRDataMathSelector< T > &Operand );
			template< typename T_LHS, typename T_RHS, typename T_Op > void SetComponentTo( int Component, const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );

			template< typename T > void ApplyToAll( T &Operation ) const;
			template< typename T > void ApplyToComponent( T &Operation, int Component ) const;
			template< typename T > void ApplyToAllLinear( T &Operation ) const;
			template< typename T > void ApplyToComponentLinear( T &Operation, int Component ) const;
			template< typename T > void ApplyChangeToAll( T &Operation );
			template< typename T > void ApplyChangeToComponent( T &Operation, int Component );
			template< typename T > void ApplyChangeToAllLinear( T &Operation );
			template< typename T > void ApplyChangeToComponentLinear( T &Operation, int Component );

			template< typename T > T SwapByteOrder( T Source ) const;

			static void FreeArray( char *ptr );
			static void FreeArray( int *ptr );
			static void FreeArray( float *ptr );
			static void FreeArray( bool *ptr );
			static void FreeObj( NMRData *ptr );
			static NMRData *GetObjForFile( const char *Filename, bool ReadOnly = false );
			static NMRData *GetObjForFile( std::string Filename, bool ReadOnly = false );
			static NMRData *GetObjForFileType( const char *Filename );
			static NMRData *GetObjForFileType( std::string Filename );
			static void CopyParameters( const NMRData *Source, NMRData *Dest );
			static void CopyDataset( const NMRData *Source, NMRData *Dest );
			static char *RemoveFileExtension( const char *Filename );
			static char *RemoveFilePath( const char *Filename );

			size_t FullSize;
			mutable size_t DataPtrMemLimit;
			bool GotData;
			mutable float Mean;
			mutable float StdDev;
			mutable float NoiseEstimate;
			mutable float Min;
			mutable float Max;
			mutable bool GotStats;
			mutable bool GotNoiseEstimate;

			static size_t DefaultDataPtrMemLimit;
			static size_t DefaultInternalCacheSize;
			static size_t DefaultPageSize;
			static bool ( *ParameterCallback ) ( NMRData *receiver );

			static const int FileType_bin = 0;
			static const int FileType_nmrPipe = 1;
			static const int FileType_XEASY = 2;
			static const int FileType_UCSF = 3;
			static const int FileType_NMRView = 4;
			static const int FileType_Text = 5;

			typedef Reference reference;
			typedef const float const_reference;
			typedef float* pointer;
			typedef const float* const_pointer;
			typedef size_t size_type;
			typedef ptrdiff_t difference_type;
			typedef Value value_type;
			typedef std::reverse_iterator< iterator > reverse_iterator;
			typedef std::reverse_iterator< const_iterator > const_reverse_iterator;

			iterator begin( void );
			const_iterator begin( void ) const;
			iterator end( void );
			const_iterator end( void ) const;
			reverse_iterator rbegin( void );
			const_reverse_iterator rbegin( void ) const;
			reverse_iterator rend( void );
			const_reverse_iterator rend( void ) const;

		protected:
			float *TranslateOffsetToPtr( size_t Offset, bool Const, DataPage **Page = 0 ) const;
			void TranslateOffsetToSubmatrixPos( size_t Offset, size_t &SubmatrixNumber, size_t &SubmatrixOffset ) const;
			std::vector< DimPosition< NMRData > > TranslateOffsetToCoord( size_t Offset ) const;
			size_t TranslateCoordToOffset( std::vector< DimPosition< NMRData > > Position ) const;

			template< typename T > void PerformMathAssignment( const T &Operand );
			template< typename T > void PerformMathAssignment_Component( int Component, const T &Operand );
			
			template< typename T > void ApplyChangeToAll_Ptr_OffsetOrder( T &Operation );
			template< typename T > void ApplyChangeToAll_Ptr_Fastest( T &Operation );
			template< typename T > void ApplyChangeToAll_ByComponent_OffsetOrder( T &Operation );
			template< typename T > void ApplyToAll_ByComponent_OffsetOrder( T &Operation ) const;
			template< typename T > void ApplyChangeToAll_ByComponent_Fastest( T &Operation );
			template< typename T > void ApplyToAll_ByComponent_Fastest( T &Operation ) const;
			template< typename T > void ApplyChangeToAll_Component_OffsetOrder( T &Operation, int Component );
			template< typename T > void ApplyToAll_Component_OffsetOrder( T &Operation, int Component ) const;
			template< typename T > void ApplyChangeToAll_Component_Fastest( T &Operation, int Component );
			template< typename T > void ApplyToAll_Component_Fastest( T &Operation, int Component ) const;

			virtual void StdClose( void );
			virtual void ClearPrivateParams( void );
			virtual void ClearCacheParams( void ) const;
			virtual void FlushCache( void );
			virtual void RefreshCache( void ) const;
			virtual void CloseCache( void ) const;
			virtual void LoadData( size_t StartOffset, size_t Size, float *Dest ) const;
			virtual void WriteData( size_t StartOffset, size_t Size, float *Source );
			virtual void WriteData( size_t StartOffset, size_t Size, float *Source ) const;
			virtual void LoadVector( int Dim, size_t Index, float *Dest ) const;
			virtual void WriteVector( int Dim, size_t Index, float *Src );
			virtual void WriteVector( int Dim, size_t Index, float *Src ) const;
			virtual void LoadPlane( int XDim, int YDim, int *Coord, float *Dest ) const;
			virtual void WritePlane( int XDim, int YDim, int *Coord, float *Src );
			virtual void WritePlane( int XDim, int YDim, int *Coord, float *Src ) const;
			virtual void LoadSubmatrix( size_t SubmatrixNumber, float *Dest ) const;
			virtual void WriteSubmatrix( size_t SubmatrixNumber, float *Src );
			virtual void WriteSubmatrix( size_t SubmatrixNumber, float *Src ) const;
			virtual void SetSubmatrixSizes( void ) const;
			virtual int *CalculateSubmatrixSizes( int NumOfDims, int *DimSizes ) const;
			virtual bool CacheDefinesSubmatrices( void ) const;
			virtual TraverserCacheOrder *GetTraverserCacheOrder( void ) const;
			virtual TraverserOffsetOrder *GetTraverserOffsetOrder( void ) const;

			char *Filename;
			char *Name;
			bool ReadOnly;

			int NumOfDimensions;
			int *DimensionSizes;
			bool IsTimeDomain;
			bool *IsTimeDomainByDim;
			bool IsComplex;
			bool *IsComplexByDim;
			int Components;
			float *DimensionScalesFreq;
			float *DimensionScalesPPM;
			char **DimensionAxisLabels;
			float *DimensionCentersPPM;
			float *DimensionCentersFreq;
			float *DimensionOriginsPPM;

			mutable OpenDataPtr *OpenDataPtrs;
			mutable int OpenDataPtrCount;

			mutable size_t InternalCacheSize;
			mutable size_t PageSize;
			mutable size_t MaxPages;
			mutable size_t OpenPages;
			mutable DataPage *CacheStart;
			mutable DataPage *CacheEnd;
			mutable std::map< size_t, DataPage * > CacheMap;
			mutable CacheModes CacheMode;
			mutable int *SubmatrixSizes;
			mutable int SubmatrixSize;
			mutable size_t SubmatrixCount;
			mutable int *SubmatrixDimCount;
			mutable int CacheDim1;
			mutable int CacheDim2;
			mutable float *MemOnlyData;
			mutable int *TempCoord;
			mutable int *SubmatrixPos;
			mutable int *PointPos;
			mutable size_t *TempDimIncrSizes;
			mutable size_t *TempVectorIncrSizes;
			mutable size_t *TempPlaneIncrSizes;
			mutable int *TempSubmatrixExtIncrSizes;
			mutable int *TempSubmatrixIntIncrSizes;

		private:
			NMRData( NMRData &Source );

			FILE *File;
			FILE *MemTempFile;
	};


    enum DimensionApodMethod
    {
        None = 0,
        SP = 1,
        EM = 2,
        GM = 3,
        GMB = 4,
        TM = 5,
        TRI = 6,
        JMOD = 7
    };
    
    struct DimensionApodInfo
    {
        DimensionApodInfo() : Method( None ), P1( 0.0F ), P2( 0.0F ), P3( 0.0F ), Size( 0 ), FirstPointCorrection( false ) {  }
        DimensionApodInfo( DimensionApodMethod Method_, float P1_, float P2_, float P3_, int Size_, float FPC_ ) : Method( Method_ ), P1( P1_ ), P2( P2_ ), P3( P3_ ), Size( Size_ ), FirstPointCorrection( FPC_ ) {  }
        
        DimensionApodMethod Method;
        float P1;
        float P2;
        float P3;
        int Size;
        float FirstPointCorrection;
    };
    
    
	class NMRData_nmrPipe : virtual public NMRData
	{
		public:
			using NMRData::operator=;

			NMRData_nmrPipe( void );
			NMRData_nmrPipe( int NumOfDims );
			NMRData_nmrPipe( const char *Filename, bool ReadOnly = false );
			NMRData_nmrPipe( std::string Filename, bool ReadOnly = false );
			virtual ~NMRData_nmrPipe( void );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false );
			virtual void CreateFile( const char *Filename );
			virtual void LoadFileHeader( void );
			virtual void WriteFileHeader( void );
			virtual void Close( void );
			virtual int GetFileTypeCode( void ) const;
			virtual const char *GetFileTypeString( void ) const;
            virtual DimensionApodInfo GetDimensionApodInfo( int DimNum ) const;

		private:
			virtual void ClearPrivateParams( void );
			virtual void LoadData( size_t StartOffset, size_t Size, float *Dest ) const;
			virtual void WriteData( size_t StartOffset, size_t Size, float *Source );

			float *BuildFDATAFromParams( void ) const;
			void LoadParamsFromFDATA( float *FDATA );

			static void WriteFDATAToFile( float *FDATA, const char *Filename );
			static float *LoadFDATAFromFile( const char *Filename );
			static float *BuildFDATAFromParams( bool SwapBytes, const NMRData *Source, const NMRData_nmrPipe *PipeObject = 0 );
			static bool IsStringParam( int Code );
			static int MakePipeFileList( const char *StartingName, int NumOfDims, int ZDimSize, int ADimSize, char ***Filelist );
			static char *GetStringPipeParam( float *FDATA, int Offset, int MaxSize );
			static void SetStringPipeParam( float *FDATA, int Offset, int MaxSize, char *In );

			mutable char *FilePattern;
			mutable char **FileList;
			mutable int FileCount;
			mutable int FileSize;
			mutable float *StdFDATA;
			mutable int ZDimSize;
			mutable int ADimSize;
			mutable bool SwapFlag;
	};


	class NMRData_Text : virtual public NMRData
	{
		public:
			using NMRData::operator=;

			NMRData_Text( void );
			NMRData_Text( int NumOfDims );
			NMRData_Text( const char *Filename, bool ReadOnly = false );
			NMRData_Text( std::string Filename, bool ReadOnly = false );
			virtual ~NMRData_Text( void );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false );
			virtual void CreateFile( const char *Filename );
			virtual void Close( void );
			virtual int GetFileTypeCode( void ) const;
			virtual const char *GetFileTypeString( void ) const;

		private:
			virtual void ClearPrivateParams( void );
			virtual void LoadData( size_t StartOffset, size_t Size, float *Dest ) const;
			virtual void WriteData( size_t StartOffset, size_t Size, float *Source );

			FILE *File;
	};


	class NMRDataSubmatrix : virtual public NMRData
	{
		public:
			using NMRData::operator=;

			NMRDataSubmatrix( void );
			NMRDataSubmatrix( int NumOfDims );
			~NMRDataSubmatrix( void );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false ) = 0;
			virtual void Close( void );

		protected:
			virtual void ClearPrivateParams( void );
			virtual void ClearSubmatrixParams( void );
			virtual void LoadData( size_t StartOffset, size_t Size, float *Dest ) const;
			virtual void WriteData( size_t StartOffset, size_t Size, float *Source );
			virtual void LoadSubmatrix( size_t SubmatrixNumber, float *Dest ) const;
			virtual void WriteSubmatrix( size_t SubmatrixNumber, float *Src );
			virtual bool CacheDefinesSubmatrices( void ) const;

			FILE *DataFile;
			int HeaderSize;
			bool SwapBytes;
	};


	class NMRData_XEASY : public NMRDataSubmatrix
	{
		public:
			using NMRData::operator=;

			NMRData_XEASY( void );
			NMRData_XEASY( int NumOfDims );
			NMRData_XEASY( const char *Filename, bool ReadOnly = false );
			NMRData_XEASY( std::string Filename, bool ReadOnly = false );
			virtual ~NMRData_XEASY( void );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false );
			virtual void CreateFile( const char *Filename );
			virtual void Close( void );
			virtual void LoadFileHeader( void );
			virtual void WriteFileHeader( void );
			virtual int GetFileTypeCode( void ) const;
			virtual const char *GetFileTypeString( void ) const;

			float XEASY16ToFloat( unsigned char *val ) const;
			unsigned char *FloatToXEASY16( float val, unsigned char *outval = 0 ) const;

		private:
			virtual void ClearPrivateParams( void );
			virtual void LoadData( size_t StartOffset, size_t Size, float *Dest ) const;
			virtual void WriteData( size_t StartOffset, size_t Size, float *Source );
			virtual void LoadSubmatrix( size_t SubmatrixNumber, float *Dest ) const;
			virtual void WriteSubmatrix( size_t SubmatrixNumber, float *Src );

			void InitExponTable( void ) const;

			FILE *ParamFile;
			mutable std::valarray< float > expon_table;
	};


	class NMRData_UCSF : public NMRDataSubmatrix
	{
		public:
			using NMRData::operator=;

			NMRData_UCSF( void );
			NMRData_UCSF( int NumOfDims );
			NMRData_UCSF( const char *Filename, bool ReadOnly = false );
			NMRData_UCSF( std::string Filename, bool ReadOnly = false );
			virtual ~NMRData_UCSF( void );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false );
			virtual void CreateFile( const char *Filename );
			virtual void LoadFileHeader( void );
			virtual void WriteFileHeader( void );
			virtual int GetFileTypeCode( void ) const;
			virtual const char *GetFileTypeString( void ) const;

		private:
			virtual void ClearPrivateParams( void );
	};


	class NMRData_NMRView : public NMRDataSubmatrix
	{
		public:
			using NMRData::operator=;

			NMRData_NMRView( void );
			NMRData_NMRView( int NumOfDims );
			NMRData_NMRView( const char *Filename, bool ReadOnly = false );
			NMRData_NMRView( std::string Filename, bool ReadOnly = false );
			virtual ~NMRData_NMRView( void );

			virtual void OpenFile( const char *Filename, bool ReadOnly = false );
			virtual void CreateFile( const char *Filename );
			virtual void LoadFileHeader( void );
			virtual void WriteFileHeader( void );
			virtual int GetFileTypeCode( void ) const;
			virtual const char *GetFileTypeString( void ) const;

		private:
			virtual void ClearPrivateParams( void );
			virtual int *CalculateSubmatrixSizes( int NumOfDims, int *DimSizes ) const;
	};


	Subset ProcessSubsetSelectionString( NMRData *Dataset, std::string SelectionString );
	const Subset ProcessSubsetSelectionString( const NMRData *Dataset, std::string SelectionString );


	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::plus< float > > operator+( const Subset &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::plus< float > > operator+( const float Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::plus< float > > operator+( const Subset &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::plus< float > > operator+( const Value &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::plus< float > > operator+( const Subset &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::plus< float > > operator+( const Reference &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::plus< float > > operator+( const Subset &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::plus< float > > operator+( const NMRData &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::plus< float > > operator+( const float Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::plus< float > > operator+( const NMRData &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::plus< float > > operator+( const Value &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::plus< float > > operator+( const NMRData &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::plus< float > > operator+( const Reference &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::plus< float > > operator+( const NMRData &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::plus< float > > operator+( const Subset &Operand2, const NMRData &Operand1 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::plus< float > > operator+( const NMRData &Operand1, const NMRData &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::minus< float > > operator-( const Subset &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::minus< float > > operator-( const float Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::minus< float > > operator-( const Subset &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::minus< float > > operator-( const Value &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::minus< float > > operator-( const Subset &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::minus< float > > operator-( const Reference &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::minus< float > > operator-( const Subset &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::minus< float > > operator-( const NMRData &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::minus< float > > operator-( const float Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::minus< float > > operator-( const NMRData &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::minus< float > > operator-( const Value &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::minus< float > > operator-( const NMRData &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::minus< float > > operator-( const Reference &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::minus< float > > operator-( const NMRData &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::minus< float > > operator-( const Subset &Operand2, const NMRData &Operand1 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::minus< float > > operator-( const NMRData &Operand1, const NMRData &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::multiplies< float > > operator*( const Subset &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::multiplies< float > > operator*( const float Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::multiplies< float > > operator*( const Subset &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::multiplies< float > > operator*( const Value &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::multiplies< float > > operator*( const Subset &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::multiplies< float > > operator*( const Reference &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::multiplies< float > > operator*( const Subset &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::multiplies< float > > operator*( const NMRData &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::multiplies< float > > operator*( const float Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::multiplies< float > > operator*( const NMRData &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::multiplies< float > > operator*( const Value &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::multiplies< float > > operator*( const NMRData &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::multiplies< float > > operator*( const Reference &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::multiplies< float > > operator*( const NMRData &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::multiplies< float > > operator*( const Subset &Operand2, const NMRData &Operand1 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::multiplies< float > > operator*( const NMRData &Operand1, const NMRData &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::divides< float > > operator/( const Subset &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::divides< float > > operator/( const float Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::divides< float > > operator/( const Subset &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::divides< float > > operator/( const Value &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::divides< float > > operator/( const Subset &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::divides< float > > operator/( const Reference &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::divides< float > > operator/( const Subset &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::divides< float > > operator/( const NMRData &Operand1, float Operand2 );
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::divides< float > > operator/( const float Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::divides< float > > operator/( const NMRData &Operand1, const Value &Operand2 );
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::divides< float > > operator/( const Value &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::divides< float > > operator/( const NMRData &Operand1, const Reference &Operand2 );
	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::divides< float > > operator/( const Reference &Operand1, const NMRData &Operand2 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::divides< float > > operator/( const NMRData &Operand1, const Subset &Operand2 );
	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::divides< float > > operator/( const Subset &Operand2, const NMRData &Operand1 );
	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::divides< float > > operator/( const NMRData &Operand1, const NMRData &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 );
	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );
	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 );

	template< typename T_LHS, typename T_RHS, typename T_Op >
	class NMRDataMathOp
	{
		public:
			NMRDataMathOp( const T_LHS &LHS, const T_RHS &RHS, const T_Op &Operation ) : lhs( LHS ), rhs( RHS ), op( Operation ) {  }

			float operator()( const float dummy = 0.0F ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			T_LHS lhs;
			T_RHS rhs;
			T_Op op;
	};


	class NMRDataMathScalar
	{
		public:
			NMRDataMathScalar( float Scalar ) : val( Scalar )  {  }

			float operator()( void ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			float val;
	};


	class NMRDataMathVal
	{
		public:
			NMRDataMathVal( const Value &Operand ) : val( Operand ), nextcomp( 0 ), comp( Operand.GetNumOfComponents() )  {  }

			float operator()( void ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			const Value &val;
			mutable int nextcomp;
			int comp;
	};


	class NMRDataMathRef
	{
		public:
			NMRDataMathRef( const Reference &Operand ) : ref( Operand ), nextcomp( 0 ), comp( Operand.GetParent()->GetNumOfComponents() )  {  }

			float operator()( void ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			const Reference &ref;
			mutable int nextcomp;
			int comp;
	};


	class NMRDataMathSubset
	{
		public:
			NMRDataMathSubset( const Subset &Operand );

			float operator()( const float dummy = 0.0F ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			const Subset &obj;
			mutable SubsetPtrTableEntry *current;
			mutable int nextcomp;
			int comp;
	};


	class NMRDataMathObj
	{
		public:
			NMRDataMathObj( const NMRData *Operand ) : obj( Operand ), ct( 0 ), ot( 0 ), mode( false ), current( 0 ), 
				nextcomp( 0 ), comp( Operand->GetNumOfComponents() ) {  }
			~NMRDataMathObj( void );

			float operator()( const float dummy = 0.0F ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			const NMRData *obj;
			mutable NMRData::TraverserCacheOrder *ct;
			mutable NMRData::TraverserOffsetOrder *ot;
			mutable bool mode;
			mutable const float *current;
			mutable int nextcomp;
			int comp;
	};


	template< typename T >
	class NMRDataMathSelector
	{
		public:
			NMRDataMathSelector( int Component, const T &MathExpression ) 
				: exp( MathExpression ), comp_selected( Component ), comp_higher( MathExpression.GetNumOfComponents() ) {  }

			float operator()( const float dummy = 0.0F ) const;
			BEC_Misc::ptr_vector< const NMRData > GetObjectList( void ) const;
			std::vector< Subset > GetSubsetList( void ) const;
			int GetNumOfComponents( void ) const;
			void SetCacheTraversal( void ) const;
			void SetOffsetTraversal( void ) const;

		private:
			T exp;
			int comp_selected;
			int comp_higher;
	};


	NMRDataMathSelector< NMRDataMathScalar > GetComponent( int Component, float Scalar );
	NMRDataMathSelector< NMRDataMathVal > GetComponent( int Component, const Value &Scalar );
	NMRDataMathSelector< NMRDataMathRef > GetComponent( int Component, const Reference &Scalar );
	NMRDataMathSelector< NMRDataMathSubset > GetComponent( int Component, const Subset &Operand );
	NMRDataMathSelector< NMRDataMathObj > GetComponent( int Component, const NMRData &Operand );
	template< typename T >
	NMRDataMathSelector< NMRDataMathSelector< T > > GetComponent( int Component, const NMRDataMathSelector< T > &Operand );
	template< typename T_LHS, typename T_RHS, typename T_Op >
	NMRDataMathSelector< NMRDataMathOp< T_LHS, T_RHS, T_Op > > GetComponent( int Component, const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand );


	template< typename T >
	struct ValToPtrOpAdaptor : std::binary_function< float *, Value, void >
	{
		ValToPtrOpAdaptor( T Operation ) : op( Operation ) { }

		void operator()( float *ptr, const Value &val ) const
		{
			for( int i = 0; i < val.GetNumOfComponents(); i++ )
				ptr[ i ] = op( ptr[ i ], val[ i ] );
		}

		T op;
	};


	struct ValToPtrConstAdaptor : std::binary_function< float *, Value, void >
	{
		void operator()( float *ptr, const Value &val ) const
		{
			for( int i = 0; i < val.GetNumOfComponents(); i++ )
				ptr[ i ] = val[ i ];
		}
	};


	struct ValToPtrComponentAdaptor : std::binary_function< float *, Value, void >
	{
		ValToPtrComponentAdaptor( int Component ) : component( Component ) {  }

		void operator()( float *ptr, const Value &val ) const
		{
			ptr[ this->component ] = val[ this->component ];
		}

		int component;
	};


	struct StatsCollectorFirstPass
	{
		StatsCollectorFirstPass( float NormalizationIn ) : Normalization( NormalizationIn ), Mean( 0.0F ), Min( 0.0F ), Max( 0.0F ), 
			GotFirstVal( false ) {  }

		void operator()( float val )
		{
			this->Mean += Normalization * val;
			if( !GotFirstVal ) this->Min = this->Max = val, GotFirstVal = true;
			else if( val < this->Min ) this->Min = val;
			else if( val > this->Max ) this->Max = val;
		}

		float Normalization;
		float Mean;
		float Min;
		bool GotFirstVal;
		float Max;
	};


	struct StatsCollectorSecondPass
	{
		StatsCollectorSecondPass( float MeanIn ) : Mean( MeanIn ), SumOfDevSq( 0.0F ) {  }

		void operator()( float val )
		{
			float dev = val - this->Mean;
			this->SumOfDevSq += dev * dev;
		}

		float Mean;
		float SumOfDevSq;
	};


	template< typename T >
	struct DataOutCopier
	{
		DataOutCopier( T &Dest ) : dest( Dest ), index( 0 ) {  }

		void operator() ( float val )
		{
			dest[ index++ ] = val;
		}

		T &dest;
		size_t index;
	};


	template< typename T >
	struct DataInCopier
	{
		DataInCopier( T &Src ) : src( Src ), index( 0 ) {  }

		float operator() ( float val )
		{
			return src[ index++ ];
		}

		T &src;
		size_t index;
	};


	class RawVarianData
	{
		public:
			RawVarianData( std::string Filename, bool ReadOnly = false );
			RawVarianData( std::string Filename, int np, int NumOfFIDs );
			~RawVarianData( void );

			int GetNumOfFIDs( void ) const;
			int GetNP( void ) const;
			std::vector< std::valarray< float > > &GetData( void );

			void ProcessSE( void );

		private:
			bool readonly;
			bool swap;
			bool store_ints;
			int fids;
			int np;
			std::vector< std::valarray< float > > data;
			FILE *file;
			VNMRFileHeader file_header;
			VNMRBlockHeader std_block_header;
	};

	
	inline Value::Value( void ) : realval( 0.0F ), complexval( 0 ), dims( 0 ), components( 0 ), complexbydim( 0 ), 
		selectorindex( 0 ), selectordim( 0 ) {  }
	inline Value::Value( float RealVal ) : realval( RealVal ), complexval( 0 ), dims( 0 ), components( 1 ), 
		selectorindex( 0 ), selectordim( 0 ) {  }
	inline Value::Value( std::vector< bool > ComplexByDim, const float *ComplexVal, int SelectorDim, int SelectorIndex ) : 
		realval( 0.0F ), selectordim( SelectorDim ), selectorindex( SelectorIndex )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !ComplexVal ) throw Exceptions::NullPointerArg();
		if( ComplexByDim.empty() ) throw Exceptions::BadComplexConfig();
#endif

		this->dims = ( int ) ComplexByDim.size();
		this->components = 1;
		for( int i = 0; i < this->dims; i++ )
			this->components *= ComplexByDim[ i ] ? 2 : 1;
		this->complexbydim = new bool[ this->dims ];
		for( int i = 0; i < this->dims; i++ )
				this->complexbydim[ i ] = ComplexByDim[ i ];
		if( this->components == 1 )
		{
			this->realval = ComplexVal[ 0 ];
			this->complexval = 0;
		}
		else
		{
			this->complexval = new float[ this->components ];
			memcpy( complexval, ComplexVal, sizeof( float ) * this->components );
		}
	}


	inline Value::Value( const Value &Source )
	{
		this->realval = Source.realval;
		this->dims = Source.dims;
		this->components = Source.components;
		this->selectordim = Source.selectordim;
		this->selectorindex = Source.selectorindex;

		if( this->components > 1 ) 
		{
			this->complexval = new float[ this->components ];
			memcpy( complexval, Source.complexval, sizeof( float ) * this->components );
		}
		else this->complexval = 0;

		if( this->dims )
		{
			this->complexbydim = new bool[ this->dims ];
			memcpy( complexbydim, Source.complexbydim, sizeof( bool ) * this->dims );
		}
		else this->complexbydim = 0;
	}


	inline Value::~Value( void )
	{
		delete [] complexbydim;
		delete [] complexval;
	}


	inline Value &Value::operator=( const Value &Source )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] = Source.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Source.Selected() )
		{
			this->realval = Source.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		delete [] complexbydim;
		delete [] complexval;

		this->realval = Source.realval;
		this->dims = Source.dims;
		this->components = Source.components;
		this->selectordim = Source.selectordim;
		this->selectorindex = Source.selectorindex;

		if( this->components > 1 ) 
		{
			this->complexval = new float[ this->components ];
			memcpy( complexval, Source.complexval, sizeof( float ) * this->components );
		}
		else this->complexval = 0;

		if( this->dims )
		{
			this->complexbydim = new bool[ this->dims ];
			memcpy( complexbydim, Source.complexbydim, sizeof( bool ) * this->dims );
		}
		else this->complexbydim = 0;

		return *this;
	}


	inline Value &Value::operator=( float RealVal )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] = RealVal;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		delete [] complexbydim;
		delete [] complexval;

		this->realval = RealVal;
		this->dims = 0;
		this->components = 1;
		this->complexval = 0;
		this->complexbydim = 0;
		this->selectordim = 0;
		this->selectorindex = 0;

		return *this;
	}


	inline Value &Value::operator=( const Reference &Source )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] = Source.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Source.Selected() )
		{
			this->realval = Source.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		delete [] complexbydim;
		delete [] complexval;

		const NMRData *parent = Source.GetParent();

		if( parent->GetNumOfComponents() == 1 )
		{
			this->realval = Source[ 0 ];
			this->dims = 0;
			this->components = 1;
			this->selectordim = 0;
			this->selectorindex = 0;
			return *this;
		}

		this->dims = parent->GetNumOfDims();
		this->components = parent->GetNumOfComponents();
		this->selectordim = 0;
		this->selectorindex = 0;

		this->complexval = new float[ this->components ];
		for( int i = 0; i < this->components; i++ )
			this->complexval[ i ] = Source[ i ];

		this->complexbydim = new bool[ this->dims ];
		std::vector< bool > ComplexByDim = parent->IsComplexAvailableByDimVector();
		for( int i = 0; i < this->dims; i++ )
			this->complexbydim[ i ] = ComplexByDim[ i ];

		return *this;
	}


	inline bool Value::operator==( const Value &Operand ) const
	{
		if( this->dims && this->selectordim == this->dims )
			return this->complexval[ this->selectorindex ] == Operand.GetSelectedVal();
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		if( Operand.components != this->components ) return false;
		if( Operand.components == 0 ) return true;
		if( Operand.components == 1 )
			return Operand.realval == this->realval;
		for( int i = 0; i < this->dims; i++ )
			if( Operand.complexbydim[ i ] != this->complexbydim[ i ] ) return false;
		for( int i = 0; i < this->components; i++ )
			if( Operand.complexval[ i ] != this->complexval[ i ] ) return false;
		return true;
	}


	inline bool Value::operator==( float RealVal ) const
	{
		if( this->dims && this->selectordim == this->dims )
			return this->complexval[ this->selectorindex ] == RealVal;
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		if( this->components == 1 ) return this->realval == RealVal;
		else return false;
	}


	inline bool Value::operator!=( const Value &Operand ) const
	{
		return !( *this == Operand );
	}


	inline bool Value::operator!=( float RealVal ) const
	{
		return !( *this == RealVal );
	}


	inline bool Value::operator<( const Value &Operand ) const
	{
		if( this->dims && this->selectordim == this->dims )
			return this->complexval[ this->selectorindex ] < Operand.GetSelectedVal();
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		if( Operand.components != this->components ) return false;
		if( Operand.components == 0 ) return true;
		if( Operand.components == 1 )
			return Operand.realval < this->realval;
		for( int i = 0; i < this->dims; i++ )
			if( Operand.complexbydim[ i ] >= this->complexbydim[ i ] ) return false;
		for( int i = 0; i < this->components; i++ )
			if( Operand.complexval[ i ] >= this->complexval[ i ] ) return false;
		return true;
	}


	inline bool Value::operator<( float RealVal ) const
	{
		if( this->dims && this->selectordim == this->dims )
			return this->complexval[ this->selectorindex ] < RealVal;
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

#ifdef BEC_NMRDATA_DEBUG
		if( this->components == 1 ) 
#endif
			return this->realval < RealVal;
#ifdef BEC_NMRDATA_DEBUG
		else throw Exceptions::NotCompatible();
#endif
	}


	inline bool Value::operator<=( const Value &Operand ) const
	{
		return *this < Operand || *this == Operand;
	}


	inline bool Value::operator<=( float RealVal ) const
	{
		return *this < RealVal || *this == RealVal;
	}


	inline bool Value::operator>( const Value &Operand ) const
	{
		return *this > Operand;
	}


	inline bool Value::operator>( float RealVal ) const
	{
		return *this > RealVal;
	}


	inline bool Value::operator>=( const Value &Operand ) const
	{
		return *this > Operand || *this == Operand;
	}


	inline bool Value::operator>=( float RealVal ) const
	{
		return *this > RealVal || *this == RealVal;
	}


	inline Value &Value::operator+=( const Reference &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] += Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval += Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( !Operand.IsCompatible( *this ) ) throw Exceptions::NotCompatible();
#endif
		if( this->components == 1 )
			this->realval += Operand[ 0 ];
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] += Operand[ i ];
		}

		return *this;
	}


	inline Value &Value::operator+=( const Value &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] += Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval += Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( Operand.components != this->components ) throw Exceptions::ComponentsDontMatch();
#endif
		if( this->components == 1 )
			this->realval += Operand.realval;
		else
		{
#ifdef BEC_NMRDATA_DEBUG
			if( Operand.dims != this->dims ) throw Exceptions::DimsDontMatch();
			for( int i = 0; i < this->dims; i++ )
				if( Operand.complexbydim[ i ] != this->complexbydim[ i ] ) throw Exceptions::ComplexConfigDoesntMatch();
#endif
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] += Operand.complexval[ i ];
		}

		return *this;
	}


	inline Value &Value::operator+=( float Scalar )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] += Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 )
			this->realval += Scalar;
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] += Scalar;
		}

		return *this;
	}


	inline Value &Value::operator-=( const Reference &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] -= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval -= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( !Operand.IsCompatible( *this ) ) throw Exceptions::NotCompatible();
#endif
		if( this->components == 1 )
			this->realval -= Operand[ 0 ];
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] -= Operand[ i ];
		}

		return *this;
	}


	inline Value &Value::operator-=( const Value &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] -= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval -= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( Operand.components != this->components ) throw Exceptions::ComponentsDontMatch();
#endif
		if( this->components == 1 )
			this->realval -= Operand.realval;
		else
		{
#ifdef BEC_NMRDATA_DEBUG
			if( Operand.dims != this->dims ) throw Exceptions::DimsDontMatch();
			for( int i = 0; i < this->dims; i++ )
				if( Operand.complexbydim[ i ] != this->complexbydim[ i ] ) throw Exceptions::ComplexConfigDoesntMatch();
#endif
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] -= Operand.complexval[ i ];
		}

		return *this;
	}


	inline Value &Value::operator-=( float Scalar )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] -= Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 )
			this->realval -= Scalar;
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] -= Scalar;
		}

		return *this;
	}


	inline Value &Value::operator*=( const Reference &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] *= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval *= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( !Operand.IsCompatible( *this ) ) throw Exceptions::NotCompatible();
#endif
		if( this->components == 1 )
			this->realval *= Operand[ 0 ];
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] *= Operand[ i ];
		}

		return *this;
	}


	inline Value &Value::operator*=( const Value &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] *= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval *= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( Operand.components != this->components ) throw Exceptions::ComponentsDontMatch();
#endif
		if( this->components == 1 )
			this->realval *= Operand.realval;
		else
		{
#ifdef BEC_NMRDATA_DEBUG
			if( Operand.dims != this->dims ) throw Exceptions::DimsDontMatch();
			for( int i = 0; i < this->dims; i++ )
				if( Operand.complexbydim[ i ] != this->complexbydim[ i ] ) throw Exceptions::ComplexConfigDoesntMatch();
#endif
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] *= Operand.complexval[ i ];
		}

		return *this;
	}


	inline Value &Value::operator*=( float Scalar )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] *= Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 )
			this->realval *= Scalar;
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] *= Scalar;
		}

		return *this;
	}


	inline Value &Value::operator/=( const Reference &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] /= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval /= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( !Operand.IsCompatible( *this ) ) throw Exceptions::NotCompatible();
#endif
		if( this->components == 1 )
			this->realval /= Operand[ 0 ];
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] /= Operand[ i ];
		}

		return *this;
	}


	inline Value &Value::operator/=( const Value &Operand )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] /= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->components == 1 && Operand.Selected() )
		{
			this->realval /= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( Operand.components != this->components ) throw Exceptions::ComponentsDontMatch();
#endif
		if( this->components == 1 )
			this->realval /= Operand.realval;
		else
		{
#ifdef BEC_NMRDATA_DEBUG
			if( Operand.dims != this->dims ) throw Exceptions::DimsDontMatch();
			for( int i = 0; i < this->dims; i++ )
				if( Operand.complexbydim[ i ] != this->complexbydim[ i ] ) throw Exceptions::ComplexConfigDoesntMatch();
#endif
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] /= Operand.complexval[ i ];
		}

		return *this;
	}


	inline Value &Value::operator/=( float Scalar )
	{
		if( this->dims && this->selectordim == this->dims )
		{
			this->complexval[ this->selectorindex ] /= Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->selectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 )
			this->realval /= Scalar;
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] /= Scalar;
		}

		return *this;
	}


	inline float &Value::operator[]( int Component )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->dims && this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 && Component == 0 ) return this->realval;
#ifdef BEC_NMRDATA_DEBUG
		else if( Component >= 0 && Component < this->components )
#endif
			return this->complexval[ Component ];
#ifdef BEC_NMRDATA_DEBUG
		else throw Exceptions::BadComponentIndex();
#endif
	}


	inline const float &Value::operator[]( int Component ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->dims && this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 && Component == 0 ) return this->realval;
#ifdef BEC_NMRDATA_DEBUG
		else if( Component >= 0 && Component < this->components )
#endif
			return this->complexval[ Component ];
#ifdef BEC_NMRDATA_DEBUG
		else throw Exceptions::BadComponentIndex();
#endif
	}


	inline float &Value::operator[]( std::vector< bool > Selector )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->dims && this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( !this->components ) throw Exceptions::ValueIsEmpty();
		else if( this->components == 1 ) throw Exceptions::BadComponentSelector();

		if( Selector.size() != this->dims ) throw Exceptions::BadComponentSelector();
#endif
		int index = 0;
		for( int i = 0; i < this->dims; i++ )
		{
			if( Selector[ i ] && this->complexbydim[ i ] )
				index += ( int ) pow( this->dims - i - 1, 2.0 );
#ifdef BEC_NMRDATA_DEBUG
			else if( Selector[ i ] ) throw Exceptions::DimNotComplex();
#endif
		}
		return this->complexval[ index ];
	}


	inline const float &Value::operator[]( std::vector< bool > Selector ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->dims && this->selectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( !this->components ) throw Exceptions::ValueIsEmpty();
		else if( this->components == 1 ) throw Exceptions::BadComponentSelector();

		if( Selector.size() != this->dims ) throw Exceptions::BadComponentSelector();
#endif
		int index = 0;
		for( int i = 0; i < this->dims; i++ )
		{
			if( Selector[ i ] && this->complexbydim[ i ] )
				index += ( int ) pow( this->dims - i - 1, 2.0 );
#ifdef BEC_NMRDATA_DEBUG
			else if( Selector[ i ] ) throw Exceptions::DimNotComplex();
#endif
		}
		return this->complexval[ index ];
	}


	inline Value Value::operator[]( bool Selector )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->components ) throw Exceptions::ValueIsEmpty();
		else if( this->components == 1 ) throw Exceptions::BadComponentSelector();

		if( this->selectordim == this->dims ) throw Exceptions::BadChainedSelector();
#endif
		Value NewVal( *this );
		if( Selector )
			NewVal.selectorindex += ( int ) pow( this->dims - this->selectordim - 1, 2.0 );
		NewVal.selectordim++;
		return NewVal;
	}


	inline const Value Value::operator[]( bool Selector ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->components ) throw Exceptions::ValueIsEmpty();
		else if( this->components == 1 ) throw Exceptions::BadComponentSelector();

		if( this->selectordim == this->dims ) throw Exceptions::BadChainedSelector();
#endif
		Value NewVal( *this );
		if( Selector )
			NewVal.selectorindex += ( int ) pow( this->dims - this->selectordim - 1, 2.0 );
		NewVal.selectordim++;
		return NewVal;
	}


	inline Value::operator float &( void )
	{
		if( this->dims && this->selectordim == this->dims )
			return this->complexval[ this->selectorindex ];
#ifdef BEC_NMRDATA_DEBUG
		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( this->components == 1 ) 
#endif
			return this->realval;
#ifdef BEC_NMRDATA_DEBUG
		else throw Exceptions::ConversionRequiresSelection();
#endif
	}


	inline Value::operator const float &( void ) const
	{
		if( this->dims && this->selectordim == this->dims )
			return this->complexval[ this->selectorindex ];
#ifdef BEC_NMRDATA_DEBUG
		if( !this->components ) throw Exceptions::ValueIsEmpty();
		if( this->components == 1 ) 
#endif
			return this->realval;
#ifdef BEC_NMRDATA_DEBUG
		else throw Exceptions::ConversionRequiresSelection();
#endif
	}


	template< typename Function > inline Function Value::ApplyToComponents( Function F )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 ) this->realval = F( this->realval );
		else
		{
			for( int i = 0; i < this->components; i++ )
				this->complexval[ i ] = F( this->complexval[ i ] );
		}

		return F;
	}


	inline bool Value::IsCompatible( Value &Operand ) const
	{
		if( Operand.components != this->components ) return false;
		if( Operand.dims != this->dims ) return false;
		return true;
	}


	inline bool Value::IsComplex( void ) const
	{
		if( this->components > 1 ) return true;
		else return false;
	}


	inline bool Value::IsEmpty( void ) const
	{
		if( !this->components ) return true;
		else return false;
	}


	inline std::vector< bool > Value::GetComplexByDim( void ) const
	{
		std::vector< bool > out;
		for( int i = 0; i < this->dims; i++ )
			out.push_back( this->complexbydim[ i ] );
		return out;
	}


	inline int Value::GetNumOfComponents( void ) const
	{
		return this->components;
	}


	inline int Value::GetNumOfDims( void ) const
	{
		return this->dims;
	}


	inline float Value::GetRealValue( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->components ) throw Exceptions::ValueIsEmpty();
#endif
		if( this->components == 1 ) return this->realval;
		else return this->complexval[ 0 ];
	}


	inline float Value::GetSelectedVal( void ) const
	{
		if( this->components == 1 ) return this->realval;
#ifdef BEC_NMRDATA_DEBUG
		else if( this->dims && this->selectordim == this->dims )
#endif
			return this->complexval[ this->selectorindex ];
#ifdef BEC_NMRDATA_DEBUG
		else throw Exceptions::BadChainedSelector();
#endif
	}


	inline bool Value::Selected( void ) const
	{
		if( this->dims && this->selectordim == this->dims ) return true;
		else return false;
	}


	inline bool operator<( float Scalar, const Value &Operand )
	{
		return Operand >= Scalar;
	}


	inline bool operator<=( float Scalar, const Value &Operand )
	{
		return Operand > Scalar;
	}


	inline bool operator>( float Scalar, const Value &Operand )
	{
		return Operand <= Scalar;
	}


	inline bool operator>=( float Scalar, const Value &Operand )
	{
		return Operand < Scalar;
	}


	inline Value operator+( const Value &Operand1, const Value &Operand2 )
	{
		Value out( Operand1 );
		out += Operand2;
		return out;
	}


	inline Value operator+( float Scalar, const Value &Operand )
	{
		Value out( Operand );
		out += Scalar;
		return out;
	}


	inline Value operator+( const Value &Operand, float Scalar )
	{
		Value out( Operand );
		out += Scalar;
		return out;
	}


	inline Value operator-( const Value &Operand1, const Value &Operand2 )
	{
		Value out( Operand1 );
		out -= Operand2;
		return out;
	}


	inline Value operator-( float Scalar, const Value &Operand )
	{
		Value out( Scalar );
		out -= Operand;
		return out;
	}


	inline Value operator-( const Value &Operand, float Scalar )
	{
		Value out( Operand );
		out -= Scalar;
		return out;
	}


	inline Value operator*( const Value &Operand1, const Value &Operand2 )
	{
		Value out( Operand1 );
		out *= Operand2;
		return out;
	}


	inline Value operator*( float Scalar, const Value &Operand )
	{
		Value out( Operand );
		out *= Scalar;
		return out;
	}


	inline Value operator*( const Value &Operand, float Scalar )
	{
		Value out( Operand );
		out *= Scalar;
		return out;
	}


	inline Value operator/( const Value &Operand1, const Value &Operand2 )
	{
		Value out( Operand1 );
		out /= Operand2;
		return out;
	}


	inline Value operator/( float Scalar, const Value &Operand )
	{
		Value out( Scalar );
		out /= Operand;
		return out;
	}


	inline Value operator/( const Value &Operand, float Scalar )
	{
		Value out( Operand );
		out /= Scalar;
		return out;
	}


	template< typename ParentClass >
	inline DimPosition< ParentClass >::DimPosition( const DimPosition &Source ) :
		parent( Source.parent ), dim( Source.dim ), pos( Source.pos ), fpos( Source.fpos ), units( Source.units ) {  }

	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > & DimPosition< ParentClass >::operator=( const DimPosition< ParentClass2 > &Source )
	{
		if( this->units == pts )
		{
			int ival = Source.GetPosPts();
			this->SetPosPts( ival );
		}
		else
		{
			float fval;
			if( this->units == Hz )
				fval = Source.GetPosHz();
			else if( this->units == ppm )
				fval = Source.GetPosPPM();
			else if( this->units == sec )
				fval = Source.GetPosSec();
			this->SetPos( fval );
		}
		return *this;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator=( T Value )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->CheckRange( Value ) ) throw Exceptions::PosOutOfRange();
#endif

		this->SetPos( Value );
		return *this;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	bool inline DimPosition< ParentClass >::operator==( const DimPosition< ParentClass2 > &Operand ) const
	{
		if( this->units == Hz )
			return fpos == Operand.GetPosHz();
		else if( this->units == ppm )
			return fpos == Operand.GetPosPPM();
		else if( this->units == pts )
			return pos == Operand.GetPosPts();
		else if( this->units == sec )
			return fpos == Operand.GetPosSec();
	}


	template< typename ParentClass > template< typename T > 
	bool inline DimPosition< ParentClass >::operator==( T Value ) const
	{
		if( this->units == pts )
			return pos == Value;
		else
			return fpos == Value;
	}

	template< typename ParentClass > template< typename ParentClass2 >
	bool inline DimPosition< ParentClass >::operator!=( const DimPosition< ParentClass2 > &Operand ) const
	{
		return !( *this == Operand );
	}


	template< typename ParentClass > template< typename T > 
	bool inline DimPosition< ParentClass >::operator!=( T Value ) const
	{
		return !( *this == Value );
	}


	template< typename ParentClass > template< typename ParentClass2 >
	bool inline DimPosition< ParentClass >::operator<( const DimPosition< ParentClass2 > &Operand ) const
	{
		if( this->units == Hz )
			return fpos < Operand.GetPosHz();
		else if( this->units == ppm )
			return fpos < Operand.GetPosPPM();
		else if( this->units == pts )
			return pos < Operand.GetPosPts();
		else if( this->units == sec )
			return fpos < Operand.GetPosSec();
	}


	template< typename ParentClass > template< typename T > 
	bool inline DimPosition< ParentClass >::operator<( T Value ) const
	{
		if( this->units == pts )
			return pos == Value;
		else
			return fpos == Value;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	bool inline DimPosition< ParentClass >::operator<=( const DimPosition< ParentClass2 > &Operand ) const
	{
		return *this < Operand || *this == Operand;
	}


	template< typename ParentClass > template< typename T > 
	bool inline DimPosition< ParentClass >::operator<=( T Value ) const
	{
		return *this < Value || *this == Value;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	bool inline DimPosition< ParentClass >::operator>( const DimPosition< ParentClass2 > &Operand ) const
	{
		return Operand < *this;
	}


	template< typename ParentClass > template< typename T > 
	bool inline DimPosition< ParentClass >::operator>( T Value ) const
	{
		return Value < *this;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	bool inline DimPosition< ParentClass >::operator>=( const DimPosition< ParentClass2 > &Operand ) const
	{
		return Operand > *this || *this == Operand;
	}


	template< typename ParentClass > template< typename T > 
	bool inline DimPosition< ParentClass >::operator>=( T Value ) const
	{
		return Value > *this || *this == Value;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator+=( const DimPosition< ParentClass2 > &Operand )
	{
		if( this->units == Hz )
		{
			float fval = Operand.GetPosHz();
			this->SetPosHz( fval + fpos );
		}
		else if( this->units == ppm )
		{
			float fval = Operand.GetPosPPM();
			this->SetPosPPM( fval + fpos );
		}
		else if( this->units == pts )
		{
			int val = Operand.GetPosPts();
			this->SetPosPts( val + pos );
		}
		else if( this->units == sec )
		{
			float fval = Operand.GetPosSec();
			this->SetPosSec( fval + fpos );
		}
		return *this;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator+=( T Value )
	{
		if( this->units == pts )
			this->SetPosPts( pos + ( int ) Value );
		else
			this->SetPos( fpos + ( float ) Value );
		return *this;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator-=( const DimPosition< ParentClass2 > &Operand )
	{
		if( this->units == Hz )
		{
			float fval = Operand.GetPosHz();
			this->SetPosHz( fval - fpos );
		}
		else if( this->units == ppm )
		{
			float fval = Operand.GetPosPPM();
			this->SetPosPPM( fval - fpos );
		}
		else if( this->units == pts )
		{
			int val = Operand.GetPosPts();
			this->SetPosPts( val - pos );
		}
		else if( this->units == sec )
		{
			float fval = Operand.GetPosSec();
			this->SetPosSec( fval - fpos );
		}
		return *this;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator-=( T Value )
	{
		if( this->units == pts )
			this->SetPosPts( pos - ( int ) Value );
		else
			this->SetPos( fpos - ( float ) Value );
		return *this;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator*=( const DimPosition< ParentClass2 > &Operand )
	{
		if( this->units == Hz )
		{
			float fval = Operand.GetPosHz();
			this->SetPosHz( fval * fpos );
		}
		else if( this->units == ppm )
		{
			float fval = Operand.GetPosPPM();
			this->SetPosPPM( fval * fpos );
		}
		else if( this->units == pts )
		{
			int val = Operand.GetPosPts();
			this->SetPosPts( val * pos );
		}
		else if( this->units == sec )
		{
			float fval = Operand.GetPosSec();
			this->SetPosSec( fval * fpos );
		}
		return *this;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator*=( T Value )
	{
		if( this->units == pts )
			this->SetPosPts( pos * ( int ) Value );
		else
			this->SetPos( fpos * ( float ) Value );
		return *this;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator/=( const DimPosition< ParentClass2 > &Operand )
	{
		if( this->units == Hz )
		{
			float fval = Operand.GetPosHz();
			this->SetPosHz( fval / fpos );
		}
		else if( this->units == ppm )
		{
			float fval = Operand.GetPosPPM();
			this->SetPosPPM( fval / fpos );
		}
		else if( this->units == pts )
		{
			int val = Operand.GetPosPts();
			this->SetPosPts( val / pos );
		}
		else if( this->units == sec )
		{
			float fval = Operand.GetPosSec();
			this->SetPosSec( fval / fpos );
		}
		return *this;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator/=( T Value )
	{
		if( this->units == pts )
			this->SetPosPts( pos / ( int ) Value );
		else
			this->SetPos( fpos / ( float ) Value );
		return *this;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator+( const DimPosition< ParentClass2 > &Operand )
	{
		DimPosition< ParentClass > out( *this );
		out += Operand;
		return out;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator+( T Value )
	{
		DimPosition< ParentClass > out( *this );
		out += Value;
		return out;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator-( const DimPosition< ParentClass2 > &Operand )
	{
		DimPosition< ParentClass > out( *this );
		out -= Operand;
		return out;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator-( T Value )
	{
		DimPosition< ParentClass > out( *this );
		out -= Value;
		return out;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator*( const DimPosition< ParentClass2 > &Operand )
	{
		DimPosition< ParentClass > out( *this );
		out *= Operand;
		return out;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator*( T Value )
	{
		DimPosition< ParentClass > out( *this );
		out *= Value;
		return out;
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator/( const DimPosition< ParentClass2 > &Operand )
	{
		DimPosition< ParentClass > out( *this );
		out /= Operand;
		return out;
	}


	template< typename ParentClass > template< typename T > 
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator/( T Value )
	{
		DimPosition< ParentClass > out( *this );
		out /= Value;
		return out;
	}


	template< typename ParentClass >
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator++( void )
	{
		this->SetPosPts( pos + 1 );
		return *this;
	}


	template< typename ParentClass >
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator++( int )
	{
		DimPosition< ParentClass > out( *this );
		this->SetPosPts( pos + 1 );
		return out;
	}


	template< typename ParentClass >
	inline DimPosition< ParentClass > &DimPosition< ParentClass >::operator--( void )
	{
		this->SetPosPts( pos - 1 );
		return *this;
	}


	template< typename ParentClass >
	inline DimPosition< ParentClass > DimPosition< ParentClass >::operator--( int )
	{
		DimPosition< ParentClass > out( *this );
		this->SetPosPts( pos - 1 );
		return out;
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::Attach( const ParentClass *NewParent, int NewDim )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !NewParent ) throw Exceptions::NullPointerArg();
		if( NewDim < 0 || NewDim > NewParent->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		this->parent = NewParent;
		this->dim = NewDim;
		this->SetPosPts( 0 );
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::AttachToSame( const DimPosition< ParentClass > &Source )
	{
		this->parent = Source.parent;
		this->dim = Source.dim;
		this->SetPosPts( 0 );
	}


	template< typename ParentClass > template< typename ParentClass2 >
	inline bool DimPosition< ParentClass >::CheckRange( const DimPosition< ParentClass2 > &Operand ) const
	{
		if( this->units == Hz )
		{
			float fval = Operand.GetPosHz();
			if( fval < this->parent->GetDimensionCenterHz( this->dim ) - ( this->parent->GetDimensionSW( this->dim ) / 2.0F ) ||
				fval > ( this->parent->GetDimensionSW( this->dim ) / 2.0F ) )
				return false;
		}
		else if( this->units == ppm )
		{
			float fval = Operand.GetPosPPM();
			if( fval < this->parent->GetDimensionCenterPPM( this->dim ) - ( this->parent->GetDimensionSW( this->dim ) / ( 2.0F * this->parent->GetDimensionSF( this->dim ) ) ) ||
				fval > ( this->parent->GetDimensionSW( this->dim ) / ( 2.0F * this->parent->GetDimensionSF( this->dim ) ) ) )
				return false;
		}
		else if( this->units == pts )
		{
			int val = Operand.GetPosPts();
			if( val < 0 || val > this->parent->GetDimensionSize( this->dim ) )
				return false;
		}
		else if( this->units == sec )
		{
			float fval = Operand.GetPosSec();
			if( fval < this->parent->GetDimensionStartTime( this->dim ) || fval > this->parent->GetDimensionEndTime( this->dim ) )
				return false;
		}
		return true;
	}

	template<> template< typename ParentClass2 >
	inline bool DimPosition< NMRData >::CheckRange( const DimPosition< ParentClass2 > &Operand ) const
	{
		if( this->units == Hz )
		{
			float fval = Operand.GetPosHz();
			if( fval < 0 - ( this->parent->DimensionScalesFreq[ this->dim ] / 2.0F ) ||
				fval > ( this->parent->DimensionScalesFreq[ this->dim ] / 2.0F ) )
				return false;
		}
		else if( this->units == ppm )
		{
			float fval = Operand.GetPosPPM();
			if( fval < 0 - ( this->parent->DimensionScalesPPM[ this->dim ] / 2.0F ) ||
				fval > ( this->parent->DimensionScalesPPM[ this->dim ] / 2.0F ) )
				return false;
		}
		else if( this->units == pts )
		{
			int val = Operand.GetPosPts();
			if( val < 0 || val > this->parent->DimensionSizes[ this->dim ] )
				return false;
		}
		else if( this->units == sec )
		{
			float fval = Operand.GetPosSec();
			if( fval < 0 ||	fval > this->parent->DimensionSizes[ this->dim ] / this->parent->DimensionScalesFreq[ this->dim ] )
				return false;
		}
		return true;
	}


	template< typename ParentClass > template< typename T > 
	inline bool DimPosition< ParentClass >::CheckRange( T Value ) const
	{
		if( this->units == Hz )
		{
			if( Value < this->parent->GetDimensionCenterHz( this->dim ) - ( this->parent->GetDimensionSW( this->dim ) / 2.0F ) ||
				Value > ( this->parent->GetDimensionSW( this->dim ) / 2.0F ) )
				return false;
		}
		else if( this->units == ppm )
		{
			if( Value < this->parent->GetDimensionCenterPPM( this->dim ) - ( this->parent->GetDimensionSW( this->dim ) / ( 2.0F * this->parent->GetDimensionSF( this->dim ) ) ) ||
				Value > ( this->parent->GetDimensionSW( this->dim ) / ( 2.0F * this->parent->GetDimensionSF( this->dim ) ) ) )
				return false;
		}
		else if( this->units == pts )
		{
			if( Value < 0 || Value > this->parent->GetDimensionSize( this->dim ) )
				return false;
		}
		else if( this->units == sec )
		{
			if( Value < this->parent->GetDimensionStartTime( this->dim ) || Value > this->parent->GetDimensionEndTime( this->dim ) )
				return false;
		}
		return true;
	}


	template<> template< typename T > 
	inline bool DimPosition< NMRData >::CheckRange( T Value ) const
	{
		if( this->units == Hz )
		{
			if( Value < 0 - ( this->parent->DimensionScalesFreq[ this->dim ] / 2.0F ) ||
				Value > ( this->parent->DimensionScalesFreq[ this->dim ] / 2.0F ) )
				return false;
		}
		else if( this->units == ppm )
		{
			if( Value < 0 - ( this->parent->DimensionScalesPPM[ this->dim ] / 2.0F ) ||
				Value > ( this->parent->DimensionScalesPPM[ this->dim ] / 2.0F ) )
				return false;
		}
		else if( this->units == pts )
		{
			if( Value < 0 || Value > this->parent->DimensionSizes[ this->dim ] )
				return false;
		}
		else if( this->units == sec )
		{
			if( Value < 0 || Value > this->parent->DimensionSizes[ this->dim ] / this->parent->DimensionScalesFreq[ this->dim ] )
				return false;
		}
		return true;
	}


	template< typename ParentClass >
	inline const ParentClass * DimPosition< ParentClass >::GetParent( void ) const
	{
		return this->parent;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetCenterPPM( void ) const
	{
		return this->parent->GetDimensionCenterPPM( this->dim );
	}

	template<>
	inline float DimPosition< NMRData >::GetCenterPPM( void ) const
	{
		return this->parent->DimensionCentersPPM[ this->dim ];
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetDownfieldHz( void ) const
	{
		return this->parent->GetDimensionCenterHz( this->dim ) - this->parent->GetDimensionSW( this->dim ) / 2.0F;
	}


	template<>
	inline float DimPosition< NMRData >::GetDownfieldHz( void ) const
	{
		return -1.0F * this->parent->DimensionScalesFreq[ this->dim ] / 2.0F;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetDownfieldPPM( void ) const
	{
		return this->parent->GetDimensionCenterPPM( this->dim ) + 
			this->parent->GetDimensionSW( this->dim ) / ( 2.0F * this->parent->GetDimensionSF( this->dim ) );
	}


	template<>
	inline float DimPosition< NMRData >::GetDownfieldPPM( void ) const
	{
		return this->parent->DimensionOriginsPPM[ this->dim ];
	}


	template< typename ParentClass >
	inline int DimPosition< ParentClass >::GetDimNumber( void ) const
	{
		return this->dim;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetDwellTime( void ) const
	{
		return 1.0F / this->parent->GetDimensionSW( this->dim );
	}

	template<>
	inline float DimPosition< NMRData >::GetDwellTime( void ) const
	{
		return 1.0F / this->parent->DimensionScalesFreq[ this->dim ];
	}


	template< typename ParentClass >
	inline char * DimPosition< ParentClass >::GetLabel( void ) const
	{
		return this->parent->GetDimensionLabel( this->dim );
	}


	template< typename ParentClass >
	inline std::string DimPosition< ParentClass >::GetLabelStr( void ) const
	{
		return this->parent->GetDimensionLabelStr( this->dim );
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetMinEvolutionTime( void ) const
	{
		return this->parent->GetDimensionStartTime( this->dim );
	}

	template<>
	inline float DimPosition< NMRData >::GetMinEvolutionTime( void ) const
	{
		return 0.0F;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetMaxEvolutionTime( void ) const
	{
		return this->parent->GetDimensionEndTime( this->dim );
	}

	template<>
	inline float DimPosition< NMRData >::GetMaxEvolutionTime( void ) const
	{
		return ( ( float ) this->parent->DimensionSizes[ this->dim ] ) / this->parent->DimensionScalesFreq[ this->dim ];
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetPosHz( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->parent->GetDimensionTD( this->dim ) ) throw Exceptions::DimNotFD();
#endif

		if( this->units == Hz ) return fpos;
		else return this->parent->GetDimensionCenterHz( this->dim ) - this->parent->GetDimensionSW( this->dim ) / 2.0F +
			( float ) pos * this->parent->GetDimensionSW( this->dim ) / ( ( float ) this->parent->GetDimensionSize( this->dim ) );
	}

	template<>
	inline float DimPosition< NMRData >::GetPosHz( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->parent->IsTimeDomainByDim[ this->dim ] ) throw Exceptions::DimNotFD();
#endif

		if( this->units == Hz ) return fpos;
		else return -1.0F * this->parent->DimensionScalesFreq[ this->dim ] / 2.0F + 
			( float ) pos * this->parent->DimensionScalesFreq[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] );
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetPosPPM( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->parent->GetDimensionTD( this->dim ) ) throw Exceptions::DimNotFD();
#endif

		if( this->units == Hz ) return fpos;
		else return this->GetDownfieldPPM() - 
			( float ) pos * this->parent->GetDimensionSW( this->dim ) / 
			( ( float ) this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSF( this->dim ) );
	}


	template<>
	inline float DimPosition< NMRData >::GetPosPPM( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->parent->IsTimeDomainByDim[ this->dim ] ) throw Exceptions::DimNotFD();
#endif

		if( this->units == Hz ) return fpos;
		else return this->parent->DimensionOriginsPPM[ this->dim ] - 
			( float ) pos * this->parent->DimensionScalesPPM[ this->dim ] / this->parent->DimensionSizes[ this->dim ];
	}


	template< typename ParentClass >
	inline int DimPosition< ParentClass >::GetPosPts( void ) const
	{
		return pos;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetPosSec( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->parent->GetDimensionTD( this->dim ) ) throw Exceptions::DimNotTD();
#endif

		if( this->units == sec ) return fpos;
		else return this->parent->GetDimensionStartTime( this->dim ) + ( float ) pos / this->parent->GetDimensionSW( this->dim );
	}


	template<>
	inline float DimPosition< NMRData >::GetPosSec( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->parent->IsTimeDomainByDim[ this->dim ] ) throw Exceptions::DimNotTD();
#endif

		if( this->units == sec ) return fpos;
		else return ( float ) pos / this->parent->DimensionScalesFreq[ this->dim ];
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetResolutionHz( void ) const
	{
		return this->parent->GetDimensionSW( this->dim ) / ( ( float ) this->parent->GetDimensionSize( this->dim ) );
	}


	template<>
	inline float DimPosition< NMRData >::GetResolutionHz( void ) const
	{
		return this->parent->DimensionScalesFreq[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] );
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetResolutionPPM( void ) const
	{
		return this->parent->GetDimensionSW( this->dim ) / 
			( ( float ) this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSF( this->dim ) );
	}


	template<>
	inline float DimPosition< NMRData >::GetResolutionPPM( void ) const
	{
		return this->parent->DimensionScalesPPM[ this->dim ] / this->parent->DimensionSizes[ this->dim ];
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetSF( void ) const
	{
		return this->parent->GetDimensionCenterPPM( this->dim );
	}


	template<>
	inline float DimPosition< NMRData >::GetSF( void ) const
	{
		return this->parent->DimensionCentersFreq[ this->dim ];
	}


	template< typename ParentClass >
	inline int DimPosition< ParentClass >::GetSize( void ) const
	{
		return this->parent->GetDimensionSize( this->dim );
	}


	template<>
	inline int DimPosition< NMRData >::GetSize( void ) const
	{
		return this->parent->DimensionSizes[ this->dim ];
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetSW( void ) const
	{
		return this->parent->GetDimensionSW( this->dim );
	}


	template<>
	inline float DimPosition< NMRData >::GetSW( void ) const
	{
		return this->parent->DimensionScalesFreq[ this->dim ];
	}


	template< typename ParentClass >
	inline UnitsType DimPosition< ParentClass >::GetUnits( void ) const
	{
		return this->units;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetUpfieldHz( void ) const
	{
		return this->parent->GetDimensionCenterHz( this->dim ) + this->parent->GetDimensionSW( this->dim ) / 2.0F;
	}


	template<>
	inline float DimPosition< NMRData >::GetUpfieldHz( void ) const
	{
		return this->parent->DimensionScalesFreq[ this->dim ] / 2.0F;
	}


	template< typename ParentClass >
	inline float DimPosition< ParentClass >::GetUpfieldPPM( void ) const
	{
		return this->GetDownfieldPPM() + this->parent->GetDimensionSW( this->dim ) / this->parent->GetDimensionSF( this->dim );
	}


	template<>
	inline float DimPosition< NMRData >::GetUpfieldPPM( void ) const
	{
		return this->parent->DimensionOriginsPPM[ this->dim ] + this->parent->DimensionScalesPPM[ this->dim ];
	}


	template< typename ParentClass >
	inline bool DimPosition< ParentClass >::InRange( void ) const
	{
		return pos >= 0 && pos < this->parent->GetDimensionSize( this->dim );
	}


	template<>
	inline bool DimPosition< NMRData >::InRange( void ) const
	{
		return pos >= 0 && pos < this->parent->DimensionSizes[ this->dim ];
	}


	template< typename ParentClass >
	inline bool DimPosition< ParentClass >::IsTimeDomain( void ) const
	{
		return this->parent->GetDimensionTD( this->dim );
	}


	template<>
	inline bool DimPosition< NMRData >::IsTimeDomain( void ) const
	{
		return this->parent->IsTimeDomainByDim[ this->dim ];
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::MoveToCenter( void )
	{
		this->SetPosPts( this->parent->GetDimensionSize( this->dim ) / 2 );
	}


	template<>
	inline void DimPosition< NMRData >::MoveToCenter( void )
	{
		this->SetPosPts( this->parent->DimensionSizes[ this->dim ] / 2 );
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::MoveToDownfieldEnd( void )
	{
		this->SetPosPts( 0 );
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::MoveToFIDStart( void )
	{
		this->SetPosPts( 0 );
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::MoveToFIDEnd( void )
	{
		this->SetPosPts( this->parent->GetDimensionSize( this->dim ) - 1 );
	}


	template<>
	inline void DimPosition< NMRData >::MoveToFIDEnd( void )
	{
		this->SetPosPts( this->parent->DimensionSizes[ this->dim ] - 1 );
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::MoveToUpfieldEnd( void )
	{
		this->SetPosPts( this->parent->GetDimensionSize( this->dim ) - 1 );
	}


	template<>
	inline void DimPosition< NMRData >::MoveToUpfieldEnd( void )
	{
		this->SetPosPts( this->parent->DimensionSizes[ this->dim ] - 1 );
	}


	template< typename ParentClass > template< typename T > 
	inline void DimPosition< ParentClass >::SetPos( T Value )
	{
		if( this->units == Hz )
		{
			this->pos = ( int ) 
				( ( Value - this->GetDownfieldHz() ) * 
				( float ) this->parent->GetDimensionSize( this->dim ) / this->parent->GetDimensionSW( this->dim ) );
			this->fpos = ( float ) Value;
		}
		else if( this->units == ppm )
		{
			this->pos = ( int ) 
				( ( this->GetDownfieldPPM() - Value ) * 
				( float ) this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSF( this->dim ) / 
				this->parent->GetDimensionSW( this->dim ) );
			this->fpos = ( float ) Value;
		}
		else if( this->units == pts )
		{
			this->pos = ( int ) Value;
			this->fpos = ( float ) Value;
		}
		else if( this->units == sec )
		{
			this->pos = ( int ) ( ( Value - this->parent->GetDimensionStartTime( this->dim ) ) * this->parent->GetDimensionSW( this->dim ) );
			this->fpos = ( float ) Value;
		}
	}


	template<> template< typename T > 
	inline void DimPosition< NMRData >::SetPos( T Value )
	{
		if( this->units == Hz )
		{
			this->pos = ( int ) 
				( ( Value + this->parent->DimensionScalesFreq[ this->dim ] / 2.0F ) * 
				( float ) this->parent->DimensionSizes[ this->dim ] / this->parent->DimensionScalesFreq[ this->dim ] );
			this->fpos = ( float ) Value;
		}
		else if( this->units == ppm )
		{
			this->pos = ( int ) 
				( ( this->parent->DimensionOriginsPPM[ this->dim ] - Value ) * 
				this->parent->DimensionSizes[ this->dim ] / this->parent->DimensionScalesPPM[ this->dim ] );
			this->fpos = ( float ) Value;
		}
		else if( this->units == pts )
		{
			this->pos = ( int ) Value;
			this->fpos = ( float ) Value;
		}
		else if( this->units == sec )
		{
			this->pos = ( int ) ( Value * this->parent->DimensionScalesFreq[ this->dim ] );
			this->fpos = ( float ) Value;
		}
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::SetPosHz( float HzPos )
	{
		this->pos = ( int ) 
			( ( HzPos - this->GetDownfieldHz() ) * 
			( float ) this->parent->GetDimensionSize( this->dim ) / this->parent->GetDimensionSW( this->dim ) );

		if( this->units == Hz )
			this->fpos = HzPos;
		else if( this->units == ppm )
		{
			this->fpos = this->GetDownfieldPPM() - ( ( float ) this->pos ) * 
				this->parent->GetDimensionSF( this->dim ) / 
				( this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSW( this->dim ) );
		}
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = this->parent->GetDimensionStartTime( this->dim ) + ( ( float ) this->pos ) / this->parent->GetDimensionSW( this->dim );
		}
	}


	template<>
	inline void DimPosition< NMRData >::SetPosHz( float HzPos )
	{
		this->pos = ( int ) 
			( ( HzPos + this->parent->DimensionScalesFreq[ this->dim ] / 2.0F ) * 
			this->parent->DimensionSizes[ this->dim ] / this->parent->DimensionScalesFreq[ this->dim ] );

		if( this->units == Hz )
			this->fpos = HzPos;
		else if( this->units == ppm )
		{
			this->fpos = this->parent->DimensionOriginsPPM[ this->dim ] - ( ( float ) this->pos ) * 
				this->parent->DimensionScalesPPM[ this->dim ] / this->parent->DimensionSizes[ this->dim ];
		}
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = ( ( float ) this->pos ) / this->parent->DimensionScalesFreq[ this->dim ];
		}
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::SetPosPPM( float PPMPos )
	{
		this->pos = ( int ) 
				( ( this->parent->GetDownfieldPPM() - PPMPos ) * 
				( float ) this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSF / 
				this->parent->GetDimensionSW( this->dim ) );

		if( this->units == Hz )
		{
			this->fpos = ( ( float ) this->pos ) * 
				this->parent->GetDimensionSW( this->dim ) / ( ( float ) this->parent->GetDimensionSize( this->dim ) ) +
				this->GetDownfieldHz();
		}
		else if( this->units == ppm )
			this->fpos = PPMPos;
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = this->parent->GetDimensionStartTime( this->dim ) + ( ( float ) this->pos ) / this->parent->GetDimensionSW( this->dim );
		}
	}


	template<>
	inline void DimPosition< NMRData >::SetPosPPM( float PPMPos )
	{
		this->pos = ( int ) 
				( ( this->parent->DimensionOriginsPPM[ this->dim ] - PPMPos ) * 
				( float ) this->parent->DimensionSizes[ this->dim ] / this->parent->DimensionScalesPPM[ this->dim ] );

		if( this->units == Hz )
		{
			this->fpos = ( ( float ) this->pos ) * 
				this->parent->DimensionScalesFreq[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] ) -
				this->parent->DimensionScalesFreq[ this->dim ] / 2.0F;
		}
		else if( this->units == ppm )
			this->fpos = PPMPos;
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = ( ( float ) this->pos ) / this->parent->DimensionScalesFreq[ this->dim ];
		}
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::SetPosPts( int PtsPos )
	{
		this->pos = PtsPos;

		if( this->units == Hz )
		{
			this->fpos = ( ( float ) this->pos ) * 
				this->parent->GetDimensionSW( this->dim ) / this->parent->GetDimensionSize( this->dim ) +
				this->GetDownfieldHz();
		}
		else if( this->units == ppm )
		{
			this->fpos = this->GetDownfieldPPM() - ( ( float ) this->pos ) * 
				this->parent->GetDimensionSW( this->dim ) / 
				( ( float ) this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSF( this->dim ) );
		}
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = this->parent->GetDimensionStartTime( this->dim ) + ( ( float ) this->pos ) / this->parent->GetDimensionSW( this->dim );
		}
	}


	template<>
	inline void DimPosition< NMRData >::SetPosPts( int PtsPos )
	{
		this->pos = PtsPos;

		if( this->units == Hz )
		{
			this->fpos = ( ( float ) this->pos ) * 
				this->parent->DimensionScalesFreq[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] ) -
				this->parent->DimensionScalesFreq[ this->dim ] / 2.0F;
		}
		else if( this->units == ppm )
		{
			this->fpos = this->parent->DimensionOriginsPPM[ this->dim ] - ( ( float ) this->pos ) * 
				this->parent->DimensionScalesPPM[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] );
		}
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = ( ( float ) this->pos ) / this->parent->DimensionScalesFreq[ this->dim ];
		}
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::SetPosSec( float SecPos )
	{
		this->pos = ( int ) ( ( SecPos - this->parent->GetDimensionStartTime( this->dim ) ) * this->parent->GetDimensionSW( this->dim ) );

		if( this->units == Hz )
		{
			this->fpos = ( ( float ) this->pos ) * 
				this->parent->GetDimensionSW( this->dim ) / ( ( float ) this->parent->GetDimensionSize( this->dim ) ) +
				this->GetDownfieldHz();
		}
		else if( this->units == ppm )
		{
			this->fpos = this->GetDownfieldPPM() - ( ( float ) this->pos ) * 
				this->parent->GetDimensionSW( this->dim ) / 
				( ( float ) this->parent->GetDimensionSize( this->dim ) * this->parent->GetDimensionSF( this->dim ) );
		}
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = SecPos;
		}
	}


	template<>
	inline void DimPosition< NMRData >::SetPosSec( float SecPos )
	{
		this->pos = ( int ) ( SecPos * this->parent->DimensionScalesFreq[ this->dim ] );

		if( this->units == Hz )
		{
			this->fpos = ( ( float ) this->pos ) * 
				this->parent->DimensionScalesFreq[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] ) -
				this->parent->DimensionScalesFreq[ this->dim ] / 2.0F;
		}
		else if( this->units == ppm )
		{
			this->fpos = this->parent->DimensionOriginsPPM[ this->dim ] - ( ( float ) this->pos ) * 
				this->parent->DimensionScalesPPM[ this->dim ] / ( ( float ) this->parent->DimensionSizes[ this->dim ] );
		}
		else if( this->units == pts )
		{
			this->fpos = ( float ) this->pos;
		}
		else if( this->units == sec )
		{
			this->fpos = SecPos;
		}
	}


	template< typename ParentClass >
	inline void DimPosition< ParentClass >::SetUnits( UnitsType Units )
	{
		this->units = Units;
	}


	template< typename ParentClass >
	inline DimPosition< ParentClass >::DimPosition( const ParentClass *Parent, int Dim, int pos, float fpos, UnitsType Units ) : 
		parent( Parent ), dim( Dim ), units( Units )
	{
		if( this->units == pts )
		{
			this->pos = pos;
			this->fpos = fpos;
		}
		else this->SetPos( fpos );
	}


	inline Reference::Reference( const Reference &Source ) : value( Source.value ), page( Source.page ), parent( Source.parent ),
		offset( Source.offset ), pageindex( Source.pageindex ), cselectordim( Source.cselectordim ), ambig( Source.ambig ),
		cselectorindex( Source.cselectorindex ), pselectordim( Source.pselectordim ), pselectorcoord( Source.pselectorcoord ),
		isconst( Source.isconst ), pselectordimmax( Source.pselectordimmax ), subset( Source.subset ), dims_to_set( Source.dims_to_set )
	{
		this->update();
	}


	inline Reference &Reference::operator=( const Reference &Source )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Source.ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] = Source.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Source.Selected() )
		{
			this->value[ 0 ] = Source.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Source ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] = Source[ i ];

		return *this;
	}


	inline Reference &Reference::operator=( const Value &Source )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] = Source.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Source.Selected() )
		{
			this->update();
			this->value[ 0 ] = Source.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Source ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] = Source[ i ];

		return *this;
	}


	inline Reference &Reference::operator=( float RealVal )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] = RealVal;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		this->value[ 0 ] = RealVal;

		return *this;
	}


	inline bool Reference::operator==( const Reference &Operand ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ] == Operand.GetSelectedVal();
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		if( !this->IsCompatible( Operand ) ) return false;
		for( int i = 0; i < this->parent->Components; i++ )
			if( this->value[ i ] != Operand[ i ] ) return false;
		return true;
	}


	inline bool Reference::operator==( const Value &Operand ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ] == Operand.GetSelectedVal();
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		if( !this->IsCompatible( Operand ) ) return false;
		for( int i = 0; i < this->parent->Components; i++ )
			if( this->value[ i ] != Operand[ i ] ) return false;
		return true;
	}


	inline bool Reference::operator==( float RealVal ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ] == RealVal;
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		if( this->parent->Components > 1 ) return false;
		return this->value[ 0 ] == RealVal;
	}


	inline bool Reference::operator!=( const Reference &Operand ) const
	{
		return !( *this == Operand );
	}


	inline bool Reference::operator!=( const Value &Operand ) const
	{
		return !( *this == Operand );
	}


	inline bool Reference::operator!=( float RealVal ) const
	{
		return !( *this == RealVal );
	}


	inline bool Reference::operator<( const Reference &Operand ) const
	{
		if( this->ambig ) throw Exceptions::RefIsAmbig();
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ] < Operand.GetSelectedVal();
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			if( !( this->value[ i ] < Operand[ i ] ) ) return false;
		return true;
	}


	inline bool Reference::operator<( const Value &Operand ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ] < Operand.GetSelectedVal();
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif

		for( int i = 0; i < this->parent->Components; i++ )
			if( !( this->value[ i ] < Operand[ i ] ) ) return false;
		return true;
	}


	inline bool Reference::operator<( float RealVal ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ] < RealVal;
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( this->parent->Components > 1 ) throw Exceptions::NotCompatible();
#endif
		return this->value[ 0 ] < RealVal;
	}


	inline bool Reference::operator<=( const Reference &Operand ) const
	{
		return ( *this < Operand ) || ( *this == Operand );
	}


	inline bool Reference::operator<=( const Value &Operand ) const
	{
		return ( *this < Operand ) || ( *this == Operand );
	}


	inline bool Reference::operator<=( float RealVal ) const
	{
		return ( *this < RealVal ) || ( *this == RealVal );
	}


	inline bool Reference::operator>( const Reference &Operand ) const
	{
		return Operand < *this;
	}


	inline bool Reference::operator>( const Value &Operand ) const
	{
		return Operand < *this;
	}


	inline bool Reference::operator>( float RealVal ) const
	{
		return RealVal < *this;
	}


	inline bool Reference::operator>=( const Reference &Operand ) const
	{
		return ( Operand < *this ) || ( *this == Operand );
	}


	inline bool Reference::operator>=( const Value &Operand ) const
	{
		return ( Operand < *this ) || ( *this == Operand );
	}


	inline bool Reference::operator>=( float RealVal ) const
	{
		return ( RealVal < *this ) || ( *this == RealVal );
	}


	inline Reference &Reference::operator+=( const Reference &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] += Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] += Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] += Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator+=( const Value &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] += Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] += Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] += Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator+=( float Scalar )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] += Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] += Scalar;

		return *this;
	}


	inline Reference &Reference::operator-=( const Reference &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] -= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] -= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] -= Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator-=( const Value &Operand )
	{
		if( this->ambig ) throw Exceptions::RefIsAmbig();
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] -= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] -= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] -= Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator-=( float Scalar )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] -= Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] -= Scalar;

		return *this;
	}


	inline Reference &Reference::operator*=( const Reference &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] *= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] *= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] *= Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator*=( const Value &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] *= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] *= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] *= Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator*=( float Scalar )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] *= Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] *= Scalar;

		return *this;
	}


	inline Reference &Reference::operator/=( const Reference &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] /= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] /= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] /= Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator/=( const Value &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] /= Operand.GetSelectedVal();
			return *this;
		}
		else if( this->parent->Components == 1 && Operand.Selected() )
		{
			this->value[ 0 ] /= Operand.GetSelectedVal();
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();

		if( !this->IsCompatible( Operand ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] /= Operand[ i ];

		return *this;
	}


	inline Reference &Reference::operator/=( float Scalar )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();

		if( this->cselectordim == this->parent->NumOfDimensions )
		{
			this->value[ this->cselectorindex ] /= Scalar;
			return *this;
		}
#ifdef BEC_NMRDATA_DEBUG
		else if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
#endif

		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] /= Scalar;

		return *this;
	}


	inline float &Reference::operator[]( int Component )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
		if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( Component < 0 || Component > this->parent->Components ) throw Exceptions::BadComponentIndex();
#endif
		this->update();
		return this->value[ Component ];
	}


	inline const float &Reference::operator[]( int Component ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
		if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( Component < 0 || Component > this->parent->Components ) throw Exceptions::BadComponentIndex();
#endif
		this->update();
		return this->value[ Component ];
	}


	inline float &Reference::operator[]( std::vector< bool > Selector )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
		if( this->cselectordim > 0 ) throw Exceptions::BadChainedSelector();
		if( this->parent->Components == 1 ) throw Exceptions::BadComponentSelector();
		if( Selector.size() != this->parent->NumOfDimensions ) throw Exceptions::BadComponentSelector();
#endif
		this->update();

		int index = 0;
		for( int i = 0; i < this->parent->NumOfDimensions; i++ )
		{
			if( Selector[ i ] && this->parent->IsComplexByDim[ i ] )
				index += ( int ) pow( 2.0, this->parent->NumOfDimensions - i - 1 );
			else if( Selector[ i ] ) throw Exceptions::DimNotComplex();
		}
		return this->value[ index ];
	}


	inline const float &Reference::operator[]( std::vector< bool > Selector ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->parent->Components == 1 ) throw Exceptions::BadComponentSelector();
		if( Selector.size() != this->parent->NumOfDimensions ) throw Exceptions::BadComponentSelector();
#endif
		this->update();

		int index = 0;
		for( int i = 0; i < this->parent->NumOfDimensions; i++ )
		{
			if( Selector[ i ] && this->parent->IsComplexByDim[ i ] )
				index += ( int ) pow( 2.0, this->parent->NumOfDimensions - i - 1 );
			else if( Selector[ i ] ) throw Exceptions::DimNotComplex();
		}
		return this->value[ index ];
	}


	inline Reference Reference::operator[]( bool Selector )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->cselectordim == this->parent->NumOfDimensions ) throw Exceptions::BadChainedSelector();
#endif
		Reference NewRef( *this );
		if( Selector )
			NewRef.cselectorindex += ( int ) pow( 2.0, this->parent->NumOfDimensions - this->cselectordim - 1 );
		NewRef.cselectordim++;
		return NewRef;
	}


	inline const Reference Reference::operator[]( bool Selector ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->cselectordim == this->parent->NumOfDimensions ) throw Exceptions::BadChainedSelector();
#endif
		Reference NewRef( *this );
		if( Selector )
			NewRef.cselectorindex += ( int ) pow( 2.0, this->parent->NumOfDimensions - this->cselectordim - 1 );
		NewRef.cselectordim++;
		return NewRef;
	}


	inline Reference Reference::operator()( int Position )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim == this->pselectordimmax ) throw Exceptions::BadChainedSelector();
#endif
		if( this->subset )
		{
			Reference NewRef( *this );
			int dim = this->dims_to_set.front();
			NewRef.pselectorcoord[ dim ].SetPosPts( NewRef.pselectorcoord[ dim ].GetPosPts() + Position );
			NewRef.dims_to_set.pop();
			if( ++NewRef.pselectordim < this->pselectordimmax ) NewRef.ambig = true;
			else NewRef.SetSelectedPos();
			return NewRef;
		}
		Reference NewRef( *this );
		NewRef.pselectorcoord.push_back( DimPosition< NMRData >( this->parent, this->pselectordim, Position, ( float ) Position, pts ) );
		if( ++NewRef.pselectordim < this->parent->NumOfDimensions ) NewRef.ambig = true;
		else NewRef.SetSelectedPos();
		return NewRef;
	}


	inline const Reference Reference::operator()( int Position ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim == this->pselectordimmax ) throw Exceptions::BadChainedSelector();
#endif
		if( this->subset )
		{
			Reference NewRef( *this );
			int dim = this->dims_to_set.front();
			NewRef.pselectorcoord[ dim ].SetPosPts( Position );
			NewRef.dims_to_set.pop();
			if( ++NewRef.pselectordim < this->pselectordimmax ) NewRef.ambig = true;
			else NewRef.SetSelectedPos();
			return NewRef;
		}
		Reference NewRef( *this );
		NewRef.pselectorcoord.push_back( DimPosition< NMRData >( this->parent, this->pselectordim, Position, ( float ) Position, pts ) );
		if( ++NewRef.pselectordim < this->parent->NumOfDimensions ) NewRef.ambig = true;
		else NewRef.SetSelectedPos();
		return NewRef;
	}


	inline Reference Reference::operator()( const DimPosition< NMRData > &Position )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim == this->pselectordimmax ) throw Exceptions::BadChainedSelector();
		if( this->subset ) throw Exceptions::BadChainedSelector();
#endif
		Reference NewRef( *this );
		NewRef.pselectorcoord.push_back( Position );
		if( ++NewRef.pselectordim < this->parent->NumOfDimensions ) NewRef.ambig = true;
		else NewRef.SetSelectedPos();
		return NewRef;
	}


	inline Reference Reference::operator()( const DimPosition< Subset > &Position )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim == this->pselectordimmax ) throw Exceptions::BadChainedSelector();
		if( !this->subset ) throw Exceptions::BadChainedSelector();
#endif
		Reference NewRef( *this );
		int dim = this->dims_to_set.front();
		NewRef.pselectorcoord[ dim ].SetPosPts( Position.GetPosPts() + Position.parent->Data->Corner[ dim ] );
		NewRef.dims_to_set.pop();
		if( ++NewRef.pselectordim < this->pselectordimmax ) NewRef.ambig = true;
		else NewRef.SetSelectedPos();
		return NewRef;
	}


	inline const Reference Reference::operator()( const DimPosition< NMRData > &Position ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim == this->pselectordimmax ) throw Exceptions::BadChainedSelector();
		if( this->subset ) throw Exceptions::BadChainedSelector();
#endif
		Reference NewRef( *this );
		NewRef.pselectorcoord.push_back( Position );
		if( ++NewRef.pselectordim < this->parent->NumOfDimensions ) NewRef.ambig = true;
		else NewRef.SetSelectedPos();
		return NewRef;
	}


	inline const Reference Reference::operator()( const DimPosition< Subset > &Position ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim == this->pselectordimmax ) throw Exceptions::BadChainedSelector();
		if( !this->subset ) throw Exceptions::BadChainedSelector();
#endif
		Reference NewRef( *this );
		int dim = this->dims_to_set.front();
		NewRef.pselectorcoord[ dim ].SetPosPts( Position.GetPosPts() + Position.parent->Data->Corner[ dim ] );
		NewRef.dims_to_set.pop();
		if( ++NewRef.pselectordim < this->pselectordimmax ) NewRef.ambig = true;
		else NewRef.SetSelectedPos();
		return NewRef;
	}


	inline Reference::operator Value( void ) const
	{
		this->update();
		return Value( this->parent->IsComplexAvailableByDimVector(), this->value, this->cselectordim, this->cselectorindex );
	}


	inline Reference::operator float &( void )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();
		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ];
		if( this->parent->Components == 1 ) return this->value[ 0 ];
		else throw Exceptions::ConversionRequiresSelection();
	}


	inline Reference::operator const float &( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();
		if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ];
		if( this->parent->Components == 1 ) return this->value[ 0 ];
		else throw Exceptions::ConversionRequiresSelection();
	}


	template< typename Function > inline Function Reference::ApplyToComponents( Function F )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();
		for( int i = 0; i < this->parent->Components; i++ )
			this->value[ i ] = F( this->value[ i ] );

		return F;
	}


	inline void Reference::Assign( const Reference &Source )
	{
		this->value = Source.value;
		this->page = Source.page;
		this->parent = Source.parent;
		this->offset = Source.offset;
		this->pageindex = Source.pageindex;
		this->isconst = Source.isconst;
		this->ambig = Source.ambig;
		this->cselectordim = Source.cselectordim;
		this->pselectordim = Source.pselectordim;
		this->pselectorcoord = Source.pselectorcoord;
		this->cachedcoord = Source.cachedcoord;
		this->subset = Source.subset;
		this->dims_to_set = Source.dims_to_set;
	}


	inline Value Reference::ConvertToValue( void ) const
	{
		this->update();
		return Value( this->parent->IsComplexAvailableByDimVector(), this->value, this->cselectordim, this->cselectorindex );
	}


	inline bool Reference::IsCompatible( const Reference &Operand ) const
	{
		if( this->parent->Components != Operand.parent->Components ) return false;
		if( this->parent->Components > 1 )
		{
			if( this->parent->NumOfDimensions != Operand.parent->NumOfDimensions ) return false;

			for( int i = 0; i < this->parent->NumOfDimensions; i++ )
				if( this->parent->IsComplexByDim[ i ] != Operand.parent->IsComplexByDim[ i ] ) return false;
		}
		return true;
	}


	inline bool Reference::IsCompatible( const Value &Operand ) const
	{
		if( this->parent->Components != Operand.components ) return false;
		if( this->parent->Components > 1 )
		{
			if( this->parent->NumOfDimensions != Operand.dims ) return false;

			for( int i = 0; i < this->parent->NumOfDimensions; i++ )
				if( this->parent->IsComplexByDim[ i ] != Operand.complexbydim[ i ] ) return false;
		}
		return true;
	}


	inline bool Reference::IsComplex( void ) const
	{
		if( this->parent->Components > 1 ) return true;
		else return false;
	}


	inline std::vector< bool > Reference::GetComplexByDim( void ) const
	{
		return this->parent->IsComplexAvailableByDimVector();
	}


	inline std::vector< DimPosition< NMRData > > Reference::GetCoordinates( void ) const
	{
		if( this->cachedcoord.empty() ) 
			this->cachedcoord = this->parent->TranslateOffsetToCoord( this->offset );
		return this->cachedcoord;
	}


	inline size_t Reference::GetOffset( void ) const
	{
		return this->offset;
	}


	inline const NMRData *Reference::GetParent( void ) const
	{
		return this->parent;
	}


	inline float Reference::GetRealValue( void ) const
	{
		this->update();
		return this->value[ 0 ];
	}


	inline Value Reference::GetValue( void ) const
	{
		this->update();
		return Value( this->parent->IsComplexAvailableByDimVector(), this->value );
	}


	inline bool Reference::Selected( void ) const
	{
		if( this->ambig ) return false;
		else if( this->cselectordim != this->parent->NumOfDimensions ) return false;
		else return true;
	}


	inline Reference::Reference( size_t Offset, DataPage *Page, float *Ptr, bool ConstState, const NMRData *Parent ) : offset( Offset ), 
		page( Page ), value( Ptr ), isconst( ConstState ), parent( Parent ), pageindex( Page ? Page->Index : 0 ), cselectordim( 0 ), cselectorindex( 0 ), 
		pselectordim( 0 ), ambig( false ), subset( false )
	{
		this->update();
	}


	inline Reference::Reference( DimPosition< NMRData > FirstDimPos, bool ConstState, const NMRData *Parent ) : offset( 0 ), page( 0 ), value( 0 ),
		isconst( ConstState ), parent( Parent ), pageindex( 0 ), cselectordim( 0 ), cselectorindex( 0 ), pselectordim( 1 ), ambig( true ),
		subset( false )
	{
		this->pselectorcoord.push_back( FirstDimPos );
		this->pselectordimmax = this->parent->NumOfDimensions;
		if( this->pselectordim == this->parent->NumOfDimensions )
			this->SetSelectedPos();
	}


	inline Reference::Reference( std::vector< DimPosition< NMRData > > DimPosSet, std::queue< int > DimsToSet, bool ConstState, const NMRData *Parent ) :
		offset( 0 ), page( 0 ), value( 0 ), isconst( ConstState ), parent( Parent), pageindex( 0 ), cselectordim( 0 ), cselectorindex( 0 ),
			pselectordim( 1 ), ambig( true ), subset( true )
	{
		this->dims_to_set = DimsToSet;
		this->pselectorcoord = DimPosSet;
		this->pselectordimmax = ( int ) DimsToSet.size() + 1;
	}


	inline float Reference::GetSelectedVal( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->ambig ) throw Exceptions::RefIsAmbig();
#endif
		this->update();
		if( this->parent->Components == 1 ) return this->value[ 0 ];
		else if( this->cselectordim == this->parent->NumOfDimensions )
			return this->value[ this->cselectorindex ];
		else throw Exceptions::BadChainedSelector();
	}


	inline void Reference::SetSelectedPos( void ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( this->pselectordim != this->pselectordimmax ) throw Exceptions::BadChainedSelector();

		for( int i = 0; i < this->parent->NumOfDimensions; i++ )
			if( !this->pselectorcoord[ i ].InRange() ) throw Exceptions::BadCoordinates();
#endif

		this->offset = this->parent->TranslateCoordToOffset( this->pselectorcoord );
		this->value = this->parent->TranslateOffsetToPtr( this->offset, this->isconst, &( this->page ) );
		this->pageindex = this->page ? this->page->Index : 0;
		this->ambig = false;
		this->pselectorcoord.clear();
		this->pselectordim = 0;
		this->subset = false;
		this->pselectordimmax = 0;
	}


	inline void Reference::update( void ) const
	{
		if( this->page && this->page->Index != this->pageindex )
		{
			this->value = this->parent->TranslateOffsetToPtr( this->offset, this->isconst, &( this->page ) );
			this->pageindex = this->page ? this->page->Index : 0;
		}
	}


	inline bool operator<( const Value &Operand1, const Reference &Operand2 )
	{
		if( Operand1.Selected() && Operand2.Selected() )
			return ( float ) Operand1 < ( float ) Operand2;

#ifdef BEC_NMRDATA_DEBUG
		if( !Operand2.IsCompatible( Operand1 ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < Operand1.GetNumOfComponents(); i++ )
			if( !( Operand1[ i ] < Operand2[ i ] ) ) return false;
		return true;
	}


	inline bool operator<( float RealVal, const Reference &Operand )
	{
		if( Operand.Selected() )
			return RealVal < ( float ) Operand;

#ifdef BEC_NMRDATA_DEBUG
		if( Operand.IsComplex() ) throw Exceptions::NotCompatible();
#endif
		return RealVal < Operand[ 0 ];
	}


	inline bool operator<=( const Value &Operand1, const Reference &Operand2 )
	{
		if( Operand1.Selected() && Operand2.Selected() )
			return ( float ) Operand1 <= ( float ) Operand2;

#ifdef BEC_NMRDATA_DEBUG
		if( !Operand2.IsCompatible( Operand1 ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < Operand1.GetNumOfComponents(); i++ )
			if( !( Operand1[ i ] <= Operand2[ i ] ) ) return false;
		return true;
	}


	inline bool operator<=( float RealVal, const Reference &Operand )
	{
		if( Operand.Selected() )
			return RealVal <= ( float ) Operand;

#ifdef BEC_NMRDATA_DEBUG
		if( Operand.IsComplex() ) throw Exceptions::NotCompatible();
#endif
		return RealVal <= Operand[ 0 ];
	}


	inline bool operator>( const Value &Operand1, const Reference &Operand2 )
	{
		if( Operand1.Selected() && Operand2.Selected() )
			return ( float ) Operand1 > ( float ) Operand2;

#ifdef BEC_NMRDATA_DEBUG
		if( !Operand2.IsCompatible( Operand1 ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < Operand1.GetNumOfComponents(); i++ )
			if( !( Operand1[ i ] > Operand2[ i ] ) ) return false;
		return true;
	}


	inline bool operator>( float RealVal, const Reference &Operand )
	{
		if( Operand.Selected() )
			return RealVal > ( float ) Operand;

#ifdef BEC_NMRDATA_DEBUG
		if( Operand.IsComplex() ) throw Exceptions::NotCompatible();
#endif
		return RealVal > Operand[ 0 ];
	}

	inline bool operator>=( const Value &Operand1, const Reference &Operand2 )
	{
		if( Operand1.Selected() && Operand2.Selected() )
			return ( float ) Operand1 >= ( float ) Operand2;

#ifdef BEC_NMRDATA_DEBUG
		if( !Operand2.IsCompatible( Operand1 ) ) throw Exceptions::NotCompatible();
#endif
		for( int i = 0; i < Operand1.GetNumOfComponents(); i++ )
			if( !( Operand1[ i ] >= Operand2[ i ] ) ) return false;
		return true;
	}


	inline bool operator>=( float RealVal, const Reference &Operand )
	{
		if( Operand.Selected() )
			return RealVal >= ( float ) Operand;

#ifdef BEC_NMRDATA_DEBUG
		if( Operand.IsComplex() ) throw Exceptions::NotCompatible();
#endif
		return RealVal >= Operand[ 0 ];
	}


	inline Value operator+( const Reference &Operand1, const Reference &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out += Operand2;
		return out;
	} 


	inline Value operator+( const Value &Operand1, const Reference &Operand2 )
	{
		Value out( Operand1 );
		out += Operand2;
		return out;
	}


	inline Value operator+( float RealVal, const Reference &Operand )
	{
		Value out = Operand.ConvertToValue();
		out += RealVal;
		return out;
	}


	inline Value operator+( const Reference &Operand1, const Value &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out += Operand2;
		return out;
	}


	inline Value operator+( const Reference &Operand1, float RealVal )
	{
		Value out = Operand1.ConvertToValue();
		out += RealVal;
		return out;
	}


	inline Value operator-( const Reference &Operand1, const Reference &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out -= Operand2;
		return out;
	} 


	inline Value operator-( const Value &Operand1, const Reference &Operand2 )
	{
		Value out( Operand1 );
		out -= Operand2;
		return out;
	}


	inline Value operator-( float RealVal, const Reference &Operand )
	{
		Value out( RealVal );
		out -= Operand;
		return out;
	}


	inline Value operator-( const Reference &Operand1, const Value &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out -= Operand2;
		return out;
	}


	inline Value operator-( const Reference &Operand1, float RealVal )
	{
		Value out = Operand1.ConvertToValue();
		out -= RealVal;
		return out;
	}


	inline Value operator*( const Reference &Operand1, const Reference &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out *= Operand2;
		return out;
	} 


	inline Value operator*( const Value &Operand1, const Reference &Operand2 )
	{
		Value out( Operand1 );
		out *= Operand2;
		return out;
	}


	inline Value operator*( float RealVal, const Reference &Operand )
	{
		Value out = Operand.ConvertToValue();
		out *= RealVal;
		return out;
	}


	inline Value operator*( const Reference &Operand1, const Value &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out *= Operand2;
		return out;
	}


	inline Value operator*( const Reference &Operand1, float RealVal )
	{
		Value out = Operand1.ConvertToValue();
		out *= RealVal;
		return out;
	}


	inline Value operator/( const Reference &Operand1, const Reference &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out /= Operand2;
		return out;
	} 


	inline Value operator/( const Value &Operand1, const Reference &Operand2 )
	{
		Value out( Operand1 );
		out /= Operand2;
		return out;
	}


	inline Value operator/( float RealVal, const Reference &Operand )
	{
		Value out( RealVal );
		out /= Operand;
		return out;
	}


	inline Value operator/( const Reference &Operand1, const Value &Operand2 )
	{
		Value out = Operand1.ConvertToValue();
		out /= Operand2;
		return out;
	}


	inline Value operator/( const Reference &Operand1, float RealVal )
	{
		Value out = Operand1.ConvertToValue();
		out /= RealVal;
		return out;
	}


	//	Start of Subset implementation

	inline Subset::Subset( const Subset &Source )
	{
		this->GotStats = Source.GotStats;
		this->GotNoiseEstimate = Source.GotNoiseEstimate;
		this->Mean = Source.Mean;
		this->NoiseEstimate = Source.NoiseEstimate;
		this->StdDev = Source.StdDev;
		this->Min = Source.Min;
		this->Max = Source.Max;

		this->Data = Source.Data;
		this->Data->RefCount++;
	}


	inline Subset::~Subset( void )
	{
		if( !( --this->Data->RefCount ) )
			delete this->Data;
	}


	inline int Subset::GetNumOfDims( void ) const
	{
		return this->Data->NumOfDimensions;
	}


	inline int Subset::GetDimensionSize( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return this->Data->SizeInt[ DimNum ];
	}


	inline int *Subset::GetDimensionSizes( void ) const
	{
		int *out = new int[ this->Data->NumOfDimensions ];
		memcpy( out, this->Data->SizeInt, this->Data->NumOfDimensions * sizeof( int ) );
		return out;
	}


	inline std::vector< int > Subset::GetDimensionSizeVector( void ) const
	{
		std::vector< int > out;
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			out.push_back( this->Data->SizeInt[ i ] );
		return out;
	}


	inline size_t Subset::GetFullSize( void ) const
	{
		return this->Data->FullSize;
	}


	inline bool Subset::IsComplexAvailable( void ) const
	{
		return this->Data->Parent->IsComplexAvailable();
	}


	inline int Subset::GetNumOfComponents( void ) const
	{
		return this->Data->Parent->GetNumOfComponents();
	}


	inline bool Subset::IsTimeDomainAvailable( void ) const
	{
		return this->Data->Parent->IsTimeDomainAvailable();
	}


	inline bool *Subset::IsComplexAvailableByDim( void ) const
	{
		bool *result_alldims = this->Data->Parent->IsComplexAvailableByDim();
		bool *result = new bool[ this->Data->Parent->NumOfDimensions ];
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			result[ i ] = result_alldims[ this->Data->DimMap[ i ] ];
		delete result_alldims;
		return result;
	}


	inline std::vector< bool > Subset::IsComplexAvailableByDimVector( void ) const
	{
		std::vector< bool > result, result_alldims = this->Data->Parent->IsComplexAvailableByDimVector();
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			result[ i ] = result_alldims[ this->Data->DimMap[ i ] ];
		return result;
	}


	inline bool *Subset::IsTimeDomainAvailableByDim( void ) const
	{
		bool *result_alldims = this->Data->Parent->IsTimeDomainAvailableByDim();
		bool *result = new bool[ this->Data->Parent->NumOfDimensions ];
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			result[ i ] = result_alldims[ this->Data->DimMap[ i ] ];
		delete result_alldims;
		return result;
	}


	inline std::vector< bool > Subset::IsTDAvailableByDimVector( void ) const
	{
		std::vector< bool > result, result_alldims = this->Data->Parent->IsTDAvailableByDimVector();
		for( int i = 0, j = 0; i < this->Data->NumOfDimensions; i++, j++ )
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			result[ i ] = result_alldims[ this->Data->DimMap[ i ] ];
		return result;
	}


	inline char *Subset::GetDimensionLabel( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return this->Data->Parent->GetDimensionLabel( this->Data->DimMap[ DimNum ] );
	}


	inline std::string Subset::GetDimensionLabelStr( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return this->Data->Parent->GetDimensionLabelStr( this->Data->DimMap[ DimNum ] );
	}


	inline void Subset::GetDimensionCalibration( int DimNum, float &SW, float &SF, float &CenterPPM ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		SW = this->GetDimensionSW( DimNum );
		SF = this->GetDimensionSF( DimNum );
		CenterPPM = this->GetDimensionCenterPPM( DimNum );
	}


	inline void Subset::GetFullDimCalibration( int DimNum, float &SW, float &SF, float &CenterPPM, float &OriginPPM, float &WidthPPM ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		SW = this->GetDimensionSW( DimNum );
		SF = this->GetDimensionSF( DimNum );
		CenterPPM = this->GetDimensionCenterPPM( DimNum );
		WidthPPM = SW / SF;
		OriginPPM = CenterPPM + WidthPPM / 2.0F;
	}


	inline float Subset::GetDimensionSW( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		float sw_full = this->Data->Parent->GetDimensionSW( this->Data->DimMap[ DimNum ] );
		return sw_full * ( float ) this->Data->SizeInt[ DimNum ] / ( float ) this->Data->Parent->DimensionSizes[ this->Data->DimMap[ DimNum ] ];
	}


	inline float Subset::GetDimensionSF( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return this->Data->Parent->GetDimensionSF( this->Data->DimMap[ DimNum ] );
	}


	inline float Subset::GetDimensionCenterPPM( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		int ext_dim = this->Data->DimMap[ DimNum ];
		float pct_of_axis = ( ( float ) this->Data->Corner[ ext_dim ] + ( ( float ) this->Data->SizeExt[ ext_dim ] ) / 2.0F ) /
			( ( float ) this->Data->Parent->DimensionSizes[ ext_dim ] );
		float widthppm_full = this->Data->Parent->DimensionScalesPPM[ this->Data->DimMap[ DimNum ] ];
		float originppm_full = this->Data->Parent->DimensionOriginsPPM[ this->Data->DimMap[ DimNum ] ];
		return originppm_full - pct_of_axis * widthppm_full;
	}


	inline float Subset::GetDimensionCenterHz( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		int ext_dim = this->Data->DimMap[ DimNum ];
		float pct_of_axis = ( ( float ) this->Data->Corner[ ext_dim ] + ( ( float ) this->Data->SizeExt[ ext_dim ] ) / 2.0F ) /
			( ( float ) this->Data->Parent->DimensionSizes[ ext_dim ] );
		float widthhz_full = this->Data->Parent->DimensionScalesFreq[ this->Data->DimMap[ DimNum ] ];
		return -widthhz_full / 2.0F + pct_of_axis * widthhz_full;
	}


	inline float Subset::GetDimensionStartTime( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
		if( !this->GetDimensionTD( DimNum ) ) throw Exceptions::DimNotTD();
#endif

		int ext_dim = this->Data->DimMap[ DimNum ];
		float pct_of_axis = ( float ) this->Data->Corner[ ext_dim ] / ( float ) this->Data->Parent->DimensionSizes[ ext_dim ];
		float dwell = 1.0F / this->Data->Parent->DimensionScalesFreq[ ext_dim ];

		return pct_of_axis * dwell;
	}


	inline float Subset::GetDimensionEndTime( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
		if( !this->GetDimensionTD( DimNum ) ) throw Exceptions::DimNotTD();
#endif

		int ext_dim = this->Data->DimMap[ DimNum ];
		float pct_of_axis = ( ( float ) this->Data->Corner[ ext_dim ] + ( float ) this->Data->SizeExt[ ext_dim ] ) 
			/ ( float ) this->Data->Parent->DimensionSizes[ ext_dim ];
		float dwell = 1.0F / this->Data->Parent->DimensionScalesFreq[ ext_dim ];

		return pct_of_axis * dwell;
	}


	inline bool Subset::GetDimensionComplexState( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return this->Data->Parent->GetDimensionComplexState( this->Data->DimMap[ DimNum ] );
	}


	inline bool Subset::GetDimensionTD( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return this->Data->Parent->GetDimensionTD( this->Data->DimMap[ DimNum ] );
	}


	inline DimPosition< Subset > Subset::GetDimPosObj( int DimNum ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( DimNum < 0 || DimNum > this->Data->NumOfDimensions ) throw Exceptions::BadDimNumber();
#endif

		return DimPosition< Subset >( this, DimNum, 0, 0.0F, pts );
	}


	inline std::vector< DimPosition< Subset > > Subset::GetDimPosSet( void ) const
	{
		std::vector< DimPosition< Subset > > out;
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			out.push_back( DimPosition< Subset >( this, i, 0, 0.0F, pts ) );
		return out;
	}


	inline Value Subset::GetBlankValue( void ) const
	{
		return this->Data->Parent->GetBlankValue();
	}


	inline char *Subset::GetFilename( void ) const
	{
		return this->Data->Parent->GetFilename();
	}


	inline std::string Subset::GetFilenameStr( void ) const
	{
		return this->Data->Parent->GetFilenameStr();
	}


	inline char *Subset::GetDatasetName( void ) const
	{
		return this->Data->Parent->GetDatasetName();
	}


	inline std::string Subset::GetDatasetNameStr( void ) const
	{
		return this->Data->Parent->GetDatasetNameStr();
	}


	inline std::valarray< float > Subset::GetDataCopyValarray( void ) const
	{
		std::valarray< float > out( this->Data->FullSize * this->GetNumOfComponents() );
		
		DataOutCopier< std::valarray< float > > copy_data( out );
		this->ApplyToAllLinear( copy_data );

		return out;
	}


	inline float *Subset::GetDataCopyPtr( float *Destination ) const
	{
		if( !Destination )
		{
			try
			{
				Destination = new float[ this->Data->FullSize * this->GetNumOfComponents() ];
			}
			catch( std::bad_alloc )
			{
				throw Exceptions::DataCopyRequestTooLarge();
			}
		}

		DataOutCopier< float * > copy_data( Destination );
		this->ApplyToAllLinear( copy_data );

		return Destination;
	}


	inline void Subset::SetDataFromCopyValarray( std::valarray< float > &Input )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Input.size() != this->Data->FullSize * this->GetNumOfComponents() )
			throw Exceptions::NotCompatible();
#endif

		DataInCopier< std::valarray< float > > copy_data( Input );
		this->ApplyChangeToAllLinear( copy_data );
	}


	inline void Subset::SetDataFromCopyPtr( const float *Input )
	{
		DataInCopier< const float * > copy_data( Input );
		this->ApplyChangeToAllLinear( copy_data );
	}


	inline float Subset::GetInterpolatedValue( const float *Coordinates ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !Coordinates ) throw Exceptions::NullPointerArg();
		for( int x = 0; x < this->Data->NumOfDimensions; x++ )
			if( Coordinates[ x ] < 0 || Coordinates[ x ] > this->GetDimensionSize( x ) - 1 ) throw Exceptions::BadCoordinates();
#endif

		BEC_Misc::stack_array_ptr< float > vals( new float[ ( int ) pow( 2.0F, this->Data->NumOfDimensions ) ] );
		BEC_Misc::stack_array_ptr< int > base( new int[ this->Data->NumOfDimensions ] );
		for( int x = 0; x < this->Data->NumOfDimensions; x++ )
			base[ x ] = ( int ) floor( Coordinates[ x ] );
		std::vector< int > current( this->Data->NumOfDimensions );

		for( int x = ( int ) pow( 2.0F, this->Data->NumOfDimensions - 1 ), y = 0; x >= 0; x--, y++ )
		{
			for( int z = 0; x < this->Data->NumOfDimensions; z++ )
			{
				current[ z ] = base[ z ];
				int mask = ( int ) pow( 2.0F, this->Data->NumOfDimensions - z - 1 );
				if( ( x & mask ) && base[ z ] + 1 < this->GetDimensionSize( z ) ) current[ z ]++;
			}
			vals[ y ] = ( *this )[ current ];
		}

		for( int x = 0; x < this->Data->NumOfDimensions; x++ )
		{
			int limity = ( int ) pow( 2.0F, this->Data->NumOfDimensions - x - 1 );
			for( int y = 0; y < limity; y++ )
			{
				float fractpart = Coordinates[ x ] - base[ x ];
				vals[ y ] = vals[ y * 2 + 1 ] + fractpart * ( vals[ 2 * y ] - vals[ y * 2 + 1 ] );
			}
		}

		float outval = vals[ 0 ];
		return outval;
	}


	inline float Subset::GetInterpolatedValue( std::vector< float > Coordinates ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Coordinates.size() != this->Data->NumOfDimensions ) throw Exceptions::BadCoordinates();
		for( int x = 0; x < this->Data->NumOfDimensions; x++ )
			if( Coordinates[ x ] < 0 || Coordinates[ x ] > this->GetDimensionSize( x ) - 1 ) throw Exceptions::BadCoordinates();
#endif

		BEC_Misc::stack_array_ptr< float > vals( new float[ ( int ) pow( 2.0F, this->Data->NumOfDimensions ) ] );
		BEC_Misc::stack_array_ptr< int > base( new int[ this->Data->NumOfDimensions ] );
		for( int x = 0; x < this->Data->NumOfDimensions; x++ )
			base[ x ] = ( int ) floor( Coordinates[ x ] );
		std::vector< int > current( this->Data->NumOfDimensions );

		for( int x = ( int ) pow( 2.0F, this->Data->NumOfDimensions - 1 ), y = 0; x >= 0; x--, y++ )
		{
			for( int z = 0; z < this->Data->NumOfDimensions; z++ )
			{
				current[ z ] = base[ z ];
				int mask = ( int ) pow( 2.0F, this->Data->NumOfDimensions - z - 1 );
				if( ( x & mask ) && base[ z ] + 1 < this->GetDimensionSize( z ) ) current[ z ]++;
			}
			vals[ y ] = ( *this )[ current ];
		}

		for( int x = 0; x < this->Data->NumOfDimensions; x++ )
		{
			int limity = ( int ) pow( 2.0F, this->Data->NumOfDimensions - x - 1 );
			for( int y = 0; y < limity; y++ )
			{
				float fractpart = Coordinates[ x ] - base[ x ];
				vals[ y ] = vals[ y * 2 + 1 ] + fractpart * ( vals[ 2 * y ] - vals[ y * 2 + 1 ] );
			}
		}

		float outval = vals[ 0 ];
		return outval;
	}


	inline Subset::iterator Subset::GetIteratorStart( void )
	{
		return iterator( this, 0, false );
	}


	inline Subset::const_iterator Subset::GetIteratorStart( void ) const
	{
		return const_iterator( this, 0, false );
	}


	inline Subset::iterator Subset::GetIteratorAt( size_t Offset )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Offset > this->Data->FullSize ) throw Exceptions::BadOffset();
#endif

		return iterator( this, Offset, false );
	}


	inline Subset::const_iterator Subset::GetIteratorAt( size_t Offset ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Offset > this->Data->FullSize ) throw Exceptions::BadOffset();
#endif

		return const_iterator( this, Offset, false );
	}


	inline Subset::iterator Subset::GetIteratorForReference( const Reference &Ref )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Ref.GetParent() != this->Data->Parent ) throw Exceptions::RefHasWrongParent();
		if( this->Data->TranslateParentOffsetToSubsetOffset( Ref.GetOffset() ) < 0 ) throw Exceptions::RefIsOutsideSubset();
#endif

		return iterator( this, this->Data->TranslateParentOffsetToSubsetOffset( Ref.GetOffset() ), false );
	}


	inline Subset::const_iterator Subset::GetIteratorForReference( const Reference &Ref ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Ref.GetParent() != this->Data->Parent ) throw Exceptions::RefHasWrongParent();
		if( this->Data->TranslateParentOffsetToSubsetOffset( Ref.GetOffset() ) < 0 ) throw Exceptions::RefIsOutsideSubset();
#endif

		return const_iterator( this, this->Data->TranslateParentOffsetToSubsetOffset( Ref.GetOffset() ), false );
	}


	inline Subset::iterator Subset::GetIteratorEnd( void )
	{
		return iterator( this, this->GetFullSize(), true );
	}


	inline Subset::const_iterator Subset::GetIteratorEnd( void ) const
	{
		return const_iterator( this, this->GetFullSize(), true );
	}


	inline void Subset::Flush( void )
	{
		if( !this->Data->Parent->ReadOnly )
		{
			NMRData *parent = const_cast< NMRData * >( this->Data->Parent );
			parent->Flush();
		}
	}


	/**	The noise estimate is coutesy of Frank Delaglio and was first presented in:
		
			F. Delaglio, S. Grzesiek, G.W. Vuister, G. Zhu, J. Pfeifer and A. Bax.
			   "NMRPipe: A Multidimensional Spectral Processing System Based on
			    UNIX Pipes."  J. Biomol. NMR, 6: 277-293 (1995).
	
		Additional documentation was provided by Frank by email on 1 Aug 2005.
	
		The method is designed to estimate the noise standard deviation correctly
		despite the presence of peaks, which would otherwise skew the distribution.
		The points within a subset are centered to give deviations, and the
		deviations are then sorted in order of increasing deviation.  One then
		calculates what standard deviation one would need to have for the point
		at the 30th percentile to have the deviation that it has.  The 30th
		percentile is chosen so that we avoid reading off a value skewed by a
		peak.
		
		To explain it differently, if one were to plot the sorted deviations against 
		the percentile of the data point within the sorted data, the result would be 
		the inverse error curve erf^(-1) scaled by the standard	deviation of the 
		distribution.  Since 68% of the points in a distribution are found within
		+/- 1 std. dev., we would normally be able to get the standard deviation by
		reading the value of the point with its deviation at the 68th percentile.
		However, for NMR data the plot would not match a scaled erf^(-1) curve over
		the full range of data: The peaks would cause the trace to skew from the
		scaled erf^(-1).  Thus we use a point at a lower percentile, which is
		unlikely to be contributed by a peak.

		The value at the 30% percentile is expected to be: sqrt(2)*inverse_erf(0.3)
		= 0.38532*std_dev.  Thus std_dev = value_30th_percentile / 0.38532.

		Frank would compute this on "a subset of vectors in one dimension;" that is,
		he would compute independent noise estimates for each of "many" vectors,
		and then take the median.

		In this library, we compute a noise estimate for a subset using all of the
		points in the subset, and we compute a noise estimate for a full dataset
		by obtaining Subset objects referencing specific vectors, computing a noise
		estimate for each Subset, and then taking the median of the estimates.
	*/
	inline void Subset::CalculateStats( void ) const
	{
		std::vector< float > data;
		if( this->Data->Parent->GetNumOfComponents() == 1 )
		{
			data.reserve( this->Data->FullSize );
			std::copy( this->begin(), this->end(), std::back_inserter( data ) );
		}
		else
		{
			int components = this->Data->Parent->GetNumOfComponents();
			data.reserve( this->Data->FullSize * components );
			for( size_t i = 0; i < this->Data->FullSize; i++ )
				for( int j = 0; j < components; j++ )
					data.push_back( ( *this )[ i ][ j ] );
		}

		float normalization = 1.0F / ( ( float ) data.size() );
		this->Mean = 0;
		this->Min = data[ 0 ];
		this->Max = data[ 0 ];
		this->StdDev = 0;
		this->NoiseEstimate = 0;

		std::vector< float >::iterator end = data.end();
		for( std::vector< float >::iterator i = data.begin(); i != end; i++ )
		{
			float val = *i;
			this->Mean += normalization * val;
			if( val < this->Min ) this->Min = val;
			if( val > this->Max ) this->Max = val;
		}

		for( std::vector< float >::iterator i = data.begin(); i != end; i++ )
		{
			float val = *i;
			float dev = val - this->Mean;

			*i = dev > 0.0F ? dev : dev * -1.0F;

			this->StdDev += dev * dev;
		}
		this->StdDev /= ( float ) ( data.size() - 1 );
		this->StdDev = sqrtf( this->StdDev );

		std::sort( data.begin(), data.end() );
		this->NoiseEstimate = BEC_Misc::fast_abs( data[ ( int ) floorf( 0.3F * ( float ) data.size() ) ] / 0.38532F );

		this->GotStats = true;
		this->GotNoiseEstimate = true;
	}


	inline void Subset::CalculateNoiseEstimate( void ) const
	{
		this->CalculateStats();
	}


	inline Reference Subset::operator[]( size_t Offset )
	{
		SubsetPtrTableEntry *element = this->Data->GetElement( Offset );
		return Reference( element->ParentOffset, element->PageTableEntry ? element->PageTableEntry->Page : 0, element->DataPtr, false, this->Data->Parent );
	}


	inline const Reference Subset::operator[]( size_t Offset ) const
	{
		SubsetPtrTableEntry *element = this->Data->GetElement( Offset );
		return Reference( element->ParentOffset, element->PageTableEntry ? element->PageTableEntry->Page : 0, element->DataPtr, true, this->Data->Parent );
	}


	inline Reference Subset::operator[]( std::vector< int > Coordinates )
	{
		SubsetPtrTableEntry *element = this->Data->GetElementForSubsetCoord( Coordinates );
		return Reference( element->ParentOffset, element->PageTableEntry ? element->PageTableEntry->Page : 0, element->DataPtr, false, this->Data->Parent );
	}


	inline const Reference Subset::operator[]( std::vector< int > Coordinates ) const
	{
		SubsetPtrTableEntry *element = this->Data->GetElementForSubsetCoord( Coordinates );
		return Reference( element->ParentOffset, element->PageTableEntry ? element->PageTableEntry->Page : 0, element->DataPtr, true, this->Data->Parent );
	}


	inline Reference Subset::operator[]( std::vector< DimPosition< Subset > > Coordinates )
	{
		SubsetPtrTableEntry *element = this->Data->GetElementForSubsetCoord( Coordinates );
		return Reference( element->ParentOffset, element->PageTableEntry ? element->PageTableEntry->Page : 0, element->DataPtr, false, this->Data->Parent );
	}


	inline const Reference Subset::operator[]( std::vector< DimPosition< Subset > > Coordinates ) const
	{
		SubsetPtrTableEntry *element = this->Data->GetElementForSubsetCoord( Coordinates );
		return Reference( element->ParentOffset, element->PageTableEntry ? element->PageTableEntry->Page : 0, element->DataPtr, true, this->Data->Parent );
	}


	inline Subset Subset::operator()( std::vector< int > Corner1, std::vector< int > Corner2 )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Corner1.size() != this->Data->NumOfDimensions || Corner2.size() != this->Data->NumOfDimensions )
			throw Exceptions::BadCoordinates();
#endif

		int parent_dims = this->Data->Parent->GetNumOfDims();
		std::vector< int > corner, size;
		for( int i = 0; i < parent_dims; i++ )
		{
			corner.push_back( this->Data->Corner[ i ] );
			size.push_back( 0 );
		}
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
		{
			int dim = this->Data->DimMap[ i ];
			corner[ dim ] += Corner1[ i ];
			size[ dim ] = Corner2[ i ] - Corner1[ i ] + 1;
		}

		return Subset( this->Data->Parent, corner, size, false );
	}


	inline const Subset Subset::operator()( std::vector< int > Corner1, std::vector< int > Corner2 ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Corner1.size() != this->Data->NumOfDimensions || Corner2.size() != this->Data->NumOfDimensions )
			throw Exceptions::BadCoordinates();
#endif

		int parent_dims = this->Data->Parent->GetNumOfDims();
		std::vector< int > corner, size;
		for( int i = 0; i < parent_dims; i++ )
		{
			corner.push_back( this->Data->Corner[ i ] );
			size.push_back( 0 );
		}
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
		{
			int dim = this->Data->DimMap[ i ];
			corner[ dim ] += Corner1[ i ];
			size[ dim ] = Corner2[ i ] - Corner1[ i ] + 1;
		}

		return Subset( this->Data->Parent, corner, size, true );
	}


	inline Subset Subset::operator()( std::vector< DimPosition< Subset > > Corner1, std::vector< DimPosition< Subset > > Corner2 )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Corner1.size() != this->Data->NumOfDimensions || Corner2.size() != this->Data->NumOfDimensions )
			throw Exceptions::BadCoordinates();
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			if( !Corner1[ i ].InRange() || !Corner2[ i ].InRange() )
				throw Exceptions::BadCoordinates();
#endif

		int parent_dims = this->Data->Parent->GetNumOfDims();
		std::vector< int > corner, size;
		for( int i = 0; i < parent_dims; i++ )
		{
			corner.push_back( this->Data->Corner[ i ] );
			size.push_back( 0 );
		}
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
		{
			int dim = this->Data->DimMap[ i ];
			corner[ dim ] += Corner1[ i ].GetPosPts();
			size[ dim ] = Corner2[ i ].GetPosPts() - Corner1[ i ].GetPosPts() + 1;
		}

		return Subset( this->Data->Parent, corner, size, false );
	}


	inline const Subset Subset::operator()( std::vector< DimPosition< Subset > > Corner1, std::vector< DimPosition< Subset > > Corner2 ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Corner1.size() != this->Data->NumOfDimensions || Corner2.size() != this->Data->NumOfDimensions )
			throw Exceptions::BadCoordinates();
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
			if( !Corner1[ i ].InRange() || !Corner2[ i ].InRange() )
				throw Exceptions::BadCoordinates();
#endif

		int parent_dims = this->Data->Parent->GetNumOfDims();
		std::vector< int > corner, size;
		for( int i = 0; i < parent_dims; i++ )
		{
			corner.push_back( this->Data->Corner[ i ] );
			size.push_back( 0 );
		}
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
		{
			int dim = this->Data->DimMap[ i ];
			corner[ dim ] += Corner1[ i ].GetPosPts();
			size[ dim ] = Corner2[ i ].GetPosPts() - Corner1[ i ].GetPosPts() + 1;
		}

		return Subset( this->Data->Parent, corner, size, true );
	}


	inline Reference Subset::operator()( int Coordinate )
	{
		std::vector< DimPosition< NMRData > > pos = this->Data->Parent->GetDimPosSet();
		std::queue< int > dims_to_set;
		for( int i = 0; i < this->Data->Parent->GetNumOfDims(); i++ )
		{
			pos[ i ].SetPosPts( this->Data->Corner[ i ] );
			if( this->Data->SizeExt[ i ] ) dims_to_set.push( i );
		}
		pos[ dims_to_set.front() ].SetPosPts( pos[ dims_to_set.front() ].GetPosPts() + Coordinate );
		dims_to_set.pop();

		return Reference( pos, dims_to_set, false, this->Data->Parent );
	}


	inline const Reference Subset::operator()( int Coordinate ) const
	{
		std::vector< DimPosition< NMRData > > pos = this->Data->Parent->GetDimPosSet();
		std::queue< int > dims_to_set;
		for( int i = 0; i < this->Data->NumOfDimensions; i++ )
		{
			pos[ i ].SetPosPts( this->Data->Corner[ i ] );
			if( !this->Data->SizeExt[ i ] ) dims_to_set.push( i );
		}
		pos[ 0 ].SetPosPts( Coordinate + this->Data->Corner[ 0 ] );

		return Reference( pos, dims_to_set, true, this->Data->Parent );
	}


	inline Subset &Subset::operator=( float Scalar )
	{
		BEC_Misc::binder_constant_as_unary< float, float > temp( Scalar );
		this->ApplyChangeToAll( temp );
		return *this;
	}


	inline Subset &Subset::operator=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrConstAdaptor > temp = std::bind2nd( ValToPtrConstAdaptor(), Scalar );
		this->ApplyChangeToAll_Ptr( temp );
		return *this;
	}


	inline Subset &Subset::operator=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrConstAdaptor > temp = std::bind2nd( ValToPtrConstAdaptor(), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr( temp );
		return *this;
	}


	inline Subset &Subset::operator=( const Subset &Operand )
	{
		this->PerformMathAssignment( NMRDataMathSubset( Operand ) );
		return *this;
	}


	inline Subset &Subset::operator=( const NMRData &Operand )
	{
		this->PerformMathAssignment( NMRDataMathObj( &Operand ) );
		return *this;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline Subset &Subset::operator=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		this->PerformMathAssignment( Operand );
		return *this;
	}


	template< typename T >
	inline void Subset::PerformMathAssignment( const T &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Operand.GetNumOfComponents() != this->GetNumOfComponents() ) throw Exceptions::ComponentsDontMatch();
#endif

		BEC_Misc::ptr_vector< const NMRData > objects = Operand.GetObjectList();

		for( BEC_Misc::ptr_vector< const NMRData >::iterator i = objects.begin(); i != objects.end(); i++ )
		{
			if( !i->GotData ) throw Exceptions::DataNotOpen();
			if( i->NumOfDimensions != this->Data->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->FullSize != this->Data->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->Data->NumOfDimensions; j++ )
			{
				if( this->GetDimensionSize( j ) != i->GetDimensionSize( j ) )
					throw Exceptions::NotCompatible();
			}
		}

		std::vector< Subset > subsets = Operand.GetSubsetList();
		for( std::vector< Subset >::iterator i = subsets.begin(); i != subsets.end(); i++ )
		{
			if( i->Data->NumOfDimensions != this->Data->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->Data->FullSize != this->Data->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->Data->NumOfDimensions; j++ )
			{
				if( this->GetDimensionSize( j ) != i->GetDimensionSize( j ) )
					throw Exceptions::NotCompatible();
			}
		}
		
		Operand.SetOffsetTraversal();
		this->ApplyChangeToAll( Operand );
	}


	template< typename T >
	inline void Subset::PerformMathAssignment_Component( int Component, const T &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Operand.GetNumOfComponents() != 1 ) throw Exceptions::ComponentsDontMatch();
		if( Component < 0 || Component >= this->GetNumOfComponents() ) throw Exceptions::BadComponentIndex();
#endif

		BEC_Misc::ptr_vector< const NMRData > objects = Operand.GetObjectList();

		for( BEC_Misc::ptr_vector< const NMRData >::iterator i = objects.begin(); i != objects.end(); i++ )
		{
			if( !i->GotData ) throw Exceptions::DataNotOpen();
			if( i->NumOfDimensions != this->Data->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->Components != this->Data->Parent->GetNumOfComponents() )
				throw Exceptions::ComponentsDontMatch();
			if( i->FullSize != this->Data->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->Data->NumOfDimensions; j++ )
			{
				if( this->GetDimensionSize( j ) != i->GetDimensionSize( j ) )
					throw Exceptions::NotCompatible();
			}
		}

		std::vector< Subset > subsets = Operand.GetSubsetList();
		for( std::vector< Subset >::iterator i = subsets.begin(); i != subsets.end(); i++ )
		{
			if( i->Data->NumOfDimensions != this->Data->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->Data->Parent->GetNumOfComponents() != this->Data->Parent->GetNumOfComponents() )
				throw Exceptions::ComponentsDontMatch();
			if( i->Data->FullSize != this->Data->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->Data->NumOfDimensions; j++ )
			{
				if( this->GetDimensionSize( j ) != i->GetDimensionSize( j ) )
					throw Exceptions::NotCompatible();
			}
		}
		
		Operand.SetOffsetTraversal();
		this->ApplyChangeToComponent( Operand, Component );
	}


	inline void Subset::operator+=( float Scalar )
	{
		std::binder1st< std::plus< float > > temp = std::bind1st( std::plus< float >(), Scalar );
		this->ApplyChangeToAll( temp );
	}


	inline void Subset::operator+=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::plus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::plus< float > >( std::plus< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator+=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::plus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::plus< float > >( std::plus< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator+=( const Subset &Operand )
	{
		*this = *this + Operand;
	}


	inline void Subset::operator+=( const NMRData &Operand )
	{
		*this = *this + Operand;
	}


	template< typename T >
	inline void Subset::operator+=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this + Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void Subset::operator+=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this + Operand;
	}


	inline void Subset::operator-=( float Scalar )
	{
		std::binder1st< std::minus< float > > temp = std::bind1st( std::minus< float >(), Scalar );
		this->ApplyChangeToAll( temp );
	}


	inline void Subset::operator-=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::minus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::minus< float > >( std::minus< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator-=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::minus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::minus< float > >( std::minus< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator-=( const Subset &Operand )
	{
		*this = *this - Operand;
	}


	inline void Subset::operator-=( const NMRData &Operand )
	{
		*this = *this - Operand;
	}


	template< typename T >
	inline void Subset::operator-=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this - Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void Subset::operator-=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this - Operand;
	}


	inline void Subset::operator*=( float Scalar )
	{
		std::binder1st< std::multiplies< float > > temp = std::bind1st( std::multiplies< float >(), Scalar );
		this->ApplyChangeToAll( temp );
	}


	inline void Subset::operator*=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::multiplies< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::multiplies< float > >( std::multiplies< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator*=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::multiplies< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::multiplies< float > >( std::multiplies< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator*=( const Subset &Operand )
	{
		*this = *this * Operand;
	}


	inline void Subset::operator*=( const NMRData &Operand )
	{
		*this = *this * Operand;
	}


	template< typename T >
	inline void Subset::operator*=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this * Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void Subset::operator*=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this * Operand;
	}


	inline void Subset::operator/=( float Scalar )
	{
		std::binder1st< std::divides< float > > temp = std::bind1st( std::divides< float >(), Scalar );
		this->ApplyChangeToAll( temp );
	}


	inline void Subset::operator/=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::divides< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::divides< float > >( std::divides< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator/=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::divides< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::divides< float > >( std::divides< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::operator/=( const Subset &Operand )
	{
		*this = *this / Operand;
	}


	inline void Subset::operator/=( const NMRData &Operand )
	{
		*this = *this / Operand;
	}


	template< typename T >
	inline void Subset::operator/=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this / Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void Subset::operator/=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this / Operand;
	}


	inline void Subset::SetComponentTo( int Component, float Scalar )
	{
		BEC_Misc::binder_constant_as_unary< float, float > temp = BEC_Misc::bind_constant_as_unary( Scalar );
		this->ApplyChangeToComponent( temp, Component );
	}


	inline void Subset::SetComponentTo( int Component, const Value &Scalar )
	{
		std::binder2nd< ValToPtrComponentAdaptor > temp = std::bind2nd( ValToPtrComponentAdaptor( Component ), Scalar );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::SetComponentTo( int Component, const Reference &Scalar )
	{
		std::binder2nd< ValToPtrComponentAdaptor > temp = std::bind2nd( ValToPtrComponentAdaptor( Component ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr( temp );
	}


	inline void Subset::SetComponentTo( int Component, const Subset &Operand )
	{
		this->PerformMathAssignment_Component( Component, NMRDataMathSubset( Operand ) );
	}


	inline void Subset::SetComponentTo( int Component, const NMRData &Operand )
	{
		this->PerformMathAssignment_Component( Component, NMRDataMathObj( &Operand ) );
	}


	template< typename T >
	inline void Subset::SetComponentTo( int Component, const NMRDataMathSelector< T > &Operand )
	{
		this->PerformMathAssignment_Component( Component, Operand );
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void Subset::SetComponentTo( int Component, const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		this->PerformMathAssignment_Component( Component, Operand );
	}


	template< typename T >
	inline void Subset::ApplyChangeToAll( T &Operation )
	{
		size_t size = this->Data->FullSize;
		int components = this->Data->Parent->GetNumOfComponents();

		if( components > 1 )
		{
			for( size_t i = 0; i < size; i++ )
			{
				this->Data->UpdatePtrTableStatus();
				SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
				if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
				float *ptr = entry.DataPtr;
				for( int j = 0; j < components; j++ )
					ptr[ j ] = Operation( ptr[ j ] );
			}
		}
		else
			for( size_t i = 0; i < size; i++ )
			{
				this->Data->UpdatePtrTableStatus();
				SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
				if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
				*( entry.DataPtr ) = Operation( *( entry.DataPtr ) );
			}
	}


	template< typename T >
	inline void Subset::ApplyToAll( T &Operation ) const
	{
		size_t size = this->Data->FullSize;
		int components = this->Data->Parent->GetNumOfComponents();

		if( components > 1 )
		{
			for( size_t i = 0; i < size; i++ )
			{
				this->Data->UpdatePtrTableStatus();
				SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
				if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
				float *ptr = entry.DataPtr;
				for( int j = 0; j < components; j++ )
					Operation( ptr[ j ] );
			}
		}
		else
			for( size_t i = 0; i < size; i++ )
			{
				this->Data->UpdatePtrTableStatus();
				SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
				if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
				Operation( *( entry.DataPtr ) );
			}
	}


	template< typename T >
	inline void Subset::ApplyChangeToComponent( T &Operation, int Component )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Component < 0 || Component > this->Data->Parent->GetNumOfComponents() ) throw Exceptions::BadComponentIndex();
#endif
		size_t size = this->Data->FullSize;

		for( size_t i = 0; i < size; i++ )
		{
			this->Data->UpdatePtrTableStatus();
			SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
			if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
			entry.DataPtr[ Component ] = Operation( entry.DataPtr[ Component ] );
		}
	}


	template< typename T >
	inline void Subset::ApplyToComponent( T &Operation, int Component ) const
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Component < 0 || Component > this->Data->Parent->GetNumOfComponents() ) throw Exceptions::BadComponentIndex();
#endif
		size_t size = this->Data->FullSize;

		for( size_t i = 0; i < size; i++ )
		{
			this->Data->UpdatePtrTableStatus();
			SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
			if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
			Operation( entry.DataPtr[ Component ] );
		}
	}


	template< typename T >
	inline void Subset::ApplyChangeToAllLinear( T &Operation )
	{
		this->ApplyChangeToAll( Operation );
	}


	template< typename T >
	inline void Subset::ApplyToAllLinear( T &Operation ) const
	{
		this->ApplyToAll( Operation );
	}


	template< typename T >
	inline void Subset::ApplyChangeToComponentLinear( T &Operation, int Component )
	{
		this->ApplyChangeToComponent( Operation, Component );
	}


	template< typename T >
	inline void Subset::ApplyToComponentLinear( T &Operation, int Component ) const
	{
		this->ApplyToComponent( Operation, Component );
	}


	inline Subset::Subset( const NMRData *Parent, std::vector< int > Corner, std::vector< int > Size, bool Const )
	{
		this->GotStats = false;
		this->GotNoiseEstimate = false;
		this->Mean = 0;
		this->NoiseEstimate = 0;
		this->StdDev = 0;
		this->Min = 0;
		this->Max = 0;

		this->Data = new SubsetData( Parent, Corner, Size, Const );
		this->Data->RefCount++;
	}


	template< typename T >
	inline void Subset::ApplyChangeToAll_Ptr( T &Operation )
	{
		size_t size = this->Data->FullSize;

		for( size_t i = 0; i < size; i++ )
		{
			SubsetPtrTableEntry &entry = this->Data->PtrTable[ i ];
			if( !entry.DataPtr ) this->Data->ReloadPtrsForPage( entry.PageTableEntry );
			Operation( entry.DataPtr );
		}
	}


	inline Subset::iterator Subset::begin( void )
	{
		return this->GetIteratorStart();
	}


	inline Subset::const_iterator Subset::begin( void ) const
	{
		return this->GetIteratorStart();
	}


	inline Subset::iterator Subset::end( void )
	{
		return this->GetIteratorEnd();
	}


	inline Subset::const_iterator Subset::end( void ) const
	{
		return this->GetIteratorEnd();
	}


	inline Subset::reverse_iterator Subset::rbegin( void )
	{
		return reverse_iterator( this->GetIteratorEnd() );
	}


	inline Subset::const_reverse_iterator Subset::rbegin( void ) const
	{
		return const_reverse_iterator( this->GetIteratorEnd() );
	}


	inline Subset::reverse_iterator Subset::rend( void )
	{
		return reverse_iterator( this->GetIteratorStart() );
	}


	inline Subset::const_reverse_iterator Subset::rend( void ) const
	{
		return const_reverse_iterator( this->GetIteratorStart() );
	}


	//	Start of SubsetData implementation


	inline SubsetData::SubsetData( void )
	{
		this->Corner = 0;
		this->NewlyInvalidPageList = 0;
		this->NumOfDimensions = 0;
		this->PtrTable = 0;
		this->RefCount = 0;
		this->SizeInt = 0;
		this->SizeExt = 0;
		this->Const = true;
	}


	inline SubsetData::SubsetData( const NMRData *Parent, std::vector< int > Corner, std::vector< int > Size, bool Const )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !Parent ) throw Exceptions::NullPointerArg();
		for( int i = 0; i < Parent->NumOfDimensions; i++ )
		{
			if( Corner[ i ] < 0 || Corner[ i ] >= Parent->DimensionSizes[ i ] ) throw Exceptions::BadSubsetCorner();
			if( Size[ i ] < 0 || Corner[ i ] + Size[ i ] > Parent->DimensionSizes[ i ] ) throw Exceptions::BadSubsetSize();
		}
#endif

		this->NumOfDimensions = 0;
		this->RefCount = 0;
		this->NewlyInvalidPageList = 0;

		this->Parent = Parent;
		this->Corner = new int[ this->Parent->NumOfDimensions ];
		this->SizeExt = new int[ this->Parent->NumOfDimensions ];
		this->FullSize = 1;
		this->Const = Const;
		for( int i = 0; i < this->Parent->NumOfDimensions; i++ )
		{
			this->Corner[ i ] = Corner[ i ];
			this->SizeExt[ i ] = Size[ i ];
			if( this->SizeExt[ i ] > 1 )
			{
				this->FullSize *= this->SizeExt[ i ];
				this->NumOfDimensions++;
			}
			else this->SizeExt[ i ] = 0;
		}

		this->SizeInt = new int[ this->NumOfDimensions ];
		this->DimIncrSize = new size_t[ this->NumOfDimensions ];
		for( int i = 0, k = 0; i < this->NumOfDimensions; i++, k++ )
		{
			while( !this->SizeExt[ k ] ) k++;
			this->DimMap.push_back( k );
			this->SizeInt[ i ] = this->SizeExt[ k ];
			this->DimIncrSize[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->DimIncrSize[ i ] *= ( this->SizeExt[ j ] > 1 ? this->SizeExt[ j ] : 1 );
		}

		this->BuildPtrTable();
	}


	inline SubsetData::~SubsetData( void )
	{
		delete [] this->Corner;
		delete [] this->SizeExt;
		delete [] this->SizeInt;
		delete [] this->PtrTable;
		delete [] this->DimIncrSize;

		for( std::map< size_t, SubsetPageTableEntry * >::iterator i = this->PageTable.begin(); i != this->PageTable.end(); i++ )
			delete i->second;

		for( std::set< DataPage * >::iterator i = this->RegisteredWith.begin(); i != this->RegisteredWith.end(); i++ )
			(*i)->RegisteredSubsets.erase( this );
	}


	inline void SubsetData::BuildPtrTable( void )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->Parent || !this->Corner || !this->SizeExt ) throw Exceptions::NullPointerArg();
		for( int i = 0; i < Parent->NumOfDimensions; i++ )
		{
			if( this->Corner[ i ] >= this->Parent->DimensionSizes[ i ] ) throw Exceptions::BadSubsetCorner();
			if( this->Corner[ i ] + this->SizeExt[ i ] > this->Parent->DimensionSizes[ i ] ) throw Exceptions::BadSubsetSize();
		}
#endif

		try
		{
			this->PtrTable = new SubsetPtrTableEntry[ this->FullSize ];
		}
		catch( std::bad_alloc )
		{
			throw Exceptions::SubsetRequestTooLarge();
		}

		std::vector< DimPosition< NMRData > > pos = this->Parent->GetDimPosSet();
		for( int i = 0; i < this->Parent->NumOfDimensions; i++ )
			pos[ i ].SetPosPts( this->Corner[ i ] );

		for( size_t i = 0; i < this->FullSize; i++ )
		{
			SubsetPtrTableEntry *current = this->PtrTable + i;
			DataPage *page = 0;

			current->ParentOffset = this->Parent->TranslateCoordToOffset( pos );
			current->DataPtr = this->Parent->TranslateOffsetToPtr( current->ParentOffset, this->Const, &page );
			current->NextEntryForPage = 0;

			if( page )
			{
				std::map< size_t, SubsetPageTableEntry * >::iterator found_pt_entry = this->PageTable.find( page->Index );
				if( found_pt_entry == this->PageTable.end() )
				{
					SubsetPageTableEntry *new_pt_entry = new SubsetPageTableEntry;
					new_pt_entry->FirstEntryForPage = current;
					new_pt_entry->NextListEntry = 0;
					new_pt_entry->PageIndex = page->Index;
					new_pt_entry->Valid = true;
					new_pt_entry->Page = page;

					this->PageTable.insert( std::pair< size_t, SubsetPageTableEntry * >( page->Index, new_pt_entry ) );
					page->RegisteredSubsets.insert( this );
					this->RegisteredWith.insert( page );
					current->PageTableEntry = new_pt_entry;
				}
				else
				{
					current->PageTableEntry = found_pt_entry->second;
					current->NextEntryForPage = found_pt_entry->second->FirstEntryForPage;
					found_pt_entry->second->FirstEntryForPage = current;
				}
			}
			else current->PageTableEntry = 0;

			for( int j = this->Parent->NumOfDimensions - 1; j >= 0; j-- )
				if( !this->SizeExt[ j ] ) continue;
				else if( ++( pos[ j ] ) == this->Corner[ j ] + this->SizeExt[ j ] ) 
					pos[ j ].SetPosPts( this->Corner[ j ] );
				else break;
		}
	}


	inline SubsetPtrTableEntry *SubsetData::GetElement( size_t Offset )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Offset > this->FullSize ) throw Exceptions::BadOffset();
#endif
		this->UpdatePtrTableStatus();

		SubsetPtrTableEntry *current = this->PtrTable + Offset;
		if( !current->DataPtr )
			this->ReloadPtrsForPage( current->PageTableEntry );
		return current;
	}


	inline SubsetPtrTableEntry *SubsetData::GetElementForSubsetCoord( const int *Coordinates )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !Coordinates ) throw Exceptions::NullPointerArg();
		for( int i = 0; i < this->NumOfDimensions; i++ )
			if( Coordinates[ i ] < 0 || Coordinates[ i ] >= this->SizeInt[ i ] ) throw Exceptions::BadCoordinates();
#endif
		this->UpdatePtrTableStatus();

		size_t offset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
			offset += Coordinates[ i ] * this->DimIncrSize[ i ];

		return this->GetElement( offset );
	}


	inline SubsetPtrTableEntry *SubsetData::GetElementForSubsetCoord( std::vector< int > Coordinates )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( Coordinates.size() != this->NumOfDimensions ) throw Exceptions::BadCoordinates();
		for( int i = 0; i < this->NumOfDimensions; i++ )
			if( Coordinates[ i ] < 0 || Coordinates[ i ] >= this->SizeInt[ i ] ) throw Exceptions::BadCoordinates();
#endif
		this->UpdatePtrTableStatus();

		size_t offset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
			offset += Coordinates[ i ] * this->DimIncrSize[ i ];

		return this->GetElement( offset );
	}


	inline SubsetPtrTableEntry *SubsetData::GetElementForSubsetCoord( std::vector< DimPosition< Subset > > Coordinates )
	{
#ifdef BEC_NMRDATA_DEBUG
		for( int i = 0; i < this->NumOfDimensions; i++ )
			if( !Coordinates[ i ].InRange() ) throw Exceptions::BadCoordinates();
#endif
		size_t offset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
			offset += Coordinates[ i ].GetPosPts() * this->DimIncrSize[ i ];

		return this->GetElement( offset );
	}


	inline SubsetPtrTableEntry *SubsetData::GetElementForParentCoord( const int *Coordinates )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !Coordinates ) throw Exceptions::NullPointerArg();
		for( int i = 0; i < this->Parent->NumOfDimensions; i++ )
		{
			if( Coordinates[ i ] < 0 || Coordinates[ i ] >= this->Parent->DimensionSizes[ i ] ) throw Exceptions::BadCoordinates();
			if( this->SizeExt[ i ] > 0 )
			{
				if( Coordinates[ i ] < this->Corner[ i ] || Coordinates[ i ] >= this->Corner[ i ] + this->SizeExt[ i ] ) 
					throw Exceptions::PtOutsideSubset();
			}
			else if( Coordinates[ i ] != this->Corner[ i ] ) throw Exceptions::PtOutsideSubset();
		}
#endif
		size_t offset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
			offset += ( Coordinates[ i ] - this->Corner[ i ] ) * this->DimIncrSize[ i ];

		return this->GetElement( offset );
	}


	inline SubsetPtrTableEntry *SubsetData::GetElementForParentCoord( std::vector< DimPosition< NMRData > > Coordinates )
	{
#ifdef BEC_NMRDATA_DEBUG
		for( int i = 0; i < this->Parent->NumOfDimensions; i++ )
		{
			if( !Coordinates[ i ].InRange() ) throw Exceptions::BadCoordinates();
			if( this->SizeExt[ i ] > 0 )
			{
				if( Coordinates[ i ].GetPosPts() < this->Corner[ i ] || Coordinates[ i ].GetPosPts() >= this->Corner[ i ] + this->SizeExt[ i ] ) 
					throw Exceptions::PtOutsideSubset();
			}
			else if( Coordinates[ i ].GetPosPts() != this->Corner[ i ] ) throw Exceptions::PtOutsideSubset();
		}
#endif
		size_t offset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
			offset += ( Coordinates[ i ].GetPosPts() - this->Corner[ i ] ) * this->DimIncrSize[ i ];

		return this->GetElement( offset );
	}


	inline void SubsetData::MarkPageInvalid( size_t PageIndex )
	{
		SubsetPageTableEntry *pt_entry = this->PageTable.find( PageIndex )->second;
		pt_entry->Valid = false;
		pt_entry->NextListEntry = this->NewlyInvalidPageList;
		this->NewlyInvalidPageList = pt_entry;
		pt_entry->Page->RegisteredSubsets.erase( this );
		this->RegisteredWith.erase( pt_entry->Page );
		pt_entry->Page = 0;
	}


	inline void SubsetData::UpdatePtrTableStatus( void )
	{
		for( SubsetPageTableEntry *current = this->NewlyInvalidPageList, *next = 0; current; current = next )
		{
			for( SubsetPtrTableEntry *ptr = current->FirstEntryForPage; ptr; ptr = ptr->NextEntryForPage )
				ptr->DataPtr = 0;

			next = current->NextListEntry;
			current->NextListEntry = 0;
		}
		this->NewlyInvalidPageList = 0;
	}


	inline void SubsetData::ReloadPtrsForPage( SubsetPageTableEntry *PageTableEntry )
	{
		this->Parent->TranslateOffsetToPtr( PageTableEntry->FirstEntryForPage->ParentOffset, this->Const, &( PageTableEntry->Page ) );

		for( SubsetPtrTableEntry *ptr = PageTableEntry->FirstEntryForPage; ptr; ptr = ptr->NextEntryForPage )
			ptr->DataPtr = this->Parent->TranslateOffsetToPtr( ptr->ParentOffset, this->Const );
		
		PageTableEntry->Valid = true;
		PageTableEntry->Page->RegisteredSubsets.insert( this );
		this->RegisteredWith.insert( PageTableEntry->Page );
	}


	inline ptrdiff_t SubsetData::TranslateParentOffsetToSubsetOffset( size_t ParentOffset )
	{
		ptrdiff_t offset;
		std::vector< DimPosition< NMRData > > pos = this->Parent->TranslateOffsetToCoord( ParentOffset );
		for( int i = 0, j = 0; i < this->Parent->NumOfDimensions; i++ )
		{
			int dimpos = pos[ i ].GetPosPts();
			if( dimpos < this->Corner[ i ] ) return -1;
			if( this->SizeExt[ i ] )
			{
				if( dimpos >= this->Corner[ i ] + this->SizeExt[ i ] ) return -1;
				
				offset += ( dimpos - this->Corner[ i ] ) * this->DimIncrSize[ j ];
				j++;
			}
			else if( dimpos != this->Corner[ i ] ) return -1;
		}
		return offset;
	}


	//	End of SubsetData implementation


	inline Subset::iterator::iterator( const Subset::iterator &Source ) : current( Source.current )
	{
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
	}

	inline Subset::iterator &Subset::iterator::operator=( const Subset::iterator &Source )
	{
		this->current.Assign( Source.current );
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
		return *this;
	}

	inline bool Subset::iterator::operator==( const Subset::iterator &x ) const
	{
		if( x.past_end ) return this->past_end;
		return this->offset == x.offset;
	}

	inline bool Subset::iterator::operator!=( const Subset::iterator &x ) const
	{
		if( x.past_end ) return !this->past_end;
		return this->offset != x.offset;
	}

	inline Reference &Subset::iterator::operator*( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return this->current;
	}

	inline Reference *Subset::iterator::operator->( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return &( this->current );
	}

	inline Subset::iterator &Subset::iterator::operator++( void )		//prefix ++
	{
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->Data->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return *this;
	}

	inline Subset::iterator Subset::iterator::operator++( int )		//postfix ++
	{
		iterator tmp( *this );
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->Data->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return tmp;
	}

	inline Subset::iterator &Subset::iterator::operator--( void )		//prefix --
	{
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return *this;
	}

	inline Subset::iterator Subset::iterator::operator--( int )		//postfix --
	{
		iterator tmp( *this );
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return tmp;
	}

	inline Subset::iterator &Subset::iterator::operator+=( size_type n )
	{
		this->offset += n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset >= this->parent->Data->FullSize )
		{
			this->offset = this->parent->Data->FullSize;
			this->past_end = true;
		}
		this->is_current = false;
		return *this;
	}

	inline Subset::iterator Subset::iterator::operator+( size_type n ) const
	{
		iterator tmp( *this );
		tmp += n;
		return tmp;
	}

	inline Subset::iterator &Subset::iterator::operator-=( size_type n )
	{
		this->offset -= n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset < this->parent->Data->FullSize ) this->past_end = false;
		this->is_current = false;
		return *this;
	}

	inline Subset::iterator Subset::iterator::operator-( size_type n ) const
	{
		iterator tmp( *this );
		tmp -= n;
		return tmp;
	}

	inline Subset::iterator::difference_type Subset::iterator::operator-( const iterator &a ) const
	{
		return this->offset - a.offset;
	}

	inline Reference &Subset::iterator::operator[]( size_type n ) const
	{
		return *( *this + n );
	}

	inline bool Subset::iterator::operator<( const iterator &x ) const
	{
		return this->offset < x.offset;
	}

	inline bool Subset::iterator::operator<=( const iterator &x ) const
	{
		return this->offset <= x.offset;
	}

	inline bool Subset::iterator::operator>( const iterator &x ) const
	{
		return this->offset > x.offset;
	}

	inline bool Subset::iterator::operator>=( const iterator &x ) const
	{
		return this->offset >= x.offset;
	}

	inline Subset::iterator::iterator( Subset *Parent, size_t Offset, bool PastEnd ) 
		: offset( Offset ), parent( Parent ), past_end( PastEnd ), is_current( PastEnd ? false : true ), current( ( *Parent )[ PastEnd ? 0 : Offset ] )
	{  }

	inline Subset::const_iterator::const_iterator( const Subset::const_iterator &Source ) : current( Source.current )
	{
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
	}

	inline Subset::const_iterator &Subset::const_iterator::operator=( const Subset::const_iterator &Source )
	{
		this->current.Assign( Source.current );
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
		return *this;
	}

	inline bool Subset::const_iterator::operator==( const Subset::const_iterator &x ) const
	{
		if( x.past_end ) return this->past_end;
		return this->offset == x.offset;
	}

	inline bool Subset::const_iterator::operator!=( const Subset::const_iterator &x ) const
	{
		if( x.past_end ) return !this->past_end;
		return this->offset != x.offset;
	}

	inline const Reference &Subset::const_iterator::operator*( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return this->current;
	}

	inline const Reference *Subset::const_iterator::operator->( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return &( this->current );
	}

	inline Subset::const_iterator &Subset::const_iterator::operator++( void )	//prefix ++
	{
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->Data->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return *this;
	}

	inline Subset::const_iterator Subset::const_iterator::operator++( int )		//postfix ++
	{
		const_iterator tmp( *this );
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->Data->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return tmp;
	}

	inline Subset::const_iterator &Subset::const_iterator::operator--( void )	//prefix --
	{
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return *this;
	}

	inline Subset::const_iterator Subset::const_iterator::operator--( int )		//postfix --
	{
		const_iterator tmp( *this );
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return tmp;
	}

	inline Subset::const_iterator &Subset::const_iterator::operator+=( size_type n )
	{
		this->offset += n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset >= this->parent->Data->FullSize )
		{
			this->offset = this->parent->Data->FullSize;
			this->past_end = true;
		}
		this->is_current = false;
		return *this;
	}

	inline Subset::const_iterator Subset::const_iterator::operator+( size_type n ) const
	{
		const_iterator tmp( *this );
		tmp += n;
		return tmp;
	}

	inline Subset::const_iterator &Subset::const_iterator::operator-=( size_type n )
	{
		this->offset -= n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset < this->parent->Data->FullSize ) this->past_end = false;
		this->is_current = false;
		return *this;
	}

	inline Subset::const_iterator Subset::const_iterator::operator-( size_type n ) const
	{
		const_iterator tmp( *this );
		tmp -= n;
		return tmp;
	}

	inline Subset::const_iterator::difference_type Subset::const_iterator::operator-( const const_iterator &a ) const
	{
		return this->offset - a.offset;
	}

	inline const Reference &Subset::const_iterator::operator[]( size_type n ) const
	{
		return *( *this + n );
	}

	inline bool Subset::const_iterator::operator<( const const_iterator &x ) const
	{
		return this->offset < x.offset;
	}

	inline bool Subset::const_iterator::operator<=( const const_iterator &x ) const
	{
		return this->offset <= x.offset;
	}

	inline bool Subset::const_iterator::operator>( const const_iterator &x ) const
	{
		return this->offset > x.offset;
	}

	inline bool Subset::const_iterator::operator>=( const const_iterator &x ) const
	{
		return this->offset >= x.offset;
	}

	inline Subset::const_iterator::const_iterator( const Subset *Parent, size_t Offset, bool PastEnd ) 
		: offset( Offset ), parent( Parent ), past_end( PastEnd ), is_current( PastEnd ? false : true ), current( ( *Parent )[ PastEnd ? 0 : Offset ] )
	{  }


	//	End of Subset::iterator implementation


	inline NMRData::iterator::iterator( const NMRData::iterator &Source ) : current( Source.current )
	{
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
	}

	inline NMRData::iterator &NMRData::iterator::operator=( const NMRData::iterator &Source )
	{
		this->current.Assign( Source.current );
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
		return *this;
	}

	inline bool NMRData::iterator::operator==( const NMRData::iterator &x ) const
	{
		if( x.past_end ) return this->past_end;
		return this->offset == x.offset;
	}

	inline bool NMRData::iterator::operator!=( const NMRData::iterator &x ) const
	{
		if( x.past_end ) return !this->past_end;
		return this->offset != x.offset;
	}

	inline Reference &NMRData::iterator::operator*( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return this->current;
	}

	inline Reference *NMRData::iterator::operator->( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return &( this->current );
	}

	inline NMRData::iterator &NMRData::iterator::operator++( void )		//prefix ++
	{
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return *this;
	}

	inline NMRData::iterator NMRData::iterator::operator++( int )		//postfix ++
	{
		iterator tmp( *this );
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return tmp;
	}

	inline NMRData::iterator &NMRData::iterator::operator--( void )		//prefix --
	{
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return *this;
	}

	inline NMRData::iterator NMRData::iterator::operator--( int )		//postfix --
	{
		iterator tmp( *this );
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return tmp;
	}

	inline NMRData::iterator &NMRData::iterator::operator+=( size_type n )
	{
		this->offset += n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset >= this->parent->FullSize )
		{
			this->offset = this->parent->FullSize;
			this->past_end = true;
		}
		this->is_current = false;
		return *this;
	}

	inline NMRData::iterator NMRData::iterator::operator+( size_type n ) const
	{
		iterator tmp( *this );
		tmp += n;
		return tmp;
	}

	inline NMRData::iterator &NMRData::iterator::operator-=( size_type n )
	{
		this->offset -= n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset < this->parent->FullSize ) this->past_end = false;
		this->is_current = false;
		return *this;
	}

	inline NMRData::iterator NMRData::iterator::operator-( size_type n ) const
	{
		iterator tmp( *this );
		tmp -= n;
		return tmp;
	}

	inline NMRData::iterator::difference_type NMRData::iterator::operator-( const iterator &a ) const
	{
		return this->offset - a.offset;
	}

	inline Reference &NMRData::iterator::operator[]( size_type n ) const
	{
		return *( *this + n );
	}

	inline bool NMRData::iterator::operator<( const iterator &x ) const
	{
		return this->offset < x.offset;
	}

	inline bool NMRData::iterator::operator<=( const iterator &x ) const
	{
		return this->offset <= x.offset;
	}

	inline bool NMRData::iterator::operator>( const iterator &x ) const
	{
		return this->offset > x.offset;
	}

	inline bool NMRData::iterator::operator>=( const iterator &x ) const
	{
		return this->offset >= x.offset;
	}

	inline NMRData::iterator::iterator( NMRData *Parent, size_t Offset, bool PastEnd ) 
		: offset( Offset ), parent( Parent ), past_end( PastEnd ), is_current( PastEnd ? false : true ), current( ( *Parent )[ PastEnd ? 0 : Offset ] )
	{  }

	inline NMRData::const_iterator::const_iterator( const NMRData::const_iterator &Source ) : current( Source.current )
	{
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
	}

	inline NMRData::const_iterator &NMRData::const_iterator::operator=( const NMRData::const_iterator &Source )
	{
		this->current.Assign( Source.current );
		this->is_current = Source.is_current;
		this->past_end = Source.past_end;
		this->offset = Source.offset;
		this->parent = Source.parent;
		return *this;
	}

	inline bool NMRData::const_iterator::operator==( const NMRData::const_iterator &x ) const
	{
		if( x.past_end ) return this->past_end;
		return this->offset == x.offset;
	}

	inline bool NMRData::const_iterator::operator!=( const NMRData::const_iterator &x ) const
	{
		if( x.past_end ) return !this->past_end;
		return this->offset != x.offset;
	}

	inline const Reference &NMRData::const_iterator::operator*( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return this->current;
	}

	inline const Reference *NMRData::const_iterator::operator->( void ) const
	{
		if( !this->is_current )
		{
			this->current.Assign( ( *( this->parent ) )[ this->offset ] );
			this->is_current = true;
		}
		return &( this->current );
	}

	inline NMRData::const_iterator &NMRData::const_iterator::operator++( void )	//prefix ++
	{
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return *this;
	}

	inline NMRData::const_iterator NMRData::const_iterator::operator++( int )		//postfix ++
	{
		const_iterator tmp( *this );
		if( !this->past_end )
		{
			this->offset++;
			if( this->offset == this->parent->FullSize ) this->past_end = true;
			this->is_current = false;
		}
		return tmp;
	}

	inline NMRData::const_iterator &NMRData::const_iterator::operator--( void )	//prefix --
	{
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return *this;
	}

	inline NMRData::const_iterator NMRData::const_iterator::operator--( int )		//postfix --
	{
		const_iterator tmp( *this );
		if( this->offset )
		{
			this->offset--;
			if( this->past_end ) this->past_end = false;
			this->is_current = false;
		}
		return tmp;
	}

	inline NMRData::const_iterator &NMRData::const_iterator::operator+=( size_type n )
	{
		this->offset += n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset >= this->parent->FullSize )
		{
			this->offset = this->parent->FullSize;
			this->past_end = true;
		}
		this->is_current = false;
		return *this;
	}

	inline NMRData::const_iterator NMRData::const_iterator::operator+( size_type n ) const
	{
		const_iterator tmp( *this );
		tmp += n;
		return tmp;
	}

	inline NMRData::const_iterator &NMRData::const_iterator::operator-=( size_type n )
	{
		this->offset -= n;
		if( this->offset < 0 ) this->offset = 0;
		if( this->offset < this->parent->FullSize ) this->past_end = false;
		this->is_current = false;
		return *this;
	}

	inline NMRData::const_iterator NMRData::const_iterator::operator-( size_type n ) const
	{
		const_iterator tmp( *this );
		tmp -= n;
		return tmp;
	}

	inline NMRData::const_iterator::difference_type NMRData::const_iterator::operator-( const const_iterator &a ) const
	{
		return this->offset - a.offset;
	}

	inline const Reference &NMRData::const_iterator::operator[]( size_type n ) const
	{
		return *( *this + n );
	}

	inline bool NMRData::const_iterator::operator<( const const_iterator &x ) const
	{
		return this->offset < x.offset;
	}

	inline bool NMRData::const_iterator::operator<=( const const_iterator &x ) const
	{
		return this->offset <= x.offset;
	}

	inline bool NMRData::const_iterator::operator>( const const_iterator &x ) const
	{
		return this->offset > x.offset;
	}

	inline bool NMRData::const_iterator::operator>=( const const_iterator &x ) const
	{
		return this->offset >= x.offset;
	}

	inline NMRData::const_iterator::const_iterator( const NMRData *Parent, size_t Offset, bool PastEnd ) 
		: offset( Offset ), parent( Parent ), past_end( PastEnd ), is_current( PastEnd ? false : true ), current( ( *Parent )[ PastEnd ? 0 : Offset ] )
	{  }


	inline void NMRData::operator+=( float Scalar )
	{
		std::binder1st< std::plus< float > > temp = std::bind1st( std::plus< float >(), Scalar );
		this->ApplyChangeToAll_ByComponent_Fastest( temp );
	}


	inline void NMRData::operator+=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::plus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::plus< float > >( std::plus< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator+=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::plus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::plus< float > >( std::plus< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator+=( const Subset &Operand )
	{
		*this = *this + Operand;
	}


	inline void NMRData::operator+=( const NMRData &Operand )
	{
		*this = *this + Operand;
	}


	template< typename T >
	inline void NMRData::operator+=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this + Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRData::operator+=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this + Operand;
	}


	inline void NMRData::operator-=( float Scalar )
	{
		std::binder1st< std::minus< float > > temp = std::bind1st( std::minus< float >(), Scalar );
		this->ApplyChangeToAll_ByComponent_Fastest( temp );
	}


	inline void NMRData::operator-=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::minus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::minus< float > >( std::minus< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator-=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::minus< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::minus< float > >( std::minus< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator-=( const Subset &Operand )
	{
		*this = *this - Operand;
	}


	inline void NMRData::operator-=( const NMRData &Operand )
	{
		*this = *this - Operand;
	}


	template< typename T >
	inline void NMRData::operator-=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this - Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRData::operator-=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this - Operand;
	}


	inline void NMRData::operator*=( float Scalar )
	{
		std::binder1st< std::multiplies< float > > temp = std::bind1st( std::multiplies< float >(), Scalar );
		this->ApplyChangeToAll_ByComponent_Fastest( temp );
	}


	inline void NMRData::operator*=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::multiplies< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::multiplies< float > >( std::multiplies< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator*=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::multiplies< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::multiplies< float > >( std::multiplies< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator*=( const Subset &Operand )
	{
		*this = *this * Operand;
	}


	inline void NMRData::operator*=( const NMRData &Operand )
	{
		*this = *this * Operand;
	}


	template< typename T >
	inline void NMRData::operator*=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this * Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRData::operator*=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this * Operand;
	}


	inline void NMRData::operator/=( float Scalar )
	{
		std::binder1st< std::divides< float > > temp = std::bind1st( std::divides< float >(), Scalar );
		this->ApplyChangeToAll_ByComponent_Fastest( temp );
	}


	inline void NMRData::operator/=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::divides< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::divides< float > >( std::divides< float >() ), Scalar );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator/=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrOpAdaptor< std::divides< float > > > temp = std::bind2nd( ValToPtrOpAdaptor< std::divides< float > >( std::divides< float >() ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::operator/=( const Subset &Operand )
	{
		*this = *this / Operand;
	}


	inline void NMRData::operator/=( const NMRData &Operand )
	{
		*this = *this / Operand;
	}


	template< typename T >
	inline void NMRData::operator/=( const NMRDataMathSelector< T > &Operand )
	{
		*this = *this / Operand;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRData::operator/=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		*this = *this / Operand;
	}


	inline NMRData &NMRData::operator=( float Scalar )
	{
		BEC_Misc::binder_constant_as_unary< float, float > temp = BEC_Misc::bind_constant_as_unary( Scalar );
		this->ApplyChangeToAll_ByComponent_Fastest( temp );
		return *this;
	}


	inline NMRData &NMRData::operator=( const Value &Scalar )
	{
		std::binder2nd< ValToPtrConstAdaptor > temp = std::bind2nd( ValToPtrConstAdaptor(), Scalar );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
		return *this;
	}


	inline NMRData &NMRData::operator=( const Reference &Scalar )
	{
		std::binder2nd< ValToPtrConstAdaptor > temp = std::bind2nd( ValToPtrConstAdaptor(), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
		return *this;
	}


	inline NMRData &NMRData::operator=( const Subset &Operand )
	{
		this->PerformMathAssignment( NMRDataMathSubset( Operand ) );
		return *this;
	}


	inline NMRData &NMRData::operator=( const NMRData &Operand )
	{
		this->PerformMathAssignment( NMRDataMathObj( &Operand ) );
		return *this;
	}


	template< typename T >
	inline NMRData &NMRData::operator=( const NMRDataMathSelector< T > &Operand )
	{
		this->PerformMathAssignment( Operand );
		return *this;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline NMRData &NMRData::operator=( const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		this->PerformMathAssignment( Operand );
		return *this;
	}


	template< typename T >
	inline void NMRData::PerformMathAssignment( const T &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		if( Operand.GetNumOfComponents() != this->Components ) throw Exceptions::ComponentsDontMatch();
#endif

		BEC_Misc::ptr_vector< const NMRData > objects = Operand.GetObjectList();
		bool cachemode = true;

		for( BEC_Misc::ptr_vector< const NMRData >::iterator i = objects.begin(); i != objects.end(); i++ )
		{
			if( !i->GotData ) throw Exceptions::DataNotOpen();
			if( i->NumOfDimensions != this->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->FullSize != this->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->NumOfDimensions; j++ )
			{
				if( this->DimensionSizes[ j ] != i->DimensionSizes[ j ] )
					throw Exceptions::NotCompatible();
			}
			if( this->CacheMode != i->CacheMode ) cachemode = false;
			else
			{
				if( this->CacheMode == Submatrix )
				{
					if( this->SubmatrixCount != i->SubmatrixCount ) cachemode = false;
					else
					{
						for( int j = 0; j < this->NumOfDimensions; j++ )
							if( i->SubmatrixSizes[ j ] != this->SubmatrixSizes[ j ] )
								cachemode = false;
					}
				}
				else if( this->CacheMode == Vector )
				{
					if( this->CacheDim1 != i->CacheDim1 ) cachemode = false;
				}
				else if( this->CacheMode == Plane )
				{
					if( this->CacheDim1 != i->CacheDim1 || this->CacheDim2 != i->CacheDim2 ) cachemode = false;
				}
				else if( this->CacheMode == MemOnly ) cachemode = false;
			}
		}

		std::vector< Subset > subsets = Operand.GetSubsetList();
		if( !subsets.empty() ) cachemode = false;
		for( std::vector< Subset >::iterator i = subsets.begin(); i != subsets.end(); i++ )
		{
			if( i->GetNumOfDims() != this->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->GetFullSize() != this->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->NumOfDimensions; j++ )
			{
				if( this->DimensionSizes[ j ] != i->GetDimensionSize( j ) )
					throw Exceptions::NotCompatible();
			}
		}
		
		if( cachemode ) Operand.SetCacheTraversal();
		else Operand.SetOffsetTraversal();

		if( cachemode ) this->ApplyChangeToAll_ByComponent_Fastest( Operand );
		else this->ApplyChangeToAll_ByComponent_OffsetOrder( Operand );
	}


	template< typename T >
	inline void NMRData::PerformMathAssignment_Component( int Component, const T &Operand )
	{
#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		if( Operand.GetNumOfComponents() != 1 ) throw Exceptions::ComponentsDontMatch();
		if( Component < 0 || Component >= this->Components ) throw Exceptions::BadComponentIndex();
#endif

		BEC_Misc::ptr_vector< const NMRData > objects = Operand.GetObjectList();
		bool cachemode = true;

		for( BEC_Misc::ptr_vector< const NMRData >::iterator i = objects.begin(); i != objects.end(); i++ )
		{
			if( !i->GotData ) throw Exceptions::DataNotOpen();
			if( i->NumOfDimensions != this->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->FullSize != this->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->NumOfDimensions; j++ )
			{
				if( this->DimensionSizes[ j ] != i->DimensionSizes[ j ] )
					throw Exceptions::NotCompatible();
			}
			if( this->CacheMode != i->CacheMode ) cachemode = false;
			else
			{
				if( this->CacheMode == Submatrix )
				{
					if( this->SubmatrixCount != i->SubmatrixCount ) cachemode = false;
					else
					{
						for( int j = 0; j < this->NumOfDimensions; j++ )
							if( i->SubmatrixSizes[ j ] != this->SubmatrixSizes[ j ] )
								cachemode = false;
					}
				}
				else if( this->CacheMode == Vector )
				{
					if( this->CacheDim1 != i->CacheDim1 ) cachemode = false;
				}
				else if( this->CacheMode == Plane )
				{
					if( this->CacheDim1 != i->CacheDim1 || this->CacheDim2 != i->CacheDim2 ) cachemode = false;
				}
				else if( this->CacheMode == MemOnly ) cachemode = false;
			}
		}

		std::vector< Subset > subsets = Operand.GetSubsetList();
		if( !subsets.empty() ) cachemode = false;
		for( std::vector< Subset >::iterator i = subsets.begin(); i != subsets.end(); i++ )
		{
			if( i->GetNumOfDims() != this->NumOfDimensions )
				throw Exceptions::NotCompatible();
			if( i->GetFullSize() != this->FullSize )
				throw Exceptions::NotCompatible();
			for( int j = 0; j < this->NumOfDimensions; j++ )
			{
				if( this->DimensionSizes[ j ] != i->GetDimensionSize( j ) )
					throw Exceptions::NotCompatible();
			}
		}
		
		if( cachemode ) Operand.SetCacheTraversal();
		else Operand.SetOffsetTraversal();

		if( cachemode ) this->ApplyChangeToAll_Component_Fastest( Operand, Component );
		else this->ApplyChangeToAll_Component_OffsetOrder( Operand, Component );
	}


	inline void NMRData::SetComponentTo( int Component, float Scalar )
	{
		BEC_Misc::binder_constant_as_unary< float, float > temp = BEC_Misc::bind_constant_as_unary( Scalar );
		this->ApplyChangeToAll_Component_Fastest( temp, Component );
	}


	inline void NMRData::SetComponentTo( int Component, const Value &Scalar )
	{
		std::binder2nd< ValToPtrComponentAdaptor > temp = std::bind2nd( ValToPtrComponentAdaptor( Component ), Scalar );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::SetComponentTo( int Component, const Reference &Scalar )
	{
		std::binder2nd< ValToPtrComponentAdaptor > temp = std::bind2nd( ValToPtrComponentAdaptor( Component ), Scalar.ConvertToValue() );
		this->ApplyChangeToAll_Ptr_Fastest( temp );
	}


	inline void NMRData::SetComponentTo( int Component, const Subset &Operand )
	{
		this->PerformMathAssignment_Component( Component, NMRDataMathSubset( Operand ) );
	}


	inline void NMRData::SetComponentTo( int Component, const NMRData &Operand )
	{
		this->PerformMathAssignment_Component( Component, NMRDataMathObj( &Operand ) );
	}


	template< typename T >
	inline void NMRData::SetComponentTo( int Component, const NMRDataMathSelector< T > &Operand )
	{
		this->PerformMathAssignment_Component( Component, Operand );
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRData::SetComponentTo( int Component, const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		this->PerformMathAssignment_Component( Component, Operand );
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline float NMRDataMathOp< T_LHS, T_RHS, T_Op >::operator()( const float dummy ) const
	{
		return this->op( this->lhs(), this->rhs() );
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathOp< T_LHS, T_RHS, T_Op >::GetObjectList( void ) const
	{
		BEC_Misc::ptr_vector< const NMRData > list_lhs = this->lhs.GetObjectList();
		BEC_Misc::ptr_vector< const NMRData > list_rhs = this->rhs.GetObjectList();

		list_lhs.insert( list_lhs.begin(), list_rhs.begin(), list_rhs.end() );
		return list_lhs;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline std::vector< Subset > NMRDataMathOp< T_LHS, T_RHS, T_Op >::GetSubsetList( void ) const
	{
		std::vector< Subset > list_lhs = this->lhs.GetSubsetList();
		std::vector< Subset > list_rhs = this->rhs.GetSubsetList();

		list_lhs.insert( list_lhs.begin(), list_rhs.begin(), list_rhs.end() );
		return list_lhs;
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline int NMRDataMathOp< T_LHS, T_RHS, T_Op >::GetNumOfComponents( void ) const
	{
		int num_lhs = this->lhs.GetNumOfComponents(), num_rhs = this->rhs.GetNumOfComponents();

		if( num_lhs && !num_rhs ) return num_lhs;
		else if( !num_lhs && num_rhs ) return num_rhs;
		else if( !num_lhs && !num_rhs ) return 0;
		else if( num_lhs == num_rhs ) return num_lhs;
		else throw Exceptions::ComponentsDontMatch();
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRDataMathOp< T_LHS, T_RHS, T_Op >::SetCacheTraversal( void ) const
	{
		this->lhs.SetCacheTraversal();
		this->rhs.SetCacheTraversal();
	}


	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline void NMRDataMathOp< T_LHS, T_RHS, T_Op >::SetOffsetTraversal( void ) const
	{
		this->lhs.SetOffsetTraversal();
		this->rhs.SetOffsetTraversal();
	}


	inline float NMRDataMathScalar::operator()( void ) const
	{
		return this->val;
	}


	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathScalar::GetObjectList( void ) const
	{
		return BEC_Misc::ptr_vector< const NMRData >();
	}


	inline std::vector< Subset > NMRDataMathScalar::GetSubsetList( void ) const
	{
		return std::vector< Subset >();
	}


	inline int NMRDataMathScalar::GetNumOfComponents( void ) const
	{
		return 0;
	}


	inline void NMRDataMathScalar::SetCacheTraversal( void ) const {  }
	inline void NMRDataMathScalar::SetOffsetTraversal( void ) const {  }


	inline float NMRDataMathVal::operator()( void ) const
	{
		int comp = this->nextcomp;
		if( ++( this->nextcomp ) == this->comp ) this->nextcomp = 0;
		return this->val[ comp ];
	}


	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathVal::GetObjectList( void ) const
	{
		return BEC_Misc::ptr_vector< const NMRData >();
	}


	inline std::vector< Subset > NMRDataMathVal::GetSubsetList( void ) const
	{
		return std::vector< Subset >();
	}


	inline int NMRDataMathVal::GetNumOfComponents( void ) const
	{
		return this->comp;
	}


	inline void NMRDataMathVal::SetCacheTraversal( void ) const {  }
	inline void NMRDataMathVal::SetOffsetTraversal( void ) const {  }


	inline float NMRDataMathRef::operator()( void ) const
	{
		float val = this->ref[ this->nextcomp ];
		if( ++( this->nextcomp ) == this->comp ) this->nextcomp = 0;
		return val;
	}


	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathRef::GetObjectList( void ) const
	{
		return BEC_Misc::ptr_vector< const NMRData >();
	}


	inline std::vector< Subset > NMRDataMathRef::GetSubsetList( void ) const
	{
		return std::vector< Subset >();
	}


	inline int NMRDataMathRef::GetNumOfComponents( void ) const
	{
		return this->comp;
	}


	inline void NMRDataMathRef::SetCacheTraversal( void ) const {  }
	inline void NMRDataMathRef::SetOffsetTraversal( void ) const {  }


	inline NMRDataMathSubset::NMRDataMathSubset( const Subset &Operand ) : obj( Operand ), current( 0 ), 
				nextcomp( 0 ), comp( Operand.GetNumOfComponents() )
	{
		this->current = this->obj.Data->PtrTable;
	}
	
	inline float NMRDataMathSubset::operator()( const float dummy ) const
	{
		if( this->nextcomp == this->comp )
		{
			this->nextcomp = 0;
			this->current++;
			if( !this->current->DataPtr )
				this->obj.Data->ReloadPtrsForPage( this->current->PageTableEntry );
		}

		return this->current->DataPtr[ this->nextcomp++ ];
	}


	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathSubset::GetObjectList( void ) const
	{
		return BEC_Misc::ptr_vector< const NMRData >();
	}


	inline std::vector< Subset > NMRDataMathSubset::GetSubsetList( void ) const
	{
		std::vector< Subset > out;
		out.push_back( this->obj );
		return out;
	}


	inline int NMRDataMathSubset::GetNumOfComponents( void ) const
	{
		return this->comp;
	}


	inline void NMRDataMathSubset::SetCacheTraversal( void ) const {  }
	inline void NMRDataMathSubset::SetOffsetTraversal( void ) const {  }


	inline NMRDataMathObj::~NMRDataMathObj( void )
	{
		delete this->ct;
		delete this->ot;
	}


	inline float NMRDataMathObj::operator()( const float dummy ) const
	{
		float val = this->current[ this->nextcomp ];
		if( ++( this->nextcomp ) == this->comp )
		{
			this->nextcomp = 0;
			this->current = this->mode ? **( this->ct ) : **( this->ot );
			this->mode ? ++( *( this->ct ) ) : ++( *( this->ot ) );
		}
		return val;
	}


	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathObj::GetObjectList( void ) const
	{
		BEC_Misc::ptr_vector< const NMRData > out;
		out.push_back( this->obj );
		return out;
	}


	inline std::vector< Subset > NMRDataMathObj::GetSubsetList( void ) const
	{
		return std::vector< Subset >();
	}


	inline int NMRDataMathObj::GetNumOfComponents( void ) const
	{
		return this->comp;
	}


	inline void NMRDataMathObj::SetCacheTraversal( void ) const
	{
		this->mode = true;
		if( this->ct ) delete this->ct;
		this->ct = this->obj->GetTraverserCacheOrder();
		this->current = **( this->ct );
		++( *( this->ct ) );
	}


	inline void NMRDataMathObj::SetOffsetTraversal( void ) const
	{
		this->mode = false;
		if( this->ot ) delete this->ot;
		this->ot = this->obj->GetTraverserOffsetOrder();
		this->current = **( this->ot );
		++( *( this->ot ) );
	}


	template< typename T >
	inline float NMRDataMathSelector< T >::operator() ( const float dummy ) const
	{
		float val;
		for( int i = 0; i < this->comp_higher; i++ )
			if( i == this->comp_selected )
				val = this->exp();
			else
				this->exp();

		return val;
	}


	template< typename T >
	inline BEC_Misc::ptr_vector< const NMRData > NMRDataMathSelector< T >::GetObjectList( void ) const
	{
		return this->exp.GetObjectList();
	}


	template< typename T >
	inline std::vector< Subset > NMRDataMathSelector< T >::GetSubsetList( void ) const
	{
		return std::vector< Subset >();
	}


	template< typename T >
	inline int NMRDataMathSelector< T >::GetNumOfComponents( void ) const
	{
		return 1;
	}


	template< typename T >
	inline void NMRDataMathSelector< T >::SetCacheTraversal( void ) const
	{
		this->exp.SetCacheTraversal();
	}


	template< typename T >
	inline void NMRDataMathSelector< T >::SetOffsetTraversal( void ) const
	{
		this->exp.SetOffsetTraversal();
	}


	inline NMRDataMathSelector< NMRDataMathScalar > GetComponent( int Component, float Scalar )
	{
		return NMRDataMathSelector< NMRDataMathScalar >( Component, NMRDataMathScalar( Scalar ) );
	}

	
	inline NMRDataMathSelector< NMRDataMathVal > GetComponent( int Component, const Value &Scalar )
	{
		return NMRDataMathSelector< NMRDataMathVal >( Component, NMRDataMathVal( Scalar ) );
	}

	
	inline NMRDataMathSelector< NMRDataMathRef > GetComponent( int Component, const Reference &Scalar )
	{
		return NMRDataMathSelector< NMRDataMathRef >( Component, NMRDataMathRef( Scalar ) );
	}

	
	inline NMRDataMathSelector< NMRDataMathSubset > GetComponent( int Component, const Subset &Operand )
	{
		return NMRDataMathSelector< NMRDataMathSubset >( Component, NMRDataMathSubset( Operand ) );
	}

	
	inline NMRDataMathSelector< NMRDataMathObj > GetComponent( int Component, const NMRData &Operand )
	{
		return NMRDataMathSelector< NMRDataMathObj >( Component, NMRDataMathObj( &Operand ) );
	}

	
	template< typename T >
	inline NMRDataMathSelector< NMRDataMathSelector< T > > GetComponent( int Component, const NMRDataMathSelector< T > &Operand )
	{
		return NMRDataMathSelector< NMRDataMathSelector< T > >( Component, Operand );
	}
	
	
	template< typename T_LHS, typename T_RHS, typename T_Op >
	inline NMRDataMathSelector< NMRDataMathOp< T_LHS, T_RHS, T_Op > > GetComponent( int Component, const NMRDataMathOp< T_LHS, T_RHS, T_Op > &Operand )
	{
		return NMRDataMathSelector< NMRDataMathOp< T_LHS, T_RHS, T_Op > >( Component, Operand );
	}


	inline const float *NMRData::TraverserOffsetOrder::operator*( void ) const
	{
		return this->current;
	}


	inline void NMRData::TraverserOffsetOrder::operator++( void )
	{
		this->offset++;
		if( this->offset < this->parent->FullSize )
			this->current = this->parent->TranslateOffsetToPtr( this->offset, true );
	}


	inline void NMRData::TraverserOffsetOrder::operator++( int )
	{
		NMRData::TraverserOffsetOrder out( *this );
		this->offset++;
		if( this->offset < this->parent->FullSize )
			this->current = this->parent->TranslateOffsetToPtr( this->offset, true );
	}

	inline NMRData::TraverserCacheOrder::TraverserCacheOrder( const NMRData *Parent ) : 
		parent( Parent ), end( Parent->CacheMap.end() ), NextIndex( 0 ), PageSize( 0 ), TempData( 0 ), TempCoord( 0 ), TempPlaneIncrSizes( 0 ),
			usetemp( false ), done( false )
	{
		( *( this->parent ) )[ this->parent->FullSize - 1 ];

		switch( this->parent->CacheMode )
		{
		case Linear:
			this->PageSize = this->parent->PageSize;
			break;
		case Submatrix:
			this->PageSize = this->parent->SubmatrixSize;
			break;
		case Plane:
			this->PageSize = this->parent->DimensionSizes[ this->parent->CacheDim1 ] * 
				this->parent->DimensionSizes[ this->parent->CacheDim2 ];
			if( !this->TempCoord ) this->TempCoord = new int[ this->parent->NumOfDimensions ];
			if( !this->TempPlaneIncrSizes )
			{
				this->TempPlaneIncrSizes = new size_t[ this->parent->NumOfDimensions ];
				for( int i = 0; i < this->parent->NumOfDimensions; i++ )
				{
					if( i == this->parent->CacheDim1 || i == this->parent->CacheDim2 ) this->TempPlaneIncrSizes[ i ] = 0;
					else
					{
						this->TempPlaneIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->parent->NumOfDimensions; j++ )
						{
							if( j == this->parent->CacheDim1 || j == this->parent->CacheDim2 ) continue;
							else this->TempPlaneIncrSizes[ i ] *= this->parent->DimensionSizes[ j ];
						}
					}
				}
			}
			break;
		case Vector:
			this->PageSize = this->parent->DimensionSizes[ this->parent->CacheDim1 ];
		}

		size_t TotalPages = 0;
		switch( this->parent->CacheMode )
		{
		case Linear:
			TotalPages = this->parent->FullSize / this->parent->PageSize + ( this->parent->FullSize % this->parent->PageSize ? 1 : 0 );
			break;
		case Submatrix:
			TotalPages = this->parent->SubmatrixCount;
			break;
		case Plane:
			TotalPages = 1;
			for( int i = 0; i < this->parent->NumOfDimensions; i++ )
			{
				if( i == this->parent->CacheDim1 || i == this->parent->CacheDim2 ) continue;
				TotalPages *= this->parent->DimensionSizes[ i ];
			}
			break;
		case Vector:
			TotalPages = 1;
			for( int i = 0; i < this->parent->NumOfDimensions; i++ )
			{
				if( i == this->parent->CacheDim1 ) continue;
				TotalPages *= this->parent->DimensionSizes[ i ];
			}
		}
		if( this->parent->OpenPages < TotalPages && this->parent->OpenPages < this->parent->MaxPages )
		{
			for( std::map< size_t, DataPage * >::iterator i = this->parent->CacheMap.begin();  
				i != this->end && this->parent->OpenPages < TotalPages && this->parent->OpenPages < this->parent->MaxPages; i++ )
			{
				while( i->second->Index > this->NextIndex )
				{
					this->parent->OpenPages++;
					DataPage *Current = new DataPage;
					Current->Index = NextIndex;
					Current->Dirty = true;
					this->parent->CacheEnd->Next = Current;
					Current->Prev = this->parent->CacheEnd;
					this->parent->CacheEnd = Current;
					Current->Next = 0;
					switch( this->parent->CacheMode )
					{
					case Linear:
						Current->DataPtr = new float[ this->parent->PageSize * this->parent->Components ];
						this->parent->LoadData( NextIndex * this->PageSize, 
							NextIndex * this->parent->PageSize + this->parent->PageSize <= this->parent->FullSize ? this->parent->PageSize : this->parent->FullSize - this->NextIndex * this->parent->PageSize, 
							Current->DataPtr );
						break;
					case Submatrix:
						Current->DataPtr = new float[ this->parent->SubmatrixSize * this->parent->Components ];
						this->parent->LoadSubmatrix( NextIndex, Current->DataPtr );
						break;
					case Plane:
						{
							Current->DataPtr = new float[ this->parent->DimensionSizes[ this->parent->CacheDim1 ] * this->parent->DimensionSizes[ this->parent->CacheDim2 ] * this->parent->Components ];
							size_t Remainder = this->NextIndex;
							for( int i = 0; i < this->parent->NumOfDimensions; i++ )
							{
								size_t x = Remainder;
								Remainder = x % this->TempPlaneIncrSizes[ i ];
								this->TempCoord[ i ] = ( int ) ( x / this->TempPlaneIncrSizes[ i ] );
							}
							this->parent->LoadPlane( this->parent->CacheDim2, this->parent->CacheDim1, this->TempCoord, Current->DataPtr );
						}
						break;
					case Vector:
						Current->DataPtr = new float[ this->parent->DimensionSizes[ this->parent->CacheDim1 ] * this->parent->Components ];
						this->parent->LoadVector( this->parent->CacheDim1, this->NextIndex, Current->DataPtr );
					}
					this->parent->CacheMap.insert( std::pair< const size_t, DataPage * >( NextIndex, Current ) );
					this->NextIndex++;
				}
			}
			this->NextIndex = 0;
		}

		this->currentpage = this->parent->CacheMap.begin();
		if( this->parent->CacheMode == Linear && this->NextIndex * this->PageSize + this->PageSize > this->parent->FullSize )
			this->CurrentPageSize = this->parent->FullSize - this->NextIndex * this->PageSize;
		else this->CurrentPageSize = this->PageSize;
		this->pageoffset = 0;
		this->TempData = new float[ this->PageSize ];
		this->NextIndex++;
	}


	inline NMRData::TraverserCacheOrder::~TraverserCacheOrder( void )
	{
		delete [] this->TempCoord;
		delete [] this->TempData;
		delete [] this->TempPlaneIncrSizes;
	}


	inline const float *NMRData::TraverserCacheOrder::operator*( void ) const
	{
		if( this->usetemp ) return this->TempData + this->pageoffset * this->parent->Components;
		else if( this->done ) return 0;
		else return this->currentpage->second->DataPtr + this->pageoffset * this->parent->Components;
	}


	inline void NMRData::TraverserCacheOrder::operator++( void )
	{
		if( ++( this->pageoffset ) == this->CurrentPageSize )
		{
			if( this->currentpage->second->Index + 1 == this->NextIndex )
				this->currentpage++;

			if( this->currentpage == this->end ) 
			{
				this->done = true;
				return;
			}

			if( this->currentpage->second->Index > this->NextIndex )
			{
				switch( this->parent->CacheMode )
				{
				case Linear:
					this->parent->LoadData( NextIndex * this->parent->PageSize, 
						this->NextIndex * this->parent->PageSize + this->parent->PageSize <= this->parent->FullSize ? this->parent->PageSize : 
						this->parent->FullSize - this->NextIndex * this->parent->PageSize, 
						this->TempData );
					break;
				case Submatrix:
					this->parent->LoadSubmatrix( this->NextIndex, this->TempData );
					break;
				case Plane:
					{
						size_t Remainder = this->NextIndex;
						for( int i = 0; i < this->parent->NumOfDimensions; i++ )
						{
							size_t x = Remainder;
							Remainder = x % this->TempPlaneIncrSizes[ i ];
							this->TempCoord[ i ] = ( int ) ( x / this->TempPlaneIncrSizes[ i ] );
						}
						this->parent->LoadPlane( this->parent->CacheDim2, this->parent->CacheDim1, this->TempCoord, this->TempData );
					}
					break;
				case Vector:
					this->parent->LoadVector( this->parent->CacheDim1, this->NextIndex, this->TempData );
				}
				this->NextIndex++;
				this->usetemp = true;
				this->pageoffset = 0;
			}
			else if( this->currentpage->second->Index == this->NextIndex )
			{
				this->usetemp = false;
				this->pageoffset = 0;

				this->CurrentPageSize = this->PageSize;
				if( this->parent->CacheMode == Linear && this->NextIndex * this->PageSize + this->PageSize > this->parent->FullSize )
					CurrentPageSize = this->parent->FullSize - NextIndex * this->PageSize;

				this->NextIndex++;
			}
		}
	}


	//	+ operators

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::plus< float > > operator+( const Subset &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::plus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathScalar( Operand2 ), std::plus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::plus< float > > operator+( const float Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::plus< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathSubset( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::plus< float > > operator+( const Subset &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::plus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathVal( Operand2 ), std::plus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::plus< float > > operator+( const Value &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::plus< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathSubset( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::plus< float > > operator+( const Subset &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::plus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathRef( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::plus< float > > operator+( const Reference &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::plus< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathSubset( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::plus< float > > operator+( const Subset &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::plus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathSubset( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::plus< float > > operator+( const NMRData &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::plus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathScalar( Operand2 ), std::plus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::plus< float > > operator+( const float Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::plus< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathObj( &Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::plus< float > > operator+( const NMRData &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::plus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathVal( Operand2 ), std::plus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::plus< float > > operator+( const Value &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::plus< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathObj( &Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::plus< float > > operator+( const NMRData &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::plus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathRef( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::plus< float > > operator+( const Reference &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::plus< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathObj( &Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::plus< float > > operator+( const NMRData &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::plus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathSubset( Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::plus< float > > operator+( const Subset &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::plus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathObj( &Operand2 ), std::plus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::plus< float > > operator+( const NMRData &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::plus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathObj( &Operand2 ), std::plus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::plus< float > >
			( Operand1, NMRDataMathScalar( Operand2 ), std::plus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
			( NMRDataMathScalar( Operand1 ), Operand2, std::plus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::plus< float > >
			( Operand1, NMRDataMathVal( Operand2 ), std::plus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
			( NMRDataMathVal( Operand1 ), Operand2, std::plus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::plus< float > >
			( Operand1, NMRDataMathRef( Operand2 ), std::plus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
			( NMRDataMathRef( Operand1 ), Operand2, std::plus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::plus< float > >
			( Operand1, NMRDataMathSubset( Operand2 ), std::plus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
			( NMRDataMathSubset( Operand1 ), Operand2, std::plus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::plus< float > >
			( Operand1, NMRDataMathObj( &Operand2 ), std::plus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
			( NMRDataMathObj( &Operand1 ), Operand2, std::plus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > 
	inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
	operator+( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::plus< float > >
			( Operand1, Operand2, std::plus< float >() );
	}

	//	- operators

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::minus< float > > operator-( const Subset &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::minus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathScalar( Operand2 ), std::minus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::minus< float > > operator-( const float Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::minus< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathSubset( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::minus< float > > operator-( const Subset &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::minus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathVal( Operand2 ), std::minus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::minus< float > > operator-( const Value &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::minus< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathSubset( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::minus< float > > operator-( const Subset &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::minus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathRef( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::minus< float > > operator-( const Reference &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::minus< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathSubset( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::minus< float > > operator-( const Subset &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::minus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathSubset( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::minus< float > > operator-( const NMRData &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::minus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathScalar( Operand2 ), std::minus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::minus< float > > operator-( const float Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::minus< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathObj( &Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::minus< float > > operator-( const NMRData &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::minus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathVal( Operand2 ), std::minus< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::minus< float > > operator-( const Value &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::minus< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathObj( &Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::minus< float > > operator-( const NMRData &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::minus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathRef( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::minus< float > > operator-( const Reference &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::minus< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathObj( &Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::minus< float > > operator-( const NMRData &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::minus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathSubset( Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::minus< float > > operator-( const Subset &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::minus< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathObj( &Operand2 ), std::minus< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::minus< float > > operator-( const NMRData &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::minus< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathObj( &Operand2 ), std::minus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::minus< float > >
			( Operand1, NMRDataMathScalar( Operand2 ), std::minus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
			( NMRDataMathScalar( Operand1 ), Operand2, std::minus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::minus< float > >
			( Operand1, NMRDataMathVal( Operand2 ), std::minus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
			( NMRDataMathVal( Operand1 ), Operand2, std::minus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::minus< float > >
			( Operand1, NMRDataMathRef( Operand2 ), std::minus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
			( NMRDataMathRef( Operand1 ), Operand2, std::minus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::minus< float > >
			( Operand1, NMRDataMathSubset( Operand2 ), std::minus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
			( NMRDataMathSubset( Operand1 ), Operand2, std::minus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::minus< float > >
			( Operand1, NMRDataMathObj( &Operand2 ), std::minus< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
			( NMRDataMathObj( &Operand1 ), Operand2, std::minus< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > 
	inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
	operator-( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::minus< float > >
			( Operand1, Operand2, std::minus< float >() );
	}

	//	* operators

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::multiplies< float > > operator*( const Subset &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::multiplies< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathScalar( Operand2 ), std::multiplies< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::multiplies< float > > operator*( const float Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::multiplies< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathSubset( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::multiplies< float > > operator*( const Subset &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::multiplies< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathVal( Operand2 ), std::multiplies< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::multiplies< float > > operator*( const Value &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::multiplies< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathSubset( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::multiplies< float > > operator*( const Subset &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::multiplies< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathRef( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::multiplies< float > > operator*( const Reference &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::multiplies< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathSubset( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::multiplies< float > > operator*( const Subset &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::multiplies< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathSubset( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::multiplies< float > > operator*( const NMRData &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::multiplies< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathScalar( Operand2 ), std::multiplies< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::multiplies< float > > operator*( const float Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::multiplies< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathObj( &Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::multiplies< float > > operator*( const NMRData &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::multiplies< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathVal( Operand2 ), std::multiplies< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::multiplies< float > > operator*( const Value &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::multiplies< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathObj( &Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::multiplies< float > > operator*( const NMRData &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::multiplies< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathRef( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::multiplies< float > > operator*( const Reference &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::multiplies< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathObj( &Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::multiplies< float > > operator*( const NMRData &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::multiplies< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathSubset( Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::multiplies< float > > operator*( const Subset &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::multiplies< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathObj( &Operand2 ), std::multiplies< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::multiplies< float > > operator*( const NMRData &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::multiplies< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathObj( &Operand2 ), std::multiplies< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::multiplies< float > >
			( Operand1, NMRDataMathScalar( Operand2 ), std::multiplies< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
			( NMRDataMathScalar( Operand1 ), Operand2, std::multiplies< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::multiplies< float > >
			( Operand1, NMRDataMathVal( Operand2 ), std::multiplies< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
			( NMRDataMathVal( Operand1 ), Operand2, std::multiplies< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::multiplies< float > >
			( Operand1, NMRDataMathRef( Operand2 ), std::multiplies< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
			( NMRDataMathRef( Operand1 ), Operand2, std::multiplies< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::multiplies< float > >
			( Operand1, NMRDataMathSubset( Operand2 ), std::multiplies< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
			( NMRDataMathSubset( Operand1 ), Operand2, std::multiplies< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::multiplies< float > >
			( Operand1, NMRDataMathObj( &Operand2 ), std::multiplies< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
			( NMRDataMathObj( &Operand1 ), Operand2, std::multiplies< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > 
	inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
	operator*( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::multiplies< float > >
			( Operand1, Operand2, std::multiplies< float >() );
	}

	//	/ operators

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::divides< float > > operator/( const Subset &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathScalar, std::divides< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathScalar( Operand2 ), std::divides< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::divides< float > > operator/( const float Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathSubset, std::divides< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathSubset( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::divides< float > > operator/( const Subset &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathVal, std::divides< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathVal( Operand2 ), std::divides< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::divides< float > > operator/( const Value &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathSubset, std::divides< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathSubset( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::divides< float > > operator/( const Subset &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathRef, std::divides< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathRef( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::divides< float > > operator/( const Reference &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathSubset, std::divides< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathSubset( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::divides< float > > operator/( const Subset &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathSubset, std::divides< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathSubset( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::divides< float > > operator/( const NMRData &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathScalar, std::divides< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathScalar( Operand2 ), std::divides< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::divides< float > > operator/( const float Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathObj, std::divides< float > >
			( NMRDataMathScalar( Operand1 ), NMRDataMathObj( &Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::divides< float > > operator/( const NMRData &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathVal, std::divides< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathVal( Operand2 ), std::divides< float >() );
	}
		
	inline NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::divides< float > > operator/( const Value &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathObj, std::divides< float > >
			( NMRDataMathVal( Operand1 ), NMRDataMathObj( &Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::divides< float > > operator/( const NMRData &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathRef, std::divides< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathRef( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::divides< float > > operator/( const Reference &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathObj, std::divides< float > >
			( NMRDataMathRef( Operand1 ), NMRDataMathObj( &Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::divides< float > > operator/( const NMRData &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathSubset, std::divides< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathSubset( Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::divides< float > > operator/( const Subset &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathObj, std::divides< float > >
			( NMRDataMathSubset( Operand1 ), NMRDataMathObj( &Operand2 ), std::divides< float >() );
	}

	inline NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::divides< float > > operator/( const NMRData &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathObj, std::divides< float > >
			( NMRDataMathObj( &Operand1 ), NMRDataMathObj( &Operand2 ), std::divides< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, float Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathScalar, std::divides< float > >
			( Operand1, NMRDataMathScalar( Operand2 ), std::divides< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( float Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathScalar, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
			( NMRDataMathScalar( Operand1 ), Operand2, std::divides< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Value &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathVal, std::divides< float > >
			( Operand1, NMRDataMathVal( Operand2 ), std::divides< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const Value &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathVal, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
			( NMRDataMathVal( Operand1 ), Operand2, std::divides< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Reference &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathRef, std::divides< float > >
			( Operand1, NMRDataMathRef( Operand2 ), std::divides< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const Reference &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathRef, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
			( NMRDataMathRef( Operand1 ), Operand2, std::divides< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const Subset &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathSubset, std::divides< float > >
			( Operand1, NMRDataMathSubset( Operand2 ), std::divides< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const Subset &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathSubset, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
			( NMRDataMathSubset( Operand1 ), Operand2, std::divides< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1 > inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRData &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathObj, std::divides< float > >
			( Operand1, NMRDataMathObj( &Operand2 ), std::divides< float >() );
	}

	template< typename T_LHS2, typename T_RHS2, typename T_Op2 > inline NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const NMRData &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathObj, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
			( NMRDataMathObj( &Operand1 ), Operand2, std::divides< float >() );
	}

	template< typename T_LHS1, typename T_RHS1, typename T_Op1, typename T_LHS2, typename T_RHS2, typename T_Op2 > 
	inline NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
	operator/( const NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 > &Operand1, const NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 > &Operand2 )
	{
		return NMRDataMathOp< NMRDataMathOp< T_LHS1, T_RHS1, T_Op1 >, NMRDataMathOp< T_LHS2, T_RHS2, T_Op2 >, std::divides< float > >
			( Operand1, Operand2, std::divides< float >() );
	}

	template< typename T >
	inline void NMRData::ApplyChangeToAll( T &Operation )
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyChangeToAll_ByComponent_Fastest( Operation );		
	}

	template< typename T >
	inline void NMRData::ApplyToAll( T &Operation ) const
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyToAll_ByComponent_Fastest( Operation );		
	}

	template< typename T >
	inline void NMRData::ApplyChangeToComponent( T &Operation, int Component )
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyChangeToAll_Component_Fastest( Operation, Component );		
	}

	template< typename T >
	inline void NMRData::ApplyToComponent( T &Operation, int Component ) const
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyToAll_Component_Fastest( Operation, Component );		
	}

	template< typename T >
	inline void NMRData::ApplyChangeToAllLinear( T &Operation )
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyChangeToAll_ByComponent_OffsetOrder( Operation );		
	}

	template< typename T >
	inline void NMRData::ApplyToAllLinear( T &Operation ) const
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyToAll_ByComponent_OffsetOrder( Operation );		
	}

	template< typename T >
	inline void NMRData::ApplyChangeToComponentLinear( T &Operation, int Component )
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyChangeToAll_Component_OffsetOrder( Operation, Component );		
	}

	template< typename T >
	inline void NMRData::ApplyToComponentLinear( T &Operation, int Component ) const
	{
		if( !this->GotData ) throw Exceptions::DataNotOpen();

		this->ApplyToAll_Component_OffsetOrder( Operation, Component );		
	}


	template< typename T > 
	inline void NMRData::ApplyChangeToAll_Ptr_OffsetOrder( T &Operation )
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
	#endif

		if( this->MemOnlyData )
		{
			if( this->Components == 1 )
				for( size_t i = 0; i < this->FullSize; i++ )
					Operation( this->MemOnlyData + i );
			else
				for( size_t i = 0; i < this->FullSize; i++ )
					Operation( this->MemOnlyData + i * this->Components );
			return;
		}

		for( size_t i = 0; i < this->FullSize; i++ )
			Operation( this->TranslateOffsetToPtr( i, false ) );
	}


	template< typename T >
	inline void NMRData::ApplyChangeToAll_Ptr_Fastest( T &Operation )
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
	#endif

		if( this->MemOnlyData )
		{
			if( this->Components == 1 )
				for( size_t i = 0; i < this->FullSize; i++ )
					Operation( this->MemOnlyData + i );
			else
				for( size_t i = 0; i < this->FullSize; i++ )
					Operation( this->MemOnlyData + i * this->Components );
			return;
		}

		( *this )[ this->FullSize - 1 ];

		std::map< size_t, DataPage * >::iterator end = this->CacheMap.end();
		size_t NextIndex = 0, PageSize = 0;
		BEC_Misc::stack_ptr< float > TempData( 0 );

		switch( this->CacheMode )
		{
		case Linear:
			PageSize = this->PageSize * this->Components;
			break;
		case Submatrix:
			PageSize = this->SubmatrixSize * this->Components;
			break;
		case Plane:
			PageSize = this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * this->Components;
			if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
			if( !TempPlaneIncrSizes )
			{
				TempPlaneIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					if( i == this->CacheDim1 || i == this->CacheDim2 ) TempPlaneIncrSizes[ i ] = 0;
					else
					{
						TempPlaneIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->NumOfDimensions; j++ )
						{
							if( j == CacheDim1 || j == CacheDim2 ) continue;
							else TempPlaneIncrSizes[ i ] *= this->DimensionSizes[ j ];
						}
					}
				}
			}
			break;
		case Vector:
			PageSize = this->DimensionSizes[ this->CacheDim1 ] * this->Components;
		}

		size_t TotalPages = 0;
		switch( this->CacheMode )
		{
		case Linear:
			TotalPages = this->FullSize / this->PageSize + ( this->FullSize % this->PageSize ? 1 : 0 );
			break;
		case Submatrix:
			TotalPages = this->SubmatrixCount;
			break;
		case Plane:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 || i == this->CacheDim2 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
			break;
		case Vector:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
		}
		if( this->OpenPages < TotalPages && this->OpenPages < this->MaxPages )
		{
			for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin();  
				i != end && this->OpenPages < TotalPages && this->OpenPages < this->MaxPages; i++ )
			{
				while( i->second->Index > NextIndex && this->OpenPages < this->MaxPages )
				{
					this->OpenPages++;
					DataPage *Current = new DataPage;
					Current->Index = NextIndex;
					Current->Dirty = true;
					this->CacheEnd->Next = Current;
					this->CacheEnd = Current;
					Current->Next = 0;
					switch( this->CacheMode )
					{
					case Linear:
						Current->DataPtr = new float[ this->PageSize * this->Components ];
						LoadData( NextIndex * this->PageSize, 
							NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
							Current->DataPtr );
						break;
					case Submatrix:
						Current->DataPtr = new float[ this->SubmatrixSize * Components ];
						LoadSubmatrix( NextIndex, Current->DataPtr );
						break;
					case Plane:
						{
							CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
							size_t Remainder = NextIndex;
							for( int i = 0; i < this->NumOfDimensions; i++ )
							{
								size_t x = Remainder;
								Remainder = x % TempPlaneIncrSizes[ i ];
								TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
							}
							LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
						}
						break;
					case Vector:
						this->CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
						LoadVector( CacheDim1, NextIndex, Current->DataPtr );
					}
					this->CacheMap.insert( std::pair< const size_t, DataPage * >( NextIndex, Current ) );
					NextIndex++;
				}
			}
			NextIndex = 0;
		}

		for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin(); i != end; i++ )
		{
			while( i->second->Index > NextIndex )
			{
				switch( this->CacheMode )
				{
				case Linear:
					if( !TempData ) TempData.set( new float[ PageSize ] );
					LoadData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					if( !TempData ) TempData.set( new float[ PageSize ] );
					LoadSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					{
						if( !TempData ) TempData.set( new float[ PageSize ] );
						size_t Remainder = NextIndex;
						for( int i = 0; i < this->NumOfDimensions; i++ )
						{
							size_t x = Remainder;
							Remainder = x % TempPlaneIncrSizes[ i ];
							TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
						}
						LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					}
					break;
				case Vector:
					if( !TempData ) TempData.set( new float[ PageSize ] );
					LoadVector( CacheDim1, NextIndex, TempData );
				}

				if( this->Components == 1 )
					for( size_t i = 0; i < PageSize; i++ )
						Operation( ( float * ) TempData + i );
				else
				{
					for( size_t i = 0; i < PageSize; i++ )
						Operation( ( float * ) TempData + i * this->Components );
				}

				switch( this->CacheMode )
				{
				case Linear:
					WriteData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					WriteSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					WritePlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					break;
				case Vector:
					WriteVector( CacheDim1, NextIndex, TempData );
				}

				NextIndex++;
			}

			size_t CurrentPageSize = this->PageSize;
			if( this->CacheMode == Linear && NextIndex == TotalPages - 1 )
				CurrentPageSize = NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize;

			float *data = i->second->DataPtr;
			if( this->Components == 1 )
				for( size_t i = 0; i < CurrentPageSize; i++ )
					Operation( data + i );
			else
				for( size_t i = 0; i < CurrentPageSize; i++ )
					Operation( data + i * this->Components );
			NextIndex++;
		}
	}


	template< typename T > 
	inline void NMRData::ApplyChangeToAll_ByComponent_OffsetOrder( T &Operation )
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
	#endif

		if( this->MemOnlyData )
		{
			if( this->Components == 1 )
				for( size_t i = 0; i < this->FullSize; i++ )
					this->MemOnlyData[ i ] = Operation( this->MemOnlyData[ i ] );
			else
				for( size_t i = 0; i < this->FullSize; i++ )
					for( int j = 0; j < this->Components; j++ )
						this->MemOnlyData[ i * this->Components + j ] = Operation( this->MemOnlyData[ i * this->Components + j ] );
			return;
		}

		if( this->Components == 1 )
		{
			for( size_t i = 0; i < this->FullSize; i++ )
			{
				float *ptr = this->TranslateOffsetToPtr( i, false );
				*ptr = Operation( *ptr );
			}
		}
		else
		{
			for( size_t i = 0; i < this->FullSize; i++ )
			{
				float *ptr = this->TranslateOffsetToPtr( i, false );
				for( int j = 0; j < this->Components; j++ )
					ptr[ j ] = Operation( ptr[ j ] );
			}
		}
	}


	template< typename T > 
	inline void NMRData::ApplyToAll_ByComponent_OffsetOrder( T &Operation ) const
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
	#endif

		if( this->MemOnlyData )
		{
			if( this->Components == 1 )
				for( size_t i = 0; i < this->FullSize; i++ )
					Operation( this->MemOnlyData[ i ] );
			else
				for( size_t i = 0; i < this->FullSize; i++ )
					for( int j = 0; j < this->Components; j++ )
						Operation( this->MemOnlyData[ i * this->Components + j ] );
			return;
		}

		if( this->Components == 1 )
		{
			for( size_t i = 0; i < this->FullSize; i++ )
				Operation( *( this->TranslateOffsetToPtr( i, true ) ) );
		}
		else
		{
			for( size_t i = 0; i < this->FullSize; i++ )
			{
				float *ptr = this->TranslateOffsetToPtr( i, true );
				for( int j = 0; j < this->Components; j++ )
					Operation( ptr[ j ] );
			}
		}
	}


	template< typename T >
	inline void NMRData::ApplyChangeToAll_ByComponent_Fastest( T &Operation )
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
	#endif

		if( this->MemOnlyData )
		{
			if( this->Components == 1 )
				for( size_t i = 0; i < this->FullSize; i++ )
					this->MemOnlyData[ i ] = Operation( this->MemOnlyData[ i ] );
			else
				for( size_t i = 0; i < this->FullSize; i++ )
					for( int j = 0; j < this->Components; j++ )
						this->MemOnlyData[ i * this->Components + j ] = Operation( this->MemOnlyData[ i * this->Components + j ] );
			return;
		}

		( *this )[ this->FullSize - 1 ];

		std::map< size_t, DataPage * >::iterator end = this->CacheMap.end();
		size_t NextIndex = 0, PageSize = 0;
		BEC_Misc::stack_ptr< float > TempData( 0 );

		switch( this->CacheMode )
		{
		case Linear:
			PageSize = this->PageSize;
			break;
		case Submatrix:
			PageSize = this->SubmatrixSize;
			break;
		case Plane:
			PageSize = this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ];
			if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
			if( !TempPlaneIncrSizes )
			{
				TempPlaneIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					if( i == this->CacheDim1 || i == this->CacheDim2 ) TempPlaneIncrSizes[ i ] = 0;
					else
					{
						TempPlaneIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->NumOfDimensions; j++ )
						{
							if( j == CacheDim1 || j == CacheDim2 ) continue;
							else TempPlaneIncrSizes[ i ] *= this->DimensionSizes[ j ];
						}
					}
				}
			}
			break;
		case Vector:
			PageSize = this->DimensionSizes[ this->CacheDim1 ];
		}

		size_t TotalPages = 0;
		switch( this->CacheMode )
		{
		case Linear:
			TotalPages = this->FullSize / this->PageSize + ( this->FullSize % this->PageSize ? 1 : 0 );
			break;
		case Submatrix:
			TotalPages = this->SubmatrixCount;
			break;
		case Plane:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 || i == this->CacheDim2 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
			break;
		case Vector:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
		}
		if( this->OpenPages < TotalPages && this->OpenPages < this->MaxPages )
		{
			for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin();  
				i != end && this->OpenPages < TotalPages && this->OpenPages < this->MaxPages; i++ )
			{
				while( i->second->Index > NextIndex && this->OpenPages < this->MaxPages )
				{
					this->OpenPages++;
					DataPage *Current = new DataPage;
					Current->Index = NextIndex;
					Current->Dirty = true;
					this->CacheEnd->Next = Current;
					Current->Prev = this->CacheEnd;
					this->CacheEnd = Current;
					Current->Next = 0;
					switch( this->CacheMode )
					{
					case Linear:
						Current->DataPtr = new float[ this->PageSize * this->Components ];
						LoadData( NextIndex * this->PageSize, 
							NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
							Current->DataPtr );
						break;
					case Submatrix:
						Current->DataPtr = new float[ this->SubmatrixSize * Components ];
						LoadSubmatrix( NextIndex, Current->DataPtr );
						break;
					case Plane:
						{
							CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
							size_t Remainder = NextIndex;
							for( int i = 0; i < this->NumOfDimensions; i++ )
							{
								size_t x = Remainder;
								Remainder = x % TempPlaneIncrSizes[ i ];
								TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
							}
							LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
						}
						break;
					case Vector:
						this->CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
						LoadVector( CacheDim1, NextIndex, Current->DataPtr );
					}
					this->CacheMap.insert( std::pair< const size_t, DataPage * >( NextIndex, Current ) );
					NextIndex++;
				}
			}
			NextIndex = 0;
		}

		for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin(); i != end; i++ )
		{
			while( i->second->Index > NextIndex )
			{
				switch( this->CacheMode )
				{
				case Linear:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					{
						if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
						size_t Remainder = NextIndex;
						for( int i = 0; i < this->NumOfDimensions; i++ )
						{
							size_t x = Remainder;
							Remainder = x % TempPlaneIncrSizes[ i ];
							TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
						}
						LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					}
					break;
				case Vector:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadVector( CacheDim1, NextIndex, TempData );
				}

				if( this->Components == 1 )
					for( size_t i = 0; i < PageSize; i++ )
						TempData[ i ] = Operation( TempData[ i ] );
				else
				{
					for( size_t i = 0; i < PageSize; i++ )
						for( int j = 0; j < this->Components; j++ )
							TempData[ i * this->Components + j ] = Operation( TempData[ i * this->Components + j ] );
				}

				switch( this->CacheMode )
				{
				case Linear:
					WriteData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					WriteSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					WritePlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					break;
				case Vector:
					WriteVector( CacheDim1, NextIndex, TempData );
				}

				NextIndex++;
			}

			size_t CurrentPageSize = PageSize;
			if( this->CacheMode == Linear && NextIndex == TotalPages - 1 )
				CurrentPageSize = NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize;

			float *data = i->second->DataPtr;
			if( this->Components == 1 )
				for( size_t i = 0; i < CurrentPageSize; i++ )
					data[ i ] = Operation( data[ i ] );
			else
			{
				for( size_t i = 0; i < CurrentPageSize; i++ )
					for( int j = 0; j < this->Components; j++ )
						data[ i * this->Components + j ] = Operation( data[ i * this->Components + j ] );
			}
			NextIndex++;
		}
	}


	template< typename T >
	inline void NMRData::ApplyToAll_ByComponent_Fastest( T &Operation ) const
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
	#endif

		if( this->MemOnlyData )
		{
			if( this->Components == 1 )
				for( size_t i = 0; i < this->FullSize; i++ )
					Operation( this->MemOnlyData[ i ] );
			else
				for( size_t i = 0; i < this->FullSize; i++ )
					for( int j = 0; j < this->Components; j++ )
						Operation( this->MemOnlyData[ i * this->Components + j ] );
			return;
		}

		( *this )[ this->FullSize - 1 ];

		std::map< size_t, DataPage * >::iterator end = this->CacheMap.end();
		size_t NextIndex = 0, PageSize = 0;
		BEC_Misc::stack_ptr< float > TempData( 0 );

		switch( this->CacheMode )
		{
		case Linear:
			PageSize = this->PageSize;
			break;
		case Submatrix:
			PageSize = this->SubmatrixSize;
			break;
		case Plane:
			PageSize = this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ];
			if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
			if( !TempPlaneIncrSizes )
			{
				TempPlaneIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					if( i == this->CacheDim1 || i == this->CacheDim2 ) TempPlaneIncrSizes[ i ] = 0;
					else
					{
						TempPlaneIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->NumOfDimensions; j++ )
						{
							if( j == CacheDim1 || j == CacheDim2 ) continue;
							else TempPlaneIncrSizes[ i ] *= this->DimensionSizes[ j ];
						}
					}
				}
			}
			break;
		case Vector:
			PageSize = this->DimensionSizes[ this->CacheDim1 ];
		}

		size_t TotalPages = 0;
		switch( this->CacheMode )
		{
		case Linear:
			TotalPages = this->FullSize / this->PageSize + ( this->FullSize % this->PageSize ? 1 : 0 );
			break;
		case Submatrix:
			TotalPages = this->SubmatrixCount;
			break;
		case Plane:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 || i == this->CacheDim2 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
			break;
		case Vector:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
		}
		if( this->OpenPages < TotalPages && this->OpenPages < this->MaxPages )
		{
			for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin();  
				i != end && this->OpenPages < TotalPages && this->OpenPages < this->MaxPages; i++ )
			{
				while( i->second->Index > NextIndex && this->OpenPages < this->MaxPages )
				{
					this->OpenPages++;
					DataPage *Current = new DataPage;
					Current->Index = NextIndex;
					Current->Dirty = false;
					this->CacheEnd->Next = Current;
					Current->Prev = this->CacheEnd;
					this->CacheEnd = Current;
					Current->Next = 0;
					switch( this->CacheMode )
					{
					case Linear:
						Current->DataPtr = new float[ this->PageSize * this->Components ];
						LoadData( NextIndex * this->PageSize, 
							NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
							Current->DataPtr );
						break;
					case Submatrix:
						Current->DataPtr = new float[ this->SubmatrixSize * Components ];
						LoadSubmatrix( NextIndex, Current->DataPtr );
						break;
					case Plane:
						{
							CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
							size_t Remainder = NextIndex;
							for( int i = 0; i < this->NumOfDimensions; i++ )
							{
								size_t x = Remainder;
								Remainder = x % TempPlaneIncrSizes[ i ];
								TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
							}
							LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
						}
						break;
					case Vector:
						this->CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
						LoadVector( CacheDim1, NextIndex, Current->DataPtr );
					}
					this->CacheMap.insert( std::pair< const size_t, DataPage * >( NextIndex, Current ) );
					NextIndex++;
				}
			}
			NextIndex = 0;
		}

		for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin(); i != end; i++ )
		{
			while( i->second->Index > NextIndex )
			{
				switch( this->CacheMode )
				{
				case Linear:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					{
						if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
						size_t Remainder = NextIndex;
						for( int i = 0; i < this->NumOfDimensions; i++ )
						{
							size_t x = Remainder;
							Remainder = x % TempPlaneIncrSizes[ i ];
							TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
						}
						LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					}
					break;
				case Vector:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadVector( CacheDim1, NextIndex, TempData );
				}

				if( this->Components == 1 )
					for( size_t i = 0; i < PageSize; i++ )
						Operation( TempData[ i ] );
				else
				{
					for( size_t i = 0; i < PageSize; i++ )
						for( int j = 0; j < this->Components; j++ )
							Operation( TempData[ i * this->Components + j ] );
				}

				NextIndex++;
			}

			size_t CurrentPageSize = PageSize;
			if( this->CacheMode == Linear && NextIndex == TotalPages - 1 )
				CurrentPageSize = NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize;

			float *data = i->second->DataPtr;
			if( this->Components == 1 )
				for( size_t i = 0; i < CurrentPageSize; i++ )
					Operation( data[ i ] );
			else
			{
				for( size_t i = 0; i < CurrentPageSize; i++ )
					for( int j = 0; j < this->Components; j++ )
						Operation( data[ i * this->Components + j ] );
			}
			NextIndex++;
		}
	}


	template< typename T > 
	inline void NMRData::ApplyChangeToAll_Component_OffsetOrder( T &Operation, int Component )
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
		if( Component < 0 || Component > this->Components ) throw Exceptions::BadComponentIndex();
	#endif

		if( this->MemOnlyData )
		{
			for( size_t i = 0; i < this->FullSize; i++ )
				this->MemOnlyData[ i * this->Components + Component ] = Operation( this->MemOnlyData[ i * this->Components + Component ] );
			return;
		}

		for( size_t i = 0; i < this->FullSize; i++ )
		{
			float *ptr = this->TranslateOffsetToPtr( i, false );
			*ptr = Operation( *ptr + Component );
		}
	}


	template< typename T > 
	inline void NMRData::ApplyToAll_Component_OffsetOrder( T &Operation, int Component ) const
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
		if( Component < 0 || Component > this->Components ) throw Exceptions::BadComponentIndex();
	#endif

		if( this->MemOnlyData )
		{
			for( size_t i = 0; i < this->FullSize; i++ )
				Operation( this->MemOnlyData[ i * this->Components + Component ] );
			return;
		}

		for( size_t i = 0; i < this->FullSize; i++ )
			Operation( *( this->TranslateOffsetToPtr( i, true ) + Component ) );
	}


	template< typename T >
	inline void NMRData::ApplyChangeToAll_Component_Fastest( T &Operation, int Component )
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
		if( Component < 0 || Component > this->Components ) throw Exceptions::BadComponentIndex();
	#endif

		if( this->MemOnlyData )
		{
			for( size_t i = 0; i < this->FullSize; i++ )
				this->MemOnlyData[ i * this->Components + Component ] = Operation( this->MemOnlyData[ i * this->Components + Component ] );
			return;
		}

		( *this )[ this->FullSize - 1 ];

		std::map< size_t, DataPage * >::iterator end = this->CacheMap.end();
		size_t NextIndex = 0, PageSize = 0;
		BEC_Misc::stack_ptr< float > TempData( 0 );

		switch( this->CacheMode )
		{
		case Linear:
			PageSize = this->PageSize;
			break;
		case Submatrix:
			PageSize = this->SubmatrixSize;
			break;
		case Plane:
			PageSize = this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ];
			if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
			if( !TempPlaneIncrSizes )
			{
				TempPlaneIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					if( i == this->CacheDim1 || i == this->CacheDim2 ) TempPlaneIncrSizes[ i ] = 0;
					else
					{
						TempPlaneIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->NumOfDimensions; j++ )
						{
							if( j == CacheDim1 || j == CacheDim2 ) continue;
							else TempPlaneIncrSizes[ i ] *= this->DimensionSizes[ j ];
						}
					}
				}
			}
			break;
		case Vector:
			PageSize = this->DimensionSizes[ this->CacheDim1 ];
		}

		size_t TotalPages = 0;
		switch( this->CacheMode )
		{
		case Linear:
			TotalPages = this->FullSize / this->PageSize + ( this->FullSize % this->PageSize ? 1 : 0 );
			break;
		case Submatrix:
			TotalPages = this->SubmatrixCount;
			break;
		case Plane:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 || i == this->CacheDim2 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
			break;
		case Vector:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
		}
		if( this->OpenPages < TotalPages && this->OpenPages < this->MaxPages )
		{
			for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin();  
				i != end && this->OpenPages < TotalPages && this->OpenPages < this->MaxPages; i++ )
			{
				while( i->second->Index > NextIndex && this->OpenPages < this->MaxPages )
				{
					this->OpenPages++;
					DataPage *Current = new DataPage;
					Current->Index = NextIndex;
					Current->Dirty = true;
					this->CacheEnd->Next = Current;
					Current->Prev = this->CacheEnd;
					this->CacheEnd = Current;
					Current->Next = 0;
					switch( this->CacheMode )
					{
					case Linear:
						Current->DataPtr = new float[ this->PageSize * this->Components ];
						LoadData( NextIndex * this->PageSize, 
							NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
							Current->DataPtr );
						break;
					case Submatrix:
						Current->DataPtr = new float[ this->SubmatrixSize * Components ];
						LoadSubmatrix( NextIndex, Current->DataPtr );
						break;
					case Plane:
						{
							CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
							size_t Remainder = NextIndex;
							for( int i = 0; i < this->NumOfDimensions; i++ )
							{
								size_t x = Remainder;
								Remainder = x % TempPlaneIncrSizes[ i ];
								TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
							}
							LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
						}
						break;
					case Vector:
						this->CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
						LoadVector( CacheDim1, NextIndex, Current->DataPtr );
					}
					this->CacheMap.insert( std::pair< const size_t, DataPage * >( NextIndex, Current ) );
					NextIndex++;
				}
			}
			NextIndex = 0;
		}

		for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin(); i != end; i++ )
		{
			while( i->second->Index > NextIndex )
			{
				switch( this->CacheMode )
				{
				case Linear:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					{
						if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
						size_t Remainder = NextIndex;
						for( int i = 0; i < this->NumOfDimensions; i++ )
						{
							size_t x = Remainder;
							Remainder = x % TempPlaneIncrSizes[ i ];
							TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
						}
						LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					}
					break;
				case Vector:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadVector( CacheDim1, NextIndex, TempData );
				}

				for( size_t i = 0; i < PageSize; i++ )
					TempData[ i * this->Components + Component ] = Operation( TempData[ i * this->Components + Component ] );

				switch( this->CacheMode )
				{
				case Linear:
					WriteData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					WriteSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					WritePlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					break;
				case Vector:
					WriteVector( CacheDim1, NextIndex, TempData );
				}

				NextIndex++;
			}

			size_t CurrentPageSize = PageSize;
			if( this->CacheMode == Linear && NextIndex == TotalPages - 1 )
				CurrentPageSize = NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize;

			float *data = i->second->DataPtr;
			for( size_t i = 0; i < CurrentPageSize; i++ )
				data[ i * this->Components + Component ] = Operation( data[ i * this->Components + Component ] );
			NextIndex++;
		}
	}

		
	template< typename T >
	inline void NMRData::ApplyToAll_Component_Fastest( T &Operation, int Component ) const
	{
	#ifdef BEC_NMRDATA_DEBUG
		if( !this->GotData ) throw Exceptions::DataNotOpen();
		if( Component < 0 || Component > this->Components ) throw Exceptions::BadComponentIndex();
	#endif

		if( this->MemOnlyData )
		{
			for( size_t i = 0; i < this->FullSize; i++ )
				Operation( this->MemOnlyData[ i * this->Components + Component ] );
			return;
		}

		( *this )[ this->FullSize - 1 ];

		std::map< size_t, DataPage * >::iterator end = this->CacheMap.end();
		size_t NextIndex = 0, PageSize = 0;
		BEC_Misc::stack_ptr< float > TempData( 0 );

		switch( this->CacheMode )
		{
		case Linear:
			PageSize = this->PageSize;
			break;
		case Submatrix:
			PageSize = this->SubmatrixSize;
			break;
		case Plane:
			PageSize = this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ];
			if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
			if( !TempPlaneIncrSizes )
			{
				TempPlaneIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					if( i == this->CacheDim1 || i == this->CacheDim2 ) TempPlaneIncrSizes[ i ] = 0;
					else
					{
						TempPlaneIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->NumOfDimensions; j++ )
						{
							if( j == CacheDim1 || j == CacheDim2 ) continue;
							else TempPlaneIncrSizes[ i ] *= this->DimensionSizes[ j ];
						}
					}
				}
			}
			break;
		case Vector:
			PageSize = this->DimensionSizes[ this->CacheDim1 ];
		}

		size_t TotalPages = 0;
		switch( this->CacheMode )
		{
		case Linear:
			TotalPages = this->FullSize / this->PageSize + ( this->FullSize % this->PageSize ? 1 : 0 );
			break;
		case Submatrix:
			TotalPages = this->SubmatrixCount;
			break;
		case Plane:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 || i == this->CacheDim2 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
			break;
		case Vector:
			TotalPages = 1;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 ) continue;
				TotalPages *= this->DimensionSizes[ i ];
			}
		}
		if( this->OpenPages < TotalPages && this->OpenPages < this->MaxPages )
		{
			for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin();  
				i != end && this->OpenPages < TotalPages && this->OpenPages < this->MaxPages; i++ )
			{
				while( i->second->Index > NextIndex && this->OpenPages < this->MaxPages )
				{
					this->OpenPages++;
					DataPage *Current = new DataPage;
					Current->Index = NextIndex;
					Current->Dirty = false;
					this->CacheEnd->Next = Current;
					Current->Prev = this->CacheEnd;
					this->CacheEnd = Current;
					Current->Next = 0;
					switch( this->CacheMode )
					{
					case Linear:
						Current->DataPtr = new float[ this->PageSize * this->Components ];
						LoadData( NextIndex * this->PageSize, 
							NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
							Current->DataPtr );
						break;
					case Submatrix:
						Current->DataPtr = new float[ this->SubmatrixSize * Components ];
						LoadSubmatrix( NextIndex, Current->DataPtr );
						break;
					case Plane:
						{
							CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
							size_t Remainder = NextIndex;
							for( int i = 0; i < this->NumOfDimensions; i++ )
							{
								size_t x = Remainder;
								Remainder = x % TempPlaneIncrSizes[ i ];
								TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
							}
							LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
						}
						break;
					case Vector:
						this->CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
						LoadVector( CacheDim1, NextIndex, Current->DataPtr );
					}
					this->CacheMap.insert( std::pair< const size_t, DataPage * >( NextIndex, Current ) );
					NextIndex++;
				}
			}
			NextIndex = 0;
		}

		for( std::map< size_t, DataPage * >::iterator i = this->CacheMap.begin(); i != end; i++ )
		{
			while( i->second->Index > NextIndex )
			{
				switch( this->CacheMode )
				{
				case Linear:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadData( NextIndex * this->PageSize, 
						NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize, 
						TempData );
					break;
				case Submatrix:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadSubmatrix( NextIndex, TempData );
					break;
				case Plane:
					{
						if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
						size_t Remainder = NextIndex;
						for( int i = 0; i < this->NumOfDimensions; i++ )
						{
							size_t x = Remainder;
							Remainder = x % TempPlaneIncrSizes[ i ];
							TempCoord[ i ] = ( int ) ( x / TempPlaneIncrSizes[ i ] );
						}
						LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, TempData );
					}
					break;
				case Vector:
					if( !TempData ) TempData.set( new float[ PageSize * this->Components ] );
					LoadVector( CacheDim1, NextIndex, TempData );
				}

				for( size_t i = 0; i < PageSize; i++ )
					Operation( TempData[ i * this->Components + Component ] );

				NextIndex++;
			}

			size_t CurrentPageSize = PageSize;
			if( this->CacheMode == Linear && NextIndex == TotalPages - 1 )
				CurrentPageSize = NextIndex * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - NextIndex * this->PageSize;

			float *data = i->second->DataPtr;
			for( size_t i = 0; i < CurrentPageSize; i++ )
				Operation( data[ i * this->Components + Component ] );
			NextIndex++;
		}
	}
}


#endif	//	BEC_NMRDATA_INCLUDED

/*	End nmrdata.h	*/
