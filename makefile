#Makefile for the nmrdata library

CXX 						= g++
LD							= g++
CXXFLAGS 					= $(include_path) -O3

boost_include_path 			= libs/boost
becmisc_include_path		= libs
include_path 				= -I$(boost_include_path) -I$(becmisc_include_path) -Iinclude/nmrdata

main_src_path				= src
targets						= nmrdata.o

main_build_path				= build
build_dirs					= $(main_build_path)
target_list					= $(addprefix $(main_build_path)/, $(targets) )

all : checkdirs libnmrdata.a

libnmrdata.a:  $(target_list)
	ar r $@ $(target_list); ranlib $@

$(main_build_path)/%.o : $(main_src_path)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

checkdirs : $(build_dirs)

$(build_dirs) :
	mkdir -p $@

clean :
	rm -rf $(build_dirs)
	rm -f libnmrdata.a

.PHONY: checkdirs clean all


