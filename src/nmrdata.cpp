/*	nmrdata.cpp
	Brian E. Coggins, 24 Jun 2004, rev. 21 Jul 2010, v.1.1.2
	Copyright (c) 2004-2010, Brian E. Coggins.  All rights reserved.

	NMRData Library:  A library for access to and manipulation of NMR data.

	Please see the library documentation for details on the use of this
	library.

	This library may only be used or distributed under the terms of a
	license received from the author.  Please consult the accompanying
	license document to determine which rights and permissions have
	been granted to you.  If you did not receive a license document
	together with this file, you MUST contact the author to obtain a
	license before using or redistributing this file.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <new>
#include <exception>
#include <limits>
#include <algorithm>
#include <iterator>
#include <vector>
#include <map>
#include <valarray>
#include <bec_misc/becmisc.h>
#include "nmrdata.h"

using namespace BEC_NMRData;
using namespace Exceptions;
using namespace BEC_Misc;

//	Large file support
#ifdef BEC_NMRDATA_LARGE_FILES
#define fseek fseeko
#else
#define off_t long
#endif


/*****************************************************************************/
/*                                                                           */
/*	Excerpted from FDATAP.H by Frank Delaglio with permission                */
/*                                                                           */
/*****************************************************************************/

/***/
/* Some useful constant definitions:
/***/

#define FDATASIZE          512   /* Length of header in 4-byte float values. */

#define FDIEEECONS   0xeeeeeeee  /* Indicates IEEE floating point format.    */
#define FDVAXCONS    0x11111111  /* Indicates DEC VAX floating point format. */
#define FDORDERCONS       2.345F /* Constant used to determine byte-order.   */ 
#define ZERO_EQUIV       -666.0  /* Might be used as equivalent for zero.    */

/***/
/* Floating point format on this computer.
/***/

#define FDFMTCONS FDIEEECONS

/***/
/* General Parameter locations:
/***/

#define FDMAGIC        0 /* Should be zero in valid NMRPipe data.            */
#define FDFLTFORMAT    1 /* Constant defining floating point format.         */
#define FDFLTORDER     2 /* Constant defining byte order.                    */

#define FDSIZE        99 /* Number of points in current dim R|I.             */
#define FDREALSIZE    97 /* Number of valid time-domain pts (obsolete).      */
#define FDSPECNUM    219 /* Number of complex 1D slices in file.             */
#define FDQUADFLAG   106 /* See Data Type codes below.                       */
#define FD2DPHASE    256 /* See 2D Plane Type codes below.                   */


/***/
/* Parameters defining number of dimensions and their order in the data;
/* a newly-converted FID has dimension order (2 1 3 4). These dimension
/* codes are a hold-over from the oldest 2D NMR definitions, where the
/* directly-acquired dimension was always t2, and the indirect dimension
/* was t1.
/***/

#define FDTRANSPOSED 221 /* 1=Transposed, 0=Not Transposed.                  */
#define FDDIMCOUNT     9 /* Number of dimensions in complete data.           */
#define FDDIMORDER    24 /* Array describing dimension order.                */

#define FDDIMORDER1   24 /* Dimension stored in X-Axis.                      */
#define FDDIMORDER2   25 /* Dimension stored in Y-Axis.                      */
#define FDDIMORDER3   26 /* Dimension stored in Z-Axis.                      */
#define FDDIMORDER4   27 /* Dimension stored in A-Axis.                      */

/***/
/* The following parameters describe the data when it is
/* in a multidimensional data stream format (FDPIPEFLAG != 0):
/***/

#define FDPIPEFLAG    57 /* Dimension code of data stream.    Non-standard.  */
#define FDPIPECOUNT   75 /* Number of functions in pipeline.  Non-standard.  */
#define FDSLICECOUNT 443 /* Number of 1D slices in stream.    Non-standard.  */
#define FDFILECOUNT  442 /* Number of files in complete data.                */

/***/
/* The following definitions are used for data streams which are
/* subsets of the complete data, as for parallel processing:
/***/

#define FDFIRSTPLANE  77 /* First Z-Plane in subset.            Non-standard. */
#define FDLASTPLANE   78 /* Last Z-Plane in subset.             Non-standard. */
#define FDPARTITION   65 /* Slice count for client-server mode. Non-standard. */

#define FDPLANELOC    14 /* Location of this plane; currently unused.         */

/***/
/* The following define max and min data values, previously used
/* for contour level setting:
/***/

#define FDMAX        247 /* Max value in real part of data.                  */
#define FDMIN        248 /* Min value in real part of data.                  */
#define FDSCALEFLAG  250 /* 1 if FDMAX and FDMIN are valid.                  */
#define FDDISPMAX    251 /* Max value, used for display generation.          */
#define FDDISPMIN    252 /* Min value, used for display generation.          */

/***/
/* Locations reserved for User customization:
/***/

#define FDUSER1       70
#define FDUSER2       71
#define FDUSER3       72
#define FDUSER4       73
#define FDUSER5       74

/***/
/* Defines location of "footer" information appended to spectral
/* data; currently unused for NMRPipe format:
/***/

#define FDLASTBLOCK  359
#define FDCONTBLOCK  360
#define FDBASEBLOCK  361
#define FDPEAKBLOCK  362
#define FDBMAPBLOCK  363
#define FDHISTBLOCK  364
#define FD1DBLOCK    365

/***/
/* Defines data and time data was converted:
/***/

#define FDMONTH      294
#define FDDAY        295
#define FDYEAR       296
#define FDHOURS      283
#define FDMINS       284
#define FDSECS       285

/***/
/* Miscellaneous Parameters:
/***/

#define FDMCFLAG      135 /* Magnitude Calculation performed.               */
#define FDNOISE       153 /* Used to contain an RMS noise estimate.         */
#define FDRANK        180 /* Estimate of matrix rank; Non-standard.         */
#define FDTEMPERATURE 157 /* Temperature, degrees C.                        */
#define FD2DVIRGIN    399 /* 0=Data never accessed, header never adjusted.  */
#define FDTAU         199 /* A Tau value (for spectral series).             */

#define FDSRCNAME    286  /* char srcFile[16]  286-289 */
#define FDUSERNAME   290  /* char uName[16]    290-293 */
#define FDOPERNAME   464  /* char oName[32]    464-471 */
#define FDTITLE      297  /* char title[60]    297-311 */
#define FDCOMMENT    312  /* char comment[160] 312-351 */

/***/
/* For meanings of these dimension-specific parameters,
/* see the corresponding ND parameters below.
/***/

#define FDF2LABEL     16
#define FDF2APOD      95
#define FDF2SW       100
#define FDF2OBS      119
#define FDF2ORIG     101
#define FDF2UNITS    152
#define FDF2QUADFLAG  56 /* Non-standard. */
#define FDF2FTFLAG   220
#define FDF2AQSIGN    64 /* Non-standard. */
#define FDF2LB       111
#define FDF2CAR       66 /* Non-standard. */
#define FDF2CENTER    79 /* Non-standard. */
#define FDF2OFFPPM   480 /* Non-standard. */
#define FDF2P0       109
#define FDF2P1       110
#define FDF2APODCODE 413
#define FDF2APODQ1   415
#define FDF2APODQ2   416
#define FDF2APODQ3   417
#define FDF2C1       418
#define FDF2ZF       108
#define FDF2X1       257 /* Non-standard. */
#define FDF2XN       258 /* Non-standard. */
#define FDF2FTSIZE    96 /* Non-standard. */
#define FDF2TDSIZE   386 /* Non-standard. */

#define FDDMXVAL      40 /* Non-standard. */
#define FDDMXFLAG     41 /* Non-standard. */

#define FDF1LABEL     18
#define FDF1APOD     428
#define FDF1SW       229 
#define FDF1OBS      218 
#define FDF1ORIG     249 
#define FDF1UNITS    234 
#define FDF1FTFLAG   222 
#define FDF1AQSIGN   475 /* Non-standard. */
#define FDF1LB       243
#define FDF1QUADFLAG  55 /* Non-standard. */
#define FDF1CAR       67 /* Non-standard. */
#define FDF1CENTER    80 /* Non-standard. */
#define FDF1OFFPPM   481 /* Non-standard. */
#define FDF1P0       245
#define FDF1P1       246
#define FDF1APODCODE 414
#define FDF1APODQ1   420
#define FDF1APODQ2   421 
#define FDF1APODQ3   422
#define FDF1C1       423
#define FDF1ZF       437
#define FDF1X1       259 /* Non-standard. */
#define FDF1XN       260 /* Non-standard. */
#define FDF1FTSIZE    98 /* Non-standard. */
#define FDF1TDSIZE   387 /* Non-standard. */

#define FDF3LABEL     20
#define FDF3APOD      50 /* Non-standard. */
#define FDF3OBS       10
#define FDF3SW        11
#define FDF3ORIG      12
#define FDF3FTFLAG    13
#define FDF3AQSIGN   476 /* Non-standard. */
#define FDF3SIZE      15
#define FDF3QUADFLAG  51 /* Non-standard. */
#define FDF3UNITS     58 /* Non-standard. */
#define FDF3P0        60 /* Non-standard. */
#define FDF3P1        61 /* Non-standard. */
#define FDF3CAR       68 /* Non-standard. */
#define FDF3CENTER    81 /* Non-standard. */
#define FDF3OFFPPM   482 /* Non-standard. */
#define FDF3APODCODE 400 /* Non-standard. */
#define FDF3APODQ1   401 /* Non-standard. */
#define FDF3APODQ2   402 /* Non-standard. */
#define FDF3APODQ3   403 /* Non-standard. */
#define FDF3C1       404 /* Non-standard. */
#define FDF3ZF       438 /* Non-standard. */
#define FDF3X1       261 /* Non-standard. */
#define FDF3XN       262 /* Non-standard. */
#define FDF3FTSIZE   200 /* Non-standard. */
#define FDF3TDSIZE   388 /* Non-standard. */

#define FDF4LABEL     22
#define FDF4APOD      53 /* Non-standard. */
#define FDF4OBS       28
#define FDF4SW        29
#define FDF4ORIG      30
#define FDF4FTFLAG    31
#define FDF4AQSIGN   477 /* Non-standard. */
#define FDF4SIZE      32
#define FDF4QUADFLAG  54 /* Non-standard. */
#define FDF4UNITS     59 /* Non-standard. */
#define FDF4P0        62 /* Non-standard. */
#define FDF4P1        63 /* Non-standard. */
#define FDF4CAR       69 /* Non-standard. */
#define FDF4CENTER    82 /* Non-standard. */
#define FDF4OFFPPM   483 /* Non-standard. */
#define FDF4APODCODE 405 /* Non-standard. */
#define FDF4APODQ1   406 /* Non-standard. */
#define FDF4APODQ2   407 /* Non-standard. */
#define FDF4APODQ3   408 /* Non-standard. */
#define FDF4C1       409 /* Non-standard. */
#define FDF4ZF       439 /* Non-standard. */
#define FDF4X1       263 /* Non-standard. */
#define FDF4XN       264 /* Non-standard. */
#define FDF4FTSIZE   201 /* Non-standard. */
#define FDF4TDSIZE   389 /* Non-standard. */

/***/
/* Header locations in use for packed text; adjust function
/* isHdrStr() and isHdrStr0 if new text locations are added:
/***/

/* 286 287 288 289                                                     */
/* 290 291 292 293                                                     */
/* 464 465 466 467  468 469 470 471                                    */
/* 297 298 299 300  301 302 303 304  305 306 307 308  309 310 311      */
/* 312 313 314 315  316 317 318 319  320 321 322 323  324 325 326 327  */
/* 328 329 330 331  332 333 334 335  336 337 338 339  340 341 342 343  */
/* 344 345 346 347  348 349 350 351                                    */

#define SIZE_NDLABEL    8
#define SIZE_F2LABEL    8
#define SIZE_F1LABEL    8
#define SIZE_F3LABEL    8
#define SIZE_F4LABEL    8

#define SIZE_SRCNAME   16
#define SIZE_USERNAME  16
#define SIZE_OPERNAME  32
#define SIZE_COMMENT  160
#define SIZE_TITLE     60

/***/
/* The following are definitions for generalized ND parameters:
/***/

#define NDPARM        1000

#define NDSIZE        (1+NDPARM)  /* Number of points in dimension.          */
#define NDAPOD        (2+NDPARM)  /* Current valid time-domain size.         */
#define NDSW          (3+NDPARM)  /* Sweep Width Hz.                         */
#define NDORIG        (4+NDPARM)  /* Axis Origin (Last Point), Hz.           */
#define NDOBS         (5+NDPARM)  /* Obs Freq MHz.                           */
#define NDFTFLAG      (6+NDPARM)  /* 1=Freq Domain 0=Time Domain.            */
#define NDQUADFLAG    (7+NDPARM)  /* Data Type Code (See Below).             */
#define NDUNITS       (8+NDPARM)  /* Axis Units Code (See Below).            */
#define NDLABEL       (9+NDPARM)  /* 8-char Axis Label.                      */
#define NDLABEL1      (9+NDPARM)  /* Subset of 8-char Axis Label.            */
#define NDLABEL2     (10+NDPARM)  /* Subset of 8-char Axis Label.            */
#define NDP0         (11+NDPARM)  /* Zero Order Phase, Degrees.              */
#define NDP1         (12+NDPARM)  /* First Order Phase, Degrees.             */
#define NDCAR        (13+NDPARM)  /* Carrier Position, PPM.                  */
#define NDCENTER     (14+NDPARM)  /* Point Location of Zero Freq.            */
#define NDAQSIGN     (15+NDPARM)  /* Sign adjustment needed for FT.          */
#define NDAPODCODE   (16+NDPARM)  /* Window function used.                   */
#define NDAPODQ1     (17+NDPARM)  /* Window parameter 1.                     */
#define NDAPODQ2     (18+NDPARM)  /* Window parameter 2.                     */
#define NDAPODQ3     (19+NDPARM)  /* Window parameter 3.                     */
#define NDC1         (20+NDPARM)  /* Add 1.0 to get First Point Scale.       */
#define NDZF         (21+NDPARM)  /* Negative of Zero Fill Size.             */
#define NDX1         (22+NDPARM)  /* Extract region origin, if any, pts.     */
#define NDXN         (23+NDPARM)  /* Extract region endpoint, if any, pts.   */
#define NDOFFPPM     (24+NDPARM)  /* Additional PPM offset (for alignment).  */
#define NDFTSIZE     (25+NDPARM)  /* Size of data when FT performed.         */
#define NDTDSIZE     (26+NDPARM)  /* Original valid time-domain size.        */
#define MAX_NDPARM   (27)

/***/
/* Axis Units, for NDUNITS:
/***/

#define FD_SEC       1
#define FD_HZ        2
#define FD_PPM       3
#define FD_PTS       4

/***/
/* 2D Plane Type, for FD2DPHASE:
/***/

#define FD_MAGNITUDE 0
#define FD_TPPI      1
#define FD_STATES    2
#define FD_IMAGE     3
#define FD_ARRAY     4

/***/
/* Data Type (FDQUADFLAG and NDQUADFLAG)
/***/

#define FD_QUAD       0
#define FD_COMPLEX    0
#define FD_SINGLATURE 1
#define FD_REAL       1
#define FD_PSEUDOQUAD 2
#define FD_SE         3
#define FD_GRAD       4

/***/
/* Sign adjustment, etc, needed for FT (NDAQSIGN):
/***/

#define ALT_NONE            0 /* No sign alternation required.                */
#define ALT_SEQUENTIAL      1 /* Sequential data needing sign alternation.    */
#define ALT_STATES          2 /* Complex data needing sign alternation.       */
#define ALT_NONE_NEG       16 /* As above, with negation of imaginaries.      */
#define ALT_SEQUENTIAL_NEG 17 /* As above, with negation of imaginaries.      */
#define ALT_STATES_NEG     18 /* As above, with negation of imaginaries.      */

#define FOLD_INVERT        -1 /* Folding requires sign inversion.             */
#define FOLD_BAD            0 /* Folding can't be performed (extracted data). */
#define FOLD_ORDINARY       1 /* Ordinary folding, no sign inversion.         */

#define DMX_ON              1 /* Use DMX adjustment.                          */
#define DMX_OFF            -1 /* Don't use DMX adjustment.                    */
#define DMX_AUTO            0 /* Use DMX adjustment if needed.                */


/*****************************************************************************/
/*                                                                           */
/*	end of excerpt from FDATAP.H                                             */
/*                                                                           */
/*****************************************************************************/


/*****************************************************************************/
/*                                                                           */
/*	Excerpted from APOD.H by Frank Delaglio with permission                  */
/*                                                                           */
/*****************************************************************************/

#define APOD_NULL  0
#define APOD_SP    1
#define APOD_EM    2
#define APOD_GM    3
#define APOD_TM    4
#define APOD_ZE    5
#define APOD_TRI   6
#define APOD_GMB   7
#define APOD_JMOD  8
#define APOD_FILE  101
#define APOD_COUNT 10 

/*****************************************************************************/
/*                                                                           */
/*	end of excerpt from APOD.H                                               */
/*                                                                           */
/*****************************************************************************/



NMRData::NMRData()
{
	this->GotData = false;
	this->DataPtrMemLimit = this->DefaultDataPtrMemLimit;
	this->NumOfDimensions = 0;
	this->ClearParameters();
}


NMRData::NMRData( int NumOfDims )
{
	this->GotData = false;
	this->DataPtrMemLimit = this->DefaultDataPtrMemLimit;
	this->NumOfDimensions = 0;
	this->ClearParameters();
	this->SetNumOfDims( NumOfDims );
}


NMRData::NMRData( const char *Filename, bool ReadOnly )
{
	this->GotData = false;
	this->DataPtrMemLimit = this->DefaultDataPtrMemLimit;
	this->InternalCacheSize = this->DefaultInternalCacheSize;
	this->NumOfDimensions = 0;
	this->ClearParameters();
	this->OpenFile( Filename, ReadOnly );
}


NMRData::NMRData( std::string Filename, bool ReadOnly )
{
	this->GotData = false;
	this->DataPtrMemLimit = this->DefaultDataPtrMemLimit;
	this->InternalCacheSize = this->DefaultInternalCacheSize;
	this->NumOfDimensions = 0;
	this->ClearParameters();
	this->OpenFile( Filename, ReadOnly );
}


NMRData::~NMRData( void )
{
	try
	{
		if( this->GotData ) this->Close();
		this->ClearParameters();
	}
	catch(...)
	{
		if( !std::uncaught_exception() ) throw;
	}
}


int NMRData::GetNumOfDims( void ) const
{
	return this->NumOfDimensions;
}


void NMRData::SetNumOfDims( int NumOfDims )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( NumOfDims < 0 ) throw BadArgument();
#endif

	if( this->NumOfDimensions )
	{
		if( this->DimensionCentersFreq ) delete [] this->DimensionCentersFreq;
		if( this->DimensionCentersPPM ) delete [] this->DimensionCentersPPM;
		if( this->DimensionOriginsPPM ) delete [] this->DimensionOriginsPPM;
		if( this->DimensionScalesFreq ) delete [] this->DimensionScalesFreq;
		if( this->DimensionScalesPPM ) delete [] this->DimensionScalesPPM;
		if( this->DimensionSizes ) delete [] this->DimensionSizes;
		if( this->DimensionAxisLabels )
		{
			for( int x = 0; x < this->NumOfDimensions; x++ )
			{
				delete [] this->DimensionAxisLabels[ x ];
			}
			delete [] this->DimensionAxisLabels;
		}
		this->IsTimeDomain = false;
		if( this->IsTimeDomainByDim ) delete [] this->IsTimeDomainByDim;
		this->IsComplex = false;
		if( this->IsComplexByDim ) delete [] this->IsComplexByDim;
		this->Components = 1;
	}
	this->NumOfDimensions = NumOfDims;
	if( NumOfDims )
	{
		this->DimensionAxisLabels = new char *[ NumOfDims ];
		for( int x = 0; x < NumOfDims; x++ )
			this->DimensionAxisLabels[ x ] = 0;
		this->DimensionCentersFreq = new float[ NumOfDims ];
		this->DimensionCentersPPM = new float[ NumOfDims ];
		this->DimensionOriginsPPM = new float[ NumOfDims ];
		this->DimensionScalesFreq = new float[ NumOfDims ];
		this->DimensionScalesPPM = new float[ NumOfDims ];
		this->DimensionSizes = new int [ NumOfDims ];
		for( int i = 0; i < NumOfDims; i++ )
		{
			this->DimensionCentersFreq[ i ] = 0.0F;
			this->DimensionCentersPPM[ i ] = 0.0F;
			this->DimensionOriginsPPM[ i ] = 0.0F;
			this->DimensionScalesFreq[ i ] = 0.0F;
			this->DimensionScalesPPM[ i ] = 0.0F;
			this->DimensionSizes[ i ] = 0;
		}
		memset( this->DimensionAxisLabels, 0, sizeof( char * ) * NumOfDims );
		this->IsComplex = false;
		this->Components = 1;
		this->IsComplexByDim = new bool[ NumOfDims ];
		memset( this->IsComplexByDim, 0, sizeof( bool ) * NumOfDims );
		this->IsTimeDomain = false;
		this->IsTimeDomainByDim = new bool[ NumOfDims ];
		memset( this->IsTimeDomainByDim, 0, sizeof( bool ) * NumOfDims );
	}
	else
	{
		this->DimensionAxisLabels = 0;
		this->DimensionCentersFreq = 0;
		this->DimensionCentersPPM = 0;
		this->DimensionOriginsPPM = 0;
		this->DimensionScalesFreq = 0;
		this->DimensionScalesPPM = 0;
		this->DimensionSizes = 0;
		this->IsComplex = false;
		this->IsComplexByDim = 0;
		this->IsTimeDomain = false;
		this->IsTimeDomainByDim = 0;
	}
}


int NMRData::GetDimensionSize( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( DimNum < 0 || DimNum > this->NumOfDimensions ) throw BadDimNumber();
#endif
	return this->DimensionSizes[ DimNum ];
}


void NMRData::SetDimensionSize( int DimNum, int Size ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	else if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->DimensionSizes[ DimNum ] = Size;
}


int *NMRData::GetDimensionSizes( void ) const
{
	if( !this->NumOfDimensions ) return 0;

	int *Receiver = new int[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
		Receiver[ x ] = this->DimensionSizes[ x ];
	return Receiver;
}


std::vector< int > NMRData::GetDimensionSizeVector( void ) const
{
	std::vector< int > v( this->NumOfDimensions );
	std::copy( this->DimensionSizes, this->DimensionSizes + this->NumOfDimensions, v.begin() );
	return v;
}


void NMRData::SetDimensionSizes( int *NewSizes )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( !NewSizes ) throw NullPointerArg();
#endif

	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->DimensionSizes[ x ] = NewSizes[ x ];
}


void NMRData::SetDimensionSizes( std::vector< int > NewSizes )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( NewSizes.size() != this->NumOfDimensions ) throw BadArgument();
#endif

	std::copy( NewSizes.begin(), NewSizes.end(), this->DimensionSizes );
}


size_t NMRData::GetFullSize( void ) const
{
	if( this->GotData ) return this->FullSize;
	if( !this->NumOfDimensions ) return 0;

	size_t Size = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		Size *= this->DimensionSizes[ x ];
	return Size;
}


bool NMRData::IsComplexAvailable( void ) const
{
	if( this->IsComplex ) return true;
	else return false;
}


int NMRData::GetNumOfComponents( void ) const
{
	return this->Components;
}


bool NMRData::IsTimeDomainAvailable( void ) const
{
	if( this->IsTimeDomain ) return true;
	else return false;
}


bool *NMRData::IsComplexAvailableByDim( void ) const
{
	if( !this->NumOfDimensions ) return 0;

	bool *Receiver = new bool[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
		Receiver[ x ] = this->IsComplexByDim[ x ];
	return Receiver;
}


std::vector< bool > NMRData::IsComplexAvailableByDimVector( void ) const
{
	std::vector< bool > v( this->NumOfDimensions );
	std::copy( this->IsComplexByDim, this->IsComplexByDim + this->NumOfDimensions, v.begin() );
	return v;
}


bool *NMRData::IsTimeDomainAvailableByDim( void ) const
{
	if( !this->NumOfDimensions ) return 0;

	bool *Receiver = new bool[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
		Receiver[ x ] = this->IsTimeDomainByDim[ x ];
	return Receiver;
}


std::vector< bool > NMRData::IsTDAvailableByDimVector( void ) const
{
	std::vector< bool > v( this->NumOfDimensions );
	std::copy( this->IsTimeDomainByDim, this->IsTimeDomainByDim + this->NumOfDimensions, v.begin() );
	return v;
}


void NMRData::SetComplexByDim( bool *ComplexByDim )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( !ComplexByDim ) throw NullPointerArg();
#endif

	this->IsComplex = false;
	this->Components = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		this->IsComplexByDim[ x ] = ComplexByDim[ x ];
		if( ComplexByDim[ x ] )
		{
			this->IsComplex = true;
			this->Components *= 2;
		}
	}
}


void NMRData::SetComplexByDim( std::vector< bool > ComplexByDim )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( ComplexByDim.size() != this->NumOfDimensions ) throw BadArgument();
#endif

	this->IsComplex = false;
	this->Components = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		this->IsComplexByDim[ x ] = ComplexByDim[ x ];
		if( ComplexByDim[ x ] )
		{
			this->IsComplex = true;
			this->Components *= 2;
		}
	}
}


void NMRData::SetTDByDim( bool *TDByDim )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( !TDByDim ) throw NullPointerArg();
#endif

	this->IsTimeDomain = false;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		this->IsTimeDomainByDim[ x ] = TDByDim[ x ];
		if( TDByDim[ x ] ) this->IsTimeDomain = true;
	}
}


void NMRData::SetTDByDim( std::vector< bool > TDByDim )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( TDByDim.size() != this->NumOfDimensions ) throw BadArgument();
#endif

	this->IsTimeDomain = false;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		this->IsTimeDomainByDim[ x ] = TDByDim[ x ];
		if( TDByDim[ x ] ) this->IsTimeDomain = true;
	}
}


char *NMRData::GetDimensionLabel( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif
	if( !this->DimensionAxisLabels || !this->DimensionAxisLabels[ DimNum ] ) return 0;

	char *Receiver = new char[ strlen( this->DimensionAxisLabels[ DimNum ] ) + 1 ];
	strcpy( Receiver, this->DimensionAxisLabels[ DimNum ] );
	return Receiver;
}


std::string NMRData::GetDimensionLabelStr( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif
	if( !this->DimensionAxisLabels || !this->DimensionAxisLabels[ DimNum ] ) return std::string( "" );

	return std::string( this->DimensionAxisLabels[ DimNum ] );
}


void NMRData::SetDimensionLabel( int DimNum, const char *Label )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( !Label ) throw NullPointerArg();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	if( this->DimensionAxisLabels[ DimNum ] ) delete [] this->DimensionAxisLabels[ DimNum ];

	this->DimensionAxisLabels[ DimNum ] = new char[ strlen( Label ) + 1 ];
	strcpy( this->DimensionAxisLabels[ DimNum ], Label );
}


void NMRData::SetDimensionLabel( int DimNum, std::string Label )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	if( this->DimensionAxisLabels[ DimNum ] ) delete [] this->DimensionAxisLabels[ DimNum ];

	this->DimensionAxisLabels[ DimNum ] = new char[ Label.length() + 1 ];
	Label.copy( this->DimensionAxisLabels[ DimNum ], Label.length() );
	this->DimensionAxisLabels[ DimNum ][ Label.length() ] = 0;
}


void NMRData::GetDimensionCalibration( int DimNum, float &SW, float &SF, float &CenterPPM ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	SW = this->DimensionScalesFreq[ DimNum ];
	SF = this->DimensionCentersFreq[ DimNum ];
	CenterPPM = this->DimensionCentersPPM[ DimNum ];
}


void NMRData::GetFullDimCalibration( int DimNum, float &SW, float &SF, float &CenterPPM, float &OriginPPM, float &WidthPPM ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	SW = this->DimensionScalesFreq[ DimNum ];
	SF = this->DimensionCentersFreq[ DimNum ];
	CenterPPM = this->DimensionCentersPPM[ DimNum ];
	OriginPPM = this->DimensionOriginsPPM[ DimNum ];
	WidthPPM = this->DimensionScalesPPM[ DimNum ];
}


float NMRData::GetDimensionSW( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	return this->DimensionScalesFreq[ DimNum ];
}


float NMRData::GetDimensionSF( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	return this->DimensionCentersFreq[ DimNum ];
}


float NMRData::GetDimensionCenterPPM( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	return this->DimensionCentersPPM[ DimNum ];
}


bool NMRData::GetDimensionComplexState( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	return this->IsComplexByDim[ DimNum ];
}


float NMRData::GetDimensionStartTime( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( DimNum < 0 || DimNum > this->NumOfDimensions ) throw Exceptions::BadDimNumber();
	if( !this->IsTimeDomainByDim[ DimNum ] ) throw Exceptions::DimNotTD();
#endif

	return 0.0F;
}


float NMRData::GetDimensionEndTime( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( DimNum < 0 || DimNum > this->NumOfDimensions ) throw Exceptions::BadDimNumber();
	if( !this->IsTimeDomainByDim[ DimNum ] ) throw Exceptions::DimNotTD();
#endif

	return ( float ) this->DimensionSizes[ DimNum ] / this->DimensionScalesFreq[ DimNum ];
}


bool NMRData::GetDimensionTD( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	return this->IsTimeDomainByDim[ DimNum ];
}


void NMRData::SetDimensionCalibration( int DimNum, float SW, float SF, float CenterPPM )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->DimensionScalesFreq[ DimNum ] = SW;
	this->DimensionCentersFreq[ DimNum ] = SF;
	this->DimensionCentersPPM[ DimNum ] = CenterPPM;
	this->DimensionScalesPPM[ DimNum ] = SW / SF;		//	= ( SW Hz )( 1 / SF MHz )( 1 MHz / 1e6 Hz )( 1e6 ppm / 1 Hz )
	this->DimensionOriginsPPM[ DimNum ] = CenterPPM + this->DimensionScalesPPM[ DimNum ] / 2.0F;
}


void NMRData::SetDimensionSW( int DimNum, float SW )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->DimensionScalesFreq[ DimNum ] = SW;
	this->DimensionScalesPPM[ DimNum ] = SW / this->DimensionCentersFreq[ DimNum ];
	this->DimensionOriginsPPM[ DimNum ] = this->DimensionCentersPPM[ DimNum ] + this->DimensionScalesPPM[ DimNum ] / 2.0F;
}


void NMRData::SetDimensionSF( int DimNum, float SF )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->DimensionCentersFreq[ DimNum ] = SF;
	this->DimensionScalesPPM[ DimNum ] = this->DimensionScalesFreq[ DimNum ] / SF;
	this->DimensionOriginsPPM[ DimNum ] = this->DimensionCentersPPM[ DimNum ] + this->DimensionScalesPPM[ DimNum ] / 2.0F;
}


void NMRData::SetDimensionCenterPPM( int DimNum, float CenterPPM )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->DimensionCentersPPM[ DimNum ] = CenterPPM;
	this->DimensionOriginsPPM[ DimNum ] = CenterPPM + this->DimensionScalesPPM[ DimNum ] / 2.0F;
}


void NMRData::SetDimensionComplexState( int DimNum, bool Complex )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->IsComplexByDim[ DimNum ] = Complex;

	this->IsComplex = false;
	this->Components = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( this->IsComplexByDim[ x ] )
		{
			this->IsComplex = true;
			this->Components *= 2;
		};
}


void NMRData::SetDimensionTD( int DimNum, bool TD )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	this->IsTimeDomainByDim[ DimNum ] = TD;

	this->IsTimeDomain = false;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( this->IsTimeDomainByDim[ x ] ) this->IsTimeDomain = true;
}


DimPosition< NMRData > NMRData::GetDimPosObj( int DimNum ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
#endif

	return DimPosition< NMRData >( this, DimNum, 0, 0.0F, pts );
}


//DimPosition< const NMRData > NMRData::GetDimPosObj( int DimNum ) const
//{
//#ifdef BEC_NMRDATA_DEBUG
//	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
//	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
//#endif
//
//	return DimPosition< const NMRData >( this, DimNum, 0, 0.0F, pts );
//}
//
//
//DimPosition< NMRData > NMRData::GetDimPosObj( int DimNum )
//{
//#ifdef BEC_NMRDATA_DEBUG
//	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
//	if( DimNum < 0 || DimNum >= this->NumOfDimensions ) throw BadDimNumber();
//#endif
//
//	return DimPosition< NMRData >( this, DimNum, 0, 0.0F, pts );
//}


std::vector< DimPosition< NMRData > > NMRData::GetDimPosSet( void ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
#endif

	std::vector< DimPosition< NMRData > > out;
	for( int i = 0; i < this->NumOfDimensions; i++ )
		out.push_back( DimPosition< NMRData >( this, i, 0, 0.0F, pts ) );
	return out;
}


//std::vector< DimPosition< const NMRData > > NMRData::GetDimPosSet( void ) const
//{
//#ifdef BEC_NMRDATA_DEBUG
//	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
//#endif
//
//	std::vector< DimPosition< NMRData > > out;
//	for( int i = 0; i < this->NumOfDimensions; i++ )
//		out.push_back( DimPosition< const NMRData >( this, i, 0, 0.0F, pts ) );
//	return out;
//}
//
//
//std::vector< DimPosition< NMRData > > NMRData::GetDimPosSet( void )
//{
//#ifdef BEC_NMRDATA_DEBUG
//	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
//#endif
//
//	std::vector< DimPosition< NMRData > > out;
//	for( int i = 0; i < this->NumOfDimensions; i++ )
//		out.push_back( DimPosition< NMRData >( this, i, 0, 0.0F, pts ) );
//	return out;
//}


Value NMRData::GetBlankValue( void ) const
{
	if( this->Components == 1 )
		return Value( 0.0F );
	else
	{
		stack_array_ptr< float > zeroval( new float[ this->Components ] );
		for( int i = 0; i < this->Components; i++ )
			zeroval[ i ] = 0.0F;
		return Value( this->IsComplexAvailableByDimVector(), zeroval );
	}
}


char *NMRData::GetFilename( void ) const
{
	if( !this->Filename ) return 0;

	char *Receiver = new char[ strlen( this->Filename ) + 1 ];
	strcpy( Receiver, this->Filename );
	return Receiver;
}


std::string NMRData::GetFilenameStr( void ) const
{
	if( !this->Filename ) return std::string( "" );

	return std::string( this->Filename );
}


char *NMRData::GetDatasetName( void ) const
{
	if( !this->Name ) return 0;

	char *Receiver = new char[ strlen( this->Name ) + 1 ];
	strcpy( Receiver, this->Name );
	return Receiver;
}


std::string NMRData::GetDatasetNameStr( void ) const
{
	if( !this->Name ) return std::string( "" );

	return std::string( this->Name );
}


void NMRData::ClearParameters( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif

	this->SetNumOfDims( 0 );
	this->ReadOnly = false;
	this->FullSize = 0;
	this->Components = 1;
	this->GotStats = 0;
	this->Mean = 0.0F;
	this->StdDev = 0.0F;
	this->NoiseEstimate = 0.0F;
	this->Min = 0.0F;
	this->Max = 0.0F;
	this->OpenDataPtrCount = 0;
	this->OpenDataPtrs = 0;
	this->Filename = 0;
	this->Name = 0;

	this->ClearCacheParams();
	this->ClearPrivateParams();
}


void NMRData::CopyParameters( const NMRData *Source )
{
	this->CopyParameters( Source, this );
}


void NMRData::CopyDataset( const NMRData *Source )
{
	this->CopyDataset( Source, this );
}


void NMRData::OpenFile( const char *Filename, bool ReadOnly )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
	if( ReadOnly ) this->ReadOnly = true;
#endif

	if( !( this->File = fopen( Filename, "rb" ) ) ) throw CantOpenFile( Filename );
	if( !ReadOnly )
	{
		fclose( this->File );
		if( !( this->File = fopen( Filename, "r+b" ) ) ) throw CantOpenFile( Filename );
	}

	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( this->DimensionSizes[ x ] || this->DimensionSizes[ x ] < 0 ) throw BadDimSize();
#endif

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= ( size_t ) this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
}


void NMRData::OpenFile( std::string Filename, bool ReadOnly )
{
	this->OpenFile( Filename.c_str(), ReadOnly );
}


void NMRData::CreateFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
#endif

	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
#endif
	if( !( this->File = fopen( Filename, "w+b" ) ) ) throw CantOpenFile( Filename );
#ifdef BEC_NMRDATA_DEBUG
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();
#endif

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	float outval = 0.0F;
	for( size_t x = 0; x < this->FullSize * this->Components; x++ )
		if( fwrite( &outval, sizeof( float ), 1, this->File ) != 1 ) throw CantWriteFile();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
}


void NMRData::CreateFile( std::string Filename )
{
	this->CreateFile( Filename.c_str() );
}


void NMRData::BuildInMem( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( this->GetFileTypeCode() != this->FileType_bin ) throw MemOnlyRequiresBaseClass();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();
#endif

	this->Filename = 0;
	this->File = 0;
	this->Name = 0;
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];

	this->GotData = true;

	if( this->DataPtrMemLimit && this->FullSize * this->Components * sizeof( float ) > this->DataPtrMemLimit )
	{
		if( !( this->MemTempFile = tmpfile() ) ) throw CantGetTmpFile();

		float outval = 0.0F;
		for( size_t x = 0; x < this->FullSize * this->Components; x++ )
			if( fwrite( &outval, sizeof( float ), 1, this->MemTempFile ) != 1 ) throw CantWriteFile();

		this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
	}
	else
	{
		try
		{
			this->MemOnlyData = new float[ this->FullSize * this->Components ];
			memset( this->MemOnlyData, 0, sizeof( float ) * this->FullSize * this->Components );
			this->CacheMode = MemOnly;
		}
		catch( std::bad_alloc )
		{
			if( !( this->MemTempFile = tmpfile() ) ) throw CantGetTmpFile();

			float outval = 0.0F;
			for( size_t x = 0; x < this->FullSize * this->Components; x++ )
				if( fwrite( &outval, sizeof( float ), 1, this->MemTempFile ) != 1 ) throw CantWriteFile();

			this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
		}
	}
}


void NMRData::LoadFileHeader( void )
{
	return;
}


void NMRData::WriteFileHeader( void )
{
	return;
}


void NMRData::WriteFileHeader( void ) const
{
	return;
}


void NMRData::InitCache( size_t CacheSize, size_t PageSize, CacheModes Mode, int Dim1, int Dim2 ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( CacheSize < 1 ) throw BadCacheSize();
#endif

	if( this->CacheStart ) this->CloseCache();

	this->CacheMode = Mode;
	if( CacheMode == MemOnly ) return;

	if( Mode == Linear )
	{
		this->InternalCacheSize = CacheSize;
		this->PageSize = PageSize;
		this->MaxPages = ( int ) ( CacheSize / PageSize );
	}
	else if( Mode == Plane )
	{
		this->MaxPages = ( int ) CacheSize / ( this->DimensionSizes[ Dim1 ] * this->DimensionSizes[ Dim2 ] );
		if( Dim1 < 0 || Dim1 >= this->NumOfDimensions || Dim2 < 0 || Dim2 >= this->NumOfDimensions )
			throw BadDimNumber();
		this->CacheDim1 = Dim1;
		this->CacheDim2 = Dim2;
	}
	else if( Mode == Submatrix )
	{
		if( this->CacheDefinesSubmatrices() )
			this->SetSubmatrixSizes();
		if( this->SubmatrixSize ) this->MaxPages = ( int ) CacheSize / SubmatrixSize;
		else this->MaxPages = ( int ) CacheSize / 4096;
	}
	else if( Mode == Vector )
	{
		this->CacheDim1 = Dim1;
		if( Dim1 < 0 || Dim1 >= this->NumOfDimensions )
			throw BadDimNumber();
		this->MaxPages = ( int ) CacheSize / this->DimensionSizes[ Dim1 ];
	}

	if( this->MaxPages < 1 ) this->MaxPages = 1;
}


float *NMRData::GetDataPtr( int *OriginCoordinates, int *Sizes )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( !OriginCoordinates ) throw NullPointerArg();
	if( !Sizes ) throw NullPointerArg();
#endif

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( OriginCoordinates[ x ] < 0 || OriginCoordinates[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestCoord();
		if( Sizes[ x ] < 0 ) throw BadDataPtrRequestSize();
		if( OriginCoordinates[ x ] + Sizes[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestSize();
	}

	this->Flush();

	size_t RequestedSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( Sizes[ x ] ) RequestedSize *= Sizes[ x ];
	}
	if( this->DataPtrMemLimit && RequestedSize * this->Components * sizeof( float ) > this->DataPtrMemLimit ) throw DataPtrRequestTooLarge();

	stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ ++this->OpenDataPtrCount ] );
	for( int x = 0; x < this->OpenDataPtrCount - 1; x++ )
		memcpy( temp + x, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
	delete [] this->OpenDataPtrs;
	this->OpenDataPtrs = temp.release();

	OpenDataPtr *newptr = this->OpenDataPtrs + this->OpenDataPtrCount - 1;
	newptr->Writeable = true;
	newptr->OriginCoordinates = new int[ this->NumOfDimensions ];
	newptr->Contiguous = false;
	newptr->StartOffset = 0;
	newptr->Size = RequestedSize;
	newptr->Sizes = new int[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		newptr->OriginCoordinates[ x ] = OriginCoordinates[ x ];
		newptr->Sizes[ x ] = Sizes [ x ];
	}

	newptr->DataPtr = new float[ RequestedSize * this->Components ];

	stack_array_ptr< int > SubmatrixPos( new int[ this->NumOfDimensions ] );
	memset( SubmatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
	stack_array_ptr< int > MatrixPos( new int[ this->NumOfDimensions ] );
	stack_array_ptr< size_t > DimIncrSize( new size_t[ this->NumOfDimensions ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		DimIncrSize[ x ] = 1;
		for( int y = x + 1; y < this->NumOfDimensions; y++ )
			DimIncrSize[ x ] *= this->DimensionSizes[ y ];
	}

	for( size_t x = 0; x < RequestedSize; x++ )
	{
		memset( MatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
		for( int y = 0; y < this->NumOfDimensions; y++ )
			MatrixPos[ y ] = OriginCoordinates[ y ] + SubmatrixPos[ y ];

		size_t offset = 0;
		for( int y = 0; y < this->NumOfDimensions; y++ )
			offset += MatrixPos[ y ] * DimIncrSize[ y ];

		Value val = ( *this )[ offset ];
		for( int y = 0; y < this->Components; y++ )
			newptr->DataPtr[ x * this->Components + y ] = val[ y ];

		for( int y = this->NumOfDimensions - 1; y >= 0; y-- )
		{
			if( ++( SubmatrixPos[ y ] ) >= ( int ) Sizes[ y ] ) SubmatrixPos[ y ] = 0;
			else break;
		}
	}

	return newptr->DataPtr;
}


float *NMRData::GetDataPtr( std::vector< int > OriginCoordinates, std::vector< int > Sizes )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( OriginCoordinates.size() != this->NumOfDimensions ) throw BadArgument();
	if( Sizes.size() != this->NumOfDimensions ) throw BadArgument();
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( OriginCoordinates[ x ] < 0 || OriginCoordinates[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestCoord();
		if( Sizes[ x ] < 0 ) throw BadDataPtrRequestSize();
		if( OriginCoordinates[ x ] + Sizes[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestSize();
	}
#endif

	this->Flush();

	size_t RequestedSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( Sizes[ x ] ) RequestedSize *= Sizes[ x ];
	}
	if( this->DataPtrMemLimit && RequestedSize * this->Components * sizeof( float ) > this->DataPtrMemLimit ) throw DataPtrRequestTooLarge();

	stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ ++this->OpenDataPtrCount ] );
	for( int x = 0; x < this->OpenDataPtrCount - 1; x++ )
		memcpy( temp + x, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
	delete [] this->OpenDataPtrs;
	this->OpenDataPtrs = temp.release();

	OpenDataPtr *newptr = this->OpenDataPtrs + this->OpenDataPtrCount - 1;
	newptr->Writeable = true;
	newptr->OriginCoordinates = new int[ this->NumOfDimensions ];
	newptr->Contiguous = false;
	newptr->StartOffset = 0;
	newptr->Size = RequestedSize;
	newptr->Sizes = new int[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		newptr->OriginCoordinates[ x ] = OriginCoordinates[ x ];
		newptr->Sizes[ x ] = Sizes [ x ];
	}

	newptr->DataPtr = new float[ RequestedSize * this->Components ];

	stack_array_ptr< int > SubmatrixPos( new int[ this->NumOfDimensions ] );
	memset( SubmatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
	stack_array_ptr< int > MatrixPos( new int[ this->NumOfDimensions ] );
	stack_array_ptr< size_t > DimIncrSize( new size_t[ this->NumOfDimensions ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		DimIncrSize[ x ] = 1;
		for( int y = x + 1; y < this->NumOfDimensions; y++ )
			DimIncrSize[ x ] *= this->DimensionSizes[ y ];
	}

	for( size_t x = 0; x < RequestedSize; x++ )
	{
		memset( MatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
		for( int y = 0; y < this->NumOfDimensions; y++ )
			MatrixPos[ y ] = OriginCoordinates[ y ] + SubmatrixPos[ y ];

		size_t offset = 0;
		for( int y = 0; y < this->NumOfDimensions; y++ )
			offset += MatrixPos[ y ] * DimIncrSize[ y ];

		Value val = ( *this )[ offset ];
		for( int y = 0; y < this->Components; y++ )
			newptr->DataPtr[ x * this->Components + y ] = val[ y ];

		for( int y = this->NumOfDimensions - 1; y >= 0; y-- )
		{
			if( ++( SubmatrixPos[ y ] ) >= ( int ) Sizes[ y ] ) SubmatrixPos[ y ] = 0;
			else break;
		}
	}

	return newptr->DataPtr;
}


float *NMRData::GetDataPtr( size_t StartOffset, size_t Size )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( StartOffset < 0 || StartOffset >= this->FullSize ) throw BadDataPtrRequestOffset();
	if( StartOffset + Size > this->FullSize ) throw BadDataPtrRequestSize();
#endif

	if( this->DataPtrMemLimit && Size / sizeof( float ) > this->DataPtrMemLimit ) throw DataPtrRequestTooLarge();

	this->Flush();

	stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ ++this->OpenDataPtrCount ] );
	for( int x = 0; x < this->OpenDataPtrCount - 1; x++ )
		memcpy( temp + x, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
	delete [] this->OpenDataPtrs;
	this->OpenDataPtrs = temp.release();

	OpenDataPtr *newptr = this->OpenDataPtrs + this->OpenDataPtrCount - 1;
	newptr->Writeable = true;
	newptr->OriginCoordinates = 0;
	newptr->Contiguous = true;
	newptr->StartOffset = StartOffset;
	newptr->Sizes = 0;
	newptr->Size = Size;

	if( this->MemOnlyData )
		newptr->DataPtr = this->MemOnlyData + StartOffset * this->Components;
	else
	{
		newptr->DataPtr = new float[ Size * this->Components ];
		this->LoadData( StartOffset, Size, newptr->DataPtr );
	}

	return newptr->DataPtr;
}


const float *NMRData::GetReadonlyDataPtr( int *OriginCoordinates, int *Sizes ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( !OriginCoordinates ) throw NullPointerArg();
	if( !Sizes ) throw NullPointerArg();

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( OriginCoordinates[ x ] < 0 || OriginCoordinates[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestCoord();
		if( Sizes[ x ] < 0 ) throw BadDataPtrRequestSize();
		if( OriginCoordinates[ x ] + Sizes[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestSize();
	}
#endif

	size_t RequestedSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( Sizes[ x ] ) RequestedSize *= Sizes[ x ];
	}
	if( this->DataPtrMemLimit && RequestedSize * this->Components * sizeof( float ) > this->DataPtrMemLimit ) throw DataPtrRequestTooLarge();

	stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ ++this->OpenDataPtrCount ] );
	for( int x = 0; x < this->OpenDataPtrCount - 1; x++ )
		memcpy( temp + x, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
	delete [] this->OpenDataPtrs;
	this->OpenDataPtrs = temp.release();

	OpenDataPtr *newptr = this->OpenDataPtrs + this->OpenDataPtrCount - 1;
	newptr->Writeable = false;
	newptr->OriginCoordinates = new int[ this->NumOfDimensions ];
	newptr->Contiguous = false;
	newptr->StartOffset = 0;
	newptr->Size = RequestedSize;
	newptr->Sizes = new int[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		newptr->OriginCoordinates[ x ] = OriginCoordinates[ x ];
		newptr->Sizes[ x ] = Sizes [ x ];
	}

	newptr->DataPtr = new float[ RequestedSize * this->Components ];

	stack_array_ptr< int > SubmatrixPos( new int[ this->NumOfDimensions ] );
	memset( SubmatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
	stack_array_ptr< int > MatrixPos( new int[ this->NumOfDimensions ] );
	stack_array_ptr< size_t > DimIncrSize( new size_t[ this->NumOfDimensions ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		DimIncrSize[ x ] = 1;
		for( int y = x + 1; y < this->NumOfDimensions; y++ )
			DimIncrSize[ x ] *= this->DimensionSizes[ y ];
	}

	for( size_t x = 0; x < RequestedSize; x++ )
	{
		memset( MatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
		for( int y = 0; y < this->NumOfDimensions; y++ )
			MatrixPos[ y ] = OriginCoordinates[ y ] + SubmatrixPos[ y ];

		size_t offset = 0;
		for( int y = 0; y < this->NumOfDimensions; y++ )
			offset += MatrixPos[ y ] * DimIncrSize[ y ];

		Value val = ( *this )[ offset ];
		for( int y = 0; y < this->Components; y++ )
			newptr->DataPtr[ x * this->Components + y ] = val[ y ];

		for( int y = this->NumOfDimensions - 1; y >= 0; y-- )
		{
			if( ++( SubmatrixPos[ y ] ) >= ( int ) Sizes[ y ] ) SubmatrixPos[ y ] = 0;
			else break;
		}
	}

	return newptr->DataPtr;
}


const float *NMRData::GetReadonlyDataPtr( std::vector< int > OriginCoordinates, std::vector< int > Sizes ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( OriginCoordinates.size() != this->NumOfDimensions ) throw BadArgument();
	if( Sizes.size() != this->NumOfDimensions ) throw BadArgument();
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( OriginCoordinates[ x ] < 0 || OriginCoordinates[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestCoord();
		if( Sizes[ x ] < 0 ) throw BadDataPtrRequestSize();
		if( OriginCoordinates[ x ] + Sizes[ x ] >= this->DimensionSizes[ x ] ) throw BadDataPtrRequestSize();
	}
#endif

	size_t RequestedSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( Sizes[ x ] ) RequestedSize *= Sizes[ x ];
	}
	if( this->DataPtrMemLimit && RequestedSize * this->Components * sizeof( float ) > this->DataPtrMemLimit ) throw DataPtrRequestTooLarge();

	stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ ++this->OpenDataPtrCount ] );
	for( int x = 0; x < this->OpenDataPtrCount - 1; x++ )
		memcpy( temp + x, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
	delete [] this->OpenDataPtrs;
	this->OpenDataPtrs = temp.release();

	OpenDataPtr *newptr = this->OpenDataPtrs + this->OpenDataPtrCount - 1;
	newptr->Writeable = false;
	newptr->OriginCoordinates = new int[ this->NumOfDimensions ];
	newptr->Contiguous = false;
	newptr->StartOffset = 0;
	newptr->Size = RequestedSize;
	newptr->Sizes = new int[ this->NumOfDimensions ];
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		newptr->OriginCoordinates[ x ] = OriginCoordinates[ x ];
		newptr->Sizes[ x ] = Sizes [ x ];
	}

	newptr->DataPtr = new float[ RequestedSize * this->Components ];

	stack_array_ptr< int > SubmatrixPos( new int[ this->NumOfDimensions ] );
	memset( SubmatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
	stack_array_ptr< int > MatrixPos( new int[ this->NumOfDimensions ] );
	stack_array_ptr< size_t > DimIncrSize( new size_t[ this->NumOfDimensions ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		DimIncrSize[ x ] = 1;
		for( int y = x + 1; y < this->NumOfDimensions; y++ )
			DimIncrSize[ x ] *= this->DimensionSizes[ y ];
	}

	for( size_t x = 0; x < RequestedSize; x++ )
	{
		memset( MatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
		for( int y = 0; y < this->NumOfDimensions; y++ )
			MatrixPos[ y ] = OriginCoordinates[ y ] + SubmatrixPos[ y ];

		size_t offset = 0;
		for( int y = 0; y < this->NumOfDimensions; y++ )
			offset += MatrixPos[ y ] * DimIncrSize[ y ];

		Value val = ( *this )[ offset ];
		for( int y = 0; y < this->Components; y++ )
			newptr->DataPtr[ x * this->Components + y ] = val[ y ];

		for( int y = this->NumOfDimensions - 1; y >= 0; y-- )
		{
			if( ++( SubmatrixPos[ y ] ) >= ( int ) Sizes[ y ] ) SubmatrixPos[ y ] = 0;
			else break;
		}
	}

	return newptr->DataPtr;
}


const float *NMRData::GetReadonlyDataPtr( size_t StartOffset, size_t Size ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( StartOffset < 0 || StartOffset >= this->FullSize ) throw BadDataPtrRequestOffset();
	if( StartOffset + Size > this->FullSize ) throw BadDataPtrRequestSize();
#endif

	if( this->DataPtrMemLimit && Size / sizeof( float ) > this->DataPtrMemLimit ) throw DataPtrRequestTooLarge();

	stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ ++this->OpenDataPtrCount ] );
	for( int x = 0; x < this->OpenDataPtrCount - 1; x++ )
		memcpy( temp + x, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
	delete [] this->OpenDataPtrs;
	this->OpenDataPtrs = temp.release();

	OpenDataPtr *newptr = this->OpenDataPtrs + this->OpenDataPtrCount - 1;
	newptr->Writeable = false;
	newptr->OriginCoordinates = 0;
	newptr->Contiguous = true;
	newptr->StartOffset = StartOffset;
	newptr->Sizes = 0;
	newptr->Size = Size;

	if( this->MemOnlyData )
		newptr->DataPtr = this->MemOnlyData + StartOffset * this->Components;
	else
	{
		newptr->DataPtr = new float[ Size * this->Components ];
		this->LoadData( StartOffset, Size, newptr->DataPtr );
	}

	return newptr->DataPtr;
}


void NMRData::CloseDataPtr( float *DataPtr )
{
	int index = -1;
	for( int x = 0; x < this->OpenDataPtrCount; x++ )
		if( DataPtr == this->OpenDataPtrs[ x ].DataPtr )
		{
			index = x;
			break;
		};
	if( index < 0 ) throw BadDataPtr();

	if( this->OpenDataPtrs[ index ].Writeable ) this->Flush();
	if( !this->MemOnlyData || !this->OpenDataPtrs[ index ].Contiguous ) delete [] this->OpenDataPtrs[ index ].DataPtr;
	if( this->OpenDataPtrs[ index ].OriginCoordinates ) delete [] this->OpenDataPtrs[ index ].OriginCoordinates;
	if( this->OpenDataPtrs[ index ].Sizes ) delete [] this->OpenDataPtrs[ index ].Sizes;

	if( this->OpenDataPtrCount - 1 )
	{
		stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ this->OpenDataPtrCount - 1 ] );
		for( int x = 0, y = 0; x < this->OpenDataPtrCount; x++ )
		{
			if( x == index ) continue;
			memcpy( temp + y, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
			y++;
		}
		delete [] this->OpenDataPtrs;
		this->OpenDataPtrs = temp.release();
		this->OpenDataPtrCount--;
	}
	else
	{
		delete [] this->OpenDataPtrs;
		this->OpenDataPtrs = 0;
		this->OpenDataPtrCount = 0;
	}

	return;
}


void NMRData::CloseDataPtr( const float *DataPtr ) const
{
	int index = -1;
	for( int x = 0; x < this->OpenDataPtrCount; x++ )
		if( DataPtr == this->OpenDataPtrs[ x ].DataPtr )
		{
			index = x;
			break;
		};
	if( index < 0 ) throw BadDataPtr();

	if( !this->MemOnlyData || !this->OpenDataPtrs[ index ].Contiguous ) delete [] this->OpenDataPtrs[ index ].DataPtr;
	if( this->OpenDataPtrs[ index ].OriginCoordinates ) delete [] this->OpenDataPtrs[ index ].OriginCoordinates;
	if( this->OpenDataPtrs[ index ].Sizes ) delete [] this->OpenDataPtrs[ index ].Sizes;

	if( this->OpenDataPtrCount - 1 )
	{
		stack_array_ptr< OpenDataPtr > temp( new OpenDataPtr[ this->OpenDataPtrCount - 1 ] );
		for( int x = 0, y = 0; x < this->OpenDataPtrCount; x++ )
		{
			if( x == index ) continue;
			memcpy( temp + y, this->OpenDataPtrs + x, sizeof( OpenDataPtr ) );
			y++;
		}
		delete [] this->OpenDataPtrs;
		this->OpenDataPtrs = temp.release();
		this->OpenDataPtrCount--;
	}
	else
	{
		delete [] this->OpenDataPtrs;
		this->OpenDataPtrs = 0;
		this->OpenDataPtrCount = 0;
	}

	return;
}


std::valarray< float > NMRData::GetDataCopyValarray( void ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
#endif

	std::valarray< float > out( this->FullSize * this->Components );
	
	DataOutCopier< std::valarray< float > > copy_data( out );
	this->ApplyToAll_ByComponent_OffsetOrder( copy_data );

	return out;
}


float *NMRData::GetDataCopyPtr( float *Destination ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
#endif

	if( !Destination )
	{
		try
		{
			Destination = new float[ this->FullSize * this->Components ];
		}
		catch( std::bad_alloc )
		{
			throw DataCopyRequestTooLarge();
		}
	}

	DataOutCopier< float * > copy_data( Destination );
	this->ApplyToAll_ByComponent_OffsetOrder( copy_data );

	return Destination;
}


void NMRData::SetDataFromCopyValarray( std::valarray< float > &Input )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Input.size() != this->FullSize * this->Components )
		throw NotCompatible();
#endif

	DataInCopier< std::valarray< float > > copy_data( Input );
	this->ApplyChangeToAll_ByComponent_OffsetOrder( copy_data );
}


void NMRData::SetDataFromCopyPtr( const float *Input )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
#endif

	DataInCopier< const float * > copy_data( Input );
	this->ApplyChangeToAll_ByComponent_OffsetOrder( copy_data );
}


Reference NMRData::operator[]( size_t Offset )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Offset >= this->FullSize ) throw BadOffset();
#endif

	DataPage *page = 0;
	float *ptr = TranslateOffsetToPtr( Offset, this->ReadOnly, &page );
	return Reference( Offset, page, ptr, this->ReadOnly, this );
}


const Reference NMRData::operator[]( size_t Offset ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Offset >= this->FullSize ) throw BadOffset();
#endif

	DataPage *page = 0;
	float *ptr = TranslateOffsetToPtr( Offset, true, &page );
	return Reference( Offset, page, ptr, true, this );
}


Reference NMRData::operator[]( std::vector< int > Coordinates )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Coordinates.size() != this->NumOfDimensions ) throw BadCoordinates();
#endif

	size_t offset = 0;
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempDimIncrSizes[ x ] = 1;
			for( int y = x + 1; y < this->NumOfDimensions; y++ )
				this->TempDimIncrSizes[ x ] *= ( size_t ) this->DimensionSizes[ y ];
		}
	}
	for( int x = 0; x < this->NumOfDimensions; x++ )
		offset += Coordinates[ x ] * this->TempDimIncrSizes[ x ];

#ifdef BEC_NMRDATA_DEBUG
	if( offset >= this->FullSize ) throw BadCoordinates();
	else
#endif
		return ( *this )[ offset ];
}


const Reference NMRData::operator[]( std::vector< int > Coordinates ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Coordinates.size() != this->NumOfDimensions ) throw BadCoordinates();
#endif

	size_t offset = 0;
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempDimIncrSizes[ x ] = 1;
			for( int y = x + 1; y < this->NumOfDimensions; y++ )
				this->TempDimIncrSizes[ x ] *= ( size_t ) this->DimensionSizes[ y ];
		}
	}
	for( int x = 0; x < this->NumOfDimensions; x++ )
		offset += Coordinates[ x ] * this->TempDimIncrSizes[ x ];

#ifdef BEC_NMRDATA_DEBUG
	if( offset >= this->FullSize ) throw BadCoordinates();
	else
#endif
		return ( *this )[ offset ];
}


Reference NMRData::operator[]( std::vector< DimPosition< NMRData > > Coordinates )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Coordinates.size() != this->NumOfDimensions ) throw BadCoordinates();
	for( int i = 0; i < this->NumOfDimensions; i++ )
		if( Coordinates[ i ].parent != this || Coordinates[ i ].dim != i || !Coordinates[ i ].InRange() )
			throw BadCoordinates();
#endif

	size_t offset = 0;
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempDimIncrSizes[ x ] = 1;
			for( int y = x + 1; y < this->NumOfDimensions; y++ )
				this->TempDimIncrSizes[ x ] *= ( size_t ) this->DimensionSizes[ y ];
		}
	}
	for( int x = 0; x < this->NumOfDimensions; x++ )
		offset += Coordinates[ x ].pos * this->TempDimIncrSizes[ x ];

#ifdef BEC_NMRDATA_DEBUG
	if( offset >= this->FullSize ) throw BadCoordinates();
	else
#endif
		return ( *this )[ offset ];
}


const Reference NMRData::operator[]( std::vector< DimPosition< NMRData > > Coordinates ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Coordinates.size() != this->NumOfDimensions ) throw BadCoordinates();
	for( int i = 0; i < this->NumOfDimensions; i++ )
		if( Coordinates[ i ].parent != this || Coordinates[ i ].dim != i || !Coordinates[ i ].InRange() )
			throw BadCoordinates();
#endif

	size_t offset = 0;
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempDimIncrSizes[ x ] = 1;
			for( int y = x + 1; y < this->NumOfDimensions; y++ )
				this->TempDimIncrSizes[ x ] *= ( size_t ) this->DimensionSizes[ y ];
		}
	}
	for( int x = 0; x < this->NumOfDimensions; x++ )
		offset += Coordinates[ x ].pos * this->TempDimIncrSizes[ x ];

#ifdef BEC_NMRDATA_DEBUG
	if( offset >= this->FullSize ) throw BadCoordinates();
	else
#endif
	return ( *this )[ offset ];
}


Subset NMRData::operator()( std::vector< int > Corner1, std::vector< int > Corner2 )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Corner1.size() != this->NumOfDimensions || Corner2.size() != this->NumOfDimensions ) throw BadCoordinates();
#endif

	std::vector< int > Size;
	for( int i = 0; i < this->NumOfDimensions; i++ )
		Size.push_back( Corner2[ i ] - Corner1[ i ] );
	return Subset( this, Corner1, Size, false );
}


const Subset NMRData::operator()( std::vector< int > Corner1, std::vector< int > Corner2 ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Corner1.size() != this->NumOfDimensions || Corner2.size() != this->NumOfDimensions ) throw BadCoordinates();
#endif

	std::vector< int > Size;
	for( int i = 0; i < this->NumOfDimensions; i++ )
		Size.push_back( Corner2[ i ] - Corner1[ i ] );
	return Subset( this, Corner1, Size, true );
}


Subset NMRData::operator()( std::vector< DimPosition< NMRData > > Corner1, std::vector< DimPosition< NMRData > > Corner2 )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Corner1.size() != this->NumOfDimensions || Corner2.size() != this->NumOfDimensions ) throw BadCoordinates();
	for( int i = 0; i < this->NumOfDimensions; i++ )
		if( Corner1[ i ].GetParent() != this || !Corner1[ i ].InRange() || Corner2[ i ].GetParent() != this || !Corner2[ i ].InRange() )
			throw BadCoordinates();
#endif

	std::vector< int > corner, size;
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		corner.push_back( Corner1[ i ].GetPosPts() );
		size.push_back( Corner2[ i ].GetPosPts() - Corner1[ i ].GetPosPts() );
	}
	return Subset( this, corner, size, false );
}


const Subset NMRData::operator()( std::vector< DimPosition< NMRData > > Corner1, std::vector< DimPosition< NMRData > > Corner2 ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Corner1.size() != this->NumOfDimensions || Corner2.size() != this->NumOfDimensions ) throw BadCoordinates();
	for( int i = 0; i < this->NumOfDimensions; i++ )
		if( Corner1[ i ].GetParent() != this || !Corner1[ i ].InRange() || Corner2[ i ].GetParent() != this || !Corner2[ i ].InRange() )
			throw BadCoordinates();
#endif

	std::vector< int > corner, size;
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		corner.push_back( Corner1[ i ].GetPosPts() );
		size.push_back( Corner2[ i ].GetPosPts() - Corner1[ i ].GetPosPts() );
	}
	return Subset( this, corner, size, true );
}


Reference NMRData::operator()( int Coordinate )
{
	if( !this->GotData ) throw DataNotOpen();
	return Reference( DimPosition< NMRData >( this, 0, Coordinate, ( float ) Coordinate, pts ), this->ReadOnly, this );
}


const Reference NMRData::operator()( int Coordinate ) const
{
	if( !this->GotData ) throw DataNotOpen();
	return Reference( DimPosition< NMRData >( this, 0, Coordinate, ( float ) Coordinate, pts ), true, this );
}


Reference NMRData::operator()( DimPosition< NMRData > Coordinate )
{
	if( !this->GotData ) throw DataNotOpen();
	return Reference( Coordinate, this->ReadOnly, this );
}


const Reference NMRData::operator()( DimPosition< NMRData > Coordinate ) const
{
	if( !this->GotData ) throw DataNotOpen();
	return Reference( Coordinate, true, this );
}


float NMRData::GetInterpolatedValue( const float *Coordinates ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( !Coordinates ) throw NullPointerArg();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( Coordinates[ x ] < 0 || Coordinates[ x ] > this->DimensionSizes[ x ] - 1 ) throw BadCoordinates();
#endif

	stack_array_ptr< float > vals( new float[ ( int ) pow( 2.0F, this->NumOfDimensions ) ] );
	stack_array_ptr< int > base( new int[ this->NumOfDimensions ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		base[ x ] = ( int ) floor( Coordinates[ x ] );
	std::vector< int > current( this->NumOfDimensions );

	for( int x = ( int ) pow( 2.0F, this->NumOfDimensions - 1 ), y = 0; x >= 0; x--, y++ )
	{
		for( int z = 0; x < this->NumOfDimensions; z++ )
		{
			current[ z ] = base[ z ];
			int mask = ( int ) pow( 2.0F, this->NumOfDimensions - z - 1 );
			if( ( x & mask ) && base[ z ] + 1 < this->DimensionSizes[ z ] ) current[ z ]++;
		}
		vals[ y ] = ( *this )[ current ];
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		int limity = ( int ) pow( 2.0F, this->NumOfDimensions - x - 1 );
		for( int y = 0; y < limity; y++ )
		{
			float fractpart = Coordinates[ x ] - base[ x ];
			vals[ y ] = vals[ y * 2 + 1 ] + fractpart * ( vals[ 2 * y ] - vals[ y * 2 + 1 ] );
		}
	}

	float outval = vals[ 0 ];
	return outval;
}


float NMRData::GetInterpolatedValue( std::vector< float > Coordinates ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
	if( Coordinates.size() != this->NumOfDimensions ) throw BadCoordinates();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( Coordinates[ x ] < 0 || Coordinates[ x ] > this->DimensionSizes[ x ] - 1 ) throw BadCoordinates();
#endif

	stack_array_ptr< float > vals( new float[ ( int ) pow( 2.0F, this->NumOfDimensions ) ] );
	stack_array_ptr< int > base( new int[ this->NumOfDimensions ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		base[ x ] = ( int ) floor( Coordinates[ x ] );
	std::vector< int > current( this->NumOfDimensions );

	for( int x = ( int ) pow( 2.0F, this->NumOfDimensions - 1 ), y = 0; x >= 0; x--, y++ )
	{
		for( int z = 0; z < this->NumOfDimensions; z++ )
		{
			current[ z ] = base[ z ];
			int mask = ( int ) pow( 2.0F, this->NumOfDimensions - z - 1 );
			if( ( x & mask ) && base[ z ] + 1 < this->DimensionSizes[ z ] ) current[ z ]++;
		}
		vals[ y ] = ( *this )[ current ];
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		int limity = ( int ) pow( 2.0F, this->NumOfDimensions - x - 1 );
		for( int y = 0; y < limity; y++ )
		{
			float fractpart = Coordinates[ x ] - base[ x ];
			vals[ y ] = vals[ y * 2 + 1 ] + fractpart * ( vals[ 2 * y ] - vals[ y * 2 + 1 ] );
		}
	}

	float outval = vals[ 0 ];
	return outval;
}


NMRData::iterator NMRData::GetIteratorStart( void )
{
	return iterator( this, 0, false );
}


NMRData::const_iterator NMRData::GetIteratorStart( void ) const
{
	return const_iterator( this, 0, false );
}


NMRData::iterator NMRData::GetIteratorAt( size_t Offset )
{
#ifdef BEC_NMRDATA_DEBUG
	if( Offset >= this->FullSize ) throw BadOffset();
#endif
	return iterator( this, Offset, false );
}


NMRData::const_iterator NMRData::GetIteratorAt( size_t Offset ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( Offset >= this->FullSize ) throw BadOffset();
#endif
	return const_iterator( this, Offset, false );
}


NMRData::iterator NMRData::GetIteratorForReference( const Reference &Ref )
{
#ifdef BEC_NMRDATA_DEBUG
	if( Ref.GetParent() != this ) throw RefHasWrongParent();
#endif
	return iterator( this, Ref.GetOffset(), false );
}


NMRData::const_iterator NMRData::GetIteratorForReference( const Reference &Ref ) const
{
	return const_iterator( Ref.GetParent(), Ref.GetOffset(), false );
}


NMRData::iterator NMRData::GetIteratorEnd( void )
{
	return iterator( this, 0, true );
}


NMRData::const_iterator NMRData::GetIteratorEnd( void ) const
{
	return const_iterator( this, 0, true );
}


void NMRData::Flush( void )
{
	if( !this->GotData ) return;
	if( this->ReadOnly ) return;

	if( this->Filename ) 
		this->WriteFileHeader();

	this->FlushCache();
	
	if( this->OpenDataPtrCount )
	{
		for( int x = 0; x < this->OpenDataPtrCount; x++ )
		{
			OpenDataPtr *current = this->OpenDataPtrs + x;
			if( current->Writeable && current->Contiguous )
			{
				if( !this->MemOnlyData ) this->WriteData( current->StartOffset, current->Size, current->DataPtr );
			}
			else if( current->Writeable )
			{
				stack_array_ptr< int > SubmatrixPos( new int[ this->NumOfDimensions ] );
				memset( SubmatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
				stack_array_ptr< int > MatrixPos( new int[ this->NumOfDimensions ] );
				stack_array_ptr< size_t > DimIncrSize( new size_t [ this->NumOfDimensions ] );
				for( int y = 0; y < this->NumOfDimensions; y++ )
				{
					DimIncrSize[ y ] = 1;
					for( int z = y + 1; z < this->NumOfDimensions; z++ )
						DimIncrSize[ y ] *= this->DimensionSizes[ z ];
				}

				for( size_t y = 0; y < current->Size; y++ )
				{
					memset( MatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
					for( int z = 0; z < this->NumOfDimensions; z++ )
						MatrixPos[ z ] = current->OriginCoordinates[ z ] + SubmatrixPos[ z ];

					size_t offset = 0;
					for( int z = 0; z < this->NumOfDimensions; z++ )
						offset += MatrixPos[ z ] * DimIncrSize[ z ];

					Reference dest = ( *this )[ offset ];
					for( int i = 0; i < this->Components; i++ )
						dest[ i ] = current->DataPtr[ y * this->Components ];

					for( int z = this->NumOfDimensions; z >= 0; z-- )
					{
						if( ++( SubmatrixPos[ z ] ) >= ( int ) current->Sizes[ z ] ) SubmatrixPos[ z ] = 0;
						else break;
					}
				}
			}
		}

		this->RefreshCache();

		for( int x = 0; x < this->OpenDataPtrCount; x++ )
		{
			OpenDataPtr *current = this->OpenDataPtrs + x;
			if( current->Contiguous )
			{
				if( !this->MemOnlyData ) this->LoadData( current->StartOffset, current->Size, current->DataPtr );
			}
			else
			{
				stack_array_ptr< int > SubmatrixPos( new int[ this->NumOfDimensions ] );
				memset( SubmatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
				stack_array_ptr< int > MatrixPos( new int[ this->NumOfDimensions ] );
				stack_array_ptr< size_t > DimIncrSize( new size_t[ this->NumOfDimensions ] );
				for( int y = 0; y < this->NumOfDimensions; y++ )
				{
					DimIncrSize[ y ] = 1;
					for( int z = y + 1; z < this->NumOfDimensions; z++ )
						DimIncrSize[ y ] *= this->DimensionSizes[ z ];
				}

				for( size_t y = 0; y < current->Size; y++ )
				{
					memset( MatrixPos, 0, sizeof( int ) * this->NumOfDimensions );
					for( int z = 0; z < this->NumOfDimensions; z++ )
						MatrixPos[ z ] = current->OriginCoordinates[ z ] + SubmatrixPos[ z ];

					size_t offset = 0;
					for( int z = 0; z < this->NumOfDimensions; z++ )
						offset += MatrixPos[ z ] * DimIncrSize[ z ];

					Value val = ( *this )[ offset ];
					for( int y = 0; y < this->Components; y++ )
						current->DataPtr[ x * this->Components + y ] = val[ y ];

					for( int z = this->NumOfDimensions; z >= 0; z-- )
					{
						if( ++( SubmatrixPos[ z ] ) >= ( int ) current->Sizes[ z ] ) SubmatrixPos[ z ] = 0;
						else break;
					}
				}
			}
		}
	}
}


void NMRData::Close()
{
	if( !this->GotData ) return;

	this->StdClose();

	if( this->File ) fclose( this->File );
	if( this->MemTempFile ) fclose( this->MemTempFile );
	if( this->MemOnlyData ) delete [] MemOnlyData;

	this->File = 0;
	this->MemTempFile = 0;
	this->MemOnlyData = 0;
	this->FullSize = 0;

	this->GotData = false;
}


void NMRData::TransposeDimensions( int Dim1, int Dim2 )
{
	int dimcount = this->NumOfDimensions;
#ifdef BEC_NMRDATA_DEBUG
	if( Dim1 < 0 || Dim1 > dimcount ) throw BadDimNumber();
	if( Dim2 < 0 || Dim2 > dimcount ) throw BadDimNumber();
#endif
	if( Dim1 == Dim2 ) return;

	if( Dim1 > Dim2 )
	{
		int a = Dim2;
		Dim1 = Dim2;
		Dim2 = a;
	}

	stack_array_ptr< int > coord( new int[ dimcount ] );
	memset( coord, 0, sizeof( int ) * dimcount );

	stack_array_ptr< int > newsizes( this->GetDimensionSizes() );
	stack_array_ptr< int > oldsizes( this->GetDimensionSizes() );

	newsizes[ Dim1 ] = oldsizes[ Dim2 ];
	newsizes[ Dim2 ] = oldsizes[ Dim1 ];

	NMRData temp;
	temp.CopyParameters( this );

	int swapint = temp.DimensionSizes[ Dim2 ];
	temp.DimensionSizes[ Dim2 ] = temp.DimensionSizes[ Dim1 ];
	temp.DimensionSizes[ Dim1 ] = swapint;

	char *swapstr = temp.DimensionAxisLabels[ Dim2 ];
	temp.DimensionAxisLabels[ Dim2 ] = temp.DimensionAxisLabels[ Dim1 ];
	temp.DimensionAxisLabels[ Dim1 ] = swapstr;

	float swap = temp.DimensionCentersFreq[ Dim2 ];
	temp.DimensionCentersFreq[ Dim2 ] = temp.DimensionCentersFreq[ Dim1 ];
	temp.DimensionCentersFreq[ Dim1 ] = swap;

	swap = temp.DimensionCentersPPM[ Dim2 ];
	temp.DimensionCentersPPM[ Dim2 ] = temp.DimensionCentersPPM[ Dim1 ];
	temp.DimensionCentersPPM[ Dim1 ] = swap;

	swap = temp.DimensionOriginsPPM[ Dim2 ];
	temp.DimensionOriginsPPM[ Dim2 ] = temp.DimensionOriginsPPM[ Dim1 ];
	temp.DimensionOriginsPPM[ Dim1 ] = swap;

	swap = temp.DimensionScalesFreq[ Dim2 ];
	temp.DimensionScalesFreq[ Dim2 ] = temp.DimensionScalesFreq[ Dim1 ];
	temp.DimensionScalesFreq[ Dim1 ] = swap;

	swap = temp.DimensionScalesPPM[ Dim2 ];
	temp.DimensionScalesPPM[ Dim2 ] = temp.DimensionScalesPPM[ Dim1 ];
	temp.DimensionScalesPPM[ Dim1 ] = swap;

	temp.BuildInMem();

	stack_array_ptr< size_t > oldincrsizes( new size_t[ dimcount ] );
	memset( oldincrsizes, 0, sizeof( int ) * dimcount );
	stack_array_ptr< size_t > newincrsizes( new size_t[ dimcount ] );
	memset( newincrsizes, 0, sizeof( int ) * dimcount );

	for( int i = 0; i < dimcount; i++ )
	{
		oldincrsizes[ i ] = 1;
		newincrsizes[ i ] = 1;
		for( int j = i + 1; j < dimcount; j++ )
		{
			oldincrsizes[ i ] *= oldsizes[ j ];
			newincrsizes[ i ] *= newsizes[ j ];
		}
	};

	for( size_t i = 0; i < this->FullSize; i++ )
	{
		size_t oldoffset = 0;
		for( int x = 0; x < dimcount; x++ )
			oldoffset += coord[ x ] * oldincrsizes[ x ];

		size_t newoffset = 0;
		for( int x = 0; x < dimcount; x++ )
		{
			if( x == Dim1 ) newoffset += coord[ Dim2 ] * newincrsizes[ Dim1 ];
			else if( x == Dim2 ) newoffset += coord[ Dim1 ] * newincrsizes[ Dim2 ];
			else newoffset += coord[ x ] * newincrsizes[ x ];
		}

		temp[ newoffset ] = ( *this )[ oldoffset ];
		for( int j = dimcount - 1; j >= 0; j-- )
		{
			if( ++( coord[ j ] ) == oldsizes[ j ] ) coord[ j ] = 0;
			else break;
		}
	}

	this->CopyDataset( &temp );

	temp.Close();
}


void NMRData::CalculateStats( void ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
#endif

	StatsCollectorFirstPass a( 1.0F / ( ( float ) this->FullSize ) );
	this->ApplyToAll( a );
	this->Mean = a.Mean;
	this->Min = a.Min;
	this->Max = a.Max;

	StatsCollectorSecondPass b( this->Mean );
	this->ApplyToAll( b );
	this->StdDev = sqrt( b.SumOfDevSq / ( ( float ) this->FullSize ) - 1.0F );

	this->CalculateNoiseEstimate();

	this->GotStats = true;
}


void NMRData::CalculateNoiseEstimate( void ) const
{
	std::vector< float > estimates;
	std::vector< int > corner1, corner2;

	if( this->NumOfDimensions == 1 )
	{
		corner1.push_back( 0 );
		corner2.push_back( this->DimensionSizes[ 0 ] - 1 );
		Subset s = ( *this )( corner1, corner2 );
		s.CalculateStats();
		this->NoiseEstimate = s.NoiseEstimate;
		this->GotNoiseEstimate = true;
		return;
	}
	else
	{
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			corner1.push_back( this->DimensionSizes[ i ] / 2 );
			corner2.push_back( this->DimensionSizes[ i ] / 2 );
		}

		for( int d1 = this->NumOfDimensions - 1, d2 = this->NumOfDimensions - 2; d1; )
		{
			corner1[ d2 ] = 0;
			corner2[ d2 ] = this->DimensionSizes[ d2 ] - 1;

			int imax = this->DimensionSizes[ d1 ] / 8 > 8 ? this->DimensionSizes[ d1 ] / 8 : 
				( this->DimensionSizes[ d1 ] > 8 ? 8 : this->DimensionSizes[ d1 ] );
			int spacing = this->DimensionSizes[ d1 ] / imax;
			if( spacing < 1 ) spacing = 1;

			for( int i = 0; i < imax; i++ )
			{
				int pos = i * spacing;
				if( pos == this->DimensionSizes[ d1 ] ) pos = this->DimensionSizes[ d1 ] - 1;
				corner1[ d1 ] = pos;
				corner2[ d1 ] = pos;

				Subset s = ( *this )( corner1, corner2 );
				s.CalculateStats();
				estimates.push_back( s.NoiseEstimate );
			}

			corner1[ d1 ] = this->DimensionSizes[ d1 ] / 2;
			corner2[ d1 ] = this->DimensionSizes[ d1 ] / 2;
			corner1[ d2 ] = this->DimensionSizes[ d2 ] / 2;
			corner2[ d2 ] = this->DimensionSizes[ d2 ] / 2;

			if( --d2 < 0 ) d2 = --d1 - 1;
		}

		std::sort( estimates.begin(), estimates.end() );
		if( estimates.size() % 2 )
			this->NoiseEstimate = ( estimates[ estimates.size() / 2 ] + estimates[ estimates.size() / 2 + 1 ] ) / 2.0F;
		else
			this->NoiseEstimate = estimates[ estimates.size() / 2 ];
		this->GotNoiseEstimate = true;
		return;
	}
}


int NMRData::GetFileTypeCode( void ) const
{
	return this->FileType_bin;
}


const char *NMRData::GetFileTypeString( void ) const
{
	return "binary";
}


std::string NMRData::GetFileTypeStr( void ) const
{
	return std::string( this->GetFileTypeString() );
}


NMRData *NMRData::GetObjForFile( const char *Filename, bool ReadOnly )
{
	stack_ptr< NMRData > obj( NMRData::GetObjForFileType( Filename ) );
	obj->OpenFile( Filename, ReadOnly );
	return obj.release();
}


NMRData *NMRData::GetObjForFile( std::string Filename, bool ReadOnly )
{
	return NMRData::GetObjForFile( ( char * ) Filename.c_str(), ReadOnly );
}


NMRData *NMRData::GetObjForFileType( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	if( strlen( Filename ) > 4 && !strcmp( Filename + ( int ) strlen( Filename ) - 4, ".txt" ) )
	{
		//	text file
		return new NMRData_Text();
	}
	else if( strlen( Filename ) > 4 && !strcmp( Filename + ( int ) strlen( Filename ) - 4, ".bin" ) )
	{
		//	bin file
		return new NMRData();
	}
	else if( strlen( Filename ) > 3 && !strcmp( Filename + ( int ) strlen( Filename ) - 3, ".ft" ) )
	{
		//	nmrPipe file
		return new NMRData_nmrPipe();
	}
	else if( strlen( Filename ) > 4 && ( !strcmp( Filename + ( int ) strlen( Filename ) - 4, ".dat" ) ||
		!strcmp( Filename + ( int ) strlen( Filename ) - 4, ".ft2" ) ||
		!strcmp( Filename + ( int ) strlen( Filename ) - 4, ".ft3" ) ||
		!strcmp( Filename + ( int ) strlen( Filename ) - 4, ".ft4" ) ||
        !strcmp( Filename + ( int ) strlen( Filename ) - 4, ".fid" ) ) )
	{
		//	nmrPipe file
		return new NMRData_nmrPipe();
	}
	else if( strlen( Filename ) > 9 && !strcmp( Filename + ( int ) strlen( Filename ) - 9, ".3D.param" ) )
	{
		//	XEASY file
		return new NMRData_XEASY();
	}
	else if( strlen( Filename ) > 5 && !strcmp( Filename + ( int ) strlen( Filename ) - 5, ".ucsf" ) )
	{
		//	UCSF/SPARKY file
		return new NMRData_UCSF();
	}
	else if( strlen( Filename ) > 3 && !strcmp( Filename + ( int ) strlen( Filename ) - 3, ".nv" ) )
	{
		//	NMRView file
		return new NMRData_NMRView();
	}
	else throw FileTypeNotRecognized( Filename );
}


NMRData *NMRData::GetObjForFileType( std::string Filename )
{
	return NMRData::GetObjForFileType( ( char * ) Filename.c_str() );
}

	
void NMRData::CopyParameters( const NMRData *Source, NMRData *Dest )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Source || !Dest ) throw NullPointerArg();
	if( Dest->GotData ) throw DataAreOpen();
#endif

	Dest->SetNumOfDims( Source->GetNumOfDims() );
	for( int x = 0; x < Dest->NumOfDimensions; x++ )
	{
		Dest->SetDimensionSize( x, Source->GetDimensionSize( x ) );

		stack_array_ptr< char > Label( Source->GetDimensionLabel( x ) );
		if( Label )	Dest->SetDimensionLabel( x, Label );

		float SW, SF, CenterPPM;
		Source->GetDimensionCalibration( x, SW, SF, CenterPPM );
		Dest->SetDimensionCalibration( x, SW, SF, CenterPPM );
	}
	stack_array_ptr< bool > ComplexByDim( Source->IsComplexAvailableByDim() );
	Dest->SetComplexByDim( ComplexByDim );

	stack_array_ptr< bool > TDByDim( Source->IsTimeDomainAvailableByDim() );
	Dest->SetTDByDim( TDByDim );
}


void NMRData::CopyDataset( const NMRData *Source, NMRData *Dest )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Source || !Dest ) throw NullPointerArg();
#endif

	if( Dest->GotData )
	{
		std::string Filename = Dest->GetFilenameStr();
		Dest->Close();
		Dest->CopyParameters( Source );
		if( !Filename.empty() )	Dest->CreateFile( Filename );
		else Dest->BuildInMem();
	}
	else
	{
		Dest->CopyParameters( Source );
		Dest->BuildInMem();
	}

	*Dest = *Source;
}


template< typename T > T NMRData::SwapByteOrder( T Source ) const
{
	T Dest;
	char *dest = ( char * ) &Dest;
	char *src = ( char * ) &Source;
	int size = sizeof( T );

	for( int x = 0, y = size - 1; x < size; x++, y-- )
		dest[ x ] = src[ y ];

	return Dest;
}


void NMRData::FreeArray( char *ptr )
{
	delete [] ptr;
}


void NMRData::FreeArray( int *ptr )
{
	delete [] ptr;
}


void NMRData::FreeArray( float *ptr )
{
	delete [] ptr;
}


void NMRData::FreeArray( bool *ptr )
{
	delete [] ptr;
}


void NMRData::FreeObj( NMRData *ptr )
{
	delete ptr;
}


char *NMRData::RemoveFileExtension( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	if( ( strlen( Filename ) > 9 && !strcmp( Filename + ( int ) strlen( Filename ) - 9, ".3D.param" ) ) ||
		( strlen( Filename ) > 6 && !strcmp( Filename + ( int ) strlen( Filename ) - 6, ".3D.16" ) ) ||
		( strlen( Filename ) > 5 && !strcmp( Filename + ( int ) strlen( Filename ) - 5, ".3D.8" ) ) )
	{
		//	remove from the end, up to the second period
		int NewLen;
		bool GotOne = false;

		for( NewLen = ( int ) strlen( Filename ) - 1; NewLen > 0; NewLen-- )
		{
			if( Filename[ NewLen ] == '.' && !GotOne ) GotOne = true;
			else if( Filename[ NewLen ] == '.' ) break;
		}

		char *Out = new char[ NewLen + 1 ];
		strncpy( Out, Filename, NewLen );
		Out[ NewLen ] = 0;
		return Out;
	}
	else
	{
		//	remove from the end, up to the first period
		int NewLen;
		for( NewLen = ( int ) strlen( Filename ) - 1; NewLen > 0; NewLen-- )
			if( Filename[ NewLen ] == '.' ) break;

		char *Out = new char[ NewLen + 1 ];
		strncpy( Out, Filename, NewLen );
		Out[ NewLen ] = 0;
		return Out;
	}
}


char *NMRData::RemoveFilePath( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	int y = 0;
	for( int x = 0; x < ( int ) strlen( Filename ); x++ )
		if( Filename[ x ] == '\\' || Filename[ x ] == '/' )
			y = x + 1;
	char *Out = new char [ ( int ) strlen( Filename ) - y + 1 ];
	strncpy( Out, Filename + y, ( int ) strlen( Filename ) - y );
	Out[ ( int ) strlen( Filename ) - y ] = 0;
	return Out;
}


void NMRData::ClearPrivateParams( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif

	this->File = 0;
	this->MemTempFile = 0;
	this->MemOnlyData = 0;
}


NMRData::iterator NMRData::begin( void )
{
	return iterator( this, 0, false );
}


NMRData::const_iterator NMRData::begin( void ) const
{
	return const_iterator( this, 0, false );
}


NMRData::iterator NMRData::end( void )
{
	return iterator( this, this->FullSize, true );
}


NMRData::const_iterator NMRData::end( void ) const
{
	return const_iterator( this, this->FullSize, true );
}


NMRData::reverse_iterator NMRData::rbegin( void )
{
	return reverse_iterator( iterator( this, this->FullSize, true ) );
}


NMRData::const_reverse_iterator NMRData::rbegin( void ) const
{
	return const_reverse_iterator( const_iterator( this, this->FullSize, true ) );
}


NMRData::reverse_iterator NMRData::rend( void )
{
	return reverse_iterator( iterator( this, 0, false ) );
}


NMRData::const_reverse_iterator NMRData::rend( void ) const
{
	return const_reverse_iterator( const_iterator( this, 0, false ) );
}


void NMRData::StdClose( void )
{
	if( !this->GotData ) return;

	this->Flush();
	this->CloseCache();
	if( this->OpenDataPtrs )
	{
		for( int i = 0; i < this->OpenDataPtrCount; i++ )
			if( !this->OpenDataPtrs[ i ].Contiguous || !this->MemOnlyData ) delete [] this->OpenDataPtrs[ i ].DataPtr;
		delete [] this->OpenDataPtrs;
		this->OpenDataPtrCount = 0;
		this->OpenDataPtrs = 0;
	}
	if( this->Filename )
	{
		delete [] this->Filename;
		this->Filename = 0;
	}
	if( this->Name )
	{
		delete [] this->Name;
		this->Name = 0;
	}
}


void NMRData::ClearCacheParams( void ) const
{
	this->InternalCacheSize = 0;
	this->PageSize = 0;
	this->CacheStart = 0;
	this->CacheEnd = 0;
	this->CacheMap.clear();
	this->OpenPages = 0;
	this->MaxPages = 0;
	this->CacheDim1 = 0;
	this->CacheDim2 = 0;
	this->CacheMode = Linear;

	if( this->CacheDefinesSubmatrices() )
	{
		this->SubmatrixSizes = 0;
		this->SubmatrixSize = 0;
		this->SubmatrixCount = 0;
		this->SubmatrixDimCount = 0;
		this->SubmatrixPos = 0;
		this->PointPos = 0;
		this->TempDimIncrSizes = 0;
		this->TempSubmatrixExtIncrSizes = 0;
		this->TempSubmatrixIntIncrSizes = 0;
	}
	this->TempCoord = 0;
	this->TempVectorIncrSizes = 0;
	this->TempPlaneIncrSizes = 0;
}


void NMRData::FlushCache( void )
{
	if( !this->GotData ) return;

	for( DataPage *CurrentPage = this->CacheStart; CurrentPage; CurrentPage = CurrentPage->Next )
	{
		if( CurrentPage->Dirty )
		{
			if( this->CacheMode == Linear ) 
			{
				this->WriteData( CurrentPage->Index * this->PageSize, 
					CurrentPage->Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : 
					this->FullSize - CurrentPage->Index * this->PageSize, CurrentPage->DataPtr );
			}
			else if( this->CacheMode == Plane )
			{
				if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
				size_t Remainder = CurrentPage->Index;
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					size_t x = Remainder;
					Remainder = x % this->TempDimIncrSizes[ i ];
					this->TempCoord[ i ] = ( int ) ( x / this->TempDimIncrSizes[ i ] );
				}
				this->WritePlane( CacheDim1, CacheDim2, this->TempCoord, CurrentPage->DataPtr );
			}
			else if( this->CacheMode == Submatrix )
			{
				this->WriteSubmatrix( CurrentPage->Index, CurrentPage->DataPtr );
			}
			else if( this->CacheMode == Vector )
			{
				this->WriteVector( CacheDim1, CurrentPage->Index, CurrentPage->DataPtr );
			}
			CurrentPage->Dirty = false;
		}
	}
}


void NMRData::RefreshCache( void ) const
{
	if( !this->GotData ) return;

	for( DataPage *CurrentPage = this->CacheStart; CurrentPage; CurrentPage = CurrentPage->Next )
	{
		if( this->CacheMode == Linear ) 
		{
			this->LoadData( CurrentPage->Index * this->PageSize, 
				CurrentPage->Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : 
				this->FullSize - CurrentPage->Index * this->PageSize, CurrentPage->DataPtr );
		}
		else if( this->CacheMode == Plane )
		{
			if( !TempCoord ) TempCoord = new int[ this->NumOfDimensions ];
			size_t Remainder = CurrentPage->Index;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				size_t x = Remainder;
				Remainder = x % this->TempDimIncrSizes[ i ];
				this->TempCoord[ i ] = ( int ) ( x / this->TempDimIncrSizes[ i ] );
			}
			this->LoadPlane( CacheDim1, CacheDim2, TempCoord, CurrentPage->DataPtr );
		}
		else if( this->CacheMode == Submatrix )
			this->LoadSubmatrix( CurrentPage->Index, CurrentPage->DataPtr );
		else if( this->CacheMode == Vector )
			this->LoadVector( CacheDim1, CurrentPage->Index, CurrentPage->DataPtr );
		CurrentPage->Dirty = false;
	}
}


void NMRData::CloseCache( void ) const
{
	if( !this->GotData ) return;

	DataPage *Next;
	for( DataPage *CurrentPage = this->CacheStart; CurrentPage; CurrentPage = Next )
	{
		delete [] CurrentPage->DataPtr;
		Next = CurrentPage->Next;
		delete CurrentPage;
	}

	if( this->CacheDefinesSubmatrices() )
	{
		if( this->SubmatrixSizes ) delete [] this->SubmatrixSizes;
		if( this->SubmatrixDimCount ) delete [] this->SubmatrixDimCount;
		if( this->TempCoord ) delete [] this->TempCoord;
		if( this->SubmatrixPos ) delete [] this->SubmatrixPos;
		if( this->PointPos ) delete [] this->PointPos;
		if( this->TempDimIncrSizes ) delete [] this->TempDimIncrSizes;
		if( this->TempSubmatrixExtIncrSizes ) delete [] this->TempSubmatrixExtIncrSizes;
		if( this->TempSubmatrixIntIncrSizes ) delete [] this->TempSubmatrixIntIncrSizes;
	}
	if( this->TempVectorIncrSizes ) delete [] this->TempVectorIncrSizes;

	this->ClearCacheParams();
}


float *NMRData::TranslateOffsetToPtr( size_t Offset, bool Const, DataPage **Page ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw Exceptions::DataNotOpen();
	if( Offset >= this->FullSize ) throw Exceptions::BadOffset();
#endif

	if( CacheMode == MemOnly )
		return this->MemOnlyData + Offset * this->Components;

	NMRData *writeptr = const_cast< NMRData * >( this );

	size_t Index = 0, PageOffset = 0;
	if( CacheMode == Linear )
	{
		Index = Offset / this->PageSize;
		PageOffset = Offset % this->PageSize;
	}
	else if( CacheMode == Plane )
	{
		if( !TempCoord ) TempCoord = new int[ this->NumOfDimensions ];
		if( !TempDimIncrSizes )
		{
			TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				TempDimIncrSizes[ i ] = 1;
				for( int j = i + 1; j < this->NumOfDimensions; j++ )
					TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
			}
		}
		if( !TempPlaneIncrSizes )
		{
			TempPlaneIncrSizes = new size_t[ this->NumOfDimensions ];
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == this->CacheDim1 || i == this->CacheDim2 ) TempPlaneIncrSizes[ i ] = 0;
				else
				{
					TempPlaneIncrSizes[ i ] = 1;
					for( int j = i + 1; j < this->NumOfDimensions; j++ )
					{
						if( j == CacheDim1 || j == CacheDim2 ) continue;
						else TempPlaneIncrSizes[ i ] *= this->DimensionSizes[ j ];
					}
				}
			}
		}

		size_t Remainder = Offset;
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			size_t x = Remainder;
			Remainder = x % TempDimIncrSizes[ i ];
			TempCoord[ i ] = ( int ) ( x / TempDimIncrSizes[ i ] );
		}
		PageOffset = TempCoord[ CacheDim1 ] * DimensionSizes[ CacheDim2 ] + TempCoord[ CacheDim2 ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			if( i == CacheDim1 || i == CacheDim2 ) continue;
			Index += TempCoord[ i ] * TempPlaneIncrSizes[ i ];
		}
	}
	else if( CacheMode == Vector )
	{
		if( CacheDim1 < this->NumOfDimensions - 1 )
		{
			if( !TempCoord ) TempCoord = new int[ this->NumOfDimensions ];
			if( !TempDimIncrSizes )
			{
				TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					TempDimIncrSizes[ i ] = 1;
					for( int j = i + 1; j < this->NumOfDimensions; j++ )
						TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
				}
			}
			if( !TempVectorIncrSizes )
			{
				TempVectorIncrSizes = new size_t[ this->NumOfDimensions ];
				for( int i = 0; i < this->NumOfDimensions; i++ )
				{
					if( i == CacheDim1 ) TempVectorIncrSizes[ i ] = 0;
					else
					{
						TempVectorIncrSizes[ i ] = 1;
						for( int j = i + 1; j < this->NumOfDimensions; j++ )
						{
							if( j == CacheDim1 ) continue;
							else TempVectorIncrSizes[ i ] *= this->DimensionSizes[ j ];
						}
					}
				}
			}

			size_t Remainder = Offset;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				size_t x = Remainder;
				Remainder = x % TempDimIncrSizes[ i ];
				TempCoord[ i ] = ( int ) ( x / TempDimIncrSizes[ i ] );
			}
			Index = 0;
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == CacheDim1 ) continue;
				Index += TempCoord[ i ] * TempVectorIncrSizes[ i ];
			}
			PageOffset = TempCoord[ CacheDim1 ];
		}
		else
		{
			Index = Offset / ( size_t ) DimensionSizes[ CacheDim1 ];
			PageOffset = Offset % ( size_t ) DimensionSizes[ CacheDim1 ];
		}
	}
	else if( CacheMode == Submatrix )
	{
		this->TranslateOffsetToSubmatrixPos( Offset, Index, PageOffset );
	}

	if( this->OpenPages && Index == this->CacheStart->Index )
	{
		if( !Const ) this->CacheStart->Dirty = true;
		if( Page ) *Page = this->CacheStart;
		return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
	}
	else if( this->OpenPages == 1 && this->OpenPages == this->MaxPages )
	{
		if( this->CacheStart->Dirty ) 
		{
			switch( this->CacheMode )
			{
			case Linear:
				writeptr->WriteData( this->CacheStart->Index * this->PageSize, 
					this->CacheStart->Index * this->PageSize <= this->FullSize ? this->PageSize : 
					this->FullSize - this->CacheStart->Index * this->PageSize, this->CacheStart->DataPtr );
				break;
			case Submatrix:
				writeptr->WriteSubmatrix( this->CacheStart->Index, this->CacheStart->DataPtr );
				break;
			case Plane:
				writeptr->WritePlane( this->CacheDim2, this->CacheDim1, this->TempCoord, this->CacheStart->DataPtr );
				break;
			case Vector:
				writeptr->WriteVector( this->CacheDim1, this->CacheStart->Index, this->CacheStart->DataPtr );
			}
		}
		if( !this->CacheStart->RegisteredSubsets.empty() )
		{
			while( !this->CacheStart->RegisteredSubsets.empty() )
				( *this->CacheStart->RegisteredSubsets.begin() )->MarkPageInvalid( this->CacheStart->Index );
		}
		this->CacheMap.erase( this->CacheStart->Index );
		this->CacheStart->Index = Index;
		if( !Const ) this->CacheStart->Dirty = true;
		else this->CacheStart->Dirty = false;
		switch( this->CacheMode )
		{
		case Linear:
			LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, CacheStart->DataPtr );
			break;
		case Submatrix:
			LoadSubmatrix( Index, this->CacheStart->DataPtr );
			break;
		case Plane:
			LoadPlane( this->CacheDim2, this->CacheDim1, this->TempCoord, this->CacheStart->DataPtr );
			break;
		case Vector:
			LoadVector( this->CacheDim1, Index, this->CacheStart->DataPtr );
		}
		this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, this->CacheStart ) );
		if( Page ) *Page = CacheStart;
		return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
	}
	else if( this->OpenPages == 1 )
	{
		this->OpenPages++;
		DataPage *Current = new DataPage;
		Current->Index = Index;
		if( !Const ) Current->Dirty = true;
		else Current->Dirty = false;
		Current->Next = this->CacheStart;
		Current->Prev = 0;
		switch( this->CacheMode )
		{
		case Linear:
			Current->DataPtr = new float[ this->PageSize * this->Components ];
			LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, Current->DataPtr );
			break;
		case Submatrix:
			Current->DataPtr = new float[ this->SubmatrixSize * Components ];
			LoadSubmatrix( Index, Current->DataPtr );
			break;
		case Plane:
			CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
			LoadPlane( CacheDim2, CacheDim1, TempCoord, Current->DataPtr );
			break;
		case Vector:
			Current->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
			LoadVector( CacheDim1, Index, Current->DataPtr );
		}
		this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, Current ) );
		this->CacheStart->Prev = Current;
		this->CacheStart = Current;
		if( Page ) *Page = Current;
		return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
	}
	else if( this->OpenPages && this->OpenPages < 128 )
	{
		DataPage *Current, *Prev, *Next; 
		for( Current = this->CacheStart->Next; Current; Current = Current->Next )
		{
			if( Index == Current->Index )
			{
				if( !Const ) Current->Dirty = true;
				Prev = Current->Prev;
				Next = Current->Next;
				Prev->Next = Next;
				if( Next ) Next->Prev = Prev;
				else this->CacheEnd = Prev;
				Current->Next = this->CacheStart;
				this->CacheStart->Prev = Current;
				this->CacheStart = Current;
				if( Page ) *Page = this->CacheStart;
				return Current->DataPtr + PageOffset * ( size_t ) this->Components;
			}
		}
		if( OpenPages == MaxPages )
		{
			Current = this->CacheEnd;
			Prev = Current->Prev;
			if( Current->Dirty ) 
			{
				switch( this->CacheMode )
				{
				case Linear:
					writeptr->WriteData( Current->Index * this->PageSize, 
					Current->Index * this->PageSize <= this->FullSize ? this->PageSize : 
					this->FullSize - Current->Index * this->PageSize, Current->DataPtr );
					break;
				case Submatrix:
					writeptr->WriteSubmatrix( Current->Index, Current->DataPtr );
					break;
				case Plane:
					writeptr->WritePlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
					break;
				case Vector:
					writeptr->WriteVector( this->CacheDim1, Current->Index, Current->DataPtr );
				}
			}
			if( !Current->RegisteredSubsets.empty() )
			{
				while( !Current->RegisteredSubsets.empty() )
					( *Current->RegisteredSubsets.begin() )->MarkPageInvalid( Current->Index );
			}
			this->CacheMap.erase( Current->Index );
			Current->Index = Index;
			if( !Const ) Current->Dirty = true;
			else Current->Dirty = false;
			Current->Next = this->CacheStart;
			Current->Prev = 0;
			switch( this->CacheMode )
			{
			case Linear:
				LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, Current->DataPtr );
				break;
			case Submatrix:
				LoadSubmatrix( Index, Current->DataPtr );
				break;
			case Plane:
				LoadPlane( CacheDim2, CacheDim1, TempCoord, Current->DataPtr );
				break;
			case Vector:
				LoadVector( CacheDim1, Index, Current->DataPtr );
			}
			this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, Current ) );
			Prev->Next = 0;
			this->CacheEnd = Prev;
			this->CacheStart->Prev = Current;
			this->CacheStart = Current;
			if( Page ) *Page = this->CacheStart;
			return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
		}
		else
		{
			OpenPages++;
			Current = new DataPage;
			Current->Index = Index;
			if( !Const ) Current->Dirty = true;
			else Current->Dirty = false;
			Current->Next = this->CacheStart;
			Current->Prev = 0;
			switch( this->CacheMode )
			{
			case Linear:
				Current->DataPtr = new float[ this->PageSize * this->Components ];
				LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, Current->DataPtr );
				break;
			case Submatrix:
				Current->DataPtr = new float[ this->SubmatrixSize * Components ];
				LoadSubmatrix( Index, Current->DataPtr );
				break;
			case Plane:
				CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
				LoadPlane( CacheDim2, CacheDim1, TempCoord, Current->DataPtr );
				break;
			case Vector:
				Current->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
				LoadVector( CacheDim1, Index, Current->DataPtr );
			}
			this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, Current ) );
			this->CacheStart->Prev = Current;
			this->CacheStart = Current;
			if( Page ) *Page = this->CacheStart;
			return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
		}
	}
	else if( this->OpenPages )
	{
		DataPage *Current, *Prev, *Next; 
		std::map< size_t, DataPage * >::iterator i = this->CacheMap.find( Index );
		if( i != this->CacheMap.end() )
		{
			Current = i->second;
			Prev = Current->Prev;
			Next = Current->Next;
			Prev->Next = Next;
			if( Next ) Next->Prev = Prev;
			else this->CacheEnd = Prev;
			Current->Next = this->CacheStart;
			this->CacheStart->Prev = Current;
			this->CacheStart = Current;
			if( Page ) *Page = this->CacheStart;
			return Current->DataPtr + PageOffset * ( size_t ) this->Components;
		}
		else if( OpenPages == MaxPages )
		{
			Current = this->CacheEnd;
			Prev = Current->Prev;
			if( Current->Dirty ) 
			{
				switch( this->CacheMode )
				{
				case Linear:
					writeptr->WriteData( Current->Index * this->PageSize, 
					( Current->Index + 1 ) * this->PageSize <= this->FullSize ? this->PageSize : 
					this->FullSize - Current->Index * this->PageSize, Current->DataPtr );
					break;
				case Submatrix:
					writeptr->WriteSubmatrix( Current->Index, Current->DataPtr );
					break;
				case Plane:
					writeptr->WritePlane( this->CacheDim2, this->CacheDim1, this->TempCoord, Current->DataPtr );
					break;
				case Vector:
					writeptr->WriteVector( this->CacheDim1, Current->Index, Current->DataPtr );
				}
			}
			if( !Current->RegisteredSubsets.empty() )
			{
				while( !Current->RegisteredSubsets.empty() )
					( *Current->RegisteredSubsets.begin() )->MarkPageInvalid( Current->Index );
			}
			this->CacheMap.erase( Current->Index );
			Current->Index = Index;
			if( !Const ) Current->Dirty = true;
			else Current->Dirty = false;
			Current->Next = this->CacheStart;
			Current->Prev = 0;
			switch( this->CacheMode )
			{
			case Linear:
				LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, Current->DataPtr );
				break;
			case Submatrix:
				LoadSubmatrix( Index, Current->DataPtr );
				break;
			case Plane:
				LoadPlane( CacheDim2, CacheDim1, TempCoord, Current->DataPtr );
				break;
			case Vector:
				LoadVector( CacheDim1, Index, Current->DataPtr );
			}
			this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, Current ) );
			Prev->Next = 0;
			this->CacheEnd = Prev;
			this->CacheStart->Prev = Current;
			this->CacheStart = Current;
			if( Page ) *Page = this->CacheStart;
			return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
		}
		else
		{
			OpenPages++;
			Current = new DataPage;
			Current->Index = Index;
			if( !Const ) Current->Dirty = true;
			else Current->Dirty = false;
			Current->Next = this->CacheStart;
			Current->Prev = 0;
			switch( this->CacheMode )
			{
			case Linear:
				Current->DataPtr = new float[ this->PageSize * this->Components ];
				LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, Current->DataPtr );
				break;
			case Submatrix:
				Current->DataPtr = new float[ this->SubmatrixSize * Components ];
				LoadSubmatrix( Index, Current->DataPtr );
				break;
			case Plane:
				CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
				LoadPlane( CacheDim2, CacheDim1, TempCoord, Current->DataPtr );
				break;
			case Vector:
				Current->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
				LoadVector( CacheDim1, Index, Current->DataPtr );
			}
			this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, Current ) );
			this->CacheStart->Prev = Current;
			this->CacheStart = Current;
			if( Page ) *Page = this->CacheStart;
			return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
		}
	}
	else
	{
		OpenPages++;
		this->CacheStart = new DataPage;
		this->CacheStart->Index = Index;
		if( !Const ) this->CacheStart->Dirty = true;
		else this->CacheStart->Dirty = false;
		this->CacheStart->Next = 0;
		this->CacheStart->Prev = 0;
		switch( this->CacheMode )
		{
		case Linear:
			this->CacheStart->DataPtr = new float[ this->PageSize * this->Components ];
			LoadData( Index * this->PageSize, Index * this->PageSize + this->PageSize <= this->FullSize ? this->PageSize : this->FullSize - Index * this->PageSize, this->CacheStart->DataPtr );
			break;
		case Submatrix:
			this->CacheStart->DataPtr = new float[ this->SubmatrixSize * Components ];
			LoadSubmatrix( Index, this->CacheStart->DataPtr );
			break;
		case Plane:
			CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->DimensionSizes[ this->CacheDim2 ] * Components ];
			LoadPlane( CacheDim2, CacheDim1, TempCoord, this->CacheStart->DataPtr );
			break;
		case Vector:
			this->CacheStart->DataPtr = new float[ this->DimensionSizes[ this->CacheDim1 ] * this->Components ];
			LoadVector( CacheDim1, Index, this->CacheStart->DataPtr );
		}
		this->CacheMap.insert( std::pair< const size_t, DataPage * >( Index, this->CacheStart ) );
		if( Page ) *Page = this->CacheStart;
		this->CacheEnd = this->CacheStart;
		return this->CacheStart->DataPtr + PageOffset * ( size_t ) this->Components;
	}
}


void NMRData::TranslateOffsetToSubmatrixPos( size_t Offset, size_t &SubmatrixNumber, size_t &SubmatrixOffset ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw Exceptions::DataNotOpen();
	if( Offset >= this->FullSize ) throw Exceptions::BadOffset();
	if( !SubmatrixSize || !SubmatrixSizes ) throw Exceptions::SubmatrixParamsNotSet();
#endif

	if( !SubmatrixPos ) SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !SubmatrixDimCount ) SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !PointPos ) PointPos = new int[ this->NumOfDimensions ];
	if( !TempCoord ) TempCoord = new int[ this->NumOfDimensions ];
	if( !TempDimIncrSizes )
	{
		TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}
	if( !TempSubmatrixExtIncrSizes )
	{
		TempSubmatrixExtIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			TempSubmatrixExtIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				TempSubmatrixExtIncrSizes[ i ] *= SubmatrixDimCount[ j ];
		}
	}
	if( !TempSubmatrixIntIncrSizes )
	{
		TempSubmatrixIntIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			TempSubmatrixIntIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				TempSubmatrixIntIncrSizes[ i ] *= SubmatrixSizes[ j ];
		}
	}

	size_t remainder = Offset;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		TempCoord[ x ] = ( int ) ( remainder / TempDimIncrSizes[ x ] );
		remainder = remainder % TempDimIncrSizes[ x ];
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		SubmatrixPos[ x ] = TempCoord[ x ] / this->SubmatrixSizes[ x ];
		PointPos[ x ] = TempCoord[ x ] % this->SubmatrixSizes[ x ];
	}

	SubmatrixNumber = 0, SubmatrixOffset = 0;
	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		SubmatrixNumber += SubmatrixPos[ x ] * TempSubmatrixExtIncrSizes[ x ];
		SubmatrixOffset += PointPos[ x ] * TempSubmatrixIntIncrSizes[ x ];
	}
}


std::vector< DimPosition< NMRData > > NMRData::TranslateOffsetToCoord( size_t Offset ) const
{
	if( !TempDimIncrSizes )
	{
		TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}

	std::vector< DimPosition< NMRData > > out;
	size_t remainder = Offset;
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		out.push_back( DimPosition< NMRData >( this, i, ( int ) ( remainder / this->TempDimIncrSizes[ i ] ), 
			( float ) ( remainder / this->TempDimIncrSizes[ i ] ), pts ) );
		remainder %= this->TempDimIncrSizes[ i ];
	}

	return out;
}


size_t NMRData::TranslateCoordToOffset( std::vector< DimPosition< NMRData > > Position ) const
{
	if( !TempDimIncrSizes )
	{
		TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}

	size_t offset = 0;
	for( int i = 0; i < this->NumOfDimensions; i++ )
		offset += Position[ i ].pos * this->TempDimIncrSizes[ i ];

	return offset;
}

	
void NMRData::LoadData( size_t StartOffset, size_t Size, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->Filename && !this->File ) throw NoFileOpen();
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Dest ) throw NullPointerArg();
#endif

	if( this->File )
	{
		if( fseek( this->File, ( off_t ) ( ( off_t ) StartOffset * this->Components * sizeof( float ) ), SEEK_SET ) ) throw CantReadFile();
		if( fread( Dest, sizeof( float ), Size * this->Components, this->File ) != Size * this->Components ) throw CantReadFile();
	}
	else if( this->MemTempFile ) 
	{
		if( fseek( this->MemTempFile, ( off_t ) ( ( off_t ) StartOffset * this->Components * sizeof( float ) ), SEEK_SET ) ) throw CantReadFile();
		if( fread( Dest, sizeof( float ), Size * this->Components, this->MemTempFile ) != Size * this->Components ) throw CantReadFile();
	}
	else memcpy( Dest, this->MemOnlyData + StartOffset * this->Components, sizeof( float ) * Size * this->Components );
}


void NMRData::WriteData( size_t StartOffset, size_t Size, float *Source )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->Filename && !this->File ) throw NoFileOpen();
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Source ) throw NullPointerArg();
#endif

	if( this->File )
	{
		if( fseek( this->File, ( off_t ) ( ( off_t ) StartOffset * this->Components * sizeof( float ) ), SEEK_SET ) ) throw CantWriteFile();
		if( fwrite( Source, sizeof( float ), Size * this->Components, this->File ) != Size * this->Components ) throw CantWriteFile();
	}
	else if( this->MemTempFile )
	{
		if( fseek( this->MemTempFile, ( off_t ) ( ( off_t ) StartOffset * this->Components * sizeof( float ) ), SEEK_SET ) ) throw CantWriteFile();
		if( fwrite( Source, sizeof( float ), Size * this->Components, this->MemTempFile ) != Size * this->Components ) throw CantWriteFile();
	}
	else memcpy( this->MemOnlyData + StartOffset * this->Components, Source, sizeof( float ) * Size * this->Components );
}


void NMRData::WriteData( size_t StartOffset, size_t Size, float *Source ) const
{
	throw WriteCallOnConstData();
}


void NMRData::LoadVector( int Dim, size_t Index, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( Dim < 0 || Dim >= this->NumOfDimensions ) throw BadDimNumber();
	if( !Dest ) throw NullPointerArg();
#endif

	if( Dim < this->NumOfDimensions - 1 )
	{
		stack_array_ptr< size_t, ArrayDeallocatorOptionalDelete< size_t > > DimIncrSizes, VectorIncrSizes;
		ArrayDeallocatorOptionalDelete< size_t > retain( false );

		if( this->TempDimIncrSizes ) DimIncrSizes.set( this->TempDimIncrSizes, retain );
		else
		{
			DimIncrSizes = new size_t[ this->NumOfDimensions ];
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				DimIncrSizes[ i ] = 1;
				for( int j = i + 1; j < this->NumOfDimensions; j++ )
					DimIncrSizes[ i ] *= this->DimensionSizes[ j ];
			}
		}

		if( this->TempVectorIncrSizes && Dim == this->CacheDim1 ) VectorIncrSizes.set( this->TempVectorIncrSizes, retain );
		else
		{
			VectorIncrSizes = new size_t[ this->NumOfDimensions ];
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == CacheDim1 ) VectorIncrSizes[ i ] = 0;
				else
				{
					for( int j = i + 1; j < this->NumOfDimensions; j++ )
					{
						if( j == CacheDim1 ) continue;
						else VectorIncrSizes[ i ] *= this->DimensionSizes[ j ];
					}
				}
			}
		}

		if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];

		size_t Remainder = Index;
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			if( i == Dim ) continue;
			size_t x = Remainder;
			Remainder = x % VectorIncrSizes[ i ];
			this->TempCoord[ i ] = ( int ) ( x / VectorIncrSizes[ i ] );
		}

		size_t StartOffset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			if( i == Dim ) continue;
			StartOffset += this->TempCoord[ i ] * DimIncrSizes[ i ];
		}

		size_t Stride = 1;
		for( int i = Dim + 1; i < this->NumOfDimensions; i++ )
			Stride *= this->DimensionSizes[ i ];

		for( int i = 0; i < this->DimensionSizes[ Dim ]; i++ )
			this->LoadData( StartOffset + i * Stride, 1, Dest + i * this->Components );
	}
	else
	{
		size_t StartOffset = Index * this->DimensionSizes[ Dim ];
		this->LoadData( StartOffset, this->DimensionSizes[ Dim ], Dest );
	}
}


void NMRData::WriteVector( int Dim, size_t Index, float *Src )
{
#ifdef BEC_NMRDATA_DEBUG
	if( Dim < 0 || Dim >= this->NumOfDimensions ) throw BadDimNumber();
	if( !Src ) throw NullPointerArg();
#endif

	if( Dim < this->NumOfDimensions - 1 )
	{
		stack_array_ptr< size_t, ArrayDeallocatorOptionalDelete< size_t > > DimIncrSizes, VectorIncrSizes;
		ArrayDeallocatorOptionalDelete< size_t > retain( false );

		if( this->TempDimIncrSizes ) DimIncrSizes.set( this->TempDimIncrSizes, retain );
		else
		{
			DimIncrSizes = new size_t[ this->NumOfDimensions ];
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				DimIncrSizes[ i ] = 1;
				for( int j = i + 1; j < this->NumOfDimensions; j++ )
					DimIncrSizes[ i ] *= this->DimensionSizes[ j ];
			}
		}

		if( this->TempVectorIncrSizes && Dim == this->CacheDim1 ) VectorIncrSizes.set( this->TempVectorIncrSizes, retain );
		else
		{
			VectorIncrSizes = new size_t[ this->NumOfDimensions ];
			for( int i = 0; i < this->NumOfDimensions; i++ )
			{
				if( i == CacheDim1 ) VectorIncrSizes[ i ] = 0;
				else
				{
					for( int j = i + 1; j < this->NumOfDimensions; j++ )
					{
						if( j == CacheDim1 ) continue;
						else VectorIncrSizes[ i ] *= this->DimensionSizes[ j ];
					}
				}
			}
		}

		if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];

		size_t Remainder = Index;
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			if( i == Dim ) continue;
			size_t x = Remainder;
			Remainder = x % VectorIncrSizes[ i ];
			this->TempCoord[ i ] = ( int ) ( x / VectorIncrSizes[ i ] );
		}

		size_t StartOffset = 0;
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			if( i == Dim ) continue;
			StartOffset += this->TempCoord[ i ] * DimIncrSizes[ i ];
		}

		size_t Stride = 1;
		for( int i = Dim + 1; i < this->NumOfDimensions; i++ )
			Stride *= this->DimensionSizes[ i ];

		for( int i = 0; i < this->DimensionSizes[ Dim ]; i++ )
			this->WriteData( StartOffset + i * Stride, 1, Src + i * this->Components );
	}
	else
	{
		size_t StartOffset = Index * this->DimensionSizes[ Dim ];
		this->WriteData( StartOffset, this->DimensionSizes[ Dim ], Src );
	}
}


void NMRData::WriteVector( int Dim, size_t Index, float *Src ) const
{
	throw WriteCallOnConstData();
}


void NMRData::LoadPlane( int XDim, int YDim, int *Coord, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( XDim < 0 || XDim >= this->NumOfDimensions ) throw BadDimNumber();
	if( YDim < 0 || YDim >= this->NumOfDimensions ) throw BadDimNumber();
	if( !Coord || !Dest ) throw NullPointerArg();
#endif

	stack_array_ptr< size_t, ArrayDeallocatorOptionalDelete< size_t > > DimIncrSizes;
	ArrayDeallocatorOptionalDelete< size_t > retain( false );

	if( this->TempDimIncrSizes ) DimIncrSizes.set( this->TempDimIncrSizes, retain );
	else
	{
		DimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			DimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				DimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}

	size_t PlaneSize = this->DimensionSizes[ XDim ] * this->DimensionSizes[ YDim ];

	for( size_t i = 0, XPos = 0, YPos = 0; i < PlaneSize; i++ )
	{
		size_t Offset = 0;
		for( int j = 0; j < this->NumOfDimensions; j++ )
		{
			if( j == XDim ) Offset += XPos * DimIncrSizes[ XDim ];
			else if( j == YDim ) Offset += YPos * DimIncrSizes[ YDim ];
			else Offset += ( size_t ) Coord[ j ] * DimIncrSizes[ j ];
		}

		this->LoadData( Offset, 1, Dest + i );

		if( ++XPos == ( size_t ) this->DimensionSizes[ XDim ] )
		{
			XPos = 0;
			YPos++;
		}
	}
}


void NMRData::WritePlane( int XDim, int YDim, int *Coord, float *Src )
{
#ifdef BEC_NMRDATA_DEBUG
	if( XDim < 0 || XDim >= this->NumOfDimensions ) throw BadDimNumber();
	if( YDim < 0 || YDim >= this->NumOfDimensions ) throw BadDimNumber();
	if( !Coord || !Src) throw NullPointerArg();
#endif

	stack_array_ptr< size_t, ArrayDeallocatorOptionalDelete< size_t > > DimIncrSizes;
	ArrayDeallocatorOptionalDelete< size_t > retain( false );

	if( this->TempDimIncrSizes ) DimIncrSizes.set( this->TempDimIncrSizes, retain );
	else
	{
		DimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			DimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				DimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}

	size_t PlaneSize = this->DimensionSizes[ XDim ] * this->DimensionSizes[ YDim ];

	for( size_t i = 0, XPos = 0, YPos = 0; i < PlaneSize; i++ )
	{
		size_t Offset = 0;
		for( int j = 0; j < this->NumOfDimensions; j++ )
		{
			if( j == XDim ) Offset += XPos * DimIncrSizes[ XDim ];
			else if( j == YDim ) Offset += YPos * DimIncrSizes[ YDim ];
			else Offset += ( size_t ) Coord[ j ] * DimIncrSizes[ j ];
		}

		this->WriteData( Offset, 1, Src + i );

		if( ++XPos == ( size_t ) this->DimensionSizes[ XDim ] )
		{
			XPos = 0;
			YPos++;
		}
	}
}


void NMRData::WritePlane( int XDim, int YDim, int *Coord, float *Src ) const
{
	throw WriteCallOnConstData();
}


void NMRData::LoadSubmatrix( size_t SubmatrixNumber, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( SubmatrixNumber >= this->SubmatrixCount ) throw BadSubmatrixNumber();
	if( !Dest ) throw NullPointerArg();
	if( !this->SubmatrixSize || !this->SubmatrixSizes ) throw SubmatrixParamsNotSet();
#endif

	if( !this->SubmatrixPos ) this->SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !this->SubmatrixDimCount ) this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !this->PointPos ) this->PointPos = new int[ this->NumOfDimensions ];
	if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}

	size_t remainder = SubmatrixNumber;
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		this->SubmatrixPos[ i ] = ( int ) remainder / this->SubmatrixDimCount[ i ];
		this->TempCoord[ i ] = this->SubmatrixPos[ i ] * this->SubmatrixSizes[ i ];
		remainder = remainder % this->SubmatrixDimCount[ i ];
	}

	memset( this->PointPos, 0, sizeof( int ) * this->NumOfDimensions );
	for( int i = 0; i < SubmatrixSize; i++ )
	{
		size_t Offset = 0;
		for( int j = 0; j < this->NumOfDimensions; j++ )
			Offset += ( this->TempCoord[ j ] + this->PointPos[ j ] ) * this->TempDimIncrSizes[ j ];

		this->LoadData( Offset, 1, Dest + i );

		for( int j = this->NumOfDimensions - 1; j >= 0; j-- )
		{
			if( ++( this->PointPos[ j ] ) == this->SubmatrixSizes[ j ] ) this->PointPos[ j ] = 0;
			else break;
		}
	}
}


void NMRData::WriteSubmatrix( size_t SubmatrixNumber, float *Src )
{
#ifdef BEC_NMRDATA_DEBUG
	if( SubmatrixNumber >= this->SubmatrixCount ) throw BadSubmatrixNumber();
	if( !Src ) throw NullPointerArg();
	if( this->Filename && !this->File ) throw NoFileOpen();
	if( !this->SubmatrixSize || !this->SubmatrixSizes ) throw SubmatrixParamsNotSet();
#endif

	if( !this->SubmatrixPos ) this->SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !this->SubmatrixDimCount ) this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !this->PointPos ) this->PointPos = new int[ this->NumOfDimensions ];
	if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}

	size_t remainder = SubmatrixNumber;
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		this->SubmatrixPos[ i ] = ( int ) remainder / this->SubmatrixDimCount[ i ];
		this->TempCoord[ i ] = this->SubmatrixPos[ i ] * this->SubmatrixSizes[ i ];
		remainder = remainder % this->SubmatrixDimCount[ i ];
	}

	memset( this->PointPos, 0, sizeof( int ) * this->NumOfDimensions );
	for( int i = 0; i < SubmatrixSize; i++ )
	{
		size_t Offset = 0;
		for( int j = 0; j < this->NumOfDimensions; j++ )
			Offset += ( this->TempCoord[ j ] + this->PointPos[ j ] ) * this->TempDimIncrSizes[ j ];

		this->WriteData( Offset, 1, Src + i );

		for( int j = this->NumOfDimensions - 1; j >= 0; j-- )
		{
			if( ++( this->PointPos[ j ] ) == this->SubmatrixSizes[ j ] ) this->PointPos[ j ] = 0;
			else break;
		}
	}
}


void NMRData::WriteSubmatrix( size_t SubmatrixNumber, float *Src ) const
{
	throw WriteCallOnConstData();
}


void NMRData::SetSubmatrixSizes( void ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
#endif

	this->SubmatrixSizes = this->CalculateSubmatrixSizes( this->NumOfDimensions, this->DimensionSizes );
	if( !this->SubmatrixSizes ) throw CouldntCalculateSubmatrixSizes();

	this->SubmatrixCount = 1;
	this->SubmatrixSize = 1;
	this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		this->SubmatrixSize *= this->SubmatrixSizes[ i ];
		this->SubmatrixDimCount[ i ] = ( int ) ceil( ( ( float ) this->DimensionSizes[ i ] ) / ( ( float ) this->SubmatrixSizes[ i ] ) );
		this->SubmatrixCount *= this->SubmatrixDimCount[ i ];
	}
}


int *NMRData::CalculateSubmatrixSizes( int NumOfDims, int *DimSizes ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !NumOfDims ) throw BadArgument();
	if( !DimSizes ) throw NullPointerArg();
#endif

	int *Out = new int[ NumOfDims ];
	for( int x = 0; x < NumOfDims; x++ )
	{
		for( int y = 16; y > 1; y /= 2 )
		{
			if( DimSizes[ x ] % ( y / 2 ) == 0 )
			{
				Out[ x ] = DimSizes[ x ] / ( y / 2 );
				break;
			}
		}
	}
	return Out;
}


bool NMRData::CacheDefinesSubmatrices( void ) const
{
	return true;
}


NMRData::TraverserCacheOrder *NMRData::GetTraverserCacheOrder( void ) const
{
	return new TraverserCacheOrder( this );
}


NMRData::TraverserOffsetOrder *NMRData::GetTraverserOffsetOrder( void ) const
{
	return new TraverserOffsetOrder( this );
}


NMRData_nmrPipe::NMRData_nmrPipe( void ) : NMRData() { this->ClearPrivateParams(); };
NMRData_nmrPipe::NMRData_nmrPipe( int NumOfDims ) : NMRData( NumOfDims ) { this->ClearPrivateParams(); };
NMRData_nmrPipe::NMRData_nmrPipe( const char *Filename, bool ReadOnly ) : NMRData() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename, ReadOnly );
};
NMRData_nmrPipe::NMRData_nmrPipe( std::string Filename, bool ReadOnly ) : NMRData() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename.c_str(), ReadOnly );
};


NMRData_nmrPipe::~NMRData_nmrPipe( void )
{
	try
	{
		if( this->GotData ) this->Close();

		if( this->FilePattern ) delete [] this->FilePattern;
		if( this->FileList )
		{
			for( int x = 0; x < this->FileCount; x++ )
				if( this->FileList[ x ] ) delete [] this->FileList[ x ];
			delete [] this->FileList;
		}
		if( this->StdFDATA ) delete [] this->StdFDATA;

		this->ClearParameters();
	}
	catch(...)
	{
		if( !std::uncaught_exception() ) throw;
	}
}


void NMRData_nmrPipe::OpenFile( const char *Filename, bool ReadOnly )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif

	if( ReadOnly ) this->ReadOnly = true;

	this->FileList = new char *[ 1 ];
	if( strchr( Filename, '%' ) )
	{
		if( strchr( Filename, '%' ) != strrchr( Filename, '%' ) )
		{
			stack_array_ptr< char > temp( new char[ strlen( Filename ) + 25 ] );
			sprintf( temp, Filename, 1, 1 );
			this->FileList[ 0 ] = new char[ strlen( temp ) + 1 ];
			strcpy( this->FileList[ 0 ], temp );
		}
		else
		{
			stack_array_ptr< char > temp( new char[ strlen( Filename ) + 15 ] );
			sprintf( temp, Filename, 1 );
			this->FileList[ 0 ] = new char[ strlen( temp ) + 1 ];
			strcpy( this->FileList[ 0 ], temp );
		}
	}
	else
	{
		this->FileList[ 0 ] = new char[ strlen( Filename ) + 1 ];
		strcpy( this->FileList[ 0 ], Filename );
		this->FileCount = 1;
	}

	FILE *TempFile = 0;
	if( !( TempFile = fopen( this->FileList[ 0 ], "rb" ) ) ) throw CantOpenFile( this->FileList[ 0 ] );
	fclose( TempFile );

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );

	this->StdFDATA = this->LoadFDATAFromFile( this->FileList[ 0 ] );
	this->LoadParamsFromFDATA( this->StdFDATA );

	if( this->NumOfDimensions < 3 ) this->FileCount = 1;
	else if( this->NumOfDimensions == 3 )
	{
		this->FileCount = this->ZDimSize = this->DimensionSizes[ 0 ];
		if( this->IsComplexByDim[ 0 ] ) this->FileCount *= 2, this->ZDimSize *= 2;
	}
	else if( this->NumOfDimensions == 4 )
	{
		this->FileCount = this->DimensionSizes[ 0 ] * this->DimensionSizes[ 1 ];
		ZDimSize = this->DimensionSizes[ 1 ];
		ADimSize = this->DimensionSizes[ 0 ];
		if( this->IsComplexByDim[ 0 ] ) this->FileCount *= 2, this->ADimSize *= 2;
		if( this->IsComplexByDim[ 1 ] ) this->FileCount *= 2, this->ZDimSize *= 2;
	}

	if( this->NumOfDimensions > 2 && 
		this->MakePipeFileList( Filename, this->NumOfDimensions, this->ZDimSize, this->ADimSize, &( this->FileList ) ) != this->FileCount )
		throw PipeFileListError();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
}


void NMRData_nmrPipe::CreateFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif
	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
#endif
	if( this->NumOfDimensions > 4 ) throw TooManyDims();

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );

	if( this->NumOfDimensions > 1 )
		this->FileSize = this->DimensionSizes[ this->NumOfDimensions - 1 ] * this->DimensionSizes[ this->NumOfDimensions - 2 ];
	else this->FileSize = this->DimensionSizes[ 0 ];

	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	int ZDimSize = 0, ADimSize = 0;

	int ComponentsPerFile = this->Components;

	if( this->NumOfDimensions < 3 ) this->FileCount = 1;
	else if( this->NumOfDimensions == 3 )
	{
		this->FileCount = ZDimSize = this->DimensionSizes[ 0 ];
		if( this->IsComplexByDim[ 0 ] ) this->FileCount *= 2, ZDimSize *= 2, ComponentsPerFile /= 2;
	}
	else if( this->NumOfDimensions == 4 )
	{
		this->FileCount = this->DimensionSizes[ 0 ] * this->DimensionSizes[ 1 ];
		ZDimSize = this->DimensionSizes[ 1 ];
		ADimSize = this->DimensionSizes[ 0 ];
		if( this->IsComplexByDim[ 0 ] ) this->FileCount *= 2, ADimSize *= 2, ComponentsPerFile /= 2;
		if( this->IsComplexByDim[ 1 ] ) this->FileCount *= 2, ZDimSize *= 2, ComponentsPerFile /= 2;
	}

	if( this->NumOfDimensions > 2 && 
		this->MakePipeFileList( Filename, this->NumOfDimensions, ZDimSize, ADimSize, &( this->FileList ) ) != this->FileCount )
		throw PipeFileListError();
	else if( this->NumOfDimensions < 3 )
	{
		this->FileList = new char *[ 1 ];
		this->FileList[ 0 ] = new char[ strlen( Filename ) + 1 ];
		strcpy( this->FileList[ 0 ], Filename );
	}

	this->StdFDATA = this->BuildFDATAFromParams();

	stack_array_ptr< float > block( new float[ this->FileSize * ComponentsPerFile ] );
	memset( block, 0, this->FileSize * ComponentsPerFile * sizeof( float ) );

	for( int x = 0; x < this->FileCount; x++ )
	{
		FILE *Out;

		if( !( Out = fopen( this->FileList[ x ], "wb" ) ) ) throw CantOpenFile( this->FileList[ x ] );
		fclose( Out );
		WriteFDATAToFile( this->StdFDATA, this->FileList[ x ] );

		if( !( Out = fopen( this->FileList[ x ], "r+b" ) ) ) throw CantOpenFile( this->FileList[ x ] );
		try
		{
			if( fseek( Out, 512 * sizeof( float ), SEEK_SET ) ) throw CantWriteFile();
			if( fwrite( block, sizeof( float ), this->FileSize * ComponentsPerFile, Out ) != this->FileSize * ComponentsPerFile ) throw CantWriteFile();
		}
		catch(...)
		{
			fclose( Out );
			throw;
		}
		fclose( Out );
	}

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
}


void NMRData_nmrPipe::LoadFileHeader( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
#endif

	if( this->StdFDATA ) delete [] this->StdFDATA;
	this->StdFDATA = this->LoadFDATAFromFile( this->FileList[ 0 ] );
	this->LoadParamsFromFDATA( this->StdFDATA );
}


void NMRData_nmrPipe::WriteFileHeader( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->GotData ) throw DataNotOpen();
#endif

	if( this->ReadOnly ) return;

	float *NewFDATA = this->BuildFDATAFromParams();
	if( this->StdFDATA ) delete [] this->StdFDATA;
	this->StdFDATA = NewFDATA;

	for( int x = 0; x < this->FileCount; x++ )
		this->WriteFDATAToFile( this->StdFDATA, this->FileList[ x ] );
}


void NMRData_nmrPipe::Close()
{
	if( !this->GotData ) return;

	this->StdClose();

	if( this->FilePattern ) delete [] this->FilePattern;
	if( this->FileList )
	{
		for( int x = 0; x < this->FileCount; x++ )
			if( this->FileList[ x ] ) delete [] this->FileList[ x ];
		delete [] this->FileList;
	}
	if( this->StdFDATA ) delete [] this->StdFDATA;

	this->FileCount = 0;
	this->FilePattern = 0;
	this->FileList = 0;
	this->FileSize = 0;
	this->StdFDATA = 0;
	this->ADimSize = 0;
	this->ZDimSize = 0;
	this->SwapFlag = false;

	this->GotData = false;
}


int NMRData_nmrPipe::GetFileTypeCode( void ) const
{
	return this->FileType_nmrPipe;
}


const char *NMRData_nmrPipe::GetFileTypeString( void ) const
{
	return "nmrPipe";
}


void NMRData_nmrPipe::ClearPrivateParams( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif

	this->FileCount = 0;
	this->FilePattern = 0;
	this->FileList = 0;
	this->FileSize = 0;
	this->StdFDATA = 0;
	this->ADimSize = 0;
	this->ZDimSize = 0;
	this->SwapFlag = false;
}


void NMRData_nmrPipe::LoadData( size_t StartOffset, size_t Size, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Dest ) throw NullPointerArg();
	if( !this->FileList || !this->FileCount ) throw NoFileOpen();
#endif

	stack_array_ptr< FILE * > Files( new FILE *[ this->Components ] );
	for( int x = 0; x < this->Components; x++ )
		Files[ x ] = 0;
	stack_array_ptr< int > FileNum( new int[ this->Components ] );
	stack_array_ptr< int > FilePos( new int[ this->Components ] );
	int PtsLeftOnVector = 0, VectorsLeftOnPlane = 0;

	try
	{
		for( size_t i = 0; i < Size; i++ )
		{
			if( !( PtsLeftOnVector-- ) )
			{
				if( !( VectorsLeftOnPlane-- ) )
				{
					for( int j = 0; j < this->Components; j++ )
						if( Files[ j ] ) fclose( Files[ j ] );
					
					if( this->NumOfDimensions == 1 )
					{
						FileNum[ 0 ] = 0;
						if( this->IsComplexByDim[ 0 ] )
							FileNum[ 1 ] = 0;
					}
					else if( this->NumOfDimensions == 2 )
					{
						FileNum[ 0 ] = 0;
						if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] )
							FileNum[ 1 ] = 0;
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] )
							FileNum[ 1 ] = 0;
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] )
							FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = 0;
					}
					else if( this->NumOfDimensions == 3 )
					{
						if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
					}
					else if( this->NumOfDimensions == 4 )
					{
						if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 1 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 2 ] = FileNum[ 2 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 2 ] = FileNum[ 2 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 1 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 2 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 3 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 4 ] = FileNum[ 5 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 4 ] = FileNum[ 5 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 8 ] = FileNum[ 9 ] = FileNum[ 10 ] = FileNum[ 11 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 12 ] = FileNum[ 13 ] = FileNum[ 14 ] = FileNum[ 15 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
					}

					for( int j = 0; j < this->Components; j++ )
					{
						Files[ j ] = 0;
						for( int k = 0; k < j; k++ )
						{
							if( FileNum[ k ] == FileNum[ j ] )
							{
								Files[ j ] = Files[ k ];
								break;
							}
						}
						if( !Files[ j ] )
							if( !( Files[ j ] = fopen( this->FileList[ FileNum[ j ] ], "rb" ) ) ) throw CantOpenFile( this->FileList[ FileNum[ j ] ] );
					}
				}

				if( this->NumOfDimensions == 1 )
				{
					FilePos[ 0 ] = 512 + ( int ) StartOffset;
					if( this->IsComplexByDim[ 0 ] )
						FilePos[ 1 ] = 512 + this->FileSize + ( int ) StartOffset;
					PtsLeftOnVector = this->FileSize - ( int ) StartOffset - ( int ) i - 1;
				}
				else if( this->NumOfDimensions == 2 )
				{
					if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] )
					{
						FilePos[ 0 ] = 512 + ( int ) StartOffset + ( int ) i;
						PtsLeftOnVector = this->FileSize - ( int ) StartOffset - ( int ) i - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] )
					{
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = ( int ) ( ( StartOffset + i ) % XDimSize ), YPos = ( int ) ( ( StartOffset + i ) / XDimSize );
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] )
					{
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = ( int ) ( ( StartOffset + i ) % XDimSize ), YPos = ( int ) ( ( StartOffset + i ) / XDimSize );
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else
					{
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = ( int ) ( ( StartOffset + i ) % XDimSize ), YPos = ( int ) ( ( StartOffset + i ) / XDimSize );
						FilePos[ 0 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
				}
				else if( this->NumOfDimensions == 3 )
				{
					if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
				}
				else if( this->NumOfDimensions == 4 )
				{
					if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = FilePos[ 2 ] = FilePos[ 3 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = FilePos[ 4 ] = FilePos[ 6 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = FilePos[ 5 ] = FilePos[ 7 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = FilePos[ 4 ] = FilePos[ 6 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = FilePos[ 5 ] = FilePos[ 7 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = FilePos[ 8 ] = FilePos[ 12 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = FilePos[ 9 ] = FilePos[ 13 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = FilePos[ 10 ] = FilePos[ 14 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = FilePos[ 11 ] = FilePos[ 15 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
				}
			}
			for( int j = 0; j < this->Components; j++ )
			{
				if( fseek( Files[ j ], sizeof( float ) * FilePos[ j ]++, SEEK_SET ) ) throw CantReadFile();
				if( fread( Dest + i * this->Components + j, sizeof( float ), 1, Files[ j ] ) != 1 ) throw CantReadFile();
				if( SwapFlag ) Dest[ i * this->Components + j ] = this->SwapByteOrder( Dest[ i * this->Components + j ] );
			}
		}
	}
	catch(...)
	{
		for( int i = 0; i < this->Components; i++ )
			fclose( Files[ i ] );
		throw;
	}

	for( int i = 0; i < this->Components; i++ )
	{
		if( Files[ i ] ) fclose( Files[ i ] );
		for( int j = i + 1; j < this->Components; j++ )
			if( Files[ j ] == Files[ i ] ) Files[ j ] = 0;
	}
}


void NMRData_nmrPipe::WriteData( size_t StartOffset, size_t Size, float *Source )
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Source ) throw NullPointerArg();
	if( !this->FileList || !this->FileCount ) throw NoFileOpen();
#endif

	stack_array_ptr< FILE * > Files( new FILE *[ this->Components ] );
	for( int x = 0; x < this->Components; x++ )
		Files[ x ] = 0;
	stack_array_ptr< int > FileNum( new int[ this->Components ] );
	stack_array_ptr< int > FilePos( new int[ this->Components ] );
	int PtsLeftOnVector = 0, VectorsLeftOnPlane = 0;

	try
	{
		for( size_t i = 0; i < Size; i++ )
		{
			if( !( PtsLeftOnVector-- ) )
			{
				if( !( VectorsLeftOnPlane-- ) )
				{
					for( int j = 0; j < this->Components; j++ )
						if( Files[ j ] ) fclose( Files[ j ] );
					
					if( this->NumOfDimensions == 1 )
					{
						FileNum[ 0 ] = 0;
						if( this->IsComplexByDim[ 0 ] )
							FileNum[ 1 ] = 0;
					}
					else if( this->NumOfDimensions == 2 )
					{
						FileNum[ 0 ] = 0;
						if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] )
							FileNum[ 1 ] = 0;
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] )
							FileNum[ 1 ] = 0;
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] )
							FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = 0;
					}
					else if( this->NumOfDimensions == 3 )
					{
						if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
					}
					else if( this->NumOfDimensions == 4 )
					{
						if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize );
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 );
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = ( int ) ( ( StartOffset + i ) / this->FileSize * 2 + 1 );
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 1 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 2 ] = FileNum[ 2 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 2 ] = FileNum[ 2 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 2 + ZPos;
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 2 + ADimSize + ZPos;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 1 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 2 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 3 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 4 ] = FileNum[ 5 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 4 ] = FileNum[ 5 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
						else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
						{
							int BaseOffset = ( int ) ( ( StartOffset + i ) / this->FileSize );
							int APos = BaseOffset / this->ADimSize, ZPos = BaseOffset % this->ADimSize;
							FileNum[ 0 ] = FileNum[ 1 ] = FileNum[ 2 ] = FileNum[ 3 ] = APos * ADimSize * 4 + ZPos * 2;
							FileNum[ 4 ] = FileNum[ 5 ] = FileNum[ 6 ] = FileNum[ 7 ] = APos * ADimSize * 4 + ZPos * 2 + 1;
							FileNum[ 8 ] = FileNum[ 9 ] = FileNum[ 10 ] = FileNum[ 11 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2;
							FileNum[ 12 ] = FileNum[ 13 ] = FileNum[ 14 ] = FileNum[ 15 ] = APos * ADimSize * 4 + ADimSize + ZPos * 2 + 1;
						}
					}

					for( int j = 0; j < this->Components; j++ )
					{
						Files[ j ] = 0;
						for( int k = 0; k < j; k++ )
						{
							if( FileNum[ k ] == FileNum[ j ] )
							{
								Files[ j ] = Files[ k ];
								break;
							}
						}
						if( !Files[ j ] )
							if( !( Files[ j ] = fopen( this->FileList[ FileNum[ j ] ], "r+b" ) ) ) throw CantOpenFile( this->FileList[ FileNum[ j ] ] );
					}
				}

				if( this->NumOfDimensions == 1 )
				{
					FilePos[ 0 ] = 512 + ( int ) StartOffset;
					if( this->IsComplexByDim[ 0 ] )
						FilePos[ 1 ] = 512 + this->FileSize + ( int ) StartOffset;
					PtsLeftOnVector = this->FileSize - ( int ) StartOffset - ( int ) i - 1;
				}
				else if( this->NumOfDimensions == 2 )
				{
					if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] )
					{
						FilePos[ 0 ] = 512 + ( int ) StartOffset + ( int ) i;
						PtsLeftOnVector = this->FileSize - ( int ) StartOffset - ( int ) i - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] )
					{
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = ( int ) ( ( StartOffset + i ) % XDimSize ), YPos = ( int ) ( ( StartOffset + i ) / XDimSize );
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] )
					{
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = ( int ) ( ( StartOffset + i ) % XDimSize ), YPos = ( int ) ( ( StartOffset + i ) / XDimSize );
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else
					{
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = ( int ) ( ( StartOffset + i ) % XDimSize ), YPos = ( int ) ( ( StartOffset + i ) / XDimSize );
						FilePos[ 0 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
				}
				else if( this->NumOfDimensions == 3 )
				{
					if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
				}
				else if( this->NumOfDimensions == 4 )
				{
					if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( !this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && !this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						FilePos[ 0 ] = FilePos[ 1 ] = FilePos[ 2 ] = FilePos[ 3 ] = 512 + BaseOffset;
						PtsLeftOnVector = this->FileSize - BaseOffset - 1;
						VectorsLeftOnPlane = 0;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && !this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = FilePos[ 4 ] = FilePos[ 6 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = FilePos[ 5 ] = FilePos[ 7 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && !this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 2 ] = FilePos[ 4 ] = FilePos[ 6 ] = 512 + YPos * 2 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 3 ] = FilePos[ 5 ] = FilePos[ 7 ] = 512 + YPos * 2 * XDimSize + XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
					else if( this->IsComplexByDim[ 0 ] && this->IsComplexByDim[ 1 ] && this->IsComplexByDim[ 2 ] && this->IsComplexByDim[ 3 ] )
					{
						int BaseOffset = ( int ) ( ( StartOffset + i ) % this->FileSize );
						int XDimSize = this->DimensionSizes[ 1 ];
						int XPos = BaseOffset % XDimSize, YPos = BaseOffset / XDimSize;
						FilePos[ 0 ] = FilePos[ 4 ] = FilePos[ 8 ] = FilePos[ 12 ] = 512 + YPos * 4 * XDimSize + XPos;
						FilePos[ 1 ] = FilePos[ 5 ] = FilePos[ 9 ] = FilePos[ 13 ] = 512 + YPos * 4 * XDimSize + XDimSize + XPos;
						FilePos[ 2 ] = FilePos[ 6 ] = FilePos[ 10 ] = FilePos[ 14 ] = 512 + YPos * 4 * XDimSize + 2 * XDimSize + XPos;
						FilePos[ 3 ] = FilePos[ 7 ] = FilePos[ 11 ] = FilePos[ 15 ] = 512 + YPos * 4 * XDimSize + 3 * XDimSize + XPos;
						PtsLeftOnVector = XDimSize - XPos - 1;
						VectorsLeftOnPlane = this->DimensionSizes[ 0 ] - YPos - 1;
					}
				}
			}
			for( int j = 0; j < this->Components; j++ )
			{
				if( fseek( Files[ j ], sizeof( float ) * FilePos[ j ]++, SEEK_SET ) ) throw CantWriteFile();
				float outval = this->SwapFlag ? this->SwapByteOrder( Source[ i * this->Components + j ] ) : Source[ i * this->Components + j ];
				if( fwrite( &outval, sizeof( float ), 1, Files[ j ] ) != 1 ) throw CantWriteFile();
			}
		}
	}
	catch(...)
	{
		for( int i = 0; i < this->Components; i++ )
			fclose( Files[ i ] );
		throw;
	}

	for( int i = 0; i < this->Components; i++ )
	{
		if( Files[ i ] ) fclose( Files[ i ] );
		for( int j = i + 1; j < this->Components; j++ )
			if( Files[ j ] == Files[ i ] ) Files[ j ] = 0;
	}
}


float *NMRData_nmrPipe::BuildFDATAFromParams( void ) const
{
	return this->BuildFDATAFromParams( this->SwapFlag, 0, this );
}


void NMRData_nmrPipe::LoadParamsFromFDATA( float *FDATA )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !FDATA ) throw NullPointerArg();
#endif

	if( FDATA[ FDFLTORDER ] != FDORDERCONS ) SwapFlag = true;
	stack_array_ptr< float > NewFDATA;
	if( this->SwapFlag )
	{
		NewFDATA = new float[ 512 ];
		memcpy( NewFDATA, FDATA, 512 * sizeof( float ) );
		FDATA = NewFDATA;

		for( int x = 0; x < 512; x++ )
			if( !this->IsStringParam( x ) )
				FDATA[ x ] = this->SwapByteOrder( FDATA[ x ] );
	}

	if( FDATA[ FDPIPEFLAG ] ) throw PipeStreamFormatNotSupported();

	if( !this->GotData )
	{
		this->SetNumOfDims( ( int ) FDATA[ FDDIMCOUNT ] );
	}

	int F1Location = 0, F2Location = 0, F3Location = 0, F4Location = 0;
	int ZDim = this->NumOfDimensions - 3, ADim = this->NumOfDimensions - 4;

	if( FDATA[ FDDIMORDER1 ] == 2.0F ) F2Location = this->NumOfDimensions - 1;
	else if( FDATA[ FDDIMORDER1 ] == 1.0F ) F1Location = this->NumOfDimensions - 1;
	else if( FDATA[ FDDIMORDER1 ] == 3.0F ) F3Location = this->NumOfDimensions - 1;
	else F4Location = this->NumOfDimensions - 1;

	if( this->NumOfDimensions >= 2 )
	{
		if( FDATA[ FDDIMORDER2 ] == 1.0F ) F1Location = this->NumOfDimensions - 2;
		else if( FDATA[ FDDIMORDER2 ] == 2.0F ) F2Location = this->NumOfDimensions - 2;
		else if( FDATA[ FDDIMORDER2 ] == 3.0F ) F3Location = this->NumOfDimensions - 2;
		else F4Location = this->NumOfDimensions - 2;
	}

	if( this->NumOfDimensions >= 3 )
	{
		if( FDATA[ FDDIMORDER3 ] == 3.0F ) F3Location = this->NumOfDimensions - 3;
		else if( FDATA[ FDDIMORDER3 ] == 1.0F ) F1Location = this->NumOfDimensions - 3;
		else if( FDATA[ FDDIMORDER3 ] == 2.0F ) F2Location = this->NumOfDimensions - 3;
		else F4Location = this->NumOfDimensions - 3;
	}

	if( this->NumOfDimensions == 4 )
	{
		if( FDATA[ FDDIMORDER4 ] == 4.0F ) F4Location = this->NumOfDimensions - 4;
		else if( FDATA[ FDDIMORDER4 ] == 1.0F ) F1Location = this->NumOfDimensions - 4;
		else if( FDATA[ FDDIMORDER4 ] == 3.0F ) F3Location = this->NumOfDimensions - 4;
		else F2Location = this->NumOfDimensions - 4;
	}

	if( !this->GotData )
	{
		stack_array_ptr< bool > ComplexState( new bool[ this->NumOfDimensions ] );
		stack_array_ptr< bool > TDState( new bool[ this->NumOfDimensions ] );

		if( FDATA[ FDF2QUADFLAG ] == FD_QUAD || FDATA[ FDF2QUADFLAG ] == FD_SE ) ComplexState[ F2Location ] = true;
		else ComplexState[ F2Location ] = false;
		if( !FDATA[ FDF2FTFLAG ] ) TDState[ F2Location ] = true;
		else TDState[ F2Location ] = false;

		if( this->NumOfDimensions >= 2 )
		{
			if( FDATA[ FDF1QUADFLAG ] == FD_QUAD || FDATA[ FDF1QUADFLAG ] == FD_SE ) ComplexState[ F1Location ] = true;
			else ComplexState[ F1Location ] = false;
			if( !FDATA[ FDF1FTFLAG ] ) TDState[ F1Location ] = true;
			else TDState[ F1Location ] = false;
		}

		if( this->NumOfDimensions >= 3 )
		{
			if( FDATA[ FDF3QUADFLAG ] == FD_QUAD || FDATA[ FDF3QUADFLAG ] == FD_SE )
			{
				ComplexState[ F3Location ] = true;
			}
            else
			{
				ComplexState[ F3Location ] = false;
			}
			if( !FDATA[ FDF3FTFLAG ] ) TDState[ F3Location ] = true;
			else TDState[ F3Location ] = false;
		}

		if( this->NumOfDimensions == 4 )
		{
			if( FDATA[ FDF4QUADFLAG ] == FD_QUAD || FDATA[ FDF4QUADFLAG ] == FD_SE )
			{
				ComplexState[ F4Location ] = true;
			}
            else
			{
				ComplexState[ F4Location ] = false;
			}
			if( !FDATA[ FDF4FTFLAG ] ) TDState[ F4Location ] = true;
			else TDState[ F4Location ] = false;
		}
        
        this->SetDimensionSize( this->GetNumOfDims() - 1, ( int ) FDATA[ FDSIZE ] );
        if( this->NumOfDimensions >= 2 ) this->SetDimensionSize( this->GetNumOfDims() - 2, ComplexState[ F2Location ] ? FDATA[ FDSPECNUM ] / 2 : FDATA[ FDSPECNUM ] );
        if( this->NumOfDimensions >= 3 ) this->SetDimensionSize( this->GetNumOfDims() - 3, ComplexState[ F3Location ] ? FDATA[ FDF3SIZE ] / 2 : FDATA[ FDF3SIZE ] );
        if( this->NumOfDimensions == 4 ) this->SetDimensionSize( this->GetNumOfDims() - 4, ComplexState[ F4Location ] ? FDATA[ FDF4SIZE ] / 2 : FDATA[ FDF4SIZE ] );

		if( this->NumOfDimensions >= 3 ) this->ZDimSize = this->GetDimensionSize( ZDim );
		if( this->NumOfDimensions == 4 ) this->ADimSize = this->GetDimensionSize( ADim );

		this->SetComplexByDim( ComplexState );
		this->SetTDByDim( TDState );

		this->FullSize = this->Components = 1;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->FullSize *= this->DimensionSizes[ x ];
			if( ComplexState[ x ] ) this->Components *= 2;
		}

		if( this->NumOfDimensions == 1 ) this->FileSize = ( int ) this->FullSize;
		else this->FileSize = this->DimensionSizes[ this->NumOfDimensions - 1 ] * this->DimensionSizes[ this->NumOfDimensions - 2 ];
	}

	this->SetDimensionLabel( F2Location, this->GetStringPipeParam( FDATA, FDF2LABEL, 7 ) );
	this->SetDimensionCalibration( F2Location, FDATA[ FDF2SW ], FDATA[ FDF2OBS ], FDATA[ FDF2CAR ] );
	if( FDATA[ FDF2X1 ] )
	{
		float OriginalPts = FDATA[ FDF2FTSIZE ];
		float OriginalSWHz = this->DimensionScalesFreq[ F2Location ] / this->DimensionSizes[ F2Location ] * OriginalPts;
		float OriginalSWPPM = OriginalSWHz / ( this->DimensionCentersFreq[ F2Location ] * 1e6F ) * 1e6F;
		float OriginalOriginPPM = this->DimensionCentersPPM[ F2Location ] + OriginalSWPPM / 2.0F;
		float LeftPPM = OriginalOriginPPM - FDATA[ FDF2X1 ] * OriginalSWPPM / OriginalPts;
		float RightPPM = OriginalOriginPPM - FDATA[ FDF2XN ] * OriginalSWPPM / OriginalPts;
		this->DimensionScalesPPM[ F2Location ] = LeftPPM - RightPPM;
		this->DimensionCentersPPM[ F2Location ] = LeftPPM - ( this->DimensionScalesPPM[ F2Location ] / 2.0F );
		this->DimensionOriginsPPM[ F2Location ] = LeftPPM;
	}

	if( this->NumOfDimensions >= 2 )
	{
		this->SetDimensionLabel( F1Location, this->GetStringPipeParam( FDATA, FDF1LABEL, 7 ) );
		this->SetDimensionCalibration( F1Location, FDATA[ FDF1SW ], FDATA[ FDF1OBS ], FDATA[ FDF1CAR ] );
		if( FDATA[ FDF1X1 ] )
		{
			float OriginalPts = FDATA[ FDF1FTSIZE ];
			float OriginalSWHz = this->DimensionScalesFreq[ F1Location ] / this->DimensionSizes[ F1Location ] * OriginalPts;
			float OriginalSWPPM = OriginalSWHz / ( this->DimensionCentersFreq[ F1Location ] * 1e6F ) * 1e6F;
			float OriginalOriginPPM = this->DimensionCentersPPM[ F1Location ] + OriginalSWPPM / 2.0F;
			float LeftPPM = OriginalOriginPPM - FDATA[ FDF1X1 ] * OriginalSWPPM / OriginalPts;
			float RightPPM = OriginalOriginPPM - FDATA[ FDF1XN ] * OriginalSWPPM / OriginalPts;
			this->DimensionScalesPPM[ F1Location ] = LeftPPM - RightPPM;
			this->DimensionCentersPPM[ F1Location ] = LeftPPM - ( this->DimensionScalesPPM[ F1Location ] / 2.0F );
			this->DimensionOriginsPPM[ F1Location ] = LeftPPM;
		}
	}

	if( this->NumOfDimensions >= 3 )
	{
		this->SetDimensionLabel( F3Location, this->GetStringPipeParam( FDATA, FDF3LABEL, 7 ) );
		this->SetDimensionCalibration( F3Location, FDATA[ FDF3SW ], FDATA[ FDF3OBS ], FDATA[ FDF3CAR ] );
		if( FDATA[ FDF3X1 ] )
		{
			float OriginalPts = FDATA[ FDF3FTSIZE ];
			float OriginalSWHz = this->DimensionScalesFreq[ F3Location ] / this->DimensionSizes[ F3Location ] * OriginalPts;
			float OriginalSWPPM = OriginalSWHz / ( this->DimensionCentersFreq[ F3Location ] * 1e6F ) * 1e6F;
			float OriginalOriginPPM = this->DimensionCentersPPM[ F3Location ] + OriginalSWPPM / 2.0F;
			float LeftPPM = OriginalOriginPPM - FDATA[ FDF3X1 ] * OriginalSWPPM / OriginalPts;
			float RightPPM = OriginalOriginPPM - FDATA[ FDF3XN ] * OriginalSWPPM / OriginalPts;
			this->DimensionScalesPPM[ F3Location ] = LeftPPM - RightPPM;
			this->DimensionCentersPPM[ F3Location ] = LeftPPM - ( this->DimensionScalesPPM[ F3Location ] / 2.0F );
			this->DimensionOriginsPPM[ F3Location ] = LeftPPM;
		}
	}

	if( this->NumOfDimensions == 4 )
	{
		this->SetDimensionLabel( F4Location, this->GetStringPipeParam( FDATA, FDF4LABEL, 7 ) );
		this->SetDimensionCalibration( F4Location, FDATA[ FDF4SW ], FDATA[ FDF4OBS ], FDATA[ FDF4CAR ] );
		if( FDATA[ FDF4X1 ] )
		{
			float OriginalPts = FDATA[ FDF4FTSIZE ];
			float OriginalSWHz = this->DimensionScalesFreq[ F4Location ] / this->DimensionSizes[ F4Location ] * OriginalPts;
			float OriginalSWPPM = OriginalSWHz / ( this->DimensionCentersFreq[ F4Location ] * 1e6F ) * 1e6F;
			float OriginalOriginPPM = this->DimensionCentersPPM[ F4Location ] + OriginalSWPPM / 2.0F;
			float LeftPPM = OriginalOriginPPM - FDATA[ FDF4X1 ] * OriginalSWPPM / OriginalPts;
			float RightPPM = OriginalOriginPPM - FDATA[ FDF4XN ] * OriginalSWPPM / OriginalPts;
			this->DimensionScalesPPM[ F4Location ] = LeftPPM - RightPPM;
			this->DimensionCentersPPM[ F4Location ] = LeftPPM - ( this->DimensionScalesPPM[ F4Location ] / 2.0F );
			this->DimensionOriginsPPM[ F4Location ] = LeftPPM;
		}
	}
}


void NMRData_nmrPipe::WriteFDATAToFile( float *FDATA, const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !FDATA || !Filename ) throw NullPointerArg();
#endif

	FILE *Out = fopen( Filename, "r+b" );
	if( !Out ) Out = fopen( Filename, "wb" );
	if( !Out ) throw CantOpenFile( Filename );

	if( fwrite( FDATA, sizeof( float ), 512, Out ) != 512 ) 
	{
		fclose( Out );
		throw CantWriteFile();
	}
	
	fclose( Out );
}


float *NMRData_nmrPipe::LoadFDATAFromFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	stack_array_ptr< float > FDATA( new float[ 512 ] );
	memset( FDATA, 0, sizeof( float ) * 512 );

	FILE *In = fopen( Filename, "rb" );
	if( !In ) throw CantOpenFile( Filename );

	if( fread( FDATA, sizeof( float ), 512, In ) != 512 )
	{
		fclose( In );
		throw CantReadFile();
	}

	fclose( In );
	return FDATA.release();
}


float *NMRData_nmrPipe::BuildFDATAFromParams( bool SwapBytes, const NMRData *Source, const NMRData_nmrPipe *PipeSource )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Source && !PipeSource ) throw NullPointerArg();
#endif
	if( !Source ) Source = PipeSource;
	int DimCount;
	if( !( DimCount = Source->GetNumOfDims() ) ) throw DimensionalityNotSet();

	NMRData tmp;

	stack_array_ptr< float > FDATA( new float[ 512 ] );
	memset( FDATA, 0, sizeof( float ) * 512 );

	stack_array_ptr< int > DimensionSizes( Source->GetDimensionSizes() );
	stack_array_ptr< bool > ComplexState( Source->IsComplexAvailableByDim() );
	stack_array_ptr< bool > TDState( Source->IsTimeDomainAvailableByDim() );

	int F2Location = DimCount - 1, F1Location = DimCount - 2, F3Location = DimCount - 3, F4Location = DimCount - 4;

	const NMRData_nmrPipe *PipeObj = PipeSource;
	if( !PipeObj ) dynamic_cast< const NMRData_nmrPipe * >( Source );
	if( PipeObj && PipeObj->StdFDATA )
	{
		memcpy( FDATA, PipeObj->StdFDATA, sizeof( float ) * 512 );
		if( PipeObj->SwapFlag )	
			for( int x = 0; x < 512; x++ )
				if( !NMRData_nmrPipe::IsStringParam( x ) )
					FDATA[ x ] = tmp.SwapByteOrder( FDATA[ x ] );

		if( FDATA[ FDDIMORDER1 ] == 2.0F ) F2Location = DimCount - 1;
		else if( FDATA[ FDDIMORDER1 ] == 1.0F ) F1Location = DimCount - 1;
		else if( FDATA[ FDDIMORDER1 ] == 3.0F ) F3Location = DimCount - 1;
		else F4Location = DimCount - 1;

		if( DimCount >= 2 )
		{
			if( FDATA[ FDDIMORDER2 ] == 1.0F ) F1Location = DimCount - 2;
			else if( FDATA[ FDDIMORDER2 ] == 2.0F ) F2Location = DimCount - 2;
			else if( FDATA[ FDDIMORDER2 ] == 3.0F ) F3Location = DimCount - 2;
			else F4Location = DimCount - 2;
		}

		if( DimCount >= 3 )
		{
			if( FDATA[ FDDIMORDER3 ] == 3.0F ) F3Location = DimCount - 3;
			else if( FDATA[ FDDIMORDER3 ] == 1.0F ) F1Location = DimCount - 3;
			else if( FDATA[ FDDIMORDER3 ] == 2.0F ) F2Location = DimCount - 3;
			else F4Location = DimCount - 3;
		}

		if( DimCount == 4 )
		{
			if( FDATA[ FDDIMORDER4 ] == 4.0F ) F4Location = DimCount - 4;
			else if( FDATA[ FDDIMORDER4 ] == 1.0F ) F1Location = DimCount - 4;
			else if( FDATA[ FDDIMORDER4 ] == 3.0F ) F3Location = DimCount - 4;
			else F2Location = DimCount - 4;
		}
	}
	else
	{
		FDATA[ FDMAGIC ] = 0.0F;
		FDATA[ FDFLTORDER ] = ( float ) FDORDERCONS;
		FDATA[ FDDIMCOUNT ] = ( float ) DimCount;
		if( Source->IsComplexAvailable() ) FDATA[ FDQUADFLAG ]  = FD_COMPLEX;
		else FDATA[ FDQUADFLAG ] = FD_REAL;

		FDATA[ FD2DPHASE ] = FD_STATES;
		FDATA[ FDDIMORDER1 ] = 2.0F;
		FDATA[ FDDIMORDER2 ] = 1.0F;
		FDATA[ FDDIMORDER3 ] = 3.0F;
		FDATA[ FDDIMORDER4 ] = 4.0F;

		if( ComplexState[ F2Location ] ) FDATA[ FDF2QUADFLAG ] = FD_COMPLEX;
		else FDATA[ FDF2QUADFLAG ] = FD_REAL;
		FDATA[ FDF2UNITS ] = FD_PPM;
		if( TDState[ F2Location ] ) FDATA[ FDF2FTFLAG ] = 0.0F;
		else FDATA[ FDF2FTFLAG ] = 1.0F;

		if( DimCount >= 2 )
		{
			if( ComplexState[ F1Location ] ) FDATA[ FDF1QUADFLAG ] = FD_COMPLEX;
			else FDATA[ FDF1QUADFLAG ] = FD_REAL;
			FDATA[ FDF1UNITS ] = FD_PPM;
			if( TDState[ F1Location ] ) FDATA[ FDF1FTFLAG ] = 0.0F;
			else FDATA[ FDF1FTFLAG ] = 1.0F;
		}
		
		if( DimCount >= 3 )
		{
			if( ComplexState[ F3Location ] ) FDATA[ FDF3QUADFLAG ] = FD_COMPLEX;
			else FDATA[ FDF3QUADFLAG ] = FD_REAL;
			FDATA[ FDF3UNITS ] = FD_PPM;
			if( TDState[ F3Location ] ) FDATA[ FDF3FTFLAG ] = 0.0F;
			else FDATA[ FDF3FTFLAG ] = 1.0F;
		}

		if( DimCount == 4 )
		{
			if( ComplexState[ F4Location ] ) FDATA[ FDF4QUADFLAG ] = FD_COMPLEX;
			else FDATA[ FDF4QUADFLAG ] = FD_REAL;
			FDATA[ FDF4UNITS ] = FD_PPM;
			if( TDState[ F4Location ] ) FDATA[ FDF4FTFLAG ] = 0.0F;
			else FDATA[ FDF4FTFLAG ] = 1.0F;
		}

		FDATA[ FDSIZE ] = ( float ) DimensionSizes[ F2Location ];
		if( DimCount >= 2 )
		{
			if( ComplexState[ F2Location ] )
				FDATA[ FDSPECNUM ] = ( float ) 2 * DimensionSizes[ F1Location ];
			else FDATA[ FDSPECNUM ] = ( float ) DimensionSizes[ F1Location ];
		}
		if( DimCount >= 3 )
		{
			if( ComplexState[ F3Location ] ) FDATA[ FDF3SIZE ] = 2 * ( float ) DimensionSizes[ F3Location ];
			else FDATA[ FDF3SIZE ] = ( float ) DimensionSizes[ F3Location ];
		}
		if( DimCount == 4 )
		{
			if( ComplexState[ F4Location ] ) FDATA[ FDF4SIZE ] = 2 * ( float ) DimensionSizes[ F4Location ];
			else FDATA[ FDF4SIZE ] = ( float ) DimensionSizes[ F4Location ];
		}
	}

	Source->GetDimensionCalibration( F2Location, FDATA[ FDF2SW ], FDATA[ FDF2OBS ], FDATA[ FDF2CAR ] );
	FDATA[ FDF2X1 ] = 0.0F;
	FDATA[ FDF2XN ] = 0.0F;
	FDATA[ FDF2FTSIZE ] = ( float ) DimensionSizes[ F2Location ];
	FDATA[ FDF2ORIG ] = FDATA[ FDF2OBS ] * FDATA[ FDF2CAR ] - FDATA[ FDF2SW ] * 
		( DimensionSizes[ F2Location ] - DimensionSizes[ F2Location ] / 2 ) / DimensionSizes[ F2Location ];
	FDATA[ FDF2CENTER ] = ( float ) ( DimensionSizes[ F2Location ] / 2 + 1 );
	stack_array_ptr< char > label( Source->GetDimensionLabel( F2Location ) );
	if( label )
		NMRData_nmrPipe::SetStringPipeParam( FDATA, FDF2LABEL, 7, label );

	if( DimCount > 1 )
	{
		Source->GetDimensionCalibration( F1Location, FDATA[ FDF1SW ], FDATA[ FDF1OBS ], FDATA[ FDF1CAR ] );
		FDATA[ FDF1X1 ] = 0.0F;
		FDATA[ FDF1XN ] = 0.0F;
		FDATA[ FDF1FTSIZE ] = ( float ) DimensionSizes[ F1Location ];
		FDATA[ FDF1ORIG ] = FDATA[ FDF1OBS ] * FDATA[ FDF1CAR ] - FDATA[ FDF1SW ] * 
			( DimensionSizes[ F1Location ] - DimensionSizes[ F1Location ] / 2 ) / DimensionSizes[ F1Location ];
		FDATA[ FDF1CENTER ] = ( float ) ( DimensionSizes[ F1Location ] / 2 + 1 );
		label = Source->GetDimensionLabel( F1Location );
		if( label )
			NMRData_nmrPipe::SetStringPipeParam( FDATA, FDF1LABEL, 7, label );
	}

	if( DimCount > 2 )
	{
		Source->GetDimensionCalibration( F3Location, FDATA[ FDF3SW ], FDATA[ FDF3OBS ], FDATA[ FDF3CAR ] );
		FDATA[ FDF3X1 ] = 0.0F;
		FDATA[ FDF3XN ] = 0.0F;
		FDATA[ FDF3FTSIZE ] = ( float ) DimensionSizes[ F3Location ];
		FDATA[ FDF3ORIG ] = FDATA[ FDF3OBS ] * FDATA[ FDF3CAR ] - FDATA[ FDF3SW ] * 
			( DimensionSizes[ F3Location ] - DimensionSizes[ F3Location ] / 2 ) / DimensionSizes[ F3Location ];
		FDATA[ FDF3CENTER ] = ( float ) ( DimensionSizes[ F3Location ] / 2 + 1 );
		label = Source->GetDimensionLabel( F3Location );
		if( label )
			NMRData_nmrPipe::SetStringPipeParam( FDATA, FDF3LABEL, 7, label );
	}

	if( DimCount > 3 )
	{
		Source->GetDimensionCalibration( F4Location, FDATA[ FDF4SW ], FDATA[ FDF4OBS ], FDATA[ FDF4CAR ] );
		FDATA[ FDF4X1 ] = 0.0F;
		FDATA[ FDF4XN ] = 0.0F;
		FDATA[ FDF4FTSIZE ] = ( float ) DimensionSizes[ F4Location ];
		FDATA[ FDF4ORIG ] = FDATA[ FDF4OBS ] * FDATA[ FDF4CAR ] - FDATA[ FDF4SW ] * 
			( DimensionSizes[ F4Location ] - DimensionSizes[ F4Location ] / 2 ) / DimensionSizes[ F4Location ];
		FDATA[ FDF4CENTER ] = ( float ) ( DimensionSizes[ F4Location ] / 2 + 1 );
		label = Source->GetDimensionLabel( F4Location );
		if( label )
			NMRData_nmrPipe::SetStringPipeParam( FDATA, FDF4LABEL, 7, label );
	}

	if( SwapBytes )	
		for( int x = 0; x < 512; x++ )
			if( !NMRData_nmrPipe::IsStringParam( x ) )
				FDATA[ x ] = tmp.SwapByteOrder( FDATA[ x ] );

	return FDATA.release();
}


DimensionApodInfo NMRData_nmrPipe::GetDimensionApodInfo( int DimNum ) const
{
    float *FDATA = this->StdFDATA;
    
    bool swap = ( FDATA[ FDFLTORDER ] != FDORDERCONS );
    stack_array_ptr< float > NewFDATA;
	if( swap )
	{
		NewFDATA = new float[ 512 ];
		memcpy( NewFDATA, FDATA, 512 * sizeof( float ) );
		FDATA = NewFDATA;
        
		for( int x = 0; x < 512; x++ )
			if( !this->IsStringParam( x ) )
				FDATA[ x ] = this->SwapByteOrder( FDATA[ x ] );
	}
    
    float apod_code, apod_q1, apod_q2, apod_q3, apod_size, apod_c1;
    
    switch( this->NumOfDimensions - DimNum )
    {
        case 1:
            //  we want to know about our dimension n - 1, which is NMRPipe dimension X
            //  so we look at FDDIMORDER1 to find out which experimental dimension is currently in X
            if( FDATA[ FDDIMORDER1 ] == 1.0F )
            {
                //  X = experimental dimension F1; get info from F1 parameters
                apod_code = FDATA[ FDF1APODCODE ];
                apod_q1 = FDATA[ FDF1APODQ1 ];
                apod_q2 = FDATA[ FDF1APODQ2 ];
                apod_q3 = FDATA[ FDF1APODQ3 ];
                apod_size = FDATA[ FDF1APOD ];
                apod_c1 = FDATA[ FDF1C1 ];
            }
            else if( FDATA[ FDDIMORDER1 ] == 2.0F )
            {
                //  X = experimental dimension F2; get info from F2 parameters
                apod_code = FDATA[ FDF2APODCODE ];
                apod_q1 = FDATA[ FDF2APODQ1 ];
                apod_q2 = FDATA[ FDF2APODQ2 ];
                apod_q3 = FDATA[ FDF2APODQ3 ];
                apod_size = FDATA[ FDF2APOD ];
                apod_c1 = FDATA[ FDF2C1 ];
            }
            else if( FDATA[ FDDIMORDER1 ] == 3.0F )
            {
                //  X = experimental dimension F3; get info from F3 parameters
                apod_code = FDATA[ FDF3APODCODE ];
                apod_q1 = FDATA[ FDF3APODQ1 ];
                apod_q2 = FDATA[ FDF3APODQ2 ];
                apod_q3 = FDATA[ FDF3APODQ3 ];
                apod_size = FDATA[ FDF3APOD ];
                apod_c1 = FDATA[ FDF3C1 ];
            }
            else if( FDATA[ FDDIMORDER1 ] == 4.0F )
            {
                //  X = experimental dimension F4; get info from F4 parameters
                apod_code = FDATA[ FDF4APODCODE ];
                apod_q1 = FDATA[ FDF4APODQ1 ];
                apod_q2 = FDATA[ FDF4APODQ2 ];
                apod_q3 = FDATA[ FDF4APODQ3 ];
                apod_size = FDATA[ FDF4APOD ];
                apod_c1 = FDATA[ FDF4C1 ];
            }
            break;
        case 2:
            //  we want to know about our dimension n - 2, which is NMRPipe dimension Y
            //  so we look at FDDIMORDER2 to find out which experimental dimension is currently in Y
            if( FDATA[ FDDIMORDER2 ] == 1.0F )
            {
                //  Y = experimental dimension F1; get info from F1 parameters
                apod_code = FDATA[ FDF1APODCODE ];
                apod_q1 = FDATA[ FDF1APODQ1 ];
                apod_q2 = FDATA[ FDF1APODQ2 ];
                apod_q3 = FDATA[ FDF1APODQ3 ];
                apod_size = FDATA[ FDF1APOD ];
                apod_c1 = FDATA[ FDF1C1 ];
            }
            else if( FDATA[ FDDIMORDER2 ] == 2.0F )
            {
                //  Y = experimental dimension F2; get info from F2 parameters
                apod_code = FDATA[ FDF2APODCODE ];
                apod_q1 = FDATA[ FDF2APODQ1 ];
                apod_q2 = FDATA[ FDF2APODQ2 ];
                apod_q3 = FDATA[ FDF2APODQ3 ];
                apod_size = FDATA[ FDF2APOD ];
                apod_c1 = FDATA[ FDF2C1 ];
            }
            else if( FDATA[ FDDIMORDER2 ] == 3.0F )
            {
                //  Y = experimental dimension F3; get info from F3 parameters
                apod_code = FDATA[ FDF3APODCODE ];
                apod_q1 = FDATA[ FDF3APODQ1 ];
                apod_q2 = FDATA[ FDF3APODQ2 ];
                apod_q3 = FDATA[ FDF3APODQ3 ];
                apod_size = FDATA[ FDF3APOD ];
                apod_c1 = FDATA[ FDF3C1 ];
            }
            else if( FDATA[ FDDIMORDER2 ] == 4.0F )
            {
                //  Y = experimental dimension F4; get info from F4 parameters
                apod_code = FDATA[ FDF4APODCODE ];
                apod_q1 = FDATA[ FDF4APODQ1 ];
                apod_q2 = FDATA[ FDF4APODQ2 ];
                apod_q3 = FDATA[ FDF4APODQ3 ];
                apod_size = FDATA[ FDF4APOD ];
                apod_c1 = FDATA[ FDF4C1 ];
            }
            break;
        case 3:
            //  we want to know about our dimension n - 3, which is NMRPipe dimension Z
            //  so we look at FDDIMORDER3 to find out which experimental dimension is currently in Z
            if( FDATA[ FDDIMORDER3 ] == 1.0F )
            {
                //  Z = experimental dimension F1; get info from F1 parameters
                apod_code = FDATA[ FDF1APODCODE ];
                apod_q1 = FDATA[ FDF1APODQ1 ];
                apod_q2 = FDATA[ FDF1APODQ2 ];
                apod_q3 = FDATA[ FDF1APODQ3 ];
                apod_size = FDATA[ FDF1APOD ];
                apod_c1 = FDATA[ FDF1C1 ];
            }
            else if( FDATA[ FDDIMORDER3 ] == 2.0F )
            {
                //  Z = experimental dimension F2; get info from F2 parameters
                apod_code = FDATA[ FDF2APODCODE ];
                apod_q1 = FDATA[ FDF2APODQ1 ];
                apod_q2 = FDATA[ FDF2APODQ2 ];
                apod_q3 = FDATA[ FDF2APODQ3 ];
                apod_size = FDATA[ FDF2APOD ];
                apod_c1 = FDATA[ FDF2C1 ];
            }
            else if( FDATA[ FDDIMORDER3 ] == 3.0F )
            {
                //  Z = experimental dimension F3; get info from F3 parameters
                apod_code = FDATA[ FDF3APODCODE ];
                apod_q1 = FDATA[ FDF3APODQ1 ];
                apod_q2 = FDATA[ FDF3APODQ2 ];
                apod_q3 = FDATA[ FDF3APODQ3 ];
                apod_size = FDATA[ FDF3APOD ];
                apod_c1 = FDATA[ FDF3C1 ];
            }
            else if( FDATA[ FDDIMORDER3 ] == 4.0F )
            {
                //  Z = experimental dimension F4; get info from F4 parameters
                apod_code = FDATA[ FDF4APODCODE ];
                apod_q1 = FDATA[ FDF4APODQ1 ];
                apod_q2 = FDATA[ FDF4APODQ2 ];
                apod_q3 = FDATA[ FDF4APODQ3 ];
                apod_size = FDATA[ FDF4APOD ];
                apod_c1 = FDATA[ FDF4C1 ];
            }
            break;
        case 4:
            //  we want to know about our dimension n - 4, which is NMRPipe dimension A
            //  so we look at FDDIMORDER4 to find out which experimental dimension is currently in A
            if( FDATA[ FDDIMORDER4 ] == 1.0F )
            {
                //  A = experimental dimension F1; get info from F1 parameters
                apod_code = FDATA[ FDF1APODCODE ];
                apod_q1 = FDATA[ FDF1APODQ1 ];
                apod_q2 = FDATA[ FDF1APODQ2 ];
                apod_q3 = FDATA[ FDF1APODQ3 ];
                apod_size = FDATA[ FDF1APOD ];
                apod_c1 = FDATA[ FDF1C1 ];
            }
            else if( FDATA[ FDDIMORDER4 ] == 2.0F )
            {
                //  A = experimental dimension F2; get info from F2 parameters
                apod_code = FDATA[ FDF2APODCODE ];
                apod_q1 = FDATA[ FDF2APODQ1 ];
                apod_q2 = FDATA[ FDF2APODQ2 ];
                apod_q3 = FDATA[ FDF2APODQ3 ];
                apod_size = FDATA[ FDF2APOD ];
                apod_c1 = FDATA[ FDF2C1 ];
            }
            else if( FDATA[ FDDIMORDER4 ] == 3.0F )
            {
                //  A = experimental dimension F3; get info from F3 parameters
                apod_code = FDATA[ FDF3APODCODE ];
                apod_q1 = FDATA[ FDF3APODQ1 ];
                apod_q2 = FDATA[ FDF3APODQ2 ];
                apod_q3 = FDATA[ FDF3APODQ3 ];
                apod_size = FDATA[ FDF3APOD ];
                apod_c1 = FDATA[ FDF3C1 ];
            }
            else if( FDATA[ FDDIMORDER4 ] == 4.0F )
            {
                //  A = experimental dimension F4; get info from F4 parameters
                apod_code = FDATA[ FDF4APODCODE ];
                apod_q1 = FDATA[ FDF4APODQ1 ];
                apod_q2 = FDATA[ FDF4APODQ2 ];
                apod_q3 = FDATA[ FDF4APODQ3 ];
                apod_size = FDATA[ FDF4APOD ];
                apod_c1 = FDATA[ FDF4C1 ];
            }
    }
    
    DimensionApodInfo apod;
    if( apod_code == APOD_NULL )
        apod.Method = None;
    else if( apod_code == APOD_SP )
        apod.Method = SP;
    else if( apod_code == APOD_EM )
        apod.Method = EM;
    else if( apod_code == APOD_GM )
        apod.Method = GM;
    else if( apod_code == APOD_GMB )
        apod.Method = GMB;
    else if( apod_code == APOD_TM )
        apod.Method = GMB;
    else if( apod_code == APOD_TRI )
        apod.Method = TRI;
    else if( apod_code == APOD_ZE )
        apod.Method = None;
    else if( apod_code == APOD_JMOD )
        apod.Method = JMOD;
    apod.P1 = apod_q1;
    apod.P2 = apod_q2;
    apod.P3 = apod_q3;
    apod.Size = ( int ) apod_size;
    apod.FirstPointCorrection = apod_c1 + 1.0F;
    
    return apod;
}

bool NMRData_nmrPipe::IsStringParam( int Code )
{
	if( Code >= FDF1LABEL && Code < FDF1LABEL + ( SIZE_NDLABEL / sizeof( float ) ) ) return true;
	if( Code >= FDF2LABEL && Code < FDF2LABEL + ( SIZE_NDLABEL / sizeof( float ) ) ) return true;
	if( Code >= FDF3LABEL && Code < FDF3LABEL + ( SIZE_NDLABEL / sizeof( float ) ) ) return true;
	if( Code >= FDF4LABEL && Code < FDF4LABEL + ( SIZE_NDLABEL / sizeof( float ) ) ) return true;
	if( Code >= FDSRCNAME && Code < FDSRCNAME + ( SIZE_SRCNAME / sizeof( float ) ) ) return true;
	if( Code >= FDUSERNAME && Code < FDUSERNAME + ( SIZE_USERNAME / sizeof( float ) ) ) return true;
	if( Code >= FDOPERNAME && Code < FDOPERNAME + ( SIZE_OPERNAME / sizeof( float ) ) ) return true;
	if( Code >= FDTITLE && Code < FDTITLE + ( SIZE_TITLE / sizeof( float ) ) ) return true;
	if( Code >= FDCOMMENT && Code < FDCOMMENT + ( SIZE_COMMENT / sizeof( float ) ) ) return true;
			
	return false;
}


int NMRData_nmrPipe::MakePipeFileList( const char *StartingName, int NumOfDims, int ZDimSize, int ADimSize, char ***Filelist )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !StartingName || !Filelist ) throw NullPointerArg();
#endif

	stack_array_ptr< char, ArrayDeallocatorOptionalDelete< char > > format;
	ArrayDeallocatorOptionalDelete< char > retain( false );
	int FileCount = 0;

	if( NumOfDims == 3 )
	{
		int p0l = 0, p1l = 0, p2s = 0, p2l = 0;
		FileCount = ZDimSize;
		if( !strchr( StartingName, '%' ) )
		{

			for( int x = 0; x < ( int ) strlen( StartingName ); x++ )
				if( StartingName[ x ] == '\\' || StartingName[ x ] == '/' ) p0l = x + 1;

			for( int x = p0l; x < ( int ) strlen( StartingName ); x++ )
			{
				if( isdigit( StartingName[ x ] ) && !p1l )
					p1l = x - p0l;
				else if( p1l && !isdigit( StartingName[ x ] ) && !p2s )
					p2s = x;
			}
			p2l = ( int ) strlen( StartingName ) - p2s;

			if( !p2s ) return 0;

			format = new char[ p0l + p1l + p2l + 5 ];

			strncpy( format, StartingName, p0l + p1l );
			strncpy( format + p0l + p1l, "%03d", 4 );
			strncpy( format + p0l + p1l + 4, StartingName + p2s, p2l );
			format[ p0l + p1l + p2l + 4 ] = 0;
		}
		else format.set( const_cast< char * >( StartingName ), retain );

		*Filelist = new char *[ FileCount ];
		
		for( int x = 0; x < FileCount; x++ )
		{
			( *Filelist )[ x ] = new char[ strlen( format ) + 128 ];
			sprintf( ( *Filelist )[ x ], format, x + 1 );
		}
	}
	else
	{
		int p0l = 0, p1l = 0, p2s = 0, p2l = 0;
		FileCount = ZDimSize * ADimSize;
		if( !strchr( StartingName, '%' ) )
		{

			for( int x = 0; x < ( int ) strlen( StartingName ); x++ )
				if( StartingName[ x ] == '\\' || StartingName[ x ] == '/' ) p0l = x + 1;

			for( int x = p0l; x < ( int ) strlen( StartingName ); x++ )
			{
				if( isdigit( StartingName[ x ] ) && !p1l )
					p1l = x - p0l;
				else if( p1l && !isdigit( StartingName[ x ] ) && !p2s )
					p2s = x;
			}
			p2l = ( int ) strlen( StartingName ) - p2s;

			if( !p2s ) return 0;

			format = new char[ p0l + p1l + p2l + 9 ];

			strncpy( format, StartingName, p0l + p1l );
			strncpy( format + p0l + p1l, "%02d%03d", 8 );
			strncpy( format + p0l + p1l + 8, StartingName + p2s, p2l );
			format[ p0l + p1l + p2l + 8 ] = 0;
		}
		else format.set( const_cast< char * >( StartingName ), retain );

		*Filelist = new char *[ FileCount ];
		
		for( int x = 0; x < ADimSize; x++ )
		{
			for( int y = 0; y < ZDimSize; y++ )
			{
				( *Filelist )[ x * ZDimSize + y ] = new char[ strlen( format ) + 128 ];
				sprintf( ( *Filelist )[ x * ZDimSize + y ], format, x + 1, y + 1 );
			}
		}
	}

	return FileCount;
}


char *NMRData_nmrPipe::GetStringPipeParam( float *FDATA, int Offset, int MaxSize )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !FDATA ) throw NullPointerArg();
#endif

	char *in, *out;

	in = ( char * ) ( FDATA + Offset );
	out = new char[ ( size_t ) ( strlen( in ) <= ( size_t ) MaxSize ? strlen( in ) + 1 : MaxSize + 1 ) ];
	strncpy( out, in, ( strlen( in ) <= ( size_t ) MaxSize  ) ? strlen( in ) : ( size_t ) MaxSize );
	out[ ( size_t ) ( strlen( in ) <= ( size_t ) MaxSize ? strlen( in ) : MaxSize ) ] = 0;
	return out;
}


void NMRData_nmrPipe::SetStringPipeParam( float *FDATA, int Offset, int MaxSize, char *In )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !FDATA || !In ) throw NullPointerArg();
#endif

	char *out = ( char * ) ( FDATA + Offset );
	strncpy( out, In, MaxSize );
}


NMRData_Text::NMRData_Text( void ) : NMRData() { this->ClearPrivateParams(); };
NMRData_Text::NMRData_Text( int NumOfDims ) : NMRData( NumOfDims ) { this->ClearPrivateParams(); };
NMRData_Text::NMRData_Text( const char *Filename, bool ReadOnly ) : NMRData() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename, ReadOnly );
};
NMRData_Text::NMRData_Text( std::string Filename, bool ReadOnly ) : NMRData() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename.c_str(), ReadOnly );
};


NMRData_Text::~NMRData_Text( void )
{
	try
	{
		if( this->GotData ) this->Close();
		this->ClearParameters();
	}
	catch(...)
	{
		if( !std::uncaught_exception() ) throw;
	}
}


void NMRData_Text::OpenFile( const char *Filename, bool ReadOnly )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
#endif

	if( ReadOnly ) this->ReadOnly = true;

	if( !( this->File = fopen( Filename, "r" ) ) ) throw CantOpenFile( Filename );
	if( !ReadOnly )
	{
		fclose( this->File );
		if( !( this->File = fopen( Filename, "r+" ) ) ) throw CantOpenFile( Filename );
	}
	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();
#endif

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
}


void NMRData_Text::CreateFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
#endif

	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
#endif

	if( !( this->File = fopen( Filename, "w+" ) ) ) throw CantOpenFile( Filename );
#ifdef BEC_NMRDATA_DEBUG
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();
#endif

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];

	for( size_t x = 0; x < this->FullSize * this->Components; x++ )
	{
		fprintf( this->File, "0.0" );
		for( int y = 1; y < this->Components; y++ )
			fprintf( this->File, "\t0.0" );
		fprintf( this->File, "\n" );
	}

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Linear );
}


void NMRData_Text::Close( void )
{
	if( !this->GotData ) return;

	this->StdClose();

	if( this->File ) fclose( this->File );
	this->File = 0;
	this->FullSize = 0;

	this->GotData = false;
}


int NMRData_Text::GetFileTypeCode( void ) const
{
	return NMRData::FileType_Text;
}


const char *NMRData_Text::GetFileTypeString( void ) const
{
	return "text";
}


void NMRData_Text::ClearPrivateParams( void )
{
	if( this->GotData ) throw DataAreOpen();

	this->File = 0;
}


void NMRData_Text::LoadData( size_t StartOffset, size_t Size, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Dest ) throw NullPointerArg();
	if( this->Filename && !this->File ) throw NoFileOpen();
#endif

	if( this->File )
	{
		char buffer[ 1024 ];
		fseek( this->File, 0, SEEK_SET );

		for( size_t i = 0; i < StartOffset; i++ )
			fgets( buffer, 1024, this->File );

		if( this->Components == 1 )
		{
			for( size_t i = 0; i < Size; i++ )
			{
				fgets( buffer, 1024, this->File );
				Dest[ StartOffset + i ] = ( float ) atof( buffer );
			}
		}
		else
		{
			for( size_t i = 0; i < Size; i++ )
			{
				int pos = 0;
				fgets( buffer, 1024, this->File );
				std::vector< std::string > tokens = str_tok( std::string( buffer ) ).process_full_string();
				for( int j = 0; j < this->Components; j++ )
					if( ( size_t ) j < tokens.size() ) Dest[ ( StartOffset + i ) * this->Components + j ] = ( float ) atof( tokens[ j ].c_str() );
					else Dest[ ( StartOffset + i ) * this->Components + j ] = 0.0F;
			}
		}
	}
}


void NMRData_Text::WriteData( size_t StartOffset, size_t Size, float *Source )
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Source ) throw NullPointerArg();
	if( this->Filename && !this->File ) throw NoFileOpen();
#endif

	if( this->File )
	{
		char buffer[ 1024 ];
		fseek( this->File, 0, SEEK_SET );
		FILE *Temp = tmpfile();

		for( size_t i = 0; i < StartOffset; i++ )
		{
			fgets( buffer, 1024, this->File );
			fputs( buffer, Temp );
		}

		if( this->Components == 1 )
		{
			fgets( buffer, 1024, this->File );
			for( size_t i = 0; i < Size; i++ )
				fprintf( Temp, "%0.3f\n", Source[ StartOffset + i ] );
		}
		else
		{
			for( size_t i = 0; i < Size; i++ )
			{
				fgets( buffer, 1024, this->File );
				fprintf( Temp, "%0.3f", Source[ ( StartOffset + i ) * this->Components ] );
				for( int j = 1; j < this->Components; j++ )
					fprintf( Temp, "\t%0.3f", Source[ ( StartOffset + i ) * this->Components + j ] );
				fprintf( Temp, "\n" );
			}
		}

		for( size_t i = StartOffset + Size; i < this->FullSize; i++ )
		{
			fgets( buffer, 1024, this->File );
			fputs( buffer, Temp );
		}

		fclose( this->File );
		fopen( this->Filename, "w+" );

		fseek( this->File, 0, SEEK_SET );
		fseek( Temp, 0, SEEK_SET );
		for( size_t i = 0; i < this->FullSize; i++ )
		{
			fgets( buffer, 1024, Temp );
			fputs( buffer, this->File );
		}

		fclose( Temp );
	}
}


NMRDataSubmatrix::NMRDataSubmatrix( void ) : NMRData(), DataFile( 0 ) {  };
NMRDataSubmatrix::NMRDataSubmatrix( int NumOfDims ) : NMRData( NumOfDims ), DataFile( 0 ) {  };
NMRDataSubmatrix::~NMRDataSubmatrix( void ) { };


void NMRDataSubmatrix::Close( void )
{
	if( !this->GotData ) return;

	this->StdClose();

	if( this->DataFile ) fclose( this->DataFile );
	this->DataFile = 0;
	this->FullSize = 0;

	if( SubmatrixSizes ) delete [] SubmatrixSizes;
	if( SubmatrixDimCount ) delete [] SubmatrixDimCount;
	if( TempCoord ) delete [] TempCoord;
	if( SubmatrixPos ) delete [] SubmatrixPos;
	if( PointPos ) delete [] PointPos;
	if( TempDimIncrSizes ) delete [] TempDimIncrSizes;
	if( TempSubmatrixExtIncrSizes ) delete [] TempSubmatrixExtIncrSizes;
	if( TempSubmatrixIntIncrSizes ) delete [] TempSubmatrixIntIncrSizes;

	SubmatrixSizes = 0;
	SubmatrixDimCount = 0;
	TempCoord = 0;
	SubmatrixPos = 0;
	PointPos = 0;
	TempDimIncrSizes = 0;
	TempSubmatrixExtIncrSizes = 0;
	TempSubmatrixIntIncrSizes = 0;

	this->GotData = false;
}


void NMRDataSubmatrix::ClearPrivateParams( void )
{
	this->ClearSubmatrixParams();
}


void NMRDataSubmatrix::ClearSubmatrixParams( void )
{
	this->SubmatrixSizes = 0;
	this->DataFile = 0;
	this->HeaderSize = 0;
	this->SwapBytes = false;
}


void NMRDataSubmatrix::LoadData( size_t StartOffset, size_t Size, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Dest ) throw NullPointerArg();
	if( !this->DataFile ) throw NoFileOpen();

	if( !this->SubmatrixSize || !this->SubmatrixSizes ) throw SubmatrixParamsNotSet();
#endif

	if( !this->SubmatrixPos ) this->SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !this->SubmatrixDimCount ) this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !this->PointPos ) this->PointPos = new int[ this->NumOfDimensions ];
	if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}
	if( !this->TempSubmatrixExtIncrSizes )
	{
		this->TempSubmatrixExtIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixExtIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixExtIncrSizes[ i ] *= SubmatrixDimCount[ j ];
		}
	}
	if( !this->TempSubmatrixIntIncrSizes )
	{
		this->TempSubmatrixIntIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixIntIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixIntIncrSizes[ i ] *= SubmatrixSizes[ j ];
		}
	}

	for( size_t i = 0; i < Size; i++ )
	{
		size_t remainder = StartOffset + i;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempCoord[ x ] = ( int ) ( remainder / this->TempDimIncrSizes[ x ] );
			remainder = remainder % this->TempDimIncrSizes[ x ];
		}

		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->SubmatrixPos[ x ] = this->TempCoord[ x ] / this->SubmatrixSizes[ x ];
			this->PointPos[ x ] = this->TempCoord[ x ] % this->SubmatrixSizes[ x ];
		}

		int SubmatrixNum = 0, PointOffset = 0;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			SubmatrixNum += this->SubmatrixPos[ x ] * this->TempSubmatrixExtIncrSizes[ x ];
			PointOffset += this->PointPos[ x ] * this->TempSubmatrixIntIncrSizes[ x ];
		}

		if( fseek( this->DataFile, ( off_t ) this->HeaderSize + sizeof( float ) * ( ( off_t ) SubmatrixNum * ( off_t ) SubmatrixSize + ( off_t ) PointOffset ), SEEK_SET ) )
			throw CantReadFile();
		if( fread( Dest + i, sizeof( float ), 1, this->DataFile ) != 1 ) throw CantReadFile();
		if( this->SwapBytes ) Dest[ i ] = this->SwapByteOrder( Dest[ i ] );
	}
}


void NMRDataSubmatrix::WriteData( size_t StartOffset, size_t Size, float *Source )
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Source ) throw NullPointerArg();
	if( !this->DataFile ) throw NoFileOpen();

	if( !this->SubmatrixSize || !this->SubmatrixSizes ) throw SubmatrixParamsNotSet();
#endif

	if( !this->SubmatrixPos ) this->SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !this->SubmatrixDimCount ) this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !this->PointPos ) this->PointPos = new int[ this->NumOfDimensions ];
	if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}
	if( !this->TempSubmatrixExtIncrSizes )
	{
		this->TempSubmatrixExtIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixExtIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixExtIncrSizes[ i ] *= SubmatrixDimCount[ j ];
		}
	}
	if( !this->TempSubmatrixIntIncrSizes )
	{
		this->TempSubmatrixIntIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixIntIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixIntIncrSizes[ i ] *= SubmatrixSizes[ j ];
		}
	}

	for( size_t i = 0; i < Size; i++ )
	{
		size_t remainder = StartOffset + i;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempCoord[ x ] = ( int ) ( remainder / this->TempDimIncrSizes[ x ] );
			remainder = remainder % this->TempDimIncrSizes[ x ];
		}

		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->SubmatrixPos[ x ] = this->TempCoord[ x ] / this->SubmatrixSizes[ x ];
			this->PointPos[ x ] = this->TempCoord[ x ] % this->SubmatrixSizes[ x ];
		}

		int SubmatrixNum = 0, PointOffset = 0;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			SubmatrixNum += this->SubmatrixPos[ x ] * this->TempSubmatrixExtIncrSizes[ x ];
			PointOffset += this->PointPos[ x ] * this->TempSubmatrixIntIncrSizes[ x ];
		}

		float outval = Source[ i ];
		if( this->SwapBytes ) outval = this->SwapByteOrder( outval );
		if( fseek( this->DataFile, ( off_t ) this->HeaderSize + sizeof( float ) * ( ( off_t ) SubmatrixNum * ( off_t ) SubmatrixSize + ( off_t ) PointOffset ), SEEK_SET ) ) 
			throw CantWriteFile();
		if( fwrite( &outval, sizeof( float ), 1, this->DataFile ) != 1 ) throw CantWriteFile();
	}
}


void NMRDataSubmatrix::LoadSubmatrix( size_t SubmatrixNumber, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( SubmatrixNumber >= this->SubmatrixCount ) throw BadSubmatrixNumber();
	if( !Dest ) throw NullPointerArg();
	if( !this->DataFile ) throw NoFileOpen();
	if( !this->SubmatrixSize ) throw SubmatrixParamsNotSet();
#endif

	if( fseek( this->DataFile, ( off_t ) this->HeaderSize + sizeof( float ) * ( off_t ) SubmatrixNumber * ( off_t ) SubmatrixSize, SEEK_SET ) )
		throw CantReadFile();
	if( fread( Dest, sizeof( float ), ( size_t ) SubmatrixSize, this->DataFile ) != SubmatrixSize )
		throw CantReadFile();
	if( this->SwapBytes )
		for( int i = 0; i < this->SubmatrixSize; i++ )
			Dest[ i ] = this->SwapByteOrder( Dest[ i ] );
}


void NMRDataSubmatrix::WriteSubmatrix( size_t SubmatrixNumber, float *Src )
{
#ifdef BEC_NMRDATA_DEBUG
	if( SubmatrixNumber >= this->SubmatrixCount ) throw BadSubmatrixNumber();
	if( !Src ) throw NullPointerArg();
	if( !this->DataFile ) throw NoFileOpen();
	if( !this->SubmatrixSize ) throw SubmatrixParamsNotSet();
#endif

	if( fseek( this->DataFile, ( off_t ) this->HeaderSize + sizeof( float ) * ( off_t ) SubmatrixNumber * ( off_t ) SubmatrixSize, SEEK_SET ) ) 
		throw CantWriteFile();
	if( !this->SwapBytes )
	{
		if( fwrite( Src, sizeof( float ), ( size_t ) SubmatrixSize, this->DataFile ) != SubmatrixSize ) 
			throw CantWriteFile();
	}
	else
	{
		stack_array_ptr< float > outvals( new float[ SubmatrixSize ] );
		memcpy( outvals, Src, sizeof( float ) * SubmatrixSize );
		for( int i = 0; i < this->SubmatrixSize; i++ )
			outvals[ i ] = this->SwapByteOrder( outvals[ i ] );
		if( fwrite( outvals, sizeof( float ), ( size_t ) SubmatrixSize, this->DataFile ) != SubmatrixSize ) 
			throw CantWriteFile();
	}
}


bool NMRDataSubmatrix::CacheDefinesSubmatrices( void ) const
{
	return false;
}


NMRData_XEASY::NMRData_XEASY( void ) : NMRDataSubmatrix() { this->ClearPrivateParams(); };
NMRData_XEASY::NMRData_XEASY( int NumOfDims ) : NMRDataSubmatrix( NumOfDims ) { this->ClearPrivateParams(); };
NMRData_XEASY::NMRData_XEASY( const char *Filename, bool ReadOnly ) : NMRDataSubmatrix() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename, ReadOnly );
};
NMRData_XEASY::NMRData_XEASY( std::string Filename, bool ReadOnly ) : NMRDataSubmatrix() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename.c_str(), ReadOnly );
};


NMRData_XEASY::~NMRData_XEASY( void )
{
	try
	{
		if( this->GotData )	this->Close();
		this->ClearParameters();
	}
	catch(...)
	{
		if( !std::uncaught_exception() ) throw;
	}
}


void NMRData_XEASY::OpenFile( const char *Filename, bool ReadOnly )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
#endif

	if( !strstr( Filename, ".3D.param" ) ) throw BadXEASYFilename();
	if( ReadOnly ) this->ReadOnly = true;

	if( !( this->ParamFile = fopen( Filename, "r" ) ) ) throw CantOpenFile( Filename );
	if( !ReadOnly )
	{
		fclose( this->ParamFile );
		if( !( this->ParamFile = fopen( Filename, "r+" ) ) ) throw CantOpenFile( Filename );
	}

	stack_array_ptr< char > datafilename( new char[ strlen( Filename ) - 2 ] );
	strncpy( datafilename, Filename, strlen( Filename ) - 5 );
	strcpy( datafilename + strlen( Filename ) - 5, "16" );

	if( ReadOnly )
	{
		if( !( this->DataFile = fopen( datafilename, "rb" ) ) ) throw CantOpenFile( datafilename );
	}
	else if( !( this->DataFile = fopen( datafilename, "r+b" ) ) ) throw CantOpenFile( datafilename );

	this->LoadFileHeader();

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->GotData = true;
	InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Submatrix );
}


void NMRData_XEASY::CreateFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	if( !strstr( Filename, ".3D.param" ) ) throw BadXEASYFilename();

	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->SetSubmatrixSizes();

	stack_array_ptr< char > datafilename( new char[ strlen( Filename ) - 2 ] );
	strncpy( datafilename, Filename, strlen( Filename ) - 5 );
	strcpy( datafilename + strlen( Filename ) - 5, "16" );

	FILE *df, *pf;
	if( !( pf = fopen( Filename, "w" ) ) ) throw CantOpenFile( Filename );

	try
	{
		fprintf( pf, "Version ....................... 1\n" );
		fprintf( pf, "Number of dimensions .......... %i\n", this->NumOfDimensions );
		fprintf( pf, "16 or 8 bit file type ......... 16\n" );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Spectrometer frequency in w%i .. %0.3f\n", x, this->DimensionCentersFreq[ x ] );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Spectral sweep width in w%i .... %0.3f\n", x, this->DimensionScalesPPM[ x ] );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Maximum chemical shift in w%i .. %0.3f\n", x, this->DimensionOriginsPPM[ x ] );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Size of spectrum in w%i ........ %i\n", x, this->DimensionSizes[ x ] );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Submatrix size in w%i .......... %i\n", x, this->SubmatrixSizes[ x ] );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Permutation for w%i ............ %i\n", x, this->NumOfDimensions - x );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Folding in w%i ................. RSH\n", x );
		fprintf( pf, "Type of spectrum .............. ?\n" );
		for( int x = 0; x < this->NumOfDimensions; x++ )
			fprintf( pf, "Identifier for dimension w%i ... %s\n", x, this->DimensionAxisLabels[ x ] );
		fprintf( pf, "\n" );
	}
	catch(...)
	{
		fclose( pf );
		throw;
	}
	fclose( pf );
	
	if( !( df = fopen( datafilename, "wb" ) ) ) throw CantOpenFile( datafilename );

	try
	{
		unsigned char outval[ 2 ];
		this->FloatToXEASY16( 0.0F, outval );

		for( size_t x = 0; x < this->FullSize; x++ )
			if( fwrite( outval, sizeof( unsigned char ), 2, df ) != 2 ) throw CantWriteFile();
	}
	catch(...)
	{
		fclose( df );
		throw;
	}
	fclose( df );

	this->OpenFile( Filename );
}


void NMRData_XEASY::Close( void )
{
	if( !this->GotData ) return;

	this->StdClose();

	if( this->DataFile ) fclose( this->DataFile );
	if( this->ParamFile ) fclose( this->ParamFile );
	this->DataFile = 0;
	this->ParamFile = 0;
	this->FullSize = 0;

	this->GotData = false;
}


void NMRData_XEASY::LoadFileHeader( void )
{
	char buffer[ 256 ];

#ifdef BEC_NMRDATA_DEBUG
	if( !this->ParamFile ) throw NoFileOpen();
#endif

	if( fseek( this->ParamFile, 0, SEEK_SET ) ) throw CantReadFile();

	if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
	if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
	if( !this->GotData )
	{
		this->SetNumOfDims( atoi( buffer + 32 ) );
		this->IsComplex = false;
		this->IsTimeDomain = false;
		this->SubmatrixSizes = new int[ this->NumOfDimensions ];
	}
	if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
	if( atoi( buffer + 32 ) != 16 ) throw BadFileHeader();

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
		this->DimensionCentersFreq[ x ] = ( float ) atof( buffer + 32 );
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
		this->DimensionScalesPPM[ x ] = ( float ) atof( buffer + 32 );
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
		this->DimensionOriginsPPM[ x ] = ( float ) atof( buffer + 32 );
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
		if( !this->GotData ) this->SetDimensionSize( x, atoi( buffer + 32 ) );
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
		if( !this->GotData ) this->SubmatrixSizes[ x ] = atoi( buffer + 32 );
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
	}

	if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( !fgets( buffer, 256, this->ParamFile ) ) throw CantReadFile();
		if( this->DimensionAxisLabels[ x ] && strcmp( this->DimensionAxisLabels[ x ], buffer + 32 ) )
		{
			delete [] this->DimensionAxisLabels[ x ];
			this->DimensionAxisLabels[ x ] = 0;
		}
		if( !this->DimensionAxisLabels[ x ] )
		{
			this->DimensionAxisLabels[ x ] = new char[ strlen( buffer + 32 ) + 1 ];
			strncpy( this->DimensionAxisLabels[ x ], buffer + 32, strlen( buffer + 32 ) );
			this->DimensionAxisLabels[ x ][ strlen( buffer + 32 ) ] = 0;
			if( this->DimensionAxisLabels[ x ][ strlen( buffer + 32 ) - 1 ] == 0x0a )
				this->DimensionAxisLabels[ x ][ strlen( buffer + 32 ) - 1 ] = 0;
		}
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		this->DimensionCentersPPM[ x ] = this->DimensionOriginsPPM[ x ] - this->DimensionScalesPPM[ x ] / 2.0F;
		this->DimensionScalesFreq[ x ] = this->DimensionScalesPPM[ x ] * this->DimensionCentersFreq[ x ];
	}

	this->SubmatrixCount = 1;
	this->SubmatrixSize = 1;
	this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		this->SubmatrixSize *= this->SubmatrixSizes[ i ];
		this->SubmatrixDimCount[ i ] = ( int ) ceil( ( ( float ) this->DimensionSizes[ i ] ) / ( ( float ) this->SubmatrixSizes[ i ] ) );
		this->SubmatrixCount *= this->SubmatrixDimCount[ i ];
	}
}


void NMRData_XEASY::WriteFileHeader( void )
{
	if( this->ReadOnly ) return;

	fclose( this->ParamFile );
	if( !( this->ParamFile = fopen( this->Filename, "w+" ) ) ) throw CantOpenFile( this->Filename );

	fprintf( this->ParamFile, "Version ....................... 1\n" );
	fprintf( this->ParamFile, "Number of dimensions .......... %i\n", this->NumOfDimensions );
	fprintf( this->ParamFile, "16 or 8 bit file type ......... 16\n" );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Spectrometer frequency in w%i .. %0.3f\n", x, this->DimensionCentersFreq[ x ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Spectral sweep width in w%i .... %0.3f\n", x, this->DimensionScalesPPM[ x ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Maximum chemical shift in w%i .. %0.3f\n", x, this->DimensionOriginsPPM[ x ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Size of spectrum in w%i ........ %i\n", x, this->DimensionSizes[ x ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Submatrix size in w%i .......... %i\n", x, SubmatrixSizes[ x ] );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Permutation for w%i ............ %i\n", x, this->NumOfDimensions - x );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Folding in w%i ................. RSH\n", x );
	fprintf( this->ParamFile, "Type of spectrum .............. ?\n" );
	for( int x = 0; x < this->NumOfDimensions; x++ )
		fprintf( this->ParamFile, "Identifier for dimension w%i ... %s\n", x, this->DimensionAxisLabels[ x ] );
	fprintf( this->ParamFile, "\n" );
}


int NMRData_XEASY::GetFileTypeCode( void ) const
{
	return NMRData::FileType_XEASY;
}


const char *NMRData_XEASY::GetFileTypeString( void ) const
{
	return "XEASY";
}


void NMRData_XEASY::ClearPrivateParams( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif

	this->ClearSubmatrixParams();
	this->ParamFile = 0;
	this->InitExponTable();
}


void NMRData_XEASY::LoadData( size_t StartOffset, size_t Size, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Dest ) throw NullPointerArg();
	if( !this->DataFile ) throw NoFileOpen();

	if( !this->SubmatrixSize || !this->SubmatrixSizes ) throw SubmatrixParamsNotSet();
#endif

	if( !this->SubmatrixPos ) this->SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !this->SubmatrixDimCount ) this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !this->PointPos ) this->PointPos = new int[ this->NumOfDimensions ];
	if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}
	if( !this->TempSubmatrixExtIncrSizes )
	{
		this->TempSubmatrixExtIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixExtIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixExtIncrSizes[ i ] *= this->SubmatrixDimCount[ j ];
		}
	}
	if( !this->TempSubmatrixIntIncrSizes )
	{
		this->TempSubmatrixIntIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixIntIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixIntIncrSizes[ i ] *= this->SubmatrixSizes[ j ];
		}
	}

	for( size_t i = 0; i < Size; i++ )
	{
		size_t remainder = StartOffset + i;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempCoord[ x ] = ( int ) ( remainder / this->TempDimIncrSizes[ x ] );
			remainder = remainder % TempDimIncrSizes[ x ];
		}

		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->SubmatrixPos[ x ] = TempCoord[ x ] / this->SubmatrixSizes[ x ];
			this->PointPos[ x ] = TempCoord[ x ] % this->SubmatrixSizes[ x ];
		}

		int SubmatrixNum = 0, PointOffset = 0;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			SubmatrixNum += this->SubmatrixPos[ x ] * this->TempSubmatrixExtIncrSizes[ x ];
			PointOffset += this->PointPos[ x ] * this->TempSubmatrixIntIncrSizes[ x ];
		}

		unsigned char inval[ 2 ];
		if( fseek( this->DataFile, ( off_t ) ( 2 * ( ( off_t ) SubmatrixNum * ( off_t ) SubmatrixSize + ( off_t ) PointOffset ) ), SEEK_SET ) )
			throw CantReadFile();
		if( fread( inval, 1, 2, this->DataFile ) != 2 ) throw CantReadFile();
		Dest[ i ] = this->XEASY16ToFloat( inval );
	}
}


void NMRData_XEASY::WriteData( size_t StartOffset, size_t Size, float *Source )
{
#ifdef BEC_NMRDATA_DEBUG
	if( StartOffset >= this->FullSize ) throw BadOffset();
	if( StartOffset + Size > this->FullSize ) throw BadSize();
	if( !Source ) throw NullPointerArg();
	if( !this->DataFile ) throw NoFileOpen();

	if( !this->SubmatrixSize || !this->SubmatrixSizes ) throw SubmatrixParamsNotSet();
#endif

	if( !this->SubmatrixPos ) this->SubmatrixPos = new int[ this->NumOfDimensions ];
	if( !this->SubmatrixDimCount ) this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	if( !this->PointPos ) this->PointPos = new int[ this->NumOfDimensions ];
	if( !this->TempCoord ) this->TempCoord = new int[ this->NumOfDimensions ];
	if( !this->TempDimIncrSizes )
	{
		this->TempDimIncrSizes = new size_t[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempDimIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempDimIncrSizes[ i ] *= this->DimensionSizes[ j ];
		}
	}
	if( !this->TempSubmatrixExtIncrSizes )
	{
		this->TempSubmatrixExtIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixExtIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixExtIncrSizes[ i ] *= this->SubmatrixDimCount[ j ];
		}
	}
	if( !this->TempSubmatrixIntIncrSizes )
	{
		this->TempSubmatrixIntIncrSizes = new int[ this->NumOfDimensions ];
		for( int i = 0; i < this->NumOfDimensions; i++ )
		{
			this->TempSubmatrixIntIncrSizes[ i ] = 1;
			for( int j = i + 1; j < this->NumOfDimensions; j++ )
				this->TempSubmatrixIntIncrSizes[ i ] *= this->SubmatrixSizes[ j ];
		}
	}

	unsigned char outval[ 2 ];
	for( size_t i = 0; i < Size; i++ )
	{
		size_t remainder = StartOffset + i;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->TempCoord[ x ] = ( int ) ( remainder / this->TempDimIncrSizes[ x ] );
			remainder = remainder % TempDimIncrSizes[ x ];
		}

		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			this->SubmatrixPos[ x ] = TempCoord[ x ] / this->SubmatrixSizes[ x ];
			this->PointPos[ x ] = TempCoord[ x ] % this->SubmatrixSizes[ x ];
		}

		int SubmatrixNum = 0, PointOffset = 0;
		for( int x = 0; x < this->NumOfDimensions; x++ )
		{
			SubmatrixNum += this->SubmatrixPos[ x ] * this->TempSubmatrixExtIncrSizes[ x ];
			PointOffset += this->PointPos[ x ] * this->TempSubmatrixIntIncrSizes[ x ];
		}

		this->FloatToXEASY16( *Source, outval );
		if( fseek( this->DataFile, ( off_t ) ( 2 * ( ( off_t ) SubmatrixNum * ( off_t ) SubmatrixSize + ( off_t ) PointOffset ) ), SEEK_SET ) ) 
			throw CantWriteFile();
		if( fwrite( outval, 1, 2, this->DataFile ) != 2 ) throw CantWriteFile();
	}
}


void NMRData_XEASY::LoadSubmatrix( size_t SubmatrixNumber, float *Dest ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->DataFile ) throw NoFileOpen();
	if( !Dest ) throw NullPointerArg();
	if( !this->SubmatrixSize ) throw SubmatrixParamsNotSet();
	if( SubmatrixNumber >= this->SubmatrixCount ) throw BadSubmatrixNumber();
#endif

	for( int i = 0; i < SubmatrixSize; i++ )
	{
		unsigned char inval[ 2 ];
		if( fseek( this->DataFile, ( off_t ) ( 2 * SubmatrixNumber * ( off_t ) SubmatrixSize + 2 * ( off_t ) i ), SEEK_SET ) ) throw CantReadFile();
		if( fread( inval, 1, 2, this->DataFile ) != 2 ) throw CantReadFile();
		Dest[ i ] = this->XEASY16ToFloat( inval );
	}
}


void NMRData_XEASY::WriteSubmatrix( size_t SubmatrixNumber, float *Src )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->DataFile ) throw NoFileOpen();
	if( !Src ) throw NullPointerArg();
	if( !this->SubmatrixSize ) throw SubmatrixParamsNotSet();
	if( SubmatrixNumber >= this->SubmatrixCount ) throw BadSubmatrixNumber();
#endif

	unsigned char outval[ 2 ];
	for( int i = 0; i < SubmatrixSize; i++ )
	{
		this->FloatToXEASY16( Src[ i ], outval );
		if( fseek( this->DataFile, ( off_t ) ( 2 * SubmatrixNumber * ( off_t ) SubmatrixSize + 2 * ( off_t ) i ), SEEK_SET ) ) throw CantWriteFile();
		if( fwrite( outval, 1, 2, this->DataFile ) != 2 ) throw CantWriteFile();
	}
}


///	Decodes a 16-bit XEASY data value into a float.
/**	The XEASY file format uses an unusual custom floating point representation,
	which can encode a very large range of values into a mere two bytes.  This
	representation is claimed to have a range from -sqrt(2)^47 -> sqrt(2)^46 with
	<1% error over the whole range.

	This description and the code below are based on the XEASY manual, chapter 3,
	and an examination of the XEASY source code.  However, no part of this code
	is copied from the XEASY code, and a comparison will show them to work in
	different ways on many points.

	Like most floating point representations, the format has two components, a 
	mantissa and an exponent.  The base of the exponent is sqrt(2).  Let a = 
	the mantissa and e = the exponent.  Then a number is decoded using:

		( a + 615 ) * sign * sqrt(2)^e / 721

	The actual packing of values into the 16-bit storage location is more
	complicated.  The first byte is the (unsigned) mantissa.  The second is
	the exponent, where exponent byte 0 means that the value is exactly zero,
	those between 1 and 47 map to positive sign and exponents 0 to 46, and those
	between 48 and 95 map to negative sign and exponent values 1 to 47, in
	reverse order.

	This code relies on precalculated exponents, generated upon construction of
	the NMRData_XEASY by InitExponTable().
*/
float NMRData_XEASY::XEASY16ToFloat( unsigned char *val ) const
{
	unsigned char ebyte = val[ 1 ], abyte = val[ 0 ];
	size_t e = 0;

	float sign = 1.0F;
	if( ebyte <= ( unsigned char ) 47 )
		e = ( size_t ) ebyte - 1;
	else
	{
		e = 95 - ( size_t ) ebyte;
		sign = -1.0F;
	}

	return ( ( float ) abyte + 615.0F ) * sign * this->expon_table[ e ] / 721.0F;
}


///	Encodes a float as a 16-bit XEASY data value.
/**	The XEASY file format uses an unusual custom floating point representation,
	which can encode a very large range of values into a mere two bytes.  This
	representation is claimed to have a range from -sqrt(2)^47 -> sqrt(2)^46 with
	<1% error over the whole range.

	It may be helpful to refer to the documentation for decoding values, which
	accompanies XEASY16ToFloat(), before considering the encoding process.

	This description and the code below are based on the XEASY manual, chapter 3,
	and an examination of the XEASY source code.  However, no part of this code
	is copied from the XEASY code, and a comparison will show them to work in
	different ways on many points.

	The XEASY representation uses a one-byte unsigned mantissa and a one-byte
	unsigned exponent of base sqrt(2).  The formulas in the XEASY manual are
	basically correct, but do not attempt to describe the rounding procedures
	used in the actual encoding process.  I have determined these by examining
	the XEASY code directly.

	To determine the exponent, one finds the exponent e for which sqrt(2)^e is
	closest to the input value.  The code below does this using a precalculated
	table of sqrt(2)^x for x between 0 and 48.  A loop determines the highest
	value of e such that sqrt(2)^e is less than the value.  A check is then
	made to determine whether sqrt(2)^e (< value) is closer to value than
	sqrt(2)^(e+1) (which is > value), and the closer one is adopted as e.  The
	exponent is then encoded as an unsigned byte, using the rule that:

		ebyte =	min( e + 1, 47 ),	value >= 0
		      = 95 - min( e, 47 ),	value < 0
	
	where e here is the adjusted e.

	The mantissa a is then determined using the formula 
	
		a = 721 * | value | / sqrt(2)^e - 615

	where e again is the adjusted e.  However, the product of 721 * | value | is
	first rounded to the nearest integer before division.

	It is recommended that you preallocate two bytes as an unsigned char[ 2 ] on
	the stack, and pass that array in the outval parameter.  In that case, this
	routine will store the encoded result in the supplied array.  Otherwise,
	it will heap-allocate a two byte array for this purpose.

	This code relies on precalculated exponents, generated upon construction of
	the NMRData_XEASY by InitExponTable().
*/
unsigned char *NMRData_XEASY::FloatToXEASY16( float val, unsigned char *outval ) const
{
	unsigned char ebyte = 0, abyte = 0;
	size_t e = 0;

	float absval = val >= 0.0F ? val : -val;

	for( size_t i = 0; i < 48; i++ )
		if( absval >= this->expon_table[ i ] )
			e = i;

	if( e && fabsf( absval - this->expon_table[ e + 1 ] ) < fabsf( absval - this->expon_table[ e ] ) ) e += 1;

	if( val >= 0 ) ebyte = e + 1 < 47 ? ( unsigned char ) ( e + 1 ) : ( unsigned char ) 47;
	else ebyte = ( unsigned char ) ( 95 - ( e < 47 ? e : 47 ) );

	if( e ) abyte = ( unsigned char ) ( floorf( 721.0F * absval + 0.5F ) / ( this->expon_table[ e ] ) - 615.0F );
	else abyte = ( unsigned char ) 0;

	if( !outval ) outval = new unsigned char[ 2 ];
	outval[ 0 ] = abyte;
	outval[ 1 ] = ebyte;
	return outval;
}


void NMRData_XEASY::InitExponTable( void ) const
{
	const float base = 1.4142135623730950488016887242097F;

	this->expon_table.resize( 49 );
	for( size_t i = 0; i <= 48; i++ )
		this->expon_table[ i ] = powf( base, ( float ) i );
}


NMRData_UCSF::NMRData_UCSF( void ) : NMRDataSubmatrix() { this->ClearPrivateParams(); };
NMRData_UCSF::NMRData_UCSF( int NumOfDims ) : NMRDataSubmatrix( NumOfDims ) { this->ClearPrivateParams(); };
NMRData_UCSF::NMRData_UCSF( const char *Filename, bool ReadOnly ) : NMRDataSubmatrix() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename, ReadOnly );
};
NMRData_UCSF::NMRData_UCSF( std::string Filename, bool ReadOnly ) : NMRDataSubmatrix() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename.c_str(), ReadOnly );
};


NMRData_UCSF::~NMRData_UCSF( void )
{
	try
	{
		if( this->GotData )	this->Close();
		this->ClearParameters();
	}
	catch(...)
	{
		if( !std::uncaught_exception() ) throw;
	}
}


void NMRData_UCSF::OpenFile( const char *Filename, bool ReadOnly )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
#endif

	if( ReadOnly ) this->ReadOnly = true;

	if( !( this->DataFile = fopen( Filename, "rb" ) ) ) throw CantOpenFile( Filename );
	if( !ReadOnly )
	{
		fclose( this->DataFile );
		if( !( this->DataFile = fopen( Filename, "r+b" ) ) ) throw CantOpenFile( Filename );
	}

	this->LoadFileHeader();

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Submatrix );
}


void NMRData_UCSF::CreateFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();
#endif

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->SetSubmatrixSizes();

	if( !( this->DataFile = fopen( Filename, "w+b" ) ) ) throw CantOpenFile( Filename );

	this->WriteFileHeader();

	float outval = this->SwapByteOrder( 0.0F );
	for( size_t x = 0; x < this->FullSize; x++ )
		if( fwrite( &outval, sizeof( float ), 1, this->DataFile ) != 1 ) throw CantWriteFile();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Submatrix );
}


void NMRData_UCSF::LoadFileHeader( void )
{
	UCSF_FILE_HEADER fh;
	UCSF_DIM_HEADER dh;

#ifdef BEC_NMRDATA_DEBUG
	if( !this->DataFile ) throw NoFileOpen();
#endif

	if( fseek( this->DataFile, 0, SEEK_SET ) ) throw CantReadFile();
	if( fread( &fh, sizeof( UCSF_FILE_HEADER ), 1, this->DataFile ) != 1 ) throw CantReadFile();

	if( strcmp( fh.id, "UCSF NMR" ) || fh.FormatVersion != 2 ) throw BadFileHeader();
	if( fh.IsComplex != 1 ) throw ComplexNotSupported();

	if( !this->GotData )
	{
		this->SetNumOfDims( fh.NumOfDimensions );
		this->SubmatrixSizes = new int[ this->NumOfDimensions ];
		this->HeaderSize = sizeof( UCSF_FILE_HEADER ) + this->NumOfDimensions * sizeof( UCSF_DIM_HEADER );
	}

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		if( fread( &dh, sizeof( UCSF_DIM_HEADER ), 1, this->DataFile ) != 1 ) throw CantReadFile();

		if( !this->GotData )
		{
			this->SetDimensionSize( x, this->SwapBytes ? this->SwapByteOrder( dh.Size ) : dh.Size );
			this->SubmatrixSizes[ x ] = this->SwapBytes ? this->SwapByteOrder( dh.SubmatrixSize ) : dh.SubmatrixSize;
		}

		this->SetDimensionLabel( x, dh.Label );
		this->SetDimensionCalibration( x, this->SwapBytes ? this->SwapByteOrder( dh.sw ) : dh.sw, 
			this->SwapBytes ? this->SwapByteOrder( dh.sf ) : dh.sf, this->SwapBytes ? this->SwapByteOrder( dh.CenterPPM ) : dh.CenterPPM );
	}

	SubmatrixCount = 1;
	SubmatrixSize = 1;
	SubmatrixDimCount = new int[ this->NumOfDimensions ];
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		SubmatrixSize *= SubmatrixSizes[ i ];
		SubmatrixDimCount[ i ] = ( int ) ceil( ( ( float ) this->DimensionSizes[ i ] ) / ( ( float ) SubmatrixSizes[ i ] ) );
		SubmatrixCount *= SubmatrixDimCount[ i ];
	}
}


void NMRData_UCSF::WriteFileHeader( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
#endif

	if( this->ReadOnly ) return;

	this->HeaderSize = sizeof( UCSF_FILE_HEADER ) + this->NumOfDimensions * sizeof( UCSF_DIM_HEADER );

	UCSF_FILE_HEADER fh;
	UCSF_DIM_HEADER dh;

	memset( &fh, 0, sizeof( UCSF_FILE_HEADER ) );
	strcpy( fh.id, "UCSF NMR" );
	fh.FormatVersion = 2;
	fh.IsComplex = 1;
	fh.NumOfDimensions = this->NumOfDimensions;

	if( fseek( this->DataFile, 0, SEEK_SET ) ) throw CantWriteFile();

	if( fwrite( &fh, sizeof( UCSF_FILE_HEADER ), 1, this->DataFile ) != 1 ) throw CantWriteFile();

	for( int x = 0; x < this->NumOfDimensions; x++ )
	{
		memset( &dh, 0, sizeof( UCSF_DIM_HEADER ) );
		strcpy( dh.Label, this->DimensionAxisLabels[ x ] );
		dh.CenterPPM = this->SwapBytes ? this->SwapByteOrder( this->DimensionCentersPPM[ x ] ) : this->DimensionCentersPPM[ x ];
		dh.sf = this->SwapBytes ? this->SwapByteOrder( this->DimensionCentersFreq[ x ] ) : this->DimensionCentersFreq[ x ];
		dh.Size = this->SwapBytes ? this->SwapByteOrder( this->DimensionSizes[ x ] ) : this->DimensionSizes[ x ];
		dh.SubmatrixSize = this->SwapBytes ? this->SwapByteOrder( this->SubmatrixSizes[ x ] ) : this->SubmatrixSizes[ x ];
		dh.sw = this->SwapBytes ? this->SwapByteOrder( this->DimensionScalesFreq[ x ] ) : this->DimensionScalesFreq[ x ];
		if( fwrite( &dh, sizeof( UCSF_DIM_HEADER ), 1, this->DataFile ) != 1 ) throw CantWriteFile();
	}
}


int NMRData_UCSF::GetFileTypeCode( void ) const
{
	return NMRData::FileType_UCSF;
}


const char *NMRData_UCSF::GetFileTypeString( void ) const
{
	return "UCSF/SPARKY";
}


void NMRData_UCSF::ClearPrivateParams( void )
{
	if( this->GotData ) throw DataAreOpen();

	this->ClearSubmatrixParams();
	this->HeaderSize = 0;

	int x = 1;
	if( *( ( char * ) &x ) == 1 ) this->SwapBytes = true;
	else this->SwapBytes = false;
}


NMRData_NMRView::NMRData_NMRView( void ) : NMRDataSubmatrix() { this->ClearPrivateParams(); };
NMRData_NMRView::NMRData_NMRView( int NumOfDims ) : NMRDataSubmatrix( NumOfDims ) { this->ClearPrivateParams(); };
NMRData_NMRView::NMRData_NMRView( const char *Filename, bool ReadOnly ) : NMRDataSubmatrix() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename, ReadOnly );
};
NMRData_NMRView::NMRData_NMRView( std::string Filename, bool ReadOnly ) : NMRDataSubmatrix() 
{
	this->ClearPrivateParams();
	this->OpenFile( Filename.c_str(), ReadOnly );
};


NMRData_NMRView::~NMRData_NMRView( void )
{
	try
	{
		if( this->GotData )	this->Close();
		this->ClearParameters();
	}
	catch(...)
	{
		if( !std::uncaught_exception() ) throw;
	}
}


void NMRData_NMRView::OpenFile( const char *Filename, bool ReadOnly )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
	if( !Filename ) throw NullPointerArg();
#endif

	if( ReadOnly ) this->ReadOnly = true;

	if( !( this->DataFile = fopen( Filename, "rb" ) ) ) throw CantOpenFile( Filename );
	if( !ReadOnly )
	{
		fclose( this->DataFile );
		if( !( this->DataFile = fopen( Filename, "r+b" ) ) ) throw CantOpenFile( Filename );
	}

	this->LoadFileHeader();

	if( this->Filename ) delete [] this->Filename;
	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	if( this->Name ) delete [] this->Name;
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Submatrix );
}


void NMRData_NMRView::CreateFile( const char *Filename )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !Filename ) throw NullPointerArg();
#endif

	if( !this->NumOfDimensions )
	{
		if( !this->ParameterCallback ) throw NoParamsAvailable();
		if( !( this->ParameterCallback( this ) ) ) throw NoParamsAvailable();
	}
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	for( int x = 0; x < this->NumOfDimensions; x++ )
		if( !this->DimensionSizes[ x ] ) throw BadDimSize();
#endif

	this->Filename = new char[ strlen( Filename ) + 1 ];
	strcpy( this->Filename, Filename );
	this->Name = this->RemoveFilePath( Filename );
	
	this->FullSize = 1;
	for( int x = 0; x < this->NumOfDimensions; x++ )
		this->FullSize *= this->DimensionSizes[ x ];
	if( this->FullSize > ( size_t ) std::numeric_limits< long >::max() ) throw DatasetTooLarge();

	this->SetSubmatrixSizes();

	if( !( this->DataFile = fopen( Filename, "w+b" ) ) ) throw CantOpenFile( Filename );

	this->WriteFileHeader();

	float outval = 0.0F;
	if( this->SwapBytes ) outval = this->SwapByteOrder( 0.0F );
	for( size_t x = 0; x < SubmatrixSize * SubmatrixCount; x++ )
		if( fwrite( &outval, sizeof( float ), 1, this->DataFile ) != 1 ) throw CantWriteFile();

	this->GotData = true;
	this->InitCache( this->DefaultInternalCacheSize, this->DefaultPageSize, Submatrix );
}


void NMRData_NMRView::LoadFileHeader( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->DataFile ) throw NoFileOpen();
#endif

	NVHEADER nvh;
	if( fseek( this->DataFile, 0, SEEK_SET ) ) throw CantReadFile();
	if( fread( &nvh, sizeof( NVHEADER ), 1, this->DataFile ) != 1 ) throw CantReadFile();

	if( nvh.magic != 874032077 && SwapByteOrder( nvh.magic ) == 874032077 )
		this->SwapBytes = true;
	else if( nvh.magic != 874032077 ) throw BadFileHeader();

	if( !this->GotData )
	{
		this->SetNumOfDims( this->SwapBytes ? this->SwapByteOrder( nvh.ndim ) : nvh.ndim );
		this->SubmatrixSizes = new int[ this->NumOfDimensions ];
	}

	for( int x = 0, y = this->NumOfDimensions - 1; x < this->NumOfDimensions; x++, y-- )
	{
		float refpt, refppm, PPMPerPt;

		if( !this->GotData ) 
		{
			this->DimensionSizes[ y ] = this->SwapBytes ? SwapByteOrder( nvh.dim[ x ].size ) : nvh.dim[ x ].size;
			this->SubmatrixSizes[ y ] = this->SwapBytes ? SwapByteOrder( nvh.dim[ x ].blksize ) : nvh.dim[ x ].blksize;
		}

		this->DimensionAxisLabels[ y ] = new char[ strlen( nvh.dim[ x ].label ) + 1 ];
		strncpy( this->DimensionAxisLabels[ y ], nvh.dim[ x ].label, strlen( nvh.dim[ x ].label ) );
		this->DimensionAxisLabels[ y ][ strlen( nvh.dim[ x ].label ) ] = 0;
		this->DimensionCentersFreq[ y ] = this->SwapBytes ? SwapByteOrder( nvh.dim[ x ].sf ) : nvh.dim[ x ].sf;
		this->DimensionScalesFreq[ y ] = this->SwapBytes ? SwapByteOrder( nvh.dim[ x ].sw ) : nvh.dim[ x ].sw;
		refpt = this->SwapBytes ? SwapByteOrder( nvh.dim[ x ].refpt ) : nvh.dim[ x ].refpt;
		refppm = this->SwapBytes ? SwapByteOrder( nvh.dim[ x ].ref ) : nvh.dim[ x ].ref;

		this->DimensionScalesPPM[ y ] = this->DimensionScalesFreq[ y ] / ( this->DimensionCentersFreq[ y ] * 1e6F ) * 1e6F;
		PPMPerPt = this->DimensionScalesPPM[ y ] / ( ( float ) this->DimensionSizes[ y ] );
		this->DimensionOriginsPPM[ y ] = refppm + ( refpt * PPMPerPt );
		this->DimensionCentersPPM[ y ] = this->DimensionOriginsPPM[ y ] - this->DimensionScalesPPM[ y ] / 2.0F;

	}

	this->SubmatrixCount = 1;
	this->SubmatrixSize = 1;
	this->SubmatrixDimCount = new int[ this->NumOfDimensions ];
	for( int i = 0; i < this->NumOfDimensions; i++ )
	{
		this->SubmatrixSize *= this->SubmatrixSizes[ i ];
		this->SubmatrixDimCount[ i ] = ( int ) ceil( ( ( float ) this->DimensionSizes[ i ] ) / ( ( float ) this->SubmatrixSizes[ i ] ) );
		this->SubmatrixCount *= this->SubmatrixDimCount[ i ];
	}
}


void NMRData_NMRView::WriteFileHeader( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( !this->NumOfDimensions ) throw DimensionalityNotSet();
	if( !this->DataFile ) throw NoFileOpen();
#endif

	if( this->ReadOnly ) return;

	if( !this->GotData ) this->SetSubmatrixSizes();

	this->HeaderSize = 2048;

	NVHEADER fh;

	memset( &fh, 0, sizeof( NVHEADER ) );
	fh.bheadersz = this->SwapBytes ? this->SwapByteOrder( 0 ) : 0;
	fh.blkelems = this->SwapBytes ? this->SwapByteOrder( 4096 ) : 4096;
	fh.fheadersz = this->SwapBytes ? this->SwapByteOrder( 2048 ) : 2048;
	fh.lvl = 0.0F;
	fh.scale = 0.0F;
	fh.ndim = this->SwapBytes ? this->SwapByteOrder( this->NumOfDimensions ) : this->NumOfDimensions;
	fh.posneg = this->SwapBytes ? this->SwapByteOrder( 3 ) : 3;
	fh.rdims = this->SwapBytes ? this->SwapByteOrder( this->NumOfDimensions ) : this->NumOfDimensions;
	fh.magic = this->SwapBytes ? this->SwapByteOrder( 874032077 ) : 874032077;

	for( int x = 0, y = this->NumOfDimensions - 1; x < this->NumOfDimensions; x++, y-- )
	{
		fh.dim[ y ].size = this->SwapBytes ? this->SwapByteOrder( this->DimensionSizes[ x ] ) : this->DimensionSizes[ x ];
		fh.dim[ y ].blksize = this->SwapBytes ? this->SwapByteOrder( this->SubmatrixSizes[ x ] ) : this->SubmatrixSizes[ x ];
		fh.dim[ y ].sf = this->SwapBytes ? this->SwapByteOrder( this->DimensionCentersFreq[ x ] ) : this->DimensionCentersFreq[ x ];
		fh.dim[ y ].sw = this->SwapBytes ? this->SwapByteOrder( this->DimensionScalesFreq[ x ] ) : this->DimensionScalesFreq[ x ];
		fh.dim[ y ].refpt = this->SwapBytes ? this->SwapByteOrder( this->DimensionSizes[ x ] / 2.0F ) : this->DimensionSizes[ x ] / 2.0F;
		fh.dim[ y ].ref = this->SwapBytes ? this->SwapByteOrder( this->DimensionCentersPPM[ x ] ) : this->DimensionCentersPPM[ x ];
		fh.dim[ y ].refunits = this->SwapBytes ? this->SwapByteOrder( 3 ) : 3;
		if( this->DimensionAxisLabels && this->DimensionAxisLabels[ x ] ) strcpy( fh.dim[ y ].label, this->DimensionAxisLabels[ x ] );
	}

	if( fseek( this->DataFile, 0, SEEK_SET ) ) throw CantWriteFile();
	if( fwrite( &fh, sizeof( NVHEADER ), 1, this->DataFile ) != 1 ) throw CantWriteFile();
}


int NMRData_NMRView::GetFileTypeCode( void ) const
{
	return NMRData::FileType_NMRView;
}


const char *NMRData_NMRView::GetFileTypeString( void ) const
{
	return "NMRView";
}


void NMRData_NMRView::ClearPrivateParams( void )
{
#ifdef BEC_NMRDATA_DEBUG
	if( this->GotData ) throw DataAreOpen();
#endif

	this->ClearSubmatrixParams();
	this->HeaderSize = 2048;
	this->SwapBytes = false;
}


int *NMRData_NMRView::CalculateSubmatrixSizes( int NumOfDims, int *DimSizes ) const
{
#ifdef BEC_NMRDATA_DEBUG
	if( !NumOfDims ) throw BadArgument();
	if( !DimSizes ) throw NullPointerArg();
#endif

	stack_array_ptr< int > Out( new int[ NumOfDims ] );
	int BaseSize = 12 / NumOfDims;
	int Excess = 12 % NumOfDims;
	for( int x = 0; x < NumOfDims; x++ )
	{
		if( Excess > 0 )
		{
			Out[ x ] = ( int ) pow( 2.0F, BaseSize + 1 );
			Excess--;
		}
		else Out[ x ] = ( int ) pow( 2.0F, BaseSize );
	}
	return Out.release();
}


Subset BEC_NMRData::ProcessSubsetSelectionString( NMRData *Dataset, std::string SelectionString )
{
	//	format examples:
	//		HN=8.36ppm							single position
	//		HN={8.5ppm_8.0ppm}					range
	//		HN={8.5ppm,0.5ppm}					left, size
	//		HN=8.36ppm,CA=26.0ppm				single positions for multiple dimensions
	//		HN=71pts							units can be "ppm", "pts" or "hz"
	//		0=71pts								dimension can be specified by number
	//		z=71pts								the last four dimensions can be specified by x/y/z/a, as in NMRPipe
	//		HN=8.36ppm,CA=35pts					units can be different for different dimensions

	const NMRData &data = *Dataset;
	std::vector< DimPosition< NMRData > > pos1 = data.GetDimPosSet();
	std::vector< DimPosition< NMRData > > pos2 = data.GetDimPosSet();
	for( std::vector< DimPosition< NMRData > >::iterator i = pos2.begin(); i != pos2.end(); ++i )
		i->SetPosPts( i->GetSize() - 1 );

	BEC_Misc::str_tok split_dims( SelectionString, "," );
	split_dims.process_full_string();
	for( std::vector< std::string >::iterator dim = split_dims.tokens.begin(); dim != split_dims.tokens.end(); ++dim )
	{
		BEC_Misc::str_tok split_params( *dim, ",", "={_}" );
		split_params.process_full_string();

		int dim_num = -1;
		std::string dim_id = split_params.tokens.front();
		if( !dim_id.compare( "x" ) ) dim_num = data.GetNumOfDims() - 1;
		else if( !dim_id.compare( "y" ) ) dim_num = data.GetNumOfDims() - 2;
		else if( !dim_id.compare( "z" ) ) dim_num = data.GetNumOfDims() - 3;
		else if( !dim_id.compare( "a" ) ) dim_num = data.GetNumOfDims() - 4;
		else
		{
			for( int j = 0; j < data.GetNumOfDims(); j++ )
				if( !dim_id.compare( pos1[ j ].GetLabelStr() ) )
					dim_num = j;

			if( dim_num < 0 )
				dim_num = atoi( dim_id.c_str() );

			if( dim_num < 0 || dim_num > data.GetNumOfDims() - 1 )
				throw Exceptions::BadSubsetString_DimID( dim_id );
		}

		std::vector< std::string >::iterator token = split_params.tokens.begin() + 1;
		if( token->compare( "=" ) )
			throw Exceptions::BadSubsetString();
		if( ++token == split_params.tokens.end() )
			throw Exceptions::BadSubsetString();

		if( !token->compare( "{" ) )
		{
			if( ++token == split_params.tokens.end() )
				throw Exceptions::BadSubsetString();
			float coord1 = atof( token->c_str() );
			std::string units1 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
			if( !units1.compare( "ppm" ) )
				pos1[ dim_num ].SetPosPPM( coord1 );
			else if( !units1.compare( "hz" ) )
				pos1[ dim_num ].SetPosHz( coord1 );
			else if( !units1.compare( "pts" ) )
				pos1[ dim_num ].SetPosPts( ( int ) coord1 );
			else
				throw Exceptions::BadSubsetString();

			if( ++token == split_params.tokens.end() )
				throw Exceptions::BadSubsetString();
			if( !token->compare( "_" ) )
			{
				if( ++token == split_params.tokens.end() )
					throw Exceptions::BadSubsetString();
				float coord2 = atof( token->c_str() );
				std::string units2 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
				if( !units2.compare( "ppm" ) )
					pos2[ dim_num ].SetPosPPM( coord2 );
				else if( !units2.compare( "hz" ) )
					pos2[ dim_num ].SetPosHz( coord2 );
				else if( !units2.compare( "pts" ) )
					pos2[ dim_num ].SetPosPts( ( int ) coord2 );
				else
					throw Exceptions::BadSubsetString();
			}
			else
			{
				if( ++token == split_params.tokens.end() )
					throw Exceptions::BadSubsetString();
				float width = atof( token->c_str() );
				std::string units2 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
				if( !units2.compare( "ppm" ) )
					pos2[ dim_num ].SetPosPPM( pos1[ dim_num ].GetPosPPM() + width );
				else if( !units2.compare( "hz" ) )
					pos2[ dim_num ].SetPosHz( pos1[ dim_num ].GetPosHz() + width );
				else if( !units2.compare( "pts" ) )
					pos2[ dim_num ].SetPosPts( pos1[ dim_num ].GetPosPts() + ( int ) width );
				else
					throw Exceptions::BadSubsetString();
			}

		}
		else
		{
			if( token == split_params.tokens.end() )
				throw Exceptions::BadSubsetString();
			float coord1 = atof( token->c_str() );
			std::string units1 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
			if( !units1.compare( "ppm" ) )
				pos1[ dim_num ].SetPosPPM( coord1 );
			else if( !units1.compare( "hz" ) )
				pos1[ dim_num ].SetPosHz( coord1 );
			else if( !units1.compare( "pts" ) )
				pos1[ dim_num ].SetPosPts( ( int ) coord1 );
			else
				throw Exceptions::BadSubsetString();

			pos2[ dim_num ].SetPosPts( pos1[ dim_num ].GetPosPts() );
		}

	}

	return data( pos1, pos2 );
}


const Subset BEC_NMRData::ProcessSubsetSelectionString( const NMRData *Dataset, std::string SelectionString )
{
	//	format examples:
	//		HN=8.36ppm							single position
	//		HN={8.5ppm_8.0ppm}					range
	//		HN={8.5ppm,0.5ppm}					left, size
	//		HN=8.36ppm,CA=26.0ppm				single positions for multiple dimensions
	//		HN=71pts							units can be "ppm", "pts" or "hz"
	//		0=71pts								dimension can be specified by number
	//		z=71pts								the last four dimensions can be specified by x/y/z/a, as in NMRPipe
	//		HN=8.36ppm,CA=35pts					units can be different for different dimensions

	const NMRData &data = *Dataset;
	std::vector< DimPosition< NMRData > > pos1 = data.GetDimPosSet();
	std::vector< DimPosition< NMRData > > pos2 = data.GetDimPosSet();
	for( std::vector< DimPosition< NMRData > >::iterator i = pos2.begin(); i != pos2.end(); ++i )
		i->SetPosPts( i->GetSize() - 1 );

	BEC_Misc::str_tok split_dims( SelectionString, "," );
	split_dims.process_full_string();
	for( std::vector< std::string >::iterator dim = split_dims.tokens.begin(); dim != split_dims.tokens.end(); ++dim )
	{
		BEC_Misc::str_tok split_params( *dim, ",", "={_}" );
		split_params.process_full_string();

		int dim_num = -1;
		std::string dim_id = split_params.tokens.front();
		if( !dim_id.compare( "x" ) ) dim_num = data.GetNumOfDims() - 1;
		else if( !dim_id.compare( "y" ) ) dim_num = data.GetNumOfDims() - 2;
		else if( !dim_id.compare( "z" ) ) dim_num = data.GetNumOfDims() - 3;
		else if( !dim_id.compare( "a" ) ) dim_num = data.GetNumOfDims() - 4;
		else
		{
			for( int j = 0; j < data.GetNumOfDims(); j++ )
				if( !dim_id.compare( pos1[ j ].GetLabelStr() ) )
					dim_num = j;

			if( dim_num < 0 )
				dim_num = atoi( dim_id.c_str() );

			if( dim_num < 0 || dim_num > data.GetNumOfDims() - 1 )
				throw Exceptions::BadSubsetString_DimID( dim_id );
		}

		std::vector< std::string >::iterator token = split_params.tokens.begin() + 1;
		if( token->compare( "=" ) )
			throw Exceptions::BadSubsetString();
		if( ++token == split_params.tokens.end() )
			throw Exceptions::BadSubsetString();

		if( !token->compare( "{" ) )
		{
			if( ++token == split_params.tokens.end() )
				throw Exceptions::BadSubsetString();
			float coord1 = atof( token->c_str() );
			std::string units1 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
			if( !units1.compare( "ppm" ) )
				pos1[ dim_num ].SetPosPPM( coord1 );
			else if( !units1.compare( "hz" ) )
				pos1[ dim_num ].SetPosHz( coord1 );
			else if( !units1.compare( "pts" ) )
				pos1[ dim_num ].SetPosPts( ( int ) coord1 );
			else
				throw Exceptions::BadSubsetString();

			if( ++token == split_params.tokens.end() )
				throw Exceptions::BadSubsetString();
			if( !token->compare( "_" ) )
			{
				if( ++token == split_params.tokens.end() )
					throw Exceptions::BadSubsetString();
				float coord2 = atof( token->c_str() );
				std::string units2 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
				if( !units2.compare( "ppm" ) )
					pos2[ dim_num ].SetPosPPM( coord2 );
				else if( !units2.compare( "hz" ) )
					pos2[ dim_num ].SetPosHz( coord2 );
				else if( !units2.compare( "pts" ) )
					pos2[ dim_num ].SetPosPts( ( int ) coord2 );
				else
					throw Exceptions::BadSubsetString();
			}
			else
			{
				if( ++token == split_params.tokens.end() )
					throw Exceptions::BadSubsetString();
				float width = atof( token->c_str() );
				std::string units2 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
				if( !units2.compare( "ppm" ) )
					pos2[ dim_num ].SetPosPPM( pos1[ dim_num ].GetPosPPM() + width );
				else if( !units2.compare( "hz" ) )
					pos2[ dim_num ].SetPosHz( pos1[ dim_num ].GetPosHz() + width );
				else if( !units2.compare( "pts" ) )
					pos2[ dim_num ].SetPosPts( pos1[ dim_num ].GetPosPts() + ( int ) width );
				else
					throw Exceptions::BadSubsetString();
			}

		}
		else
		{
			float coord1 = atof( token->c_str() );
			std::string units1 = token->substr( token->find_last_of( "-+.0123456789" ) + 1 );
			if( !units1.compare( "ppm" ) )
				pos1[ dim_num ].SetPosPPM( coord1 );
			else if( !units1.compare( "hz" ) )
				pos1[ dim_num ].SetPosHz( coord1 );
			else if( !units1.compare( "pts" ) )
				pos1[ dim_num ].SetPosPts( ( int ) coord1 );
			else
				throw Exceptions::BadSubsetString();

			pos2[ dim_num ].SetPosPts( pos1[ dim_num ].GetPosPts() );
		}

	}

	return data( pos1, pos2 );
}


union float_or_int
{
	int i;
	float f;
};


RawVarianData::RawVarianData( std::string Filename, bool ReadOnly ) : readonly( ReadOnly ), fids( 0 ), np( 0 ), swap( false ), file( 0 ), 
	store_ints( true )
{
	int test = 1;
	if( ( ( char * ) &test )[ 0 ] == '\1' ) this->swap = true;

	if( !( this->file = fopen( Filename.c_str(), ReadOnly ? "rb" : "r+b" ) ) )
		throw CantOpenFile( Filename.c_str() );

	if( fread( &( this->file_header ), sizeof( VNMRFileHeader ), 1, this->file ) != 1 ) throw CantReadFile();
	this->fids = this->swap ? SwapByteOrder( this->file_header.nblocks ) : this->file_header.nblocks;
	this->np = this->swap ? SwapByteOrder( this->file_header.np ) : this->file_header.np;
	if( ( this->swap? SwapByteOrder( this->file_header.status ) : this->file_header.status ) & 8 ) this->store_ints = false;

	stack_array_ptr< float > buffer( new float[ this->np ] );
	for( int i = 0; i < this->fids; i++ )
	{
		if( !i )
		{
			if( fread( &( this->std_block_header ), sizeof( VNMRBlockHeader ), 1, this->file ) != 1 ) throw CantReadFile();
		}
		else if( fseek( this->file, sizeof( VNMRBlockHeader ), SEEK_CUR ) ) throw CantReadFile();

		if( fread( buffer, sizeof( float ), this->np, this->file ) != this->np ) throw CantReadFile();
		if( this->store_ints )
		{
			std::valarray< float > fid( this->np );
			for( int j = 0; j < this->np; j++ )
			{
				float_or_int val;
				val.f = buffer[ j ];
				int intval = val.i;

				if( this->swap ) intval = SwapByteOrder( intval );
				fid[ j ] = ( float ) intval;
			}
			this->data.push_back( fid );
		}
		else
		{
			std::valarray< float > fid( buffer, this->np );
			if( this->swap ) fid = fid.apply( SwapByteOrder );
			this->data.push_back( fid );
		}
	}
}


RawVarianData::RawVarianData( std::string Filename, int np, int NumOfFIDs ) : readonly( false ), fids( NumOfFIDs ), np( np ), swap( false ), file( 0 ), 
	store_ints( true )
{
	int test = 1;
	if( ( ( char * ) &test )[ 0 ] == '\1' ) this->swap = true;

	if( !( this->file = fopen( Filename.c_str(), "wb" ) ) )
		throw CantOpenFile( Filename.c_str() );

	VNMRFileHeader fh;
	fh.nblocks = this->swap ? SwapByteOrder( NumOfFIDs ) : NumOfFIDs;
	fh.ntraces = this->swap ? SwapByteOrder( ( int ) 1 ) : 1;
	fh.np = this->swap ? SwapByteOrder( np ) : np;
	fh.ebytes = this->swap ? SwapByteOrder( ( int ) 4 ) : 4;
	fh.tbytes = this->swap ? SwapByteOrder( ( int ) ( np * 4 ) ) : np * 4;
	fh.bbytes = this->swap ? SwapByteOrder( ( int ) ( np * 4 + sizeof( VNMRBlockHeader ) ) ) : np * 4 + sizeof( VNMRBlockHeader );
	fh.vers_id = ( short ) 0;
	fh.status = this->swap ? SwapByteOrder( ( short ) 0x45 ) : ( short ) 0x45;
	fh.nbheaders = this->swap ? SwapByteOrder( ( int ) 1 ) : 1;

	if( fwrite( &fh, sizeof( VNMRFileHeader ), 1, this->file ) != 1 ) throw CantWriteFile();
	this->file_header = fh;

	VNMRBlockHeader bh;
	bh.scale = 0;
	bh.status = this->swap ? SwapByteOrder( ( short ) 0x45 ) : ( short ) 0x45;
	bh.index = 0;
	bh.mode = 0;
	bh.ctcount = this->swap ? SwapByteOrder( ( int ) 4 ) : 4;
	bh.lpval = 0;
	bh.rpval = 0;
	bh.lvl = 0;
	bh.tlt = 0;

	this->std_block_header = bh;

	stack_array_ptr< int > buffer( new int[ this->np ] );
	memset( buffer.get(), 0, this->np * sizeof( int ) );
	for( int i = 0; i < NumOfFIDs; i++ )
	{
		std::valarray< float > fid( 0.0F, np );
		this->data.push_back( fid );

		bh.index = this->swap ? SwapByteOrder( ( short ) ( i + 1 ) ) : ( short ) ( i + 1 );
		if( fwrite( &bh, sizeof( VNMRBlockHeader ), 1, this->file ) != 1 ) throw CantWriteFile();
		if( fwrite( buffer, sizeof( float ), np, this->file ) != np ) throw CantWriteFile();
	}
}


RawVarianData::~RawVarianData( void )
{
	if( !this->readonly )
	{
		if( fseek( this->file, sizeof( VNMRFileHeader ), SEEK_SET ) ) throw CantWriteFile();

		stack_array_ptr< float > buffer( new float[ this->np ] );
		for( int i = 0; i < this->fids; i++ )
		{
			this->std_block_header.index = this->swap ? SwapByteOrder( ( short ) ( i + 1 ) ) : ( short ) ( i + 1 );
			if( fwrite( &( this->std_block_header ), sizeof( VNMRBlockHeader ), 1, this->file ) != 1 ) throw CantWriteFile();

			if( this->store_ints )
			{
				for( int j = 0; j < this->np; j++ )
				{
					int intval = ( int ) this->data[ i ][ j ];
					if( this->swap ) intval = SwapByteOrder( intval );

					float_or_int val;
					val.i = intval;
					buffer[ j ] = val.f;
				}
			}
			else
			{
				if( this->swap ) 
				{
					for( int j = 0; j < this->np; j++ )
						buffer[ j ] = SwapByteOrder( this->data[ i ][ j ] );
				}
				else 
				{
					for( int j = 0; j < this->np; j++ )
						buffer[ j ] = this->data[ i ][ j ];
				}
			}
			if( fwrite( buffer, sizeof( float ), this->np, this->file ) != this->np ) throw CantWriteFile();
		}
	}

	fclose( this->file );
}


int RawVarianData::GetNumOfFIDs( void ) const
{
	return this->fids;
}


int RawVarianData::GetNP( void ) const
{
	return this->np;
}


std::vector< std::valarray< float > > &RawVarianData::GetData( void )
{
	return this->data;
}


void RawVarianData::ProcessSE( void )
{
	for( int i = 0; i < this->fids / 2; i++ )
		for( int j = 0; j < this->np / 2; j++ )
		{
			float plus_real = this->data[ i * 2 ][ j * 2 ];
			float plus_imag = this->data[ i * 2 ][ j * 2 + 1 ];
			float minus_real = this->data[ i * 2 + 1 ][ j * 2 ];
			float minus_imag = this->data[ i * 2 + 1 ][ j * 2 + 1 ];

			float cos_real = plus_real - minus_real;
			float cos_imag = plus_imag - minus_imag;
			float sin_real = plus_imag + minus_imag;
			float sin_imag = -( plus_real + minus_real );

			this->data[ i * 2 ][ j * 2 ] = cos_real;
			this->data[ i * 2 ][ j * 2 + 1 ] = cos_imag;
			this->data[ i * 2 + 1 ][ j * 2 ] = sin_real;
			this->data[ i * 2 + 1 ][ j * 2 + 1 ] = sin_imag;
		}
}


Exceptions::BaseException::BaseException( void ) {  }
Exceptions::BaseException::~BaseException( void ) {  }
const char *Exceptions::BaseException::what( void ) const
{
	return "An error occured in the NMRData library.";
}


const char *Exceptions::BadArgument::what( void ) const
{
	return "An invalid argument was supplied to an NMRData library function.";
}


const char *Exceptions::DataAreOpen::what( void ) const
{
	return "A library function that requires the dataset to be closed was called while the dataset was open.";
}


const char *Exceptions::DataNotOpen::what( void ) const
{
	return "A library function that requires the dataset to be open was called while the dataset was closed.";
}


const char *Exceptions::NullPointerArg::what( void ) const
{
	return "A improper null pointer was supplied to a library function.";
}


const char *Exceptions::BadDimNumber::what( void ) const
{
	return "The supplied dimension number is invalid for the dimensionality of the dataset.";
}


const char *Exceptions::BadCacheSize::what( void ) const
{
	return "An invalid cache size request was supplied to the NMRData library.";
}


const char *Exceptions::BadDataPtrRequestCoord::what( void ) const
{
	return "An invalid coordinate was given in a request for a data block.";
}


const char *Exceptions::BadDataPtrRequestOffset::what( void ) const
{
	return "An invalid offset was given in a request for a data block.";
}


const char *Exceptions::BadDataPtrRequestSize::what( void ) const
{
	return "An invalid size was given in a request for a data block.";
}


const char *Exceptions::DataPtrRequestTooLarge::what( void ) const
{
	return "The requested data block is larger than the current memory limit permits.";
}


const char *Exceptions::BadDataPtr::what( void ) const
{
	return "The pointer supplied to be closed does not correspond to an open data block.";
}


const char *Exceptions::BadOffset::what( void ) const
{
	return "A data position index (offset) supplied to a library function was invalid.";
}


const char *Exceptions::BadSize::what( void ) const
{
	return "The size parameter to a data request was invalid.";
}


const char *Exceptions::BadCoordinates::what( void ) const
{
	return "A set of coordinates for a data point supplied to a library function was invalid.";
}


const char *Exceptions::BadSubmatrixNumber::what( void ) const
{
	return "A submatrix number supplied in a data request was invalid.";
}


Exceptions::FileTypeNotRecognized::FileTypeNotRecognized( void )
{
	this->msg = new char[ strlen( "The file type for the provided filename could not be recognized from its extension." ) + 1 ];
	sprintf( this->msg, "The file type for the provided filename could not be recognized from its extension." );
}


Exceptions::FileTypeNotRecognized::FileTypeNotRecognized( const FileTypeNotRecognized &Source )
{
	this->msg = new char[ strlen( Source.msg ) + 1 ];
	strcpy( this->msg, Source.msg );
}


Exceptions::FileTypeNotRecognized::FileTypeNotRecognized( const char *Filename )
{
	this->msg = new char[ strlen( "The file type for the provided filename, , could not be recognized from its extension." ) + strlen( Filename ) + 1 ];
	sprintf( this->msg, "The file type for the provided filename, %s, could not be recognized from its extension.", Filename );
}


Exceptions::FileTypeNotRecognized::~FileTypeNotRecognized( void )
{
	delete [] this->msg;
}


const char *Exceptions::FileTypeNotRecognized::what( void ) const
{
	return this->msg;
}


FileTypeNotRecognized &Exceptions::FileTypeNotRecognized::operator =( const FileTypeNotRecognized &Source )
{
	delete [] this->msg;
	this->msg = new char[ strlen( Source.msg ) + 1 ];
	strcpy( this->msg, Source.msg );
	return *this;
}


const char *Exceptions::ComplexNotSupported::what( void ) const
{
	return "The file type specified does not support complex or hypercomplex data, or the current library routines for that format do not support complex or hypercomplex data.";
}


const char *Exceptions::BadXEASYFilename::what( void ) const
{
	return "The supplied filename does not have the extension .3D.param, which is expected for XEASY files.";
}


const char *Exceptions::DimensionalityNotSet::what( void ) const
{
	return "The dimensionality of the dataset has not yet been set.";
}

Exceptions::CantOpenFile::CantOpenFile( void )
{
	this->msg = new char[ strlen( "The file could not be accessed." ) + 1 ];
	sprintf( this->msg, "The file could not be accessed." );
}


Exceptions::CantOpenFile::CantOpenFile( const CantOpenFile &Source )
{
	this->msg = new char[ strlen( Source.msg ) + 1 ];
	strcpy( this->msg, Source.msg );
}


Exceptions::CantOpenFile::CantOpenFile( const char *Filename )
{
	this->msg = new char[ strlen( Filename ) + strlen( "The file  could not be accessed." ) + 1 ];
	sprintf( this->msg, "The file %s could not be accessed.", Filename );
}


Exceptions::CantOpenFile::~CantOpenFile( void )
{
	delete [] this->msg;
}


const char *Exceptions::CantOpenFile::what( void ) const
{
	return this->msg;
}


CantOpenFile &Exceptions::CantOpenFile::operator =( const CantOpenFile &Source )
{
	delete [] this->msg;
	this->msg = new char[ strlen( Source.msg ) + 1 ];
	strcpy( this->msg, Source.msg );
	return *this;
}


const char *Exceptions::BadDimSize::what( void ) const
{
	return "A dimension size is set to zero or a negative number.";
}


const char *Exceptions::NoParamsAvailable::what( void ) const
{
	return "The file is of a type that has no header, and it was not possible to obtain dimensionality and size parameters.";
}

const char *Exceptions::DatasetTooLarge::what( void ) const
{
	return "The requested size of the dataset is larger than the operating system and/or standard system library supports.";
}


const char *Exceptions::CantReadFile::what( void ) const
{
	return "An error occured while attempting to read from a data file.";
}


const char *Exceptions::CantWriteFile::what( void ) const
{
	return "An error occured while attempting to write to a data file.";
}


const char *Exceptions::MemOnlyRequiresBaseClass::what( void ) const
{
	return "An attempt was made to create a memory-only dataset using an NMRData derived class.  Memory-only datasets can only be created using the NMRData base class.";
}


const char *Exceptions::CantGetTmpFile::what( void ) const
{
	return "An error occured while attempting to create a temporary file.";
}


const char *Exceptions::SubmatrixParamsNotSet::what( void ) const
{
	return "A library function was called to manipulate submatrices, without submatrix parameters available.";
}


const char *Exceptions::CouldntCalculateSubmatrixSizes::what( void ) const
{
	return "An error occured while attempting to calculate submatrix sizes.";
}


const char *Exceptions::NoFileOpen::what( void ) const
{
	return "A request was made to load or write data, without an open data file.";
}


const char *Exceptions::PipeFileListError::what( void ) const
{
	return "An error occured while interpreting the nmrPipe filename pattern: the pattern could not be interpreted.";
}


const char *Exceptions::TooManyDims::what( void ) const
{
	return "The selected file format does not support the number of dimensions requested.";
}


const char *Exceptions::BadFileHeader::what( void ) const
{
	return "The file does not have the correct header for its file type.";
}


const char *Exceptions::PipeStreamFormatNotSupported::what( void ) const
{
	return "The NMRPipe multidimensional stream format is not currently supported.";
}


const char *Exceptions::BadComplexConfig::what( void ) const
{
	return "An invalid request was made of a library value/reference object:  The hypercomplex configuration that was supplied is not valid.";
}


const char *Exceptions::ComponentsDontMatch::what( void ) const
{
	return "The requested operation cannot be performed, because the objects do not have the same number of complex components.";
}


const char *Exceptions::DimsDontMatch::what( void ) const
{
	return "The requested operation cannot be performed, because the hypercomplex objects do not originate from the same dimensionality.";
}


const char *Exceptions::ComplexConfigDoesntMatch::what( void ) const
{
	return "The requested operation cannot be performed, because the objects are not hypercomplex in the same dimensions.";
}


const char *Exceptions::BadComponentIndex::what( void ) const
{
	return "An invalid complex component index was supplied.";
}


const char *Exceptions::BadComponentSelector::what( void ) const
{
	return "An invalid complex component selector was supplied.";
}


const char *Exceptions::DimNotComplex::what( void ) const
{
	return "An imaginary component was requested for a dimension that is not complex.";
}


const char *Exceptions::BadChainedSelector::what( void ) const
{
	return "Either too many or too few complex component or dimension position selectors were chained.";
}


const char *Exceptions::ValueIsEmpty::what( void ) const
{
	return "The operation is not possible because the value object is empty.";
}


const char *Exceptions::NotCompatible::what( void ) const
{
	return "The request cannot be completed, because the objects are not compatible.";
}


const char *Exceptions::ChainedSelectionError::what( void ) const
{
	return "The attempted operation following chained selection is not valid.";
}


const char *Exceptions::ConversionRequiresSelection::what( void ) const
{
	return "Conversion of a hypercomplex value to a real number requires the desired component to be selected.";
}


const char *Exceptions::RefIsAmbig::what( void ) const
{
	return "The reference is ambiguous; a chained selector should be used first to define an unambiguous position.";
}


const char *Exceptions::PosOutOfRange::what( void ) const
{
	return "A coordinate value is out of range for the dimension in question.";
}


const char *Exceptions::DimNotFD::what( void ) const
{
	return "The specified dimension is not in the frequency domain.";
}


const char *Exceptions::DimNotTD::what( void ) const
{
	return "The specified dimension is not in the time domain.";
}


const char *Exceptions::RefHasWrongParent::what( void ) const
{
	return "The reference object does not belong to the correct parent dataset.";
}


const char *Exceptions::RefIsOutsideSubset::what( void ) const
{
	return "The reference object does not refer to an element of the subset.";
}


const char *Exceptions::SubsetRequestTooLarge::what( void ) const
{
	return "The subset could not be created, because insufficient memory is available.";
}


const char *Exceptions::BadSubsetCorner::what( void ) const
{
	return "The subset request is invalid, because the corner coordinates are outside the dataset.";
}


const char *Exceptions::BadSubsetSize::what( void ) const
{
	return "The subset request is invalid, because the subset size would place some of the subset outside the dataset.";
}


const char *Exceptions::PtOutsideSubset::what( void ) const
{
	return "The request is invalid, because the coordinates correspond to a point outside of the subset.";
}


const char *Exceptions::WriteCallOnConstData::what( void ) const
{
	return "An attempt was made to write data from inside a const NMRData object.";
}


const char *Exceptions::DataCopyRequestTooLarge::what( void ) const
{
	return "The data copy could not be created, because insufficient memory is available.";
}


const char *Exceptions::BadSubsetString::what( void ) const
{
	return "The subset selection string is not valid.";
}


const char *Exceptions::BadSubsetString_DimID::what( void ) const
{
	return ( std::string( "The dimension ID " ) + this->param + std::string( " could not be matched with any of the dimensions of the experiment." ) ).c_str();
}


size_t NMRData::DefaultDataPtrMemLimit = 50000000;
size_t NMRData::DefaultInternalCacheSize = 200000000;
size_t NMRData::DefaultPageSize = 10000;
bool ( *NMRData::ParameterCallback ) ( NMRData *receiver ) = 0;


/*	End nmrdata.cpp		*/





